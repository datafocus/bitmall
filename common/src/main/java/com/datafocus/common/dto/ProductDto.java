package com.datafocus.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;



public class ProductDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String carrierOperatorName;
	
	private String provinceName;
	
	private List<Product> products = new ArrayList<>();
	
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public String getCarrierOperatorName() {
		return carrierOperatorName;
	}

	public void setCarrierOperatorName(String carrierOperatorName) {
		this.carrierOperatorName = carrierOperatorName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public static class Product implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private int id;
		
		private String productName;
		
		private Integer carrierOperator;
		
		private String province;
		
		private BigDecimal price;
		
		private BigDecimal discount;
		
		private String description;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public Integer getCarrierOperator() {
			return carrierOperator;
		}

		public void setCarrierOperator(Integer carrierOperator) {
			this.carrierOperator = carrierOperator;
		}

		public String getProvince() {
			return province;
		}

		public void setProvince(String province) {
			this.province = province;
		}

		public BigDecimal getPrice() {
			return price;
		}

		public void setPrice(BigDecimal price) {
			this.price = price;
		}

		public BigDecimal getDiscount() {
			return discount;
		}

		public void setDiscount(BigDecimal discount) {
			this.discount = discount;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
		
		
	}

	
	
	
}
