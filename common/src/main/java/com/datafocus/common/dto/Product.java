package com.datafocus.common.dto;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6590557067770845195L;

	private String productId;// 产品ID

	private String productName;// 产品名称

	private BigDecimal stPrice;// 原价

	private BigDecimal salePrice;// 销售价

	private String desc;// 产品描述

	private String vendor;// 运营商

	private String province;// 地区

	private String city;// 地区
	
	private int state;// 状态(1:上架，2:下架)

	private String type;// STAND_PACKAGE("标准包"), SPECIL_PACKAGE("特殊包");
	
	private int displayPriority; // 显示优先级

    private Boolean isActivity ;

	public String groupKey(String operatorId) {
		return operatorId+this.type + this.vendor + this.province + this.city;
	}

	@JsonProperty("productId")
	public String getProductId() {
		return productId;
	}

	@JsonProperty(value = "productCode")
	public void setProductId(String productId) {
		this.productId = productId;
	}

	@JsonProperty("productName")
	public String getProductName() {
		return productName;
	}

	@JsonProperty("productName")
	public void setProductName(String productName) {
		this.productName = productName;
	}

	@JsonSerialize(using=PriceJsonSerializer.class)
	@JsonProperty("stPrice")
	public BigDecimal getStPrice() {
		return stPrice;
	}
	
	@JsonSerialize(using=PriceJsonSerializer.class)
	@JsonProperty("stPrice")
	public void setStPrice(BigDecimal stPrice) {
		this.stPrice = stPrice;
	}
	
	@JsonSerialize(using=PriceJsonSerializer.class)
	@JsonProperty("salePrice")
	public BigDecimal getSalePrice() {
		return salePrice;
	}

	@JsonSerialize(using=PriceJsonSerializer.class)
	@JsonProperty("salePrice")
	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	@JsonProperty("desc")
	public String getDesc() {
		return desc;
	}

	@JsonProperty("remark")
	public void setDesc(String desc) {
		this.desc = desc;
	}

	@JsonProperty("vendor")
	public String getVendor() {
		return vendor;
	}

	@JsonProperty("carrierOperator")
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	@JsonProperty("province")
	public String getProvince() {
		return province;
	}

	@JsonProperty("province")
	public void setProvince(String province) {
		this.province = province;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}

	@JsonProperty("state")
	public int getState() {
		return state;
	}
	@JsonProperty("status")
	public void setState(int state) {
		this.state = state;
	}
	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	public int getDisplayPriority() {
		return displayPriority;
	}

	public void setDisplayPriority(int displayPriority) {
		this.displayPriority = displayPriority;
	}

	public class ProductWapper {
		@JsonProperty("productInfoList")
		private List<Product> list;

		public List<Product> getList() {
			return list;
		}

		public void setList(List<Product> list) {
			this.list = list;
		}
	}
	
	public static class PriceJsonSerializer extends  JsonSerializer<BigDecimal>{

		@Override
		public void serialize(BigDecimal value, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			 jgen.writeString(value.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
		}
	}

    public Boolean getIsActivity() {
        return isActivity;
    }

    public Product setIsActivity(Boolean activity) {
        isActivity = activity;
        return this;
    }
}
