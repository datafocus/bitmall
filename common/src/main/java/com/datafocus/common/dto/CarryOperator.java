/**
 * 
 */
package com.datafocus.common.dto;

/**
 * @author Administrator
 * 
 */
public enum CarryOperator {

	UNKNOWN("未知", 0), CMCC("移动", 1), CUCC("联通", 2), CTCC("电信", 3);

	// 成员变量
	private String name;
	private int value;

	private CarryOperator(String name, int value) {
		this.name = name;
		this.value = value;
	}

	// 普通方法
	public static String getName(int value) {
		for (CarryOperator c : CarryOperator.values()) {
			if (c.getValue() == value) {
				return c.name;
			}
		}
		return null;
	}

	public static CarryOperator valueOf(int value) {
		switch (value) {
		case 1:
			return CMCC;
		case 2:
			return CUCC;
		case 3:
			return CTCC;
		default:
			return UNKNOWN;
		}
	}

	// get set 方法
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
