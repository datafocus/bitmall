package com.datafocus.common.dto;

import java.math.BigDecimal;

public class ProductVO {

	private int id;
	
	private CarryOperator carryOperator;
	
	private String province;
	
	private String provinceName;
	
	private String productName;
	
	private BigDecimal price;
	
	private BigDecimal discount;
	
	private BigDecimal redpack;
	
	private String description;
	
	private int status;
	
	private int redpackStatus;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CarryOperator getCarryOperator() {
		return carryOperator;
	}

	public void setCarryOperator(CarryOperator carryOperator) {
		this.carryOperator = carryOperator;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public BigDecimal getRedpack() {
		return redpack;
	}

	public void setRedpack(BigDecimal redpack) {
		this.redpack = redpack;
	}

	public int getRedpackStatus() {
		return redpackStatus;
	}

	public void setRedpackStatus(int redpackStatus) {
		this.redpackStatus = redpackStatus;
	}
	
}
