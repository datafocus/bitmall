package com.datafocus.common.dto;

import java.math.BigDecimal;

public class Cps {
	
	private int fansNum;//粉丝数
	
	private int orderCount; //总订单数
	
	private int orderSuccessCount;//成功订单数
	
	private int orderFailCount; //失败订单数
	
	private BigDecimal orderSum; //订单流水金额

	public int getFansNum() {
		return fansNum;
	}

	public void setFansNum(int fansNum) {
		this.fansNum = fansNum;
	}

	public int getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(int orderCount) {
		this.orderCount = orderCount;
	}

	public int getOrderSuccessCount() {
		return orderSuccessCount;
	}

	public void setOrderSuccessCount(int orderSuccessCount) {
		this.orderSuccessCount = orderSuccessCount;
	}

	public BigDecimal getOrderSum() {
		if(orderSum==null){
			return BigDecimal.ZERO;
		}
		return orderSum;
	}

	public void setOrderSum(BigDecimal orderSum) {
		this.orderSum = orderSum;
	}

	public int getOrderFailCount() {
		return orderFailCount;
	}

	public void setOrderFailCount(int orderFailCount) {
		this.orderFailCount = orderFailCount;
	}

	
}
