package com.datafocus.common.dto;

/**
 * Created by liuheng on 2017/8/3.
 */
public class UserInfoQuery {
    private Integer page = 0;
    private Integer size = 20;
    private Integer sex;
    private Integer subscribe;
    private String startTime;
    private String endTime;
    private String nickName;
    private String orderStartTime;
    private String orderEndTime;
    private String orderPhone;

    public UserInfoQuery() {
    }

    public UserInfoQuery(Integer sex, Integer subscribe, String startTime, String endTime, String nickName, String orderStartTime, String orderEndTime, String orderPhone) {
        this.sex = sex;
        this.subscribe = subscribe;
        this.startTime = startTime;
        this.endTime = endTime;
        this.nickName = nickName;
        this.orderStartTime = orderStartTime;
        this.orderEndTime = orderEndTime;
        this.orderPhone = orderPhone;
    }

    public Integer getSex() {
        return sex;
    }

    public UserInfoQuery setSex(Integer sex) {
        this.sex = sex;
        return this;
    }

    public Integer getSubscribe() {
        return subscribe;
    }

    public UserInfoQuery setSubscribe(Integer subscribe) {
        this.subscribe = subscribe;
        return this;
    }

    public String getStartTime() {
        return startTime;
    }

    public UserInfoQuery setStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    public String getEndTime() {
        return endTime;
    }

    public UserInfoQuery setEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getNickName() {
        return nickName;
    }

    public UserInfoQuery setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public String getOrderStartTime() {
        return orderStartTime;
    }

    public UserInfoQuery setOrderStartTime(String orderStartTime) {
        this.orderStartTime = orderStartTime;
        return this;
    }

    public String getOrderEndTime() {
        return orderEndTime;
    }

    public UserInfoQuery setOrderEndTime(String orderEndTime) {
        this.orderEndTime = orderEndTime;
        return this;
    }

    public String getOrderPhone() {
        return orderPhone;
    }

    public UserInfoQuery setOrderPhone(String orderPhone) {
        this.orderPhone = orderPhone;
        return this;
    }

    public Integer getPage() {
        return page;
    }

    public UserInfoQuery setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public UserInfoQuery setSize(Integer size) {
        this.size = size;
        return this;
    }
}
