package com.datafocus.common.dto;

import java.math.BigDecimal;

public class Statistics {
	
	private int num;
	
	private String dateTime;

	private BigDecimal money;
	
	private String carrierOperatorName;
	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getCarrierOperatorName() {
		return carrierOperatorName;
	}

	public void setCarrierOperatorName(String carrierOperatorName) {
		this.carrierOperatorName = carrierOperatorName;
	}
	
	

}
