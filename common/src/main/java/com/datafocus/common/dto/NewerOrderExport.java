package com.datafocus.common.dto;

import com.datafocus.common.annotation.ExcelAttribute;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by layne on 2017/10/11.
 */
public class NewerOrderExport {

    @ExcelAttribute(name = "订单号")
    private String orderNo;

    @ExcelAttribute(name = "订单号码")
    private String orderPhone;

    @ExcelAttribute(name = "订单价格")
    private BigDecimal orderPrice;


    @ExcelAttribute(name = "订单创建时间")
    private Date orderTime;

    @ExcelAttribute(name = "包型")
    private String flowPackage;

    @ExcelAttribute(name = "订单状态", prompt = "0： 未充值 1：充值成功 2：充值失败")
    private int rechargeStatus;//0： 未充值 1：充值成功 2：充值失败

    @ExcelAttribute(name = "订单利润")
    private Double profit;

    private String openId;

    private int subscribe;

    @ExcelAttribute(name = "昵称")
    private String nickName;

    @ExcelAttribute(name = "性别")
    private String sex;

    @ExcelAttribute(name = "地区")
    private String regoin;

    @ExcelAttribute(name = "关注时间")
    private Date subscribeTime;

    @ExcelAttribute(name = "卡券数量")
    private int cardNum;
    @ExcelAttribute(name = "订单数量")
    private int orderNum;

    public String getOrderNo() {
        return orderNo;
    }

    public NewerOrderExport setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

    public String getOrderPhone() {
        return orderPhone;
    }

    public NewerOrderExport setOrderPhone(String orderPhone) {
        this.orderPhone = orderPhone;
        return this;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public NewerOrderExport setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
        return this;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public NewerOrderExport setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
        return this;
    }

    public String getFlowPackage() {
        return flowPackage;
    }

    public NewerOrderExport setFlowPackage(String flowPackage) {
        this.flowPackage = flowPackage;
        return this;
    }

    public int getRechargeStatus() {
        return rechargeStatus;
    }

    public NewerOrderExport setRechargeStatus(int rechargeStatus) {
        this.rechargeStatus = rechargeStatus;
        return this;
    }

    public Double getProfit() {
        return profit;
    }

    public NewerOrderExport setProfit(Double profit) {
        this.profit = profit;
        return this;
    }

    public String getOpenId() {
        return openId;
    }

    public NewerOrderExport setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public int getSubscribe() {
        return subscribe;
    }

    public NewerOrderExport setSubscribe(int subscribe) {
        this.subscribe = subscribe;
        return this;
    }

    public String getNickName() {
        return nickName;
    }

    public NewerOrderExport setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public NewerOrderExport setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getRegoin() {
        return regoin;
    }

    public NewerOrderExport setRegoin(String regoin) {
        this.regoin = regoin;
        return this;
    }

    public Date getSubscribeTime() {
        return subscribeTime;
    }

    public NewerOrderExport setSubscribeTime(Date subscribeTime) {
        this.subscribeTime = subscribeTime;
        return this;
    }

    public int getCardNum() {
        return cardNum;
    }

    public NewerOrderExport setCardNum(int cardNum) {
        this.cardNum = cardNum;
        return this;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public NewerOrderExport setOrderNum(int orderNum) {
        this.orderNum = orderNum;
        return this;
    }
}
