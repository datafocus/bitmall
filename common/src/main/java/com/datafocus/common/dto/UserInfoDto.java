package com.datafocus.common.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserInfoDto {
	
	
	private String openId;
	
	private int subscribe;
	
	private String nickName;
	
	private int sex;
	
	private String city;
	
	private String province;
	
	private String country;
	
	private String language;
	
	private String headImgurl;
	
	private Date subscribeTime;
	
	private String unionid;
	
	private String remark;
	
	private int groupid;
	
	private int cardNum;
	
	private int orderNum;
	
	private String sceneId; //用户来源二维码场景ID
	
	private String source;//用户来源名称
	
	private List<UserTags> userTags = new ArrayList<>();

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public int getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(int subscribe) {
		this.subscribe = subscribe;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHeadImgurl() {
		return headImgurl;
	}

	public void setHeadImgurl(String headImgurl) {
		this.headImgurl = headImgurl;
	}

	public Date getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(Date subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getGroupid() {
		return groupid;
	}

	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}

	public List<UserTags> getUserTags() {
		return userTags;
	}

	public void setUserTags(List<UserTags> userTags) {
		this.userTags = userTags;
	}
	
	public int getCardNum() {
		return cardNum;
	}

	public void setCardNum(int cardNum) {
		this.cardNum = cardNum;
	}

	public int getOrderNum() {
		return orderNum;
	}

    public UserInfoDto setOrderNum(int orderNum) {
        this.orderNum = orderNum;
        return this;
    }

    public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}




	public static class UserTags{
		
		private String openId;
		
		private int tagId;
		
		private String name;

		public String getOpenId() {
			return openId;
		}

		public void setOpenId(String openId) {
			this.openId = openId;
		}

		public int getTagId() {
			return tagId;
		}

		public void setTagId(int tagId) {
			this.tagId = tagId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
	}
}
