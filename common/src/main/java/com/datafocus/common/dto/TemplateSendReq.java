package com.datafocus.common.dto;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class TemplateSendReq {
    private Integer tagId;
    @NotNull
    private String templateType;
    @NotNull
    private String first;
    @NotNull
    private String keyword1;
    @NotNull
    private String keyword2;
    private String keyword3;
    @NotNull
    private String remark;
    @NotNull
    private String remarkColor;
    private String url;

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public String getFirst() {
        return first;
    }

    public TemplateSendReq setFirst(String first) {
        this.first = first;
        return this;
    }

    public String getKeyword1() {
        return keyword1;
    }

    public TemplateSendReq setKeyword1(String keyword1) {
        this.keyword1 = keyword1;
        return this;
    }

    public String getKeyword2() {
        return keyword2;
    }

    public TemplateSendReq setKeyword2(String keyword2) {
        this.keyword2 = keyword2;
        return this;
    }

    public String getKeyword3() {
        return keyword3;
    }

    public TemplateSendReq setKeyword3(String keyword3) {
        this.keyword3 = keyword3;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemarkColor() {
        return remarkColor;
    }

    public void setRemarkColor(String remarkColor) {
        this.remarkColor = remarkColor;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
