package com.datafocus.common.dto;

public class CardDtoMgt {

	private String cardId;
	
	private String cardType;
	
	private String title;
	
	private String subTitle;
	
	private String type;
	
	private String beginTimestamp;
	
	private String endTimestamp;
	
	private int fixedTerm;
	
	private int fixedBeginTerm;
	
	private String quantity;
	
	private int cardCheck;
	
	private String brandName;
	
	private String logoUrl;
	
	private String createTime;
	
	private int isGiveFriend;
	
	private int isDelete;
	
	private int isUse;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBeginTimestamp() {
		return beginTimestamp;
	}

	public void setBeginTimestamp(String beginTimestamp) {
		this.beginTimestamp = beginTimestamp;
	}

	public String getEndTimestamp() {
		return endTimestamp;
	}

	public void setEndTimestamp(String endTimestamp) {
		this.endTimestamp = endTimestamp;
	}

	public int getFixedTerm() {
		return fixedTerm;
	}

	public void setFixedTerm(int fixedTerm) {
		this.fixedTerm = fixedTerm;
	}

	public int getFixedBeginTerm() {
		return fixedBeginTerm;
	}

	public void setFixedBeginTerm(int fixedBeginTerm) {
		this.fixedBeginTerm = fixedBeginTerm;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public int getCardCheck() {
		return cardCheck;
	}

	public void setCardCheck(int cardCheck) {
		this.cardCheck = cardCheck;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public int getIsGiveFriend() {
		return isGiveFriend;
	}

	public void setIsGiveFriend(int isGiveFriend) {
		this.isGiveFriend = isGiveFriend;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public int getIsUse() {
		return isUse;
	}

	public void setIsUse(int isUse) {
		this.isUse = isUse;
	}
	
}
