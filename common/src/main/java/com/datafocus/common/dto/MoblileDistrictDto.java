package com.datafocus.common.dto;

public class MoblileDistrictDto {

    private int id;

    private String phone;

    private String provinceCode;

    private String cityCode;

    private String vendorCode;

    private String vendor;

    private String province;

    private String city;

    public String getVPCKey(String operatorId, String type) {
        return operatorId + type + convete(this.vendorCode) + this.provinceCode + this.cityCode;
    }

    public String getVP0000Key(String operatorId, String type) {
        return operatorId + type + convete(this.vendorCode) + this.getProvinceCode() + "0000";
    }

    public String getV000000Key(String operatorId, String type) {
        return operatorId + type + convete(this.vendorCode) + "000000";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String convete(String code) {
        switch (code) {
            case "1":
                return "CMCC";
            case "2":
                return "CUCC";
            case "3":
                return "CTCC";
        }
        return null;
    }

}
