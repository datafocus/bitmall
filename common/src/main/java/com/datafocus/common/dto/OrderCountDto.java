package com.datafocus.common.dto;

/**
 * Created by layne on 2017/12/25.
 */
public class OrderCountDto {
    private Integer count;
    private String openId;

    public Integer getCount() {
        return count;
    }

    public OrderCountDto setCount(Integer count) {
        this.count = count;
        return this;
    }

    public String getOpenId() {
        return openId;
    }

    public OrderCountDto setOpenId(String openId) {
        this.openId = openId;
        return this;
    }
}
