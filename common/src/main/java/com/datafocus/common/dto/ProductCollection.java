package com.datafocus.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4256632037999721173L;
	
	private Integer carrierOperator; 
	
	private String province;
	
	private List<Traffic> traffics =  new ArrayList<>();
	
	public Integer getCarrierOperator() {
		return carrierOperator;
	}

	public void setCarrierOperator(Integer carrierOperator) {
		this.carrierOperator = carrierOperator;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public List<Traffic> getTraffics() {
		return traffics;
	}

	public void setTraffics(List<Traffic> traffics) {
		this.traffics = traffics;
	}

	public static class Traffic{
		
		private Integer id;
		
		private String productName;
		
		private BigDecimal price;
		
		private BigDecimal discount;
		
		private String description;
		
		private Integer status;
		
		private int redpackStatus;
		
		private BigDecimal redpack;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public BigDecimal getPrice() {
			return price;
		}

		public void setPrice(BigDecimal price) {
			this.price = price;
		}

		public BigDecimal getDiscount() {
			return discount;
		}

		public void setDiscount(BigDecimal discount) {
			this.discount = discount;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}

		public int getRedpackStatus() {
			return redpackStatus;
		}

		public void setRedpackStatus(int redpackStatus) {
			this.redpackStatus = redpackStatus;
		}

		public BigDecimal getRedpack() {
			return redpack;
		}

		public void setRedpack(BigDecimal redpack) {
			this.redpack = redpack;
		}
		
	}
}
