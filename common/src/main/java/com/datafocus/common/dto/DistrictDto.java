package com.datafocus.common.dto;

import java.io.Serializable;

public class DistrictDto  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2205268469905993111L;

	private Integer carryOperator;
	
	private String province;
	
	private String dname;
	
	private String carryOperatorName;
	
	private String city;

	public Integer getCarryOperator() {
		return carryOperator;
	}

	public void setCarryOperator(Integer carryOperator) {
		this.carryOperator = carryOperator;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getCarryOperatorName() {
		return carryOperatorName;
	}

	public void setCarryOperatorName(String carryOperatorName) {
		this.carryOperatorName = carryOperatorName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
