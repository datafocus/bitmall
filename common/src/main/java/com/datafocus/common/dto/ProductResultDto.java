package com.datafocus.common.dto;

import java.util.ArrayList;
import java.util.List;

public class ProductResultDto {

	private String vendor; //运营商
	
	private String province;//省份
	
	private String city;//城市
	
	private int productLevle;//0代表地市流量，1：分省流量，2：全国流量
	
	private List<Product> standProducts = new ArrayList<>(); //通用流量产品信息
	
	private List<Product> specilProducts = new ArrayList<>(); //特殊流量产品信息
	
	private List<CardDto> useCard = new ArrayList<>();//运营商，省份,城市下可适用的卡券
	
	private List<CardDto> notUseCard = new ArrayList<>();//运营商，省份，城市下不可适用的卡券
	
	private List<CardDto> productUseCard = new ArrayList<>();//运营商，省份，城市，产品下可适用的卡券
	
	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<Product> getStandProducts() {
		return standProducts;
	}

	public void setStandProducts(List<Product> standProducts) {
		this.standProducts = standProducts;
	}

	public List<Product> getSpecilProducts() {
		return specilProducts;
	}

	public void setSpecilProducts(List<Product> specilProducts) {
		this.specilProducts = specilProducts;
	}

	public List<CardDto> getUseCard() {
		return useCard;
	}

	public void setUseCard(List<CardDto> useCard) {
		this.useCard = useCard;
	}

	public List<CardDto> getNotUseCard() {
		return notUseCard;
	}

	public void setNotUseCard(List<CardDto> notUseCard) {
		this.notUseCard = notUseCard;
	}

	public List<CardDto> getProductUseCard() {
		return productUseCard;
	}

	public void setProductUseCard(List<CardDto> productUseCard) {
		this.productUseCard = productUseCard;
	}

	public int getProductLevle() {
		return productLevle;
	}

	public void setProductLevle(int productLevle) {
		this.productLevle = productLevle;
	}
	
}
