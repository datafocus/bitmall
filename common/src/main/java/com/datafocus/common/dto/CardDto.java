package com.datafocus.common.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("rawtypes")
public class CardDto  implements Comparable{

	private String cardId;

	private String userCardCode;
	
	@JsonIgnore
	private int isDelete;

	@JsonIgnore
	private int isUse;

	private String cardType;

	@JsonIgnore
	private String brandName;

	private String title;

	private String subTitle;
	
	@JsonIgnore
	private String color;

	@JsonIgnore
	private String type;
	
	@JsonIgnore
	private Date endTimestamp;
	
	@JsonIgnore
	private Date beginTimestamp;

	private int fixeTerm;

	private int fixedBeginTerm;

	@JsonIgnore
	private int leastCost;

	private int reduceCost;

	private int discount;
	
	@JsonIgnore
	private int status ; //卡券是否可用状态  0:可用，1：已过期，2：已使用，3：未到生效时间
	
	@JsonIgnore
	private Date getTime; //卡券领取 时间
	
	private String endTime;//卡券结束日期
	
	private boolean cheapFlag;//标记最优 
	
	private boolean costlyFlag;//用券更贵
	
	private BigDecimal cheapPrice;//优惠额度
	
	private String description;//使用须知

	private List<CardAcceptCategory> cardAcceptCategorys = new ArrayList<>();
	
	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getUserCardCode() {
		return userCardCode;
	}

	public void setUserCardCode(String userCardCode) {
		this.userCardCode = userCardCode;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public int getIsUse() {
		return isUse;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setIsUse(int isUse) {
		this.isUse = isUse;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getEndTimestamp() {
		return endTimestamp;
	}

	public void setEndTimestamp(Date endTimestamp) {
		this.endTimestamp = endTimestamp;
	}

	public int getFixeTerm() {
		return fixeTerm;
	}

	public void setFixeTerm(int fixeTerm) {
		this.fixeTerm = fixeTerm;
	}

	public int getFixedBeginTerm() {
		return fixedBeginTerm;
	}

	public void setFixedBeginTerm(int fixedBeginTerm) {
		this.fixedBeginTerm = fixedBeginTerm;
	}

	public int getLeastCost() {
		return leastCost;
	}

	public void setLeastCost(int leastCost) {
		this.leastCost = leastCost;
	}

	public int getReduceCost() {
		return reduceCost;
	}

	public void setReduceCost(int reduceCost) {
		this.reduceCost = reduceCost;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public List<CardAcceptCategory> getCardAcceptCategorys() {
		return cardAcceptCategorys;
	}

	public void setCardAcceptCategorys(List<CardAcceptCategory> cardAcceptCategorys) {
		this.cardAcceptCategorys = cardAcceptCategorys;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public Date getGetTime() {
		return getTime;
	}

	public void setGetTime(Date getTime) {
		this.getTime = getTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Date getBeginTimestamp() {
		return beginTimestamp;
	}

	public void setBeginTimestamp(Date beginTimestamp) {
		this.beginTimestamp = beginTimestamp;
	}

	public boolean isCheapFlag() {
		return cheapFlag;
	}

	public void setCheapFlag(boolean cheapFlag) {
		this.cheapFlag = cheapFlag;
	}

	public boolean isCostlyFlag() {
		return costlyFlag;
	}

	public void setCostlyFlag(boolean costlyFlag) {
		this.costlyFlag = costlyFlag;
	}



	public BigDecimal getCheapPrice() {
		return cheapPrice;
	}

	public void setCheapPrice(BigDecimal cheapPrice) {
		this.cheapPrice = cheapPrice;
	}



	public static class CardAcceptCategory {

		private String productId;

		private int carrierOperator;

		private String province;

		private String productName;

		public String getProductId() {
			return productId;
		}

		public void setProductId(String productId) {
			this.productId = productId;
		}

		public int getCarrierOperator() {
			return carrierOperator;
		}

		public void setCarrierOperator(int carrierOperator) {
			this.carrierOperator = carrierOperator;
		}

		public String getProvince() {
			return province;
		}

		public void setProvince(String province) {
			this.province = province;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

	}


	@Override
	public int compareTo(Object o) {
		 if(this.status < ((CardDto) o).getStatus())
			 return -1;
		 if(this.status > ((CardDto) o).getStatus())
			 return 1;
		 return 0;
	}

}
