package com.datafocus.common.dto;

import com.datafocus.common.annotation.ExcelAttribute;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 流量订单 pojo
 * @author yyhinfo
 *
 */
public class FlowOrderDto implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7767019215762520925L;

	private int id;
	
	@ExcelAttribute(name="订单号")
	private String orderNo;
	
	@ExcelAttribute(name="订单号码")
	private String orderPhone;
	
	@ExcelAttribute(name="订单价格")
	private BigDecimal orderPrice;
	
	private BigDecimal redpack;
	
	@ExcelAttribute(name="支付类型")
	private String payType;
	
	@ExcelAttribute(name="订单支付状态",prompt="0：未支付、1：已支付")
	private int orderStatus; // 0：未支付,1：已支付
	
	private int productId;
	
	@ExcelAttribute(name="订单创建时间")
	private Date orderTime;
	
	@ExcelAttribute(name="订单支付时间")
	private Date modifyTime;
	
	@ExcelAttribute(name="包型")
	private String flowPackage;
	
	private int submitStatus ;//流量充值提交状态 0：代表未提交，1：充值中，2：提交失败
	
	private int rechargeStatus;//0： 未充值 1：充值成功 2：充值失败
	
	@ExcelAttribute(name="充值提交时间")
	private Date rechargeSubmitTime;
	
	@ExcelAttribute(name="充值回调时间")
	private Date rechargeCallBackTime;
	
	private String msgid; //流量星平台返回的订单id
	
	private String  rechargeDesc;
	
	private String gallerySubmitCode;
	
	private int customeService;

	private CarryOperator carrierOperator;
	
	private String coName;
	
	private String province;
	
	private String appkey;
	
	private String cardId;
	
	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCoName() {
		return coName;
	}

	public void setCoName(String coName) {
		this.coName = coName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderPhone() {
		return orderPhone;
	}

	public BigDecimal getRedpack() {
		return redpack;
	}

	public void setRedpack(BigDecimal redpack) {
		this.redpack = redpack;
	}

	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}

	public BigDecimal getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(BigDecimal orderPrice) {
		this.orderPrice = orderPrice;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getFlowPackage() {
		return flowPackage;
	}

	public void setFlowPackage(String flowPackage) {
		this.flowPackage = flowPackage;
	}
 
	public Date getRechargeSubmitTime() {
		return rechargeSubmitTime;
	}

	public void setRechargeSubmitTime(Date rechargeSubmitTime) {
		this.rechargeSubmitTime = rechargeSubmitTime;
	}

	public Date getRechargeCallBackTime() {
		return rechargeCallBackTime;
	}

	public void setRechargeCallBackTime(Date rechargeCallBackTime) {
		this.rechargeCallBackTime = rechargeCallBackTime;
	}

	public int getSubmitStatus() {
		return submitStatus;
	}

	public void setSubmitStatus(int submitStatus) {
		this.submitStatus = submitStatus;
	}

	public int getRechargeStatus() {
		return rechargeStatus;
	}

	public void setRechargeStatus(int rechargeStatus) {
		this.rechargeStatus = rechargeStatus;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

	public String getRechargeDesc() {
		return rechargeDesc;
	}

	public void setRechargeDesc(String rechargeDesc) {
		this.rechargeDesc = rechargeDesc;
	}

	public String getGallerySubmitCode() {
		return gallerySubmitCode;
	}

	public void setGallerySubmitCode(String gallerySubmitCode) {
		this.gallerySubmitCode = gallerySubmitCode;
	}

	public int getCustomeService() {
		return customeService;
	}

	public void setCustomeService(int customeService) {
		this.customeService = customeService;
	}

	public CarryOperator getCarrierOperator() {
		return carrierOperator;
	}

	public void setCarrierOperator(CarryOperator carrierOperator) {
		this.carrierOperator = carrierOperator;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}
	
	
}
