package com.datafocus.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductInfo {

    private String operatorId;

    private String carrierOperator;

    private String remark;

    private String productCode;

    private String status;

    private String displayPriority;

    private String productSize;

    private String type;

    private String stPrice;

    private String city;

    private String province;

    private String salePrice;

    private String productName;

    public String groupKey(String operatorId) {
        return operatorId + this.type + convete(this.carrierOperator) + this.province + this.city;
    }

    private String convete(String code) {
        switch (code) {
            case "1":
                return "CMCC";
            case "2":
                return "CUCC";
            case "3":
                return "CTCC";
        }
        return code;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getCarrierOperator() {
        return carrierOperator;
    }

    public void setCarrierOperator(String carrierOperator) {
        this.carrierOperator = carrierOperator;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDisplayPriority() {
        return displayPriority;
    }

    public void setDisplayPriority(String displayPriority) {
        this.displayPriority = displayPriority;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStPrice() {
        return stPrice;
    }

    public void setStPrice(String stPrice) {
        this.stPrice = stPrice;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "ClassPojo [operatorId = " + operatorId + ", carrierOperator = " + carrierOperator + ", remark = " + remark + ", productCode = " + productCode + ", status = " + status + ", displayPriority = " + displayPriority + ", productSize = " + productSize + ", type = " + type + ", stPrice = " + stPrice + ", city = " + city + ", province = " + province + ", salePrice = " + salePrice + ", productName = " + productName + "]";
    }
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProductInfoWapper {

        @JsonProperty("priceInfoList")
        private List<ProductInfo> list;

        private String provinceCode;
        private String cityCode;
        private String carryOperator;

        public List<ProductInfo> getList() {
            return list;
        }

        public void setList(List<ProductInfo> list) {
            this.list = list;
        }

        public String getProvinceCode() {
            return provinceCode;
        }

        public void setProvinceCode(String provinceCode) {
            this.provinceCode = provinceCode;
        }

        public String getCityCode() {
            return cityCode;
        }

        public ProductInfoWapper setCityCode(String cityCode) {
            this.cityCode = cityCode;
            return this;
        }

        public String getCarryOperator() {
            return carryOperator;
        }

        public ProductInfoWapper setCarryOperator(String carryOperator) {
            this.carryOperator = carryOperator;
            return this;
        }
    }
}
