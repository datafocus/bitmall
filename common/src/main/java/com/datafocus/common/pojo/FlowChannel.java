package com.datafocus.common.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 流量通道 pojo
 * @author yyhinfo
 *
 */
public class FlowChannel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6258685807425813455L;
	
	private int id;
	
	private int province;
	
	private int carrierOperator;
	
	private int status;
	
	private Date createTime;
	
	private Date modifyTime;

	private String provinceName;
	
	private String carrierOperatorName;
	
	private String remark;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProvince() {
		return province;
	}

	public void setProvince(int province) {
		this.province = province;
	}

	public int getCarrierOperator() {
		return carrierOperator;
	}

	public void setCarrierOperator(int carrierOperator) {
		this.carrierOperator = carrierOperator;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCarrierOperatorName() {
		return carrierOperatorName;
	}

	public void setCarrierOperatorName(String carrierOperatorName) {
		this.carrierOperatorName = carrierOperatorName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
 

	
	       
	
	
}
