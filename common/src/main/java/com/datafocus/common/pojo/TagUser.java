package com.datafocus.common.pojo;

import java.io.Serializable;

/**
 * Created by layne on 2017/9/27.
 */
public class TagUser implements Serializable {
    private static final long serialVersionUID = -4665972898472812598L;
    private Integer id;
    private String openId;
    private Integer tagId;

    public Integer getId() {
        return id;
    }

    public TagUser setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getOpenId() {
        return openId;
    }

    public TagUser setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public Integer getTagId() {
        return tagId;
    }

    public TagUser setTagId(Integer tagId) {
        this.tagId = tagId;
        return this;
    }
}
