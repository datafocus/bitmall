package com.datafocus.common.pojo;

import java.util.Date;

/**
 * Created by layne on 2017/12/6.
 */
public class EnterRecord {
    /**
     * 记录id
     */
    private Long id;

    /**
     * 进入公众号的openid
     */
    private String openId;

    /**
     * 进入公众号时间
     */
    private Date enterTime;

    public Long getId() {
        return id;
    }

    public EnterRecord setId(Long id) {
        this.id = id;
        return this;
    }

    public String getOpenId() {
        return openId;
    }

    public EnterRecord setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public Date getEnterTime() {
        return enterTime;
    }

    public EnterRecord setEnterTime(Date enterTime) {
        this.enterTime = enterTime;
        return this;
    }
}
