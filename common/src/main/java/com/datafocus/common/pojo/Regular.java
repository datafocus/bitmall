package com.datafocus.common.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 自动回复规则
 * 
 * @author yyhinfo
 *
 */
public class Regular implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 规则id
	 */
	private Integer id;

	/**
	 * 规则名称
	 */
	private String regularName;
	/**
	 * 是否全部回复 0：是，1：否
	 */
	private Integer full;

	/**
	 * 关键字列表
	 */
	private List<KeyWord> keyWords = new ArrayList<>();

	/**
	 * 消息列表
	 */
	private List<KeyWordMessage> keyWordMessages = new ArrayList<>();

	
	
	public Integer getFull() {
		return full;
	}

	public void setFull(Integer full) {
		this.full = full;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRegularName() {
		return regularName;
	}

	public void setRegularName(String regularName) {
		this.regularName = regularName;
	}

	public List<KeyWord> getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(List<KeyWord> keyWords) {
		this.keyWords = keyWords;
	}

	public List<KeyWordMessage> getKeyWordMessages() {
		return keyWordMessages;
	}

	public void setKeyWordMessages(List<KeyWordMessage> keyWordMessages) {
		this.keyWordMessages = keyWordMessages;
	}

	/**
	 * 关键字
	 * 
	 * @author yyhinfo
	 *
	 */
	public static class KeyWord implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 415552432245985043L;

		/**
		 * 规则id
		 */
		private Integer regularId;

		/**
		 * 关键字
		 */
		private String key;

		/**
		 * 匹配规则   0:精确匹配，1：模糊匹配
		 */
		private Integer match;

		public Integer getRegularId() {
			return regularId;
		}

		public void setRegularId(Integer regularId) {
			this.regularId = regularId;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public Integer getMatch() {
			return match;
		}

		public void setMatch(Integer match) {
			this.match = match;
		}

	}

	/**
	 * 关键字对应的消息
	 * 
	 * @author yyhinfo
	 *
	 */
	public static class KeyWordMessage implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5324695952369706038L;

		/**
		 * 规则id
		 */
		private Integer regularId;

		/**
		 * 消息类型   0：文本，2：图片,3:卡券，4：图文，5：语音
		 */
		private Integer msgType;

		/**
		 * 消息类容
		 */
		private String message;

		public int getRegularId() {
			return regularId;
		}

		public void setRegularId(Integer regularId) {
			this.regularId = regularId;
		}

		public Integer getMsgType() {
			return msgType;
		}

		public void setMsgType(Integer msgType) {
			this.msgType = msgType;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}
}