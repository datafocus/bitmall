package com.datafocus.common.pojo;

import java.util.Date;

public class Ticket {
	
	private String activityName;
	
	private String actionName;
	
	private int sceneId;
	
	private String ticket;
	
	private String url;
	
	private int fansNum;
	
	private Date createTime;
	
	private Integer expireSeconds;

    public String getActivityName() {
        return activityName;
    }

    public Ticket setActivityName(String activityName) {
        this.activityName = activityName;
        return this;
    }

    public String getActionName() {
        return actionName;
    }

    public Ticket setActionName(String actionName) {
        this.actionName = actionName;
        return this;
    }

    public int getSceneId() {
        return sceneId;
    }

    public Ticket setSceneId(int sceneId) {
        this.sceneId = sceneId;
        return this;
    }

    public String getTicket() {
        return ticket;
    }

    public Ticket setTicket(String ticket) {
        this.ticket = ticket;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Ticket setUrl(String url) {
        this.url = url;
        return this;
    }

    public int getFansNum() {
        return fansNum;
    }

    public Ticket setFansNum(int fansNum) {
        this.fansNum = fansNum;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Ticket setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Integer getExpireSeconds() {
        return expireSeconds;
    }

    public Ticket setExpireSeconds(Integer expireSeconds) {
        this.expireSeconds = expireSeconds;
        return this;
    }
}
