package com.datafocus.common.pojo;

import java.io.Serializable;

/**
 * Created by liuheng on 2017/9/1.
 */
public class AlarmConfig implements Serializable{

    private Integer id;
    private Integer baseTimes;
    private Integer conTimes;
    private Integer percent;
    private Integer status;
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBaseTimes() {
        return baseTimes;
    }

    public void setBaseTimes(Integer baseTimes) {
        this.baseTimes = baseTimes;
    }

    public Integer getConTimes() {
        return conTimes;
    }

    public void setConTimes(Integer conTimes) {
        this.conTimes = conTimes;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


















