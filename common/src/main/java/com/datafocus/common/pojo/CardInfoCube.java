package com.datafocus.common.pojo;

import com.datafocus.weixin.mp.card.bean.CardDataCube.CardInfoDataCube;

public class CardInfoCube  extends CardInfoDataCube{

	private String cardName;

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	
	
	
}
