package com.datafocus.common.pojo;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by layne on 2017/12/5.
 */
public class MsgMarketingRes extends MsgMarketingStatistics {
    /**
     * 新关注用户数
     */
    private Long subscriberCount;

    /**
     * 取关用户数
     */
    private Long unSubscriberCount;

    /**
     * 领券用户数
     */
    private Long cardCount;

    /**
     * 进入h5 用户数
     */
    private Long enterConut;
    /**
     * 解析号码数
     */
    private Long typeNumConut;

    /**
     * 消费用户数
     */
    private Long payCount;

    /**
     * 生成订单数
     */
    private Long orderCount;

    /**
     * 支付成功订单数
     */
    private Long paidOrderCount;

    /**
     * 充值成功订单数
     */
    private Long successCount;
    /**
     * 营收
     */
    private BigDecimal amount;

    /**
     * 利润
     */
    private BigDecimal profit;

    /**
     * 关注率
     */
    private Double subRate;

    /**
     * 进入H5率
     */
    private Double enterRate;
    /**
     * 支付率
     */
    private Double payRate;

    /**
     * 充值成功率
     */
    private Double successRate;

    /**
     * 总体转化率
     */
    private Double totalRate;


    public MsgMarketingRes(MsgMarketingStatistics msgMarketingStatistics) {
        BeanWrapper srcBean = new BeanWrapperImpl(msgMarketingStatistics);
        PropertyDescriptor[] pds = srcBean.getPropertyDescriptors();
        Set<String> emptyName = new HashSet<String>();
        for (PropertyDescriptor p : pds) {
            Object srcValue = srcBean.getPropertyValue(p.getName());
            if (srcValue == null) emptyName.add(p.getName());
        }
        String[] result = new String[emptyName.size()];
        BeanUtils.copyProperties(msgMarketingStatistics, this, result);
    }

    public Long getSubscriberCount() {
        return subscriberCount;
    }

    public MsgMarketingRes setSubscriberCount(Long subscriberCount) {
        this.subscriberCount = subscriberCount;
        return this;
    }

    public Long getUnSubscriberCount() {
        return unSubscriberCount;
    }

    public MsgMarketingRes setUnSubscriberCount(Long unSubscriberCount) {
        this.unSubscriberCount = unSubscriberCount;
        return this;
    }

    public Long getCardCount() {
        return cardCount;
    }

    public MsgMarketingRes setCardCount(Long cardCount) {
        this.cardCount = cardCount;
        return this;
    }

    public Long getEnterConut() {
        return enterConut;
    }

    public MsgMarketingRes setEnterConut(Long enterConut) {
        this.enterConut = enterConut;
        return this;
    }

    public Long getTypeNumConut() {
        return typeNumConut;
    }

    public MsgMarketingRes setTypeNumConut(Long typeNumConut) {
        this.typeNumConut = typeNumConut;
        return this;
    }

    public Long getPayCount() {
        return payCount;
    }

    public MsgMarketingRes setPayCount(Long payCount) {
        this.payCount = payCount;
        return this;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public MsgMarketingRes setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
        return this;
    }

    public Long getSuccessCount() {
        return successCount;
    }

    public MsgMarketingRes setSuccessCount(Long successCount) {
        this.successCount = successCount;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public MsgMarketingRes setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public MsgMarketingRes setProfit(BigDecimal profit) {
        this.profit = profit;
        return this;
    }

    public Double getSubRate() {
        return subRate;
    }

    public MsgMarketingRes setSubRate(Double subRate) {
        this.subRate = subRate;
        return this;
    }

    public Double getEnterRate() {
        return enterRate;
    }

    public MsgMarketingRes setEnterRate(Double enterRate) {
        this.enterRate = enterRate;
        return this;
    }

    public Double getPayRate() {
        return payRate;
    }

    public MsgMarketingRes setPayRate(Double payRate) {
        this.payRate = payRate;
        return this;
    }

    public Double getSuccessRate() {
        return successRate;
    }

    public MsgMarketingRes setSuccessRate(Double successRate) {
        this.successRate = successRate;
        return this;
    }

    public Double getTotalRate() {
        return totalRate;
    }

    public MsgMarketingRes setTotalRate(Double totalRate) {
        this.totalRate = totalRate;
        return this;
    }

    public Long getPaidOrderCount() {
        return paidOrderCount;
    }

    public MsgMarketingRes setPaidOrderCount(Long paidOrderCount) {
        this.paidOrderCount = paidOrderCount;
        return this;
    }
}
