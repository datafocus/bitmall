package com.datafocus.common.pojo;

import java.io.Serializable;
import java.util.List;

public class Role implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2022243591332687362L;

	private int id;
	
	private String state;
	
	private String name;
	
	private String roleKey;
	
	private String description;
	
 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoleKey() {
		return roleKey;
	}

	public void setRoleKey(String roleKey) {
		this.roleKey = roleKey;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
	public static String getRoles(List<String> list) {

		if (list.size() == 0)
			return BLANK_STRING;
		StringBuilder sbBuilder = new StringBuilder();
		// sbBuilder.append(",");
		sbBuilder.append("uhomeRoles[");
		for (int i = 0, flag = 1; i < list.size(); i++) {
			String s = list.get(i);
			sbBuilder.append(s);
			if (flag % list.size() != 0)
				sbBuilder.append(",");
			flag++;
		}
		sbBuilder.append("]");
		return sbBuilder.toString();

	}
	
	private static final String BLANK_STRING = "";
}
