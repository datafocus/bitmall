package com.datafocus.common.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Resources implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 7184130117295475875L;

    private int id;

    private String name;

    private int parentId;

    private String resKey;

    private String type;

    private String resUrl;

    private String resAddress;

    private int level;

    private String icon;

    private int ishide;

    private String description;

    private List<Role> roles = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getResKey() {
        return resKey;
    }

    public void setResKey(String resKey) {
        this.resKey = resKey;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResUrl() {
        return resUrl;
    }

    public void setResUrl(String resUrl) {
        this.resUrl = resUrl;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getIshide() {
        return ishide;
    }

    public void setIshide(int ishide) {
        this.ishide = ishide;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getResAddress() {
        return resAddress;
    }

    public Resources setResAddress(String resAddress) {
        this.resAddress = resAddress;
        return this;
    }
}
