package com.datafocus.common.pojo;

/**
 * 二元组
 *
 * @param <K>
 * @param <V>
 */
public class Tuple2<K, V> {

    public final K _1;
    public final V _2;

    public Tuple2(K k, V v) {
        _1 = k;
        _2 = v;
    }
}
