package com.datafocus.common.pojo;

public class UserInfo  {


	private String sex;

	private String nickname;

	private String remark;

	private String unionid;

	private String city;

	private String country;

	private String subscribeTime;

	private String subscribe;

	private String province;

	private String openid;

	private String groupid;

	private String language;

	private String headimgurl;

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(String subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

	public String getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	@Override
	public String toString() {
		return "ClassPojo [sex = " + sex + ", nickname = " + nickname + ", remark = " + remark + ", unionid = "
				+ unionid + ", city = " + city + ", country = " + country + ", subscribe_time = " + subscribeTime
				+ ",subscribe = " + subscribe + ", province = " + province + ", openid = " + openid + ", groupid = "
				+ groupid + ", language = " + language + ", headimgurl = " + headimgurl + "]";
	}
}
