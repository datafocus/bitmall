package com.datafocus.common.pojo;

import java.io.Serializable;

/**
 * 合作商订单
 *
 * @author layne
 * @date 17-7-5
 */
public class PartnerOrder implements Serializable {
    private static final long serialVersionUID = -2495065537012626842L;
    private int id;
    /**
     * 合作商标识
     */
    private String partnerKey;
    /**
     * 订单号
     */
    private String orderNo;

    public int getId() {
        return id;
    }

    public PartnerOrder setId(int id) {
        this.id = id;
        return this;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public PartnerOrder setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
        return this;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public PartnerOrder setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }
}
