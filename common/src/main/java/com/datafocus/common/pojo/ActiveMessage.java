package com.datafocus.common.pojo;

import java.io.Serializable;

/**
 * 被动消息实体
 * @author yyhinfo
 *
 */
public class ActiveMessage implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	
	/**
	 * 消息内容
	 * 
	 */
	private String message;
	
	/**
	 * 消息类型 0：文本，1：图片,2:卡券
	 */
	private int msgType;
	
	/**
	 * 回复类型 ，0：关注自动回复；1：发送消息之后自动回复
	 */
	private int type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getMsgType() {
		return msgType;
	}

	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	
	
}
