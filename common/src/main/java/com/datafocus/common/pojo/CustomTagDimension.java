package com.datafocus.common.pojo;

import java.util.List;

public class CustomTagDimension {
	
	private int id;
	
	private String dimensionName;
	
	private List<CustomTag> customTags;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDimensionName() {
		return dimensionName;
	}

	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

	public List<CustomTag> getCustomTag() {
		return customTags;
	}

	public void setCustomTag(List<CustomTag> customTags) {
		this.customTags = customTags;
	}
	
	
	

}
