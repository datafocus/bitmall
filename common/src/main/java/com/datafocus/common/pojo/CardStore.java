package com.datafocus.common.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 卡券货架实体
 * @author yyhinfo
 *
 */
public class CardStore {
	
	private String pageId;
	
	private String url;
	
	private String banner;
	
	private String title;
	
	private int canShare;
	
	private Date createTime;
	
	private String scene;
	
	private List<CardStoreExt> cardStoreExt = new ArrayList<>();
	
	
	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBanner() {
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCanShare() {
		return canShare;
	}

	public void setCanShare(int canShare) {
		this.canShare = canShare;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	
	public List<CardStoreExt> getCardStoreExt() {
		return cardStoreExt;
	}

	public void setCardStoreExt(List<CardStoreExt> cardStoreExt) {
		this.cardStoreExt = cardStoreExt;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public static class CardStoreExt{
		
		private String pageId;
		
		private String cardId;
		
		private String thumbUrl;

		private String name;//卡券名称
		
		
		public String getPageId() {
			return pageId;
		}

		public void setPageId(String pageId) {
			this.pageId = pageId;
		}

		public String getCardId() {
			return cardId;
		}

		public void setCardId(String cardId) {
			this.cardId = cardId;
		}

		public String getThumbUrl() {
			return thumbUrl;
		}

		public void setThumbUrl(String thumbUrl) {
			this.thumbUrl = thumbUrl;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		
	}
}
