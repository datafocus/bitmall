package com.datafocus.common.pojo;

import com.datafocus.common.annotation.ExcelAttribute;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 话费订单
 * @author yyhinfo
 *
 */
public class BillOrder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;//订单主键ID
	
	private String orderNo;//订单号
	
	@ExcelAttribute(name="充值手机号")
	private String orderPhone;//充值号码
	
	private String openId;//用户唯一标识
	
	@ExcelAttribute(name="充值金额")
	private BigDecimal amount;//充值金额
	
	private BigDecimal salePrice;//销售价
	
	private String vendor;//运营商
	
	private String province;//身份
	
	private int payState;//支付状态
	
	private int dealState;//充值状态
	
	private int refundState;//退款状态
	
	private Date createTime;//订单创建时间
	
	private Date payTime;//订单支付时间
	
	private Date modifyTime;//订单修改时间(充值回调时间)
	
	private String desc;//产品描述

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderPhone() {
		return orderPhone;
	}

	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public int getDealState() {
		return dealState;
	}

	public void setDealState(int dealState) {
		this.dealState = dealState;
	}

	public int getRefundState() {
		return refundState;
	}

	public void setRefundState(int refundState) {
		this.refundState = refundState;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public int getPayState() {
		return payState;
	}

	public void setPayState(int payState) {
		this.payState = payState;
	}

}
