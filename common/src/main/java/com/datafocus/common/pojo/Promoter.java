package com.datafocus.common.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 推客
 */
public class Promoter implements Serializable {

    private static final long serialVersionUID = -8940096335400355743L;
    /**
     * id 数据库id
     */
    private Integer id;
    /**
     * openid
     */
    private String openId;
    /**
     * 场景ID 对应二维码的场景
     */
    private Integer scenesId;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 二维码地址
     */
    private String tickt;
    /**
     * 创建时间
     */
    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public Promoter setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getOpenId() {
        return openId;
    }

    public Promoter setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public Integer getScenesId() {
        return scenesId;
    }

    public Promoter setScenesId(Integer scenesId) {
        this.scenesId = scenesId;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Promoter setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getTickt() {
        return tickt;
    }

    public Promoter setTickt(String tickt) {
        this.tickt = tickt;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Promoter setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Promoter setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }
}
