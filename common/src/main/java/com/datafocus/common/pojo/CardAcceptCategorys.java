package com.datafocus.common.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * 卡券使用商品
 * @author yyhinfo
 *
 */
public class CardAcceptCategorys {
	
	private List<CardAcceptCategory> cardAcceptCategorys = new ArrayList<>();
	
	public List<CardAcceptCategory> getCardAcceptCategorys() {
		return cardAcceptCategorys;
	}

	public void setCardAcceptCategorys(List<CardAcceptCategory> cardAcceptCategorys) {
		this.cardAcceptCategorys = cardAcceptCategorys;
	}

	public static class CardAcceptCategory{
		/**
		 * 卡券id
		 */
		private String cardId;
		/**
		 * 产品id
		 */
		private String productId;
		/**
		 * 产品对应的运营商
		 */
		private int carrierOperator;
		/**
		 * 产品对应的地区
		 */
		private String province;
		/**
		 * 产品对应的城市
		 */
		private String city;
		
		/**
		 * 产品类型
		 */
		private String productType;
		
		private String productName;
		
		
		public String getProductType() {
			return productType;
		}
		public void setProductType(String productType) {
			this.productType = productType;
		}
		public String getCardId() {
			return cardId;
		}
		public void setCardId(String cardId) {
			this.cardId = cardId;
		}
		public String getProductId() {
			return productId;
		}
		public void setProductId(String productId) {
			this.productId = productId;
		}
		public int getCarrierOperator() {
			return carrierOperator;
		}
		public void setCarrierOperator(int carrierOperator) {
			this.carrierOperator = carrierOperator;
		}
		public String getProvince() {
			return province;
		}
		public void setProvince(String province) {
			this.province = province;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getProductName() {
			return productName;
		}
		public void setProductName(String productName) {
			this.productName = productName;
		}
		
		
	}
}
