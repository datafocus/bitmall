package com.datafocus.common.pojo;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Account implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7764579245416106361L;
	
	@NotNull
	private Integer id;

	@NotNull
	private String accountId;

	@NotNull
	private String accountName;
	@NotNull
	private String password;
	
	private String locked;

	@NotNull
	private Integer deletestatus;
	
	private Date createTime;
	
	private String credentialsSalt;

	private List<Role> roles = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public Account setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public Account setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getAccountName() {
        return accountName;
    }

    public Account setAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Account setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getLocked() {
        return locked;
    }

    public Account setLocked(String locked) {
        this.locked = locked;
        return this;
    }

    public Integer getDeletestatus() {
        return deletestatus;
    }

    public Account setDeletestatus(Integer deletestatus) {
        this.deletestatus = deletestatus;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Account setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getCredentialsSalt() {
        return credentialsSalt;
    }

    public Account setCredentialsSalt(String credentialsSalt) {
        this.credentialsSalt = credentialsSalt;
        return this;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public Account setRoles(List<Role> roles) {
        this.roles = roles;
        return this;
    }

}
