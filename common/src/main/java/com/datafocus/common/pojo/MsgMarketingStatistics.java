package com.datafocus.common.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by layne on 2017/12/5.
 */
public class MsgMarketingStatistics implements Serializable {
    /**
     * 活动ID
     */
    private Long id;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 短信发送成功次数
     */
    private Long msgSuccessCount;

    /**
     * 短信供应运营商
     */
    private String platformOperator;

    /**
     * 短信内容
     */
    private String msgContent;

    /**
     * 短信发送省份
     */
    private String province;

    /**
     * 短信发送地市
     */
    private String city;

    /**
     * 运营商
     */
    private String operator;
    /**
     * 活动类型
     */
    private String activityType;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    public Long getId() {
        return id;
    }

    public MsgMarketingStatistics setId(Long id) {
        this.id = id;
        return this;
    }

    public String getActivityName() {
        return activityName;
    }

    public MsgMarketingStatistics setActivityName(String activityName) {
        this.activityName = activityName;
        return this;
    }

    public Long getMsgSuccessCount() {
        return msgSuccessCount;
    }

    public MsgMarketingStatistics setMsgSuccessCount(Long msgSuccessCount) {
        this.msgSuccessCount = msgSuccessCount;
        return this;
    }

    public String getPlatformOperator() {
        return platformOperator;
    }

    public MsgMarketingStatistics setPlatformOperator(String platformOperator) {
        this.platformOperator = platformOperator;
        return this;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public MsgMarketingStatistics setMsgContent(String msgContent) {
        this.msgContent = msgContent;
        return this;
    }

    public String getProvince() {
        return province;
    }

    public MsgMarketingStatistics setProvince(String province) {
        this.province = province;
        return this;
    }

    public String getCity() {
        return city;
    }

    public MsgMarketingStatistics setCity(String city) {
        this.city = city;
        return this;
    }

    public String getOperator() {
        return operator;
    }

    public MsgMarketingStatistics setOperator(String operator) {
        this.operator = operator;
        return this;
    }

    public String getActivityType() {
        return activityType;
    }

    public MsgMarketingStatistics setActivityType(String activityType) {
        this.activityType = activityType;
        return this;
    }

    public Date getStartTime() {
        return startTime;
    }

    public MsgMarketingStatistics setStartTime(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    public Date getEndTime() {
        return endTime;
    }

    public MsgMarketingStatistics setEndTime(Date endTime) {
        this.endTime = endTime;
        return this;
    }

    @Override
    public String toString() {
        return "MsgMarketingStatistics{" +
                "id=" + id +
                ", activityName='" + activityName + '\'' +
                ", msgSuccessCount=" + msgSuccessCount +
                ", platformOperator='" + platformOperator + '\'' +
                ", msgContent='" + msgContent + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", operator='" + operator + '\'' +
                ", activityType='" + activityType + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
