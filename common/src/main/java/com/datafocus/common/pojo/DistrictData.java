package com.datafocus.common.pojo;

import java.io.Serializable;
import java.util.List;

public class DistrictData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5046625481104269065L;

	private String id;
	
	private String name;
	
	private List<Cities> cities;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Cities> getCities() {
		return cities;
	}

	public void setCities(List<Cities> cities) {
		this.cities = cities;
	}

	public static class Cities implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 7804029965389000373L;

		private String id;
		
		private String name;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		
	}
}
