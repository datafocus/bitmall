package com.datafocus.common.pojo;

import java.io.Serializable;

/**
 * 合作商
 *
 * @author layne
 * @date 17-7-5
 */
public class Partner implements Serializable {
    private static final long serialVersionUID = 8272950564346630713L;

    private int id;
    /**
     * 合作商名字
     */
    private String partnerName;
    /**
     * 合作商标识
     */
    private String partnerKey;

    public Partner(String partnerName) {
        this.partnerName = partnerName;
    }

    public int getId() {
        return id;
    }

    public Partner() {
    }

    public Partner setId(int id) {
        this.id = id;
        return this;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public Partner setPartnerName(String partnerName) {
        this.partnerName = partnerName;
        return this;
    }

    public String getPartnerKey() {
        return partnerKey;
    }

    public Partner setPartnerKey(String partnerKey) {
        this.partnerKey = partnerKey;
        return this;
    }

}
