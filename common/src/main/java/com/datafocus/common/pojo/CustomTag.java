package com.datafocus.common.pojo;

public class CustomTag {
	
	private int tagId;
	
	private String tagName;
	
	private int wxTagId;
	
	private int num;

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public int getWxTagId() {
		return wxTagId;
	}

	public void setWxTagId(int wxTagId) {
		this.wxTagId = wxTagId;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
}
