package com.datafocus.common.exception;

/**
 * @author layne
 * @date 17-7-26
 */
public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    protected int code;
    protected Object[] args;
    protected Object arg;

    public ApplicationException() {
        super();
    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(int code, String message) {
        super(message);
        this.code = code;
    }

    public ApplicationException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public ApplicationException(String message, Object[] args) {
        super(message);
        this.args = args;
    }

    public ApplicationException(String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.args = args;
    }

    public ApplicationException(int code, String message, Object[] args) {
        super(message);
        this.code = code;
        this.args = args;
    }

    public ApplicationException(int code, String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.code = code;
        this.args = args;
    }

    public int getCode() {
        return this.code;
    }

    public Object[] getArgs() {
        return this.args;
    }
}
