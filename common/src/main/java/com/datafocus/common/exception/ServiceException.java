package com.datafocus.common.exception;

/**
 * @author layne
 * @date 17-7-26
 */
public class ServiceException extends BusinessException {
    private static final long serialVersionUID = 1L;

    public ServiceException(int code, String message) {
        super(code, message);
    }

    public ServiceException(int code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message 错误信息
     * @param args    额外参数
     */
    public ServiceException(String message, Object[] args) {
        super(message);
        this.args = args;
    }

    /**
     * @param message 错误信息
     * @param cause   原始异常
     * @param args    额外参数
     */
    public ServiceException(String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.args = args;
    }

    /**
     * @param code    错误码
     * @param message 错误信息
     * @param args    额外参数
     */
    public ServiceException(int code, String message, Object[] args) {
        super(message);
        this.code = code;
        this.args = args;
    }

    /**
     * @param code    错误码
     * @param message 错误信息
     * @param cause   原始异常
     * @param args    额外参数
     */
    public ServiceException(int code, String message, Throwable cause, Object[] args) {
        super(message, cause);
        this.code = code;
        this.args = args;
    }

}
