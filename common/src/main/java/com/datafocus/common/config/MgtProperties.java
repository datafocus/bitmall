package com.datafocus.common.config;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * Created by liuheng on 2017/8/1.
 */
public class MgtProperties implements Serializable {

    private String username;

    private String password;

    private String rechargeUrl;

    private String callbackUrl;

    private String queryRechargeStatusUrl;

    private long runTime;

    private String notifyTemplateUrl;

    private boolean imagesPurgeJobEnable;

    private String domain;

    private String shareCardId ;

    private Map<String, String> marketTemplate;

    private Integer marketThreadNum;

    public Map<String, String> getMarketTemplate() {
        return marketTemplate;
    }

    public void setMarketTemplate(Map<String, String> marketTemplate) {
        this.marketTemplate = marketTemplate;
    }

    public Integer getMarketThreadNum() {
        return marketThreadNum;
    }

    public void setMarketThreadNum(Integer marketThreadNum) {
        this.marketThreadNum = marketThreadNum;
    }

    public String getDomain() {
        return domain;
    }

    public MgtProperties setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public MgtProperties setUsername(String username) {
        this.username = username;
        return this;
    }

    public boolean isImagesPurgeJobEnable() {
        return imagesPurgeJobEnable;
    }

    public MgtProperties setImagesPurgeJobEnable(boolean imagesPurgeJobEnable) {
        this.imagesPurgeJobEnable = imagesPurgeJobEnable;
        return this;
    }

    public MgtProperties setPassword(String password) {
        this.password = password;

        return this;
    }

    public String getRechargeUrl() {
        return rechargeUrl;
    }

    public MgtProperties setRechargeUrl(String rechargeUrl) {
        this.rechargeUrl = rechargeUrl;
        return this;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public MgtProperties setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
        return this;
    }

    public String getQueryRechargeStatusUrl() {
        return queryRechargeStatusUrl;
    }

    public MgtProperties setQueryRechargeStatusUrl(String queryRechargeStatusUrl) {
        this.queryRechargeStatusUrl = queryRechargeStatusUrl;
        return this;
    }

    public long getRunTime() {
        return runTime;
    }

    public MgtProperties setRunTime(long runTime) {
        this.runTime = runTime;
        return this;
    }

    public String getNotifyTemplateUrl() {
        return notifyTemplateUrl;
    }

    public MgtProperties setNotifyTemplateUrl(String notifyTemplateUrl) {
        this.notifyTemplateUrl = notifyTemplateUrl;
        return this;
    }

    public String getShareCardId() {
        return shareCardId;
    }

    public MgtProperties setShareCardId(String shareCardId) {
        this.shareCardId = shareCardId;
        return this;
    }
}
