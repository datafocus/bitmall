package com.datafocus.common.config;

import java.io.Serializable;

public class ProtalProperties implements Serializable {

    private static final long serialVersionUID = 3752511175242479842L;
    /**
     * 移动正则
     */
    private String cmcc;
    /**
     * 联通正则
     */
    private String cucc;

    /**
     * 电信正则
     */
    private String ctcc;

    /**
     * 微信支付异步通知地址
     */
    private String notifyUrl;

    /**
     * 获取流量星产品url地址
     */
    private String flowProductUrl;

    /**
     * 模板消息通知地址
     */
    private String notifyTemplateUrl;

    /**
     * 流量星客户ID
     */
    private String operatorId;

    private String templateSuccess;

    private String templateFail;

    private String templateDelay;

    private String templateAccept;

    private String templateAlarm;

    public String getCmcc() {
        return cmcc;
    }

    public ProtalProperties setCmcc(String cmcc) {
        this.cmcc = cmcc;
        return this;
    }

    public String getCucc() {
        return cucc;
    }

    public ProtalProperties setCucc(String cucc) {
        this.cucc = cucc;
        return this;
    }

    public String getCtcc() {
        return ctcc;
    }

    public ProtalProperties setCtcc(String ctcc) {
        this.ctcc = ctcc;
        return this;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public ProtalProperties setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
        return this;
    }

    public String getFlowProductUrl() {
        return flowProductUrl;
    }

    public ProtalProperties setFlowProductUrl(String flowProductUrl) {
        this.flowProductUrl = flowProductUrl;
        return this;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public ProtalProperties setOperatorId(String operatorId) {
        this.operatorId = operatorId;
        return this;
    }

    public String getTemplateSuccess() {
        return templateSuccess;
    }

    public ProtalProperties setTemplateSuccess(String templateSuccess) {
        this.templateSuccess = templateSuccess;
        return this;
    }

    public String getTemplateFail() {
        return templateFail;
    }

    public ProtalProperties setTemplateFail(String templateFail) {
        this.templateFail = templateFail;
        return this;
    }

    public String getTemplateDelay() {
        return templateDelay;
    }

    public ProtalProperties setTemplateDelay(String templateDelay) {
        this.templateDelay = templateDelay;
        return this;
    }

    public String getTemplateAccept() {
        return templateAccept;
    }

    public ProtalProperties setTemplateAccept(String templateAccept) {
        this.templateAccept = templateAccept;
        return this;
    }

    public String getNotifyTemplateUrl() {
        return notifyTemplateUrl;
    }

    public ProtalProperties setNotifyTemplateUrl(String notifyTemplateUrl) {
        this.notifyTemplateUrl = notifyTemplateUrl;
        return this;
    }

    public String getTemplateAlarm() {
        return templateAlarm;
    }

    public ProtalProperties setTemplateAlarm(String templateAlarm) {
        this.templateAlarm = templateAlarm;
        return this;
    }

}
