package com.datafocus.common.config;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author layne
 * @date 17-7-25
 */
public class MapProperties {
    /**
     * 自定义权限校验的map
     */
    Map<String, String> filterChainDefinitions;

    Map<String, BigDecimal> activityProductPrice;

    Map<String, Integer> activityProductLimit;

    public Map<String, String> getFilterChainDefinitions() {
        return filterChainDefinitions;
    }

    public MapProperties setFilterChainDefinitions(Map<String, String> filterChainDefinitions) {
        this.filterChainDefinitions = filterChainDefinitions;
        return this;
    }

    public Map<String, BigDecimal> getActivityProductPrice() {
        return activityProductPrice;
    }

    public MapProperties setActivityProductPrice(Map<String, BigDecimal> activityProductPrice) {
        this.activityProductPrice = activityProductPrice;
        return this;
    }

    public Map<String, Integer> getActivityProductLimit() {
        return activityProductLimit;
    }

    public MapProperties setActivityProductLimit(Map<String, Integer> activityProductLimit) {
        this.activityProductLimit = activityProductLimit;
        return this;
    }
}
