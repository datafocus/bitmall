package com.datafocus.common.interfaces;

import com.datafocus.common.dto.DistrictDto;
import com.datafocus.common.dto.MoblileDistrictDto;
import com.datafocus.common.pojo.DistrictData;
import com.datafocus.common.pojo.PubDistrict;

import java.util.List;

/**
 * @author layne
 * @date 17-8-3
 */
public interface IDistrictController {

    DistrictDto district(String mobile);

    List<PubDistrict> findAllPubDistrict();

    PubDistrict findPubDistrictByDname(String name);

    List<DistrictData> findDistrictData();

    MoblileDistrictDto mobileDistrict(String phone);
}
