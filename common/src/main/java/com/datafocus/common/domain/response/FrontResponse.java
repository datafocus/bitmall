package com.datafocus.common.domain.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 前置响应
 * Created by layne on 2017/6/9.
 */
public class FrontResponse<T> {
    /**
     * 响应码
     */
    private int code;

    /**
     * 响应结果中文描述
     */
    private String message = "";

    /**
     * 响应数据
     */
    private T data;



    public int getCode() {
        return code;
    }

    public FrontResponse setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public FrontResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public FrontResponse setData(T data) {
        this.data = data;
        return this;
    }

    public static FrontResponse success() {
        return new FrontResponse().setCode(0).setMessage("success");
    }

    public static FrontResponse response() {
        return new FrontResponse();
    }

}
