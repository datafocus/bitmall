package com.datafocus.common.domain.page;

/**
 * Created by layne on 2017/6/9.
 */
public class FlowPage {

    private int page = 0;

    private int size = 20;

    private boolean count;

    private String orderBy;

    public FlowPage() {
    }

    public FlowPage(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public FlowPage setPage(int page) {
        this.page = page;
        return this;
    }

    public boolean isCount() {
        return count;
    }

    public FlowPage setCount(boolean count) {
        this.count = count;
        return this;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public FlowPage setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public int getSize() {
        return size;
    }

    public FlowPage setSize(int size) {
        this.size = size;
        return this;
    }

    public static FlowPage defaultPage() {
        return new FlowPage();
    }
}
