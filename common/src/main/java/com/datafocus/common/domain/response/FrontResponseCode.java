package com.datafocus.common.domain.response;

/**
 * 返回给前端代码
 *
 * @author layne
 * @date 17-7-4
 */
public enum FrontResponseCode {

    SYSTEM_ERROR(-1, "系统错误"),

    SERVICE_ERROR(1, "服务失败"),

    SERVICE_TIMEOUT(2, "服务超时"),

    OPENID_TIMEOUT(3, "openid过期"),

    NO_PERMISIONS(403, "权限不够"),


    NEED_LOGIN(405, "需要登录"),


    NULL_PARAM(1000, "参数为空"),


    ACCOUNT_LOCKED(1001, "账户被锁定"),

    WRONG_PASSWORD(1002, "帐号或者密码不正确");

    private String msg;

    private Integer code;

    FrontResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public FrontResponseCode setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public FrontResponseCode setCode(Integer code) {
        this.code = code;
        return this;
    }
}
