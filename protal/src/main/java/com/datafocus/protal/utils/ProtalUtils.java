package com.datafocus.protal.utils;

import com.datafocus.common.dto.CarryOperator;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by liuheng on 2017/7/28.
 */
public class ProtalUtils {

    // 获得客户端真实IP地址的方法二：
    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return null ==ip ?"" : ip.split(",")[0];
    }

    public static CarryOperator checkMobile(String line,String cmcc,String cucc,String ctcc)
    {
        Pattern py = Pattern.compile(cmcc);
        Pattern py2 = Pattern.compile(cucc);
        Pattern py3 = Pattern.compile(ctcc);
        Matcher my = py.matcher(line.trim());
        Matcher my2 = py2.matcher(line.trim());
        Matcher my3 = py3.matcher(line.trim());
        if (my.matches()) {
            return CarryOperator.CMCC;
        } else if (my2.matches()) {
            return CarryOperator.CUCC;
        } else if (my3.matches()) {
            return CarryOperator.CTCC;
        } else {
            return CarryOperator.UNKNOWN;
        }
    }
}
