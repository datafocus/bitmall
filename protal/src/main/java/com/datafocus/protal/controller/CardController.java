package com.datafocus.protal.controller;

import com.datafocus.common.annotation.WxScope;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.dto.CardDto;
import com.datafocus.service.service.CardService;
import com.datafocus.service.utils.RedisUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.Map;

/**
 * 卡券处理器
 *
 * @author yyhinfo
 */
@Api(description = "卡券处理器")
@RestController
@RequestMapping("/card")
public class CardController {


    @Autowired
    private CardService cardService;

    @Autowired
    private RedisUtil<String, String> redisUtil;

    @ApiOperation("用户卡劵列表")
    @WxScope
    @Deprecated
    @GetMapping(value = "/list")
    public FrontResponse list(@RequestParam("page") Integer page,@RequestParam("size") Integer size,@ApiIgnore @RequestAttribute String openId){
        PageInfo<CardDto> pageInfo=cardService.list(openId, page, size);
        Map<String, Object> data = new HashMap<>();
        data.put("hasMore", pageInfo.isHasNextPage());
        data.put("count", pageInfo.getTotal());
        data.put("content", pageInfo.getList());
        return FrontResponse.success().setData(data);
    }

    /**
     * 获取包型下适用的卡券
     *
     * @param productId
     * @return
     */
    @ApiOperation("获取包型下适用的卡券")
    @WxScope
    @GetMapping(value = "/findCardByOpenId")
    public FrontResponse findCardByOpenId(@RequestParam("productId") String productId,@RequestParam("productLevle") Integer productLevle,
                                          @RequestParam("productName") String productName,@ApiIgnore @RequestAttribute String openId) {
        return FrontResponse.success().setData(cardService.findCardByOpenId(openId, productId, productLevle, productName));
    }
}
