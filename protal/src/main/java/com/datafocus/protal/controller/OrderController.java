package com.datafocus.protal.controller;

import com.datafocus.common.annotation.WxScope;
import com.datafocus.common.config.MapProperties;
import com.datafocus.common.config.ProtalProperties;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.dto.DistrictDto;
import com.datafocus.common.pojo.BillOrder;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.protal.handler.AlarmHandler;
import com.datafocus.protal.utils.ProtalUtils;
import com.datafocus.service.service.BillOrderService;
import com.datafocus.service.service.OrderService;
import com.datafocus.service.service.ProductService;
import com.datafocus.service.service.impl.TemplateMsgServiceImpl;
import com.datafocus.service.service.remote.DistrictService;
import com.datafocus.service.utils.BaseResult;
import com.datafocus.service.utils.DFUtils;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.weixin.common.jsapi.JsAPISignature;
import com.datafocus.weixin.common.util.JsonMapper;
import com.datafocus.weixin.common.util.XmlObjectMapper;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.jsapi.JsAPIs;
import com.datafocus.weixin.pay.base.PaySetting;
import com.datafocus.weixin.pay.mp.JsSigns;
import com.datafocus.weixin.pay.mp.Orders;
import com.datafocus.weixin.pay.mp.bean.JSSignature;
import com.datafocus.weixin.pay.mp.bean.PaymentNotification;
import com.datafocus.weixin.pay.mp.bean.UnifiedOrderRequest;
import com.datafocus.weixin.pay.mp.bean.UnifiedOrderResponse;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Api(description = "订单管理模块")
@RestController
@RequestMapping("/order")
public class OrderController {

    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private TemplateMsgServiceImpl templateMsgService;

    @Autowired
    private BillOrderService billOrderService;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private Orders orders;

    @Autowired
    private AlarmHandler alarmHandler;

    @Autowired
    private AppSetting mpSetting;

    @Autowired
    private PaySetting paySetting;

    @Resource
    private ProductService productService;

    @Resource
    private ProtalProperties protalProperties;

    private JsAPIs jsAPIs;

    private JsSigns jsSigns;

    @PostConstruct
    public void init() {
        jsAPIs = JsAPIs.with(mpSetting);
        jsSigns = JsSigns.with(paySetting);
    }



    @ApiOperation("流量充值记录")
    @WxScope
    @Deprecated
    @GetMapping(value = "/list")
    public FrontResponse list(@RequestParam("page") Integer page, @RequestParam("size") Integer size, @ApiIgnore @RequestAttribute String openId) {
        PageInfo<FlowOrder> pageInfo = orderService.list(openId, page, size);
        Map<String, Object> result = new HashMap<>();
        result.put("content", pageInfo.getList());
        result.put("count", pageInfo.getTotal());
        result.put("hasMore", pageInfo.isHasNextPage());
        return FrontResponse.success().setData(result);
    }

    @ApiOperation("用户一个月流量充值记录")
    @WxScope
    @GetMapping(value = "/feeFlowOrders")
    public FrontResponse feeFlowOrders(@ApiIgnore @RequestAttribute String openId) {
        return FrontResponse.success().setData(orderService.list(openId));
    }

    @ApiOperation("用户话费充值记录")
    @Deprecated
    @WxScope
    @GetMapping(value = "/freeBillOrders")
    public FrontResponse freeBillOrders(@ApiIgnore @RequestAttribute String openId) {
        return FrontResponse.success().setData(orderService.freeBillOrders(openId));
    }


    /**
     * 创建支付订单，调用微信统一下单平台，生成订单
     *
     * @return
     */
    @ApiOperation("统一下单接口")
    @Deprecated
    @WxScope
    @PostMapping(value = "/create")
    public FrontResponse order(@ApiIgnore HttpServletRequest request, @ApiIgnore @RequestAttribute String openId,
                               @RequestParam("phone") String phone, @RequestParam("productId") String productId,
                               @RequestParam("payType") String payType, @RequestParam("cardId") String cardId,
                               @RequestParam("userCardCode") String userCardCode) {
        Map<String, Object> params = new HashMap<>();
        params.put("phone", phone);
        params.put("productId", productId);
        params.put("payType", payType);
        params.put("cardId", cardId);
        params.put("userCardCode", userCardCode);
        params.put("openId", openId);
        params.put("ip", ProtalUtils.getIpAddr(request));
        logger.info("创建订单开始--->{}", params.toString());
        long beferoTime = new Date().getTime();
        JSSignature jSSignature = orderService.saveOrder(params);
        JsAPISignature jsAPISignature = jsAPIs.createJsAPISignature(request.getRequestURI());

        Map<String, Object> data = new HashMap<>();
        data.put("jSSignature", jSSignature);
        data.put("jsAPISignature", jsAPISignature);
        long afterTime = new Date().getTime();
        logger.info("订单创建完毕--->{},耗时{}毫秒", params.toString(), (afterTime - beferoTime));
        return FrontResponse.success().setData(data);
    }


    /**
     * 创建流量订单
     *
     * @return
     */
    @ApiOperation("流量下单接口")
    @WxScope
    @PostMapping(value = "/flowOrder")
    public FrontResponse flowOrder(@ApiIgnore HttpServletRequest request, @ApiIgnore @RequestAttribute String openId,
                                   @RequestParam("mobile") String mobile, @RequestParam(value = "cardId",required = false) String cardId,
                                   @RequestParam(value = "cardCode",required = false) String cardCode,
                                   @RequestParam("productId") String productId, @RequestParam(value = "partnerKey",required = false) String partnerKey,@RequestParam(value = "flag",required = false) String flag) {

        boolean thanksProduct=false;
        List<String> productList=productService.activityProductsList();
        logger.info("productId==========={}", productId);
        if(productList!=null&&productList.size()>0){
            logger.info("产品==========={}", JsonMapper.defaultMapper().toJson(productList));
            for(String s:productList){
                if((protalProperties.getOperatorId()+productId).equals(s)){
                    thanksProduct=true;
                    break;
                }
            }
        }
        if(thanksProduct){
            if(StringUtils.isNotBlank(cardId)||StringUtils.isNotBlank(cardCode)){
                return FrontResponse.response().setCode(1).setMessage("活动产品不能使用卡券");
            }
            logger.info("活动产品");
            cardId="thanks";
        }
        Map<String, Object> params = new HashMap<>();
        params.put("ip", ProtalUtils.getIpAddr(request));
        params.put("partnerKey", partnerKey);
        params.put("openId", openId);
        params.put("mobile", mobile);
        params.put("cardId", cardId);
        params.put("cardCode", cardCode);
        params.put("productId", productId);
        JSSignature signature = orderService.createFlowOrder(params);
        return FrontResponse.success().setData(signature);
    }

    /**
     * 话费订单创建
     *
     * @param mobile    充值手机号
     * @param productId 产品ID
     * @return
     */
    @ApiOperation("话费下单接口")
    @WxScope
    @PostMapping(value = "/billOrder")
    public FrontResponse billOrder(@RequestParam String mobile, @RequestParam Integer productId, @ApiIgnore @RequestAttribute String openId, @ApiIgnore HttpServletRequest request) {

        DistrictDto districtDto = districtService.district(mobile.substring(0, 7));
        String vendor = districtDto.getCarryOperatorName();
        String province = districtDto.getDname();
        if (!vendor.equals("移动") && !province.equals("广东")) throw new RuntimeException("非广东移动号段，暂时不支持充值");
        BigDecimal amount = DFUtils.getAmountByProductId(productId);
        BigDecimal salePrice = DFUtils.getSaleByProductId(productId);
        BillOrder order = new BillOrder();
        order.setAmount(amount);
        order.setSalePrice(salePrice);
        order.setDesc(amount.toString() + "元慢充话费");
        order.setOrderPhone(mobile);
        order.setOrderNo(DFUtils.createBillNo("2000"));
        order.setOpenId(openId);
        order.setVendor(vendor);
        order.setProvince(province);
        order.setPayState(0);//未支付
        UnifiedOrderResponse orderResponse = unifiedOrder(order, ProtalUtils.getIpAddr(request));
        JSSignature jSSignature = jsSigns.createJsSignature(orderResponse.getPrepayId());
        billOrderService.saveBillOrder(order);
        return FrontResponse.success().setData(jSSignature);
    }

    /**
     * 支付回调
     *
     * @param request
     * @return
     */
    @ApiIgnore
    @PostMapping(value = "/webhooks")
    public void webhooks(HttpServletRequest request,HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        try {
            String raw = IOUtils.toString(request.getInputStream());
            logger.info("微信支付通知报文:{}", raw);
            PaymentNotification paymentNotification = XmlObjectMapper.nonEmptyMapper().fromXml(raw, PaymentNotification.class);
            orderService.webhooks(paymentNotification);
            logger.info("微信支付回调成功");
            response.getWriter().println("success");
        } catch (Exception e) {
            logger.error("微信支付异步回调通知系统异常--------->", e);
            response.getWriter().println("fail");
        }
    }

    @GetMapping("/operatorCallBack")
    public String operatorCallBack(@RequestParam("orderNo") String orderNo){
        orderService.operatorCallBack(orderNo);
        return "success";
    }


    /**
     * 手机充值状态回调
     *
     * @param request
     * @throws Exception
     */
    @ApiIgnore
    @PostMapping(value = "/rechargeCallback")
    public void rechargeCallback(HttpServletRequest request, HttpServletResponse response)throws IOException{
        response.setContentType("text/html");
        try {
            String msgIn = IOUtils.toString(request.getInputStream());
            Integer result=orderService.rechargeCallBack(msgIn);
            alarmHandler.submitAlarm(result);
            logger.info("流量星平台回调成功");
            response.getWriter().println("success");
        }catch (Exception e){
            logger.error("流量星平台回调失败，系统异常----------------->", e);
            response.getWriter().println("fail");
        }
    }

    /**
     * 微信公众平台发送模板消息
     *
     * @param orderId------>订单号
     * @param msgType------>消息类型 1：流量充值成功，2：流量充值失败,3：流量充值中，4：流量充值业务受理
     */
    @ApiIgnore
    @PostMapping(value = "/sendTemplateMessage")
    public BaseResult sendTemplateMessage(@RequestParam("orderId") String orderId, @RequestParam("msgType") String msgType) {
        return templateMsgService.sendTempletMsg(orderId, msgType);
    }

  /*
    微信公众平台发送模板消息

    @param orderId------>订单号
     @param msgType------>消息类型 1：流量充值成功，2：流量充值失败,3：流量充值中，4：流量充值业务受理
    @ApiIgnore
    @PostMapping(value = "/sendTemplateMessage")
    public BaseResult sendTemplateMessage(@RequestParam String orderId, @RequestParam String msgType) {
        return templateMsgService.sendTempletMsg(orderId, msgType);
    }*/

    //构建统一下单
    private UnifiedOrderRequest bulidUnifiedOrderRequest(BillOrder order, String ip) {
        UnifiedOrderRequest unifiedOrderRequest = new UnifiedOrderRequest();
        unifiedOrderRequest.setBody("手机充值中心");
        unifiedOrderRequest.setDetail(order.getDesc());
        unifiedOrderRequest.setTradeNumber(order.getOrderNo());
        unifiedOrderRequest.setTotalFee(order.getSalePrice().multiply(new BigDecimal(100)).intValue());
        unifiedOrderRequest.setBillCreatedIp(ip);
        unifiedOrderRequest.setNotifyUrl(protalProperties.getNotifyUrl());
        unifiedOrderRequest.setTradeType("JSAPI");
        unifiedOrderRequest.setOpenId(order.getOpenId());
        return unifiedOrderRequest;
    }

    private UnifiedOrderResponse unifiedOrder(BillOrder order, String ip) {
        UnifiedOrderRequest unifiedOrderRequest = bulidUnifiedOrderRequest(order, ip);
        UnifiedOrderResponse unifiedOrderResponse = orders.unifiedOrder(unifiedOrderRequest);
        return unifiedOrderResponse;
    }

}
