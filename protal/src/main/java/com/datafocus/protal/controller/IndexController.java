package com.datafocus.protal.controller;

import com.datafocus.common.annotation.WxScope;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.dto.UserInfoDto;
import com.datafocus.service.service.EnterRecordService;
import com.datafocus.service.service.UserInfoService;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.weixin.common.decrypt.AesException;
import com.datafocus.weixin.common.jsapi.CardJsSignature;
import com.datafocus.weixin.common.oauth2.AccessToken;
import com.datafocus.weixin.common.util.URLEncoder;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.jsapi.CardJsAPIs;
import com.datafocus.weixin.mp.jsapi.JsAPIs;
import com.datafocus.weixin.mp.user.Users;
import com.datafocus.weixin.pay.base.PaySetting;
import com.focus.weixin.open.oauth2.OpenOAuth2s;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Api(description = "微信相关	")
@RestController
@RequestMapping("/wx")
public class IndexController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    private Users users;

    private CardJsAPIs cardJsAPIs;

    private JsAPIs jsAPIs;

    private OpenOAuth2s openOAuth2s;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private RedisUtil<String, String> redisUtil;

    @Autowired
    private AppSetting mpSetting;

    @Autowired
    private com.datafocus.weixin.open.base.AppSetting openSetting;

    @Autowired
    EnterRecordService enterRecordService;

    @PostConstruct
    public void init() {
        users = Users.with(mpSetting);
        cardJsAPIs = CardJsAPIs.with(mpSetting);
        jsAPIs = JsAPIs.with(mpSetting);
        openOAuth2s = OpenOAuth2s.with(openSetting);
    }

    /**
     * 获取card js  签名
     */
    @ApiOperation("获取card js签名")
    @Deprecated
    @GetMapping(value = "/getCardSignature")
    public FrontResponse getCardSignature(@RequestParam("shopId") String shopId, @RequestParam("cardType") String cardType,
                                          @RequestParam("cardId") String cardId) throws AesException {
        CardJsSignature cardJsSignature = cardJsAPIs.createCardSignature(shopId, cardType, cardId);
        return FrontResponse.success().setData(cardJsSignature);
    }

    /**
     * 创建JsAPI签名
     *
     * @param url
     * @return
     */
    @ApiOperation("获取jsApi签名")
    @GetMapping(value = "/createJsAPISignature")
    public FrontResponse createJsAPISignature(@RequestParam("url") String url) {
        return FrontResponse.success().setData(jsAPIs.createJsAPISignature(url));
    }

    /**
     * code获取openid保存
     *
     * @param
     * @return
     */
    @ApiOperation("推送授权码")
    @PostMapping(value = "/submitCode")
    public FrontResponse submitCode(@RequestParam("code") String code, @ApiIgnore HttpServletResponse response) {
        AccessToken token = openOAuth2s.getAccessToken(code);
        if (StringUtils.isEmpty(token.getOpenId())) {
            logger.warn("获取OpenId为空");
            return FrontResponse.response().setMessage("未能获取OpenId，请尝试刷新或联系客服！");
        } else {
            logger.info("获取用户openId成功,code=" + code + ",openId=" + token.getOpenId());
            redisUtil.addOrUpdate(code, token.getOpenId(), 60 * 30);
            Cookie c = new Cookie("jsessionid", code);
            c.setPath("/");
            c.setMaxAge(60 * 30);
            response.addCookie(c);
            return FrontResponse.success();
        }

    }

    @WxScope
    @ApiOperation("获取当前用户信息")
    @GetMapping(value = "/userInfo")
    public FrontResponse userInfo(@ApiIgnore @RequestAttribute String openId) {
        UserInfoDto userInfoDto = userInfoService.get(openId);
        return FrontResponse.success().setData(userInfoDto);
    }


    @ApiOperation("获取用户信息根据openid")
    @GetMapping(value = "/userInfoByOpenId")
    public FrontResponse userInfoByOpenId(@RequestParam("openId") String openId) {
        UserInfoDto userInfoDto = userInfoService.get(openId);
        return FrontResponse.success().setData(userInfoDto);
    }

    @GetMapping(value = "/authUser")
    public void authUser(HttpServletResponse response, @RequestParam("return_url") String return_url, @RequestParam(value = "type",required = false) String type) throws IOException {
        StringBuffer returnUrl = new StringBuffer();
        if (StringUtils.isNotBlank(type)) {
            returnUrl.append("http://").append(mpSetting.getAppId());
            returnUrl.append(".bitemen.cn/bitmall-protal/wx/authUserCallBack?return_url=").append(return_url);
            returnUrl.append("&type=").append(type);
        } else {
            String domain = return_url.substring(0, return_url.indexOf("bitemen.cn/"));
            returnUrl.append(domain);
            returnUrl.append("bitemen.cn/bitmall-protal/wx/authUserCallBack?return_url=").append(return_url);
        }
        StringBuffer url = new StringBuffer();
        url.append("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");
        url.append(mpSetting.getAppId()).append("&redirect_uri=").append(URLEncoder.encode(returnUrl.toString()));
        url.append("&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect");
        response.sendRedirect(url.toString());
    }

    @GetMapping(value = "/authUserCallBack")
    public void authUserCallBack(HttpServletResponse response, @RequestParam("code") String code, @RequestParam("return_url") String return_url, @RequestParam(value="type",required = false) String type) throws IOException {
        AccessToken token = openOAuth2s.getAccessToken(code);
        redisUtil.addOrUpdate(code, token.getOpenId(), 60 * 30);
        try {
            enterRecordService.enterRecord(token.getOpenId());
        } catch (Exception e) {
            logger.error("统计用户进入公众号出错", e);
        }
        logger.info("公众号授权成功");
        if (StringUtils.isNotBlank(type)) {
            String openid = redisUtil.getValue("pay_" + token.getOpenId());
            if (StringUtils.isBlank(openid)) {
                String domain = return_url.substring(0, return_url.indexOf("bitemen.cn/"));
                logger.info("重定向到" + domain + "bitemen.cn");
                response.sendRedirect(domain + "bitemen.cn/bitmall-protal/wx/authUser?return_url=" + return_url + "&userKey=" + code);
                return;
            }
        }
        response.sendRedirect(return_url + "?code=" + code);
    }
}
