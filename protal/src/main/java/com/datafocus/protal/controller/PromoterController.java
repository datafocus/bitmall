package com.datafocus.protal.controller;

import com.datafocus.common.annotation.WxScope;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.Promoter;
import com.datafocus.service.service.PromoterService;
import com.datafocus.service.service.TicketService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.Map;

/**
 * 推客控制器
 */
@Api(description = "推客控制器")
@RestController
@RequestMapping("/promoter")
public class PromoterController {

    @Autowired
    private PromoterService promoterService;

    @Autowired
    private TicketService ticketService;

    @WxScope
    @PostMapping("/apply")
    public FrontResponse apply(@RequestAttribute @ApiIgnore String openId) {
        Promoter promoter = new Promoter();
        promoter.setOpenId(openId);
        String tickt = promoterService.save(promoter);
        byte[] bytes = ticketService.getQrcode(tickt);
        Map<String, Object> data = new HashMap<>();
        data.put("bytes", bytes);
        return FrontResponse.success().setData(data);
    }

    @WxScope
    @GetMapping("/myFlowAnalysis")
    public FrontResponse myFlowAnalysis(@RequestAttribute @ApiIgnore String openId) {
        return FrontResponse.success().setData(promoterService.myFlowAnalysis(openId));
    }

    @WxScope
    @GetMapping("/isNewer")
    public FrontResponse isNewer(@RequestAttribute @ApiIgnore String openId) {
        boolean isNewer = promoterService.isNewer(openId);
        HashMap<String, Boolean> data = new HashMap<>();
        data.put("isNewer", isNewer);
        return FrontResponse.success().setData(data);
    }
}
