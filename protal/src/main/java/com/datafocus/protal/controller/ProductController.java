package com.datafocus.protal.controller;

import com.datafocus.common.annotation.WxScope;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.exception.ServiceException;
import com.datafocus.service.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by liuheng on 2017/7/26.
 */
@Api(description = "产品管理模块")
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 列出手机号所在地区的流量产品
     *
     * @param mobile
     * @return
     */

    @ApiOperation(value = "获取运营商地区下的流量产品")
    @Deprecated
    @GetMapping(value = "/list/{mobile}")
    public FrontResponse list(@PathVariable("mobile") String mobile) {
        if (mobile.length() < 11)
            throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), "手机号格式错误");
        return FrontResponse.success().setData(productService.list(mobile));
    }

    /**
     * 查找手机号所在地区
     *
     * @param mobile
     * @return
     */
    @GetMapping(value = "/queryMobileStation/{mobile}")
    @ApiOperation(value = "获取手机号归属地")
    public FrontResponse queryMobileStation(@PathVariable("mobile") String mobile) {
        if (mobile.length() < 11)
            throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), "手机号格式错误");
        return FrontResponse.success().setData(productService.queryMobileStation(mobile));
    }

    @ApiOperation(value = "从流量星获取流量产品放入缓存")
    @GetMapping(value = "/productNotify")
    public ResponseEntity productNotify() {
        try {
            productService.getProductForFlowStar();
            return new ResponseEntity<String>("success", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("fail", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 查找用户普通和特殊流量产品
     *
     * @param mobile
     * @return
     */
    @GetMapping(value = "/getProduct/{mobile}")
    @WxScope
    @ApiOperation(value = "获取用户流量产品")
    public FrontResponse getProduct(@PathVariable("mobile") String mobile, @ApiIgnore @RequestAttribute String openId) {
        return FrontResponse.success().setData(productService.findTraffic(mobile, openId));
    }

    @GetMapping(value = "/getActivityProducts")
    public FrontResponse getActivityProducts(String mobile) throws IOException {
        return FrontResponse.success().setData(productService.findActivityProducts(mobile));
    }
    @GetMapping(value = "/activityProductsList")
    public FrontResponse activityProductsList() throws IOException {
        return FrontResponse.success().setData(productService.activityProductsList());
    }
}
