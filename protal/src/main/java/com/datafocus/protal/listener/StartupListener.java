package com.datafocus.protal.listener;

import com.datafocus.service.mapper.FlowOrderMapper;
import com.datafocus.service.service.AlarmConfigService;
import com.datafocus.service.service.ProductService;
import com.datafocus.service.task.*;
import com.datafocus.service.utils.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Component
public class StartupListener implements ApplicationListener<ApplicationPreparedEvent> {

	private static final Logger logger = LoggerFactory.getLogger(StartupListener.class);

	private  ExecutorService executorService = Executors.newCachedThreadPool();

    @Autowired
    private RefundOrderDelayService refundDelayService;

    @Autowired
    private RechargeOrderDelayService rechargeDelayService;

    @Autowired
    private OnDelayedListener onDelayedListener;

    @Autowired
    private FlowOrderMapper flowOrderMapper;

    @Autowired
    private ProductService productService;

    @Autowired
	private AlarmConfigService alarmConfigService;

    @Autowired
	private RedisUtil<String,String> redisUtil;
    @Override
    public void onApplicationEvent(ApplicationPreparedEvent evt)
    {
    	logger.info(">>>>>>>>>>>>系统启动完成，onApplicationEvent()");
        if (evt.getApplicationContext().getParent() == null) {
            return;
        }
        //启动退款查询订单处理
        refundDelayService.start(onDelayedListener);
        //查找需要入队的订单
        executorService.execute(new Runnable(){
            @Override
            public void run() {
        		logger.info("查找需要入队的退款中的订单");
        		List<String> orders = flowOrderMapper.findOrderByRefund();
        		if(orders !=null && !orders.isEmpty()){
        			logger.info("需要入队的退款订单数:{}条",orders.size());
        			for (String orderNo : orders) {
        				int startTime = RefundOrderDelayed.startTimeMap.get(1);//第一次查询退款状态
        				RefundOrderDelayed refundOrder = new RefundOrderDelayed(1,orderNo,startTime);
        				refundDelayService.add(refundOrder);
					}
        		}else{
        			logger.info("没有需要入队查询退款状态的订单");
        		}
            }
        });

        //启动充值延时订单处理
        rechargeDelayService.start(onDelayedListener);

        executorService.execute(new Runnable() {
			@Override
			public void run() {
				logger.info("查找需要入队的充值延时的订单");

				List<String> orders = flowOrderMapper.findOrderByOrderStatusAndSubmitStatus();
				
				if(orders !=null && !orders.isEmpty()){
					logger.info("需要入队的充值延时订单数:{}条",orders.size());
					for(String orderNo : orders){
						RechargeOrderDelayed rechargeOrderDelayed =  new RechargeOrderDelayed(orderNo,1);
						rechargeDelayService.add(rechargeOrderDelayed);
					}
				}else{
					logger.info("没有需要入队的充值延时订单,0条");
				}
			}
		});
        
        //初始化流量星产品信息
        productService.getProductForFlowStar();
        alarmConfigService.init();
	}
}
