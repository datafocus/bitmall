package com.datafocus.protal;

import com.datafocus.service.config.MybatisConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * @author layne
 * @date 17-7-21
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.datafocus.service", "com.datafocus.protal"})
@EnableEurekaClient
@EnableFeignClients({"com.datafocus.service.service.remote"})
@AutoConfigureAfter(MybatisConfig.class)
public class ProtalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProtalApplication.class, args);
    }
}
