package com.datafocus.protal.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.datafocus.common.annotation.WxScope;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.exception.ApplicationException;
import com.datafocus.service.utils.RedisUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author layne
 * @date 17-7-26
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    private static Logger logger = LoggerFactory.getLogger(WebConfig.class);
    @Autowired
    private RedisUtil<String, String> redisUtil;

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(mappingJackson2HttpMessageConverter());
        super.configureMessageConverters(converters);
    }

    @Bean
    public Converter<String, Date> customDateConverter() {
        return new Converter<String, Date>() {
            @Override
            public Date convert(String s) {
                try {
                    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(s);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
    }


    private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.TEXT_HTML);
        mediaTypes.add(MediaType.TEXT_PLAIN);
        mediaTypes.add(MediaType.APPLICATION_XML);
        mediaTypes.add(MediaType.TEXT_XML);
        mediaTypes.add(MediaType.APPLICATION_JSON);
        mediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
        mediaTypes.add(MediaType.MULTIPART_FORM_DATA);
        converter.setSupportedMediaTypes(mediaTypes);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        converter.setObjectMapper(objectMapper);
        return converter;
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new WxSocpeFilter());
        super.addInterceptors(registry);
    }

    /* private class CrossOriginFilter extends HandlerInterceptorAdapter {
         @Override
         public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
             response.setHeader("Access-Control-Allow-Origin", "*");
             response.setHeader("Access-Control-Allow-Methods", "*");
             response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
             response.setHeader("Content-Type", "application/json;charset=utf-8");
             return super.preHandle(request, response, handler);
         }
     }
 */
    private class WxSocpeFilter extends HandlerInterceptorAdapter {
        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
            String msg = "";
            if (!(handler instanceof HandlerMethod)) return true;
            HandlerMethod method = (HandlerMethod) handler;
            WxScope wxScope = method.getMethod().getAnnotation(WxScope.class);
            if (wxScope == null) return true;
            Cookie cookies[] = request.getCookies();
            if (cookies != null && cookies.length > 0) {
                msg = "找不到key=jsessionId的cookie";
                for (Cookie c : cookies) {
                    if (c.getName().equals("jsessionid")) {
                        long a = System.currentTimeMillis();
                        String opendId = redisUtil.getValue(c.getValue());
                        if (StringUtils.isNotBlank(opendId)) {
                            request.setAttribute("openId", opendId);
                            return true;
                        }
                        long b = System.currentTimeMillis();
                        msg = "从缓存中获取的openId为空,获取时间：" + (b - a) + "，code=" + c.getValue();
                        Cookie cookie = new Cookie("jsessionid", "");
                        cookie.setPath("/");
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                        break;
                    }
                }
            } else {
                msg = "找不到cookie";
            }
            logger.error("正在访问的方法：" + method.getMethod().getName() + "，错误信息：" + msg);

            throw new ApplicationException(FrontResponseCode.OPENID_TIMEOUT.getCode(), "openid授权超时");
        }
    }

    @Bean
    public FilterRegistrationBean webStatFilterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.setEnabled(true);
        Map<String, String> params = new HashMap<>();
        params.put("exclusions", "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*");
        filterRegistrationBean.setInitParameters(params);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

    @Bean
    public ServletRegistrationBean statViewServletRegistrationBean() {
        ServletRegistrationBean statView = new ServletRegistrationBean();
        statView.setServlet(new StatViewServlet());
        statView.addUrlMappings("/druid/*");
        statView.addInitParameter("loginUsername", "admin");
        statView.addInitParameter("loginPassword", "123456");
        statView.addInitParameter("resetEnable", "false");
        return statView;
    }

    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("http://wx20955debbe776a4a.bitemen.cn");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/wx/createJsAPISignature", buildConfig());
        return new CorsFilter(source);
    }
}
