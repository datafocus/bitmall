package com.datafocus.mgt.handler;

import com.datafocus.common.pojo.AlarmConfig;
import com.datafocus.common.pojo.AlarmUser;
import com.datafocus.service.service.AlarmConfigService;
import com.datafocus.service.service.AlarmUserService;
import com.datafocus.service.service.TemplateMsgService;
import com.datafocus.service.utils.SmsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by liuheng on 2017/8/31.
 */
@Component
public class AlarmHandler {
    private static Logger logger = LoggerFactory.getLogger(AlarmHandler.class);
    private static int failConTimes=0;
    private static int id=2;

    @Autowired
    private TemplateMsgService templates;
    @Autowired
    private AlarmConfigService alarmConfigService;
    @Autowired
    private AlarmUserService alarmUserService;

    @Value("${alarm.sms.url}")
    private String smsUrl;
    @Value("${alarm.sms.username}")
    private String smsUsername;
    @Value("${alarm.sms.password}")
    private String smsPassword;


    public void submitAlarm(int status){
        AlarmConfig alarmConfig=alarmConfigService.getConfig(id);
        if(alarmConfig.getStatus()==1){
            if(status==0&&failConTimes==0) return;
            synchronized(this){
                if(status==0){
                    failConTimes=0; //重置连续次数
                    logger.info("重置连续失败次数为0");
                }else{
                    failConTimes++;
                    logger.info("连续失败次数+1:"+failConTimes);
                    if(failConTimes>=alarmConfig.getConTimes()){
                        sendMsg(String.valueOf(failConTimes));
                        logger.info("连续失败次数"+failConTimes+"已达阈值，发送消息，重置连续失败次数为0");
                        failConTimes=0;
                    }
                }
            }
        }
    }

    public  void sendMsg(String info){
        List<AlarmUser> list= alarmUserService.listOpenUser();
        if(list!=null&&list.size()>0) {
            for (AlarmUser alarmUser : list) {
                templates.sendAlarmMessage(1,"提交订单到流量星有问题,快去看看",alarmUser.getOpenId(),info);
                String smsContent="【平台告警】提交订单到流量星异常，微信消息查看详情，快去看看！！！";
                SmsUtils.send(smsContent,alarmUser.getMobile(),smsUrl,smsUsername,smsPassword);
            }
        }
    }
}
