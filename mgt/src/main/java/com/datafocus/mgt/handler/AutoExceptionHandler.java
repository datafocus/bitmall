package com.datafocus.mgt.handler;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.exception.ApplicationException;
import com.datafocus.common.exception.ServiceException;
import com.datafocus.weixin.common.exception.WxRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author layne
 * @date 17-7-25
 */
@ControllerAdvice
public class AutoExceptionHandler {

    private Logger logger= LoggerFactory.getLogger(AutoExceptionHandler.class);

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public FrontResponse exceptionHandler(Throwable throwable) {
        throwable.printStackTrace();
        logger.error(FrontResponseCode.SYSTEM_ERROR.getCode()+"--"+throwable.getMessage(),throwable);
        return FrontResponse.response().setCode(FrontResponseCode.SYSTEM_ERROR.getCode()).setMessage(throwable.getMessage());
    }

    @ExceptionHandler(ApplicationException.class)
    @ResponseBody
    public FrontResponse exceptionHandler(ApplicationException exception) {
        logger.error(exception.getCode()+"--"+exception.getMessage());
        return FrontResponse.response().setCode(exception.getCode()).setMessage(exception.getMessage());
    }

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public FrontResponse exceptionHandler(ServiceException exception) {
        logger.info(exception.getCode()+"--"+exception.getMessage());
        return FrontResponse.response().setCode(exception.getCode()).setMessage(exception.getMessage());
    }

    @ExceptionHandler(WxRuntimeException.class)
    @ResponseBody
    public FrontResponse exceptionHandler(WxRuntimeException exception) {
        logger.error(exception.getCode()+"--"+exception.getMessage(),exception);
        return FrontResponse.response().setCode(exception.getCode()).setMessage(exception.getMessage());
    }
}
