package com.datafocus.mgt.shiro.config;

/**
 * Created by layne on 17-6-19.
 */

import com.datafocus.mgt.shiro.chain.Section;
import com.datafocus.mgt.shiro.credentials.RetryLimitHashedCredentialsMatcher;
import com.datafocus.mgt.shiro.filter.KickoutSessionControlFilter;
import com.datafocus.mgt.shiro.filter.UhomeRolesAuthorizationFilter;
import com.datafocus.mgt.shiro.realm.MyShiroRealm;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.session.mgt.quartz.QuartzSessionValidationScheduler;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.session.mgt.WebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.Map;


@Configuration
public class ShiroConfiguration {
    /**
     * 用户授权/认证信息Cache, 采用EhCache 缓存
     *
     * @return
     */
    @Bean
    public EhCacheManager ehCacheManager() {
        EhCacheManager em = new EhCacheManager();
        em.setCacheManagerConfigFile("classpath:conf/ehcache-shiro.xml");
        return em;
    }

    @Bean(name = "lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor getLifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public DefaultAdvisorAutoProxyCreator getDefaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator daap = new DefaultAdvisorAutoProxyCreator();
        daap.setProxyTargetClass(true);
        return daap;
    }

    @Bean(name = "securityManager")
    public DefaultWebSecurityManager defaultWebSecurityManager(MyShiroRealm myShiroRealm,
                                                               CacheManager cacheManager,
                                                               WebSessionManager sessionManager) {
        DefaultWebSecurityManager dwsm = new DefaultWebSecurityManager();
        //添加授权信息
        dwsm.setRealm(myShiroRealm);
        dwsm.setSessionManager(sessionManager);
        dwsm.setCacheManager(cacheManager);
        return dwsm;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor aasa = new AuthorizationAttributeSourceAdvisor();
        aasa.setSecurityManager(securityManager);
        return aasa;
    }

    @Bean
    public FilterRegistrationBean delegatingFilterProxy() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        DelegatingFilterProxy proxy = new DelegatingFilterProxy();
        proxy.setTargetFilterLifecycle(true);
        proxy.setTargetBeanName("shiroFilter");
        filterRegistrationBean.setFilter(proxy);
        return filterRegistrationBean;
    }

    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean shiroFilterFactoryBean(org.apache.shiro.mgt.SecurityManager securityManager,
                                                         KickoutSessionControlFilter kickoutSessionControlFilter,
                                                         Section section) {
        ShiroFilterFactoryBean filterFactoryBean = new ShiroFilterFactoryBean();
        filterFactoryBean.setSecurityManager(securityManager);
        // 没有登录跳转
        filterFactoryBean.setLoginUrl("/405");
        // 权限不够页面
        filterFactoryBean.setUnauthorizedUrl("/403");
        Map<String, Filter> filters = new HashMap<>();
        filters.put("uhomeRoles", new UhomeRolesAuthorizationFilter());
        filters.put("kickout", kickoutSessionControlFilter);
        filterFactoryBean.setFilters(filters);
        //加载权限控制规则
        filterFactoryBean.setFilterChainDefinitionMap(section.getObject());
        return filterFactoryBean;
    }


    /**
     * 授权信息 可以配置多个
     *
     * @param cacheManager
     * @return
     */
    @Bean(name = "myShiroRealm")
    public MyShiroRealm myShiroRealm(EhCacheManager cacheManager, RetryLimitHashedCredentialsMatcher retryLimitHashedCredentialsMatcher) {
        MyShiroRealm realm = new MyShiroRealm();
        realm.setCredentialsMatcher(retryLimitHashedCredentialsMatcher);
        realm.setCacheManager(cacheManager);
        return realm;
    }

    /**
     * hashAlgorithmName必须的，没有默认值。可以有MD5或者SHA-1，如果对密码安全有更高要求可以用SHA-256或者更高。
     * 这里使用MD5 storedCredentialsHexEncoded默认是true，此时用的是密码加密用的是Hex编码；false时用Base64编码
     * hashIterations迭代次数，默认值是1。
     *
     * @param ehCacheManager
     * @return
     */
    @Bean
    public RetryLimitHashedCredentialsMatcher retryLimitHashedCredentialsMatcher(EhCacheManager ehCacheManager) {
        RetryLimitHashedCredentialsMatcher matcher = new RetryLimitHashedCredentialsMatcher(ehCacheManager);

        matcher.setHashAlgorithmName("md5");
        matcher.setHashIterations(2);
        matcher.setStoredCredentialsHexEncoded(true);
        return matcher;
    }

    /**
     * 会话DAO
     *
     * @return
     */
    @Bean
    public SessionDAO sessionDAO() {
        EnterpriseCacheSessionDAO sessionDAO = new EnterpriseCacheSessionDAO();
        sessionDAO.setActiveSessionsCacheName("shiro-activeSessionCache");
        sessionDAO.setSessionIdGenerator(new JavaUuidSessionIdGenerator());
        return sessionDAO;
    }

    /**
     * 会话调度验证器
     * 全局的会话信息检测扫描信息间隔30分钟
     *
     * @return
     */
    @Bean
    public QuartzSessionValidationScheduler sessionValidationScheduler() {
        QuartzSessionValidationScheduler sessionValidationScheduler = new QuartzSessionValidationScheduler();
        sessionValidationScheduler.setSessionValidationInterval(1440000);
        return sessionValidationScheduler;
    }

    /**
     * 会话管理器
     * 全局的会话信息设置成12小时,sessionValidationSchedulerEnabled参数就是是否开启扫描
     *
     * @param sessionValidationScheduler
     * @param sessionDAO
     * @return
     */
    @Bean
    public WebSessionManager webSessionManager(QuartzSessionValidationScheduler sessionValidationScheduler, SessionDAO sessionDAO, SimpleCookie cookie) {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setGlobalSessionTimeout(43200000);
        sessionManager.setDeleteInvalidSessions(true);
        sessionManager.setSessionValidationSchedulerEnabled(true);
        sessionValidationScheduler.setSessionManager(sessionManager);
        sessionManager.setSessionValidationScheduler(sessionValidationScheduler);
        sessionManager.setSessionDAO(sessionDAO);
        sessionManager.setSessionIdCookie(cookie);
        return sessionManager;
    }

    @Bean
    public SimpleCookie simpleCookie() {
        SimpleCookie cookie = new SimpleCookie("BITSESSIONID");
        return cookie;
    }

    @Bean
    public KickoutSessionControlFilter kickoutSessionControlFilter(CacheManager cacheManager, WebSessionManager sessionManager) {
        KickoutSessionControlFilter sessionControlFilter = new KickoutSessionControlFilter();
        sessionControlFilter.setCacheManager(cacheManager);
        sessionControlFilter.setSessionManager(sessionManager);
        //踢之前登录的用户
        sessionControlFilter.setKickoutAfter(false);
        sessionControlFilter.setMaxSession(1);
        sessionControlFilter.setKickoutUrl("");
        return sessionControlFilter;
    }
}