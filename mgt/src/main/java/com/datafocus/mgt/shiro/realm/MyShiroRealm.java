package com.datafocus.mgt.shiro.realm;


import com.datafocus.common.pojo.Account;
import com.datafocus.common.pojo.Role;
import com.datafocus.service.mapper.AccountMapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by layne on 17-6-19.
 */

public class MyShiroRealm extends AuthorizingRealm {

    private static final Logger logger = LoggerFactory.getLogger(MyShiroRealm.class);

    @Autowired
    private AccountMapper accountMapper;

    /**
     * 权限认证，为当前登录的Subject授予角色和权限
     * <p>
     * 经测试：本例中该方法的调用时机为需授权资源被访问时
     * 经测试：并且每次访问需授权资源时都会执行该方法中的逻辑，这表明本例中默认并未启用AuthorizationCache
     * 经测试：如果连续访问同一个URL（比如刷新），该方法不会被重复调用，Shiro有一个时间间隔（也就是cache时间，在ehcache-shiro.xml中配置），超过这个时间间隔再刷新页面，该方法会被执行
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        logger.info("##################执行Shiro权限认证##################");
        String loginName = SecurityUtils.getSubject().getPrincipal().toString();

        if (loginName != null) {

            Account account = accountMapper.findAccountRole(loginName);

            //权限信息对象info,用来存放查出的用户的所有的角色（role）及权限（permission）
            SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

            List<Role> roles = account.getRoles();

            List<String> lists = roles.stream().map(Role::getId).map(String::valueOf).collect(Collectors.toList());

            info.addStringPermissions(lists);
            return info;
        }
        return null;
    }

    /**
     * 登录认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken authenticationToken) throws AuthenticationException {
        String accountName = (String) authenticationToken.getPrincipal();

        Account account = accountMapper.findAccountByAccountName(accountName);

        if (account == null) {
            throw new UnknownAccountException();// 没找到帐号
        }
        if ("2".equals(account.getLocked())) {
            throw new LockedAccountException(); // 帐号锁定
        }

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo
                (account.getAccountName(),
                        account.getPassword(), getName());
        //盐是用户名+随机数
        authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes(accountName + account.getCredentialsSalt()));

        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("userSession", account);
        session.setAttribute("userSessionId", account.getAccountId());
        clearCache(authenticationInfo.getPrincipals());
        return authenticationInfo;
    }
}
