package com.datafocus.mgt.shiro.chain;

import com.datafocus.common.config.MapProperties;
import com.datafocus.common.pojo.Resources;
import com.datafocus.common.pojo.Role;
import com.datafocus.service.mapper.ResourcesMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 安全过滤链管理器
 *
 * @author layne
 */
@Component
public class ShiroFilerChainManager {

    private static final Log log = LogFactory.getLog(ShiroFilerChainManager.class);
    @Autowired
    private ResourcesMapper resourcesMapper;
    @Autowired
    private MapProperties mapProperties;

    @Autowired
    @Qualifier("shiroFilter")
    private ShiroFilterFactoryBean shiroFilterFactoryBean;

    public void updatePermission() {
        synchronized (shiroFilterFactoryBean) {

            AbstractShiroFilter shiroFilter = null;
            try {
                shiroFilter = (AbstractShiroFilter) shiroFilterFactoryBean.getObject();
            } catch (Exception e) {
                log.error("更新url资源权限获取shiroFilter 系统异常", e);
            }
            // 获取过滤管理器
            PathMatchingFilterChainResolver filterChainResolver = (PathMatchingFilterChainResolver) shiroFilter
                    .getFilterChainResolver();
            DefaultFilterChainManager manager = (DefaultFilterChainManager) filterChainResolver.getFilterChainManager();

            // 清空初始权限配置
            manager.getFilterChains().clear();

            shiroFilterFactoryBean.getFilterChainDefinitionMap().clear();

            List<Resources> resourcess = resourcesMapper.findAllResRoles(null);

            for (Resources resources : resourcess) {
                List<Role> roles = resources.getRoles();

                List<String> lists = new ArrayList<>();
                for (Role role : roles) {
                    lists.add(String.valueOf(role.getId()));
                }
                manager.createChain(resources.getResUrl(), Role.getRoles(lists));
            }
            Map<String, String> filterChainDefinitions = mapProperties.getFilterChainDefinitions();
            filterChainDefinitions.forEach((k, v) -> manager.createChain(k, v));
        }
    }


}
