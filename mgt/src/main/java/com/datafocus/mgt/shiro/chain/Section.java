package com.datafocus.mgt.shiro.chain;


import com.datafocus.common.config.MapProperties;
import com.datafocus.common.pojo.Resources;
import com.datafocus.common.pojo.Role;
import com.datafocus.service.service.ResourcesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Section extends HashMap<String, String> implements Serializable {
    private static final long serialVersionUID = -5785092219233505243L;
    @Autowired
    private MapProperties mapProperties;
    @Autowired
    private ResourcesService resourcesService;

    public Section getObject() {

        putAll(mapProperties.getFilterChainDefinitions());

        List<Resources> resourcess = resourcesService.findAllResRoles(null);
        resourcess.forEach(res -> {
            List<Role> roles = res.getRoles();
            List<String> lists = roles.stream().map(Role::getId).map(String::valueOf).collect(Collectors.toList());
            if (!StringUtils.isEmpty(res.getResAddress()))
                put(res.getResAddress(), Role.getRoles(lists));
        });
        return this;
    }

}
