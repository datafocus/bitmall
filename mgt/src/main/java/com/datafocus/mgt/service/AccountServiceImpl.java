package com.datafocus.mgt.service;


import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.exception.ServiceException;
import com.datafocus.common.pojo.Account;
import com.datafocus.common.pojo.AccountRole;
import com.datafocus.mgt.service.impl.AccountService;
import com.datafocus.mgt.shiro.chain.ShiroFilerChainManager;
import com.datafocus.service.mapper.AccountMapper;
import com.datafocus.service.mapper.AccountRoleMapper;
import com.datafocus.service.utils.PasswordHelper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private AccountRoleMapper accountRoleMapper;

    @Autowired
    private ShiroFilerChainManager shiroFilerChainManager;

    @Override
    public void saveAccount(Account account) {
        if (StringUtils.isEmpty(account.getPassword()) || StringUtils.isEmpty(account.getAccountName()))
            throw new ServiceException(FrontResponseCode.NULL_PARAM.getCode(), "帐号或者密码不能为空");
        PasswordHelper.encryptPassword(account);
        account.setLocked("0");
        account.setDeletestatus(0);
        UUID uuid = UUID.randomUUID();
        account.setAccountId(uuid.toString().replace("-", ""));
        accountMapper.saveAccount(account);
    }

    @Override
    public Page findAccountGetLimit(Account account, FlowPage flowPage) {
        PageHelper.startPage(flowPage.getPage(), flowPage.getSize(), " createTime desc");
        return accountMapper.findAccountGetLimit(account);
    }

    @Override
    @Transactional
    public void deleteAccount(Account account) {
        Objects.requireNonNull(account, "账户信息不能为空");
        if (StringUtils.isEmpty(account.getAccountId()))
            throw new ServiceException(FrontResponseCode.NULL_PARAM.getCode(), "账户ID为空");
        accountMapper.deleteAccount(account.getAccountId());
        accountRoleMapper.deleteAccountRole(account.getAccountId());
        shiroFilerChainManager.updatePermission();
    }

    @Override
    @Transactional
    public void saveAccoutRole(List<AccountRole> accountRoles) {
        if (!accountRoles.isEmpty() && accountRoles.size() > 0) {
            //删除用户和角色的关联关系
            accountMapper.deleteAccountRole(accountRoles.get(0).getAccountId());
            //保存用户和角色的关联关系
            accountMapper.saveAccoutRole(accountRoles);
            shiroFilerChainManager.updatePermission();
        }
    }

    @Override
    public Account findAccountRole(String accountName) {
        Objects.requireNonNull(accountName, "账户名字不能为空");
        return accountMapper.findAccountRole(accountName);
    }

    @Override
    public void updateAccountPassword(Account account) {
        if (StringUtils.isEmpty(account.getPassword()))
            throw new ServiceException(FrontResponseCode.NULL_PARAM.getCode(), "参数不能为空");

        Session session = SecurityUtils.getSubject().getSession();

        Account currentAccount = (Account) session.getAttribute("userSession");
        Objects.requireNonNull(currentAccount, "需要登录");

        account.setAccountName(currentAccount.getAccountName());
        account.setAccountId(currentAccount.getAccountId());

        PasswordHelper.encryptPassword(account);
        accountMapper.updateAccountPassword(account);
    }

    @Override
    public boolean accouontIsExistByAccountName(String accountName) {
        Account account = accountMapper.findAccountByAccountName(accountName);
        return account != null && StringUtils.isNotBlank(account.getAccountId());
    }

    @Override
    public List<Account> findAccount(Account account) {
        return accountMapper.findAccount(account);
    }
}
