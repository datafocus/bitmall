package com.datafocus.mgt.service.impl;

import com.datafocus.common.dto.TemplateSendReq;

public interface ActiveTemplateService {

    void sendMarket(TemplateSendReq sendReq);
}
