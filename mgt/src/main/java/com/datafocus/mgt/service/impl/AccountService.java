package com.datafocus.mgt.service.impl;


import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.Account;
import com.datafocus.common.pojo.AccountRole;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface AccountService {
    /**
     * @param account
     * @throws Exception
     */
    void saveAccount(Account account);

    Page<Map> findAccountGetLimit(@Param("account") Account account, FlowPage flowPage);

    /**
     * 删除账户
     *
     * @param account
     */
    void deleteAccount(Account account);

    /**
     * 添加角色
     *
     * @param accountRoles
     */
    void saveAccoutRole(List<AccountRole> accountRoles);

    /**
     * 根据账户名称查找
     *
     * @param accountName
     * @return
     */
    Account findAccountRole(String accountName);

    /**
     * 更新密码
     *
     * @param account
     */
    void updateAccountPassword(Account account);

    /**
     * 账户是否存在
     * @param accountName
     * @return
     */
    boolean accouontIsExistByAccountName(String accountName);

    /**
     *
     * @param account
     * @return
     */
    List<Account> findAccount(Account account);
}
