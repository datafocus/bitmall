package com.datafocus.mgt.service.impl;

import com.datafocus.common.config.MgtProperties;
import com.datafocus.common.dto.TemplateSendReq;
import com.datafocus.service.service.TagsService;
import com.datafocus.service.service.UserInfoService;
import com.datafocus.weixin.common.exception.WxRuntimeException;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.template.Data;
import com.datafocus.weixin.mp.template.Templates;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ActiveTemplateServiceImpl implements ActiveTemplateService {
    private Logger logger = LoggerFactory.getLogger(ActiveTemplateServiceImpl.class);
    @Autowired
    private TagsService tagsService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private MgtProperties mgtProperties;
    @Autowired
    private AppSetting mpSetting;
    private Templates templates;
    private ExecutorService marketThreadPool;

    @PostConstruct
    public void init(){
        templates= Templates.with(mpSetting);
        marketThreadPool= Executors.newFixedThreadPool(mgtProperties.getMarketThreadNum());
    }


    @Override
    public void sendMarket(TemplateSendReq sendReq) {
        Map<String, Data> data = this.buildMarket(sendReq);
        String templateId=mgtProperties.getMarketTemplate().get(sendReq.getTemplateType());
        if(StringUtils.isNotBlank(templateId)){
            List<String> openIdList=null;
            if(sendReq.getTagId()==null){
                openIdList= userInfoService.getAllOpenId();
            }else{
                openIdList= tagsService.getOpenIdList(sendReq.getTagId());
            }
            if(openIdList!=null&&openIdList.size()>0){
                logger.info("msg=开始发送，title=营销模板消息，num={},templateId={}",openIdList.size(),templateId);
                for(String openId:openIdList){
                    marketThreadPool.execute(new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                templates.send(openId, templateId,sendReq.getUrl(),data);
                                logger.info("msg=发送成功，title=营销模板消息，openid={},templateId={}",openId,templateId);
                            }catch (WxRuntimeException e){
                                logger.info("msg=发送失败，title=营销模板消息，openid={},templateId={}",openId,templateId);
                                logger.error("营销模板消息异常",e);
                            }
                        }
                    }));
                }
            }
        }
    }

    private Map<String,Data> buildMarket(TemplateSendReq sendReq){
        Map<String, Data> messages = new HashMap<>();
        messages.put("first", new Data(sendReq.getFirst(), "#173177"));
        messages.put("keyword1", new Data(sendReq.getKeyword1(), "#173177"));
        messages.put("keyword2",new Data(sendReq.getKeyword2(), "#173177"));
        if (StringUtils.isNotBlank(sendReq.getKeyword3())) {
            messages.put("keyword3", new Data(sendReq.getKeyword3(), "#173177"));
        }
        messages.put("remark", new Data(sendReq.getRemark(), sendReq.getRemarkColor()));
        return messages;
    }

}
