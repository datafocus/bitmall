package com.datafocus.mgt.task;

import com.datafocus.service.mapper.CardDataCubeMapper;
import com.datafocus.weixin.mp.card.Cards;
import com.datafocus.weixin.mp.card.bean.CardDataCube;
import com.datafocus.weixin.mp.card.bean.CardDataCube.CardInfoDataCube;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * 卡券数据统计数据拉取任务
 * @author yyhinfo
 */
@Component("cardDataCubeTask")
public class CardDataCubeTask {
	
	@Autowired
	private CardDataCubeMapper cardDataCubeMapper;
	@Autowired
	private Cards cards;
	private static final Logger logger = LoggerFactory.getLogger(CardDataCubeTask.class);
	/**
	 * 调用该接口拉取本商户的总体数据情况，包括时间区间内的各指标总量。
	 * 
	 * 每天早上8：5分启动
	 */
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
	//获取前一天的卡券概况数据
	
    @Scheduled(cron = "0 5 8 ? * *")
	public void cardBizuinInfo(){
      	try {
	    	final Date date =  DateUtils.addDays(new Date(), -1);
	    	List<CardDataCube> cardDataCubes = cards.getCardBizuinInfo(SDF.format(date), SDF.format(date), 1);
			cardDataCubeMapper.saveCardDataCubes(cardDataCubes);
			logger.info("-------------------卡券概况统计数据拉取完毕--------------");
		} catch (Exception e) {
			logger.error("拉取卡券概况数据统计失败、异常",e);
		}
	}
    
    @Scheduled(cron = "0 5 8 ? * *")
    public void getCardCardInfo(){
    	try {
    		final Date date =  DateUtils.addDays(new Date(), -1);
    		List<CardInfoDataCube> cardInfoDataCubes = cards.getCardCardInfo("", SDF.format(date), SDF.format(date), 1, false);
    		cardDataCubeMapper.saveCardInfoDataCubes(cardInfoDataCubes);
    		logger.info("-------------------卡券数据统计详细数据拉取完毕----------");
		} catch (Exception e) {
			logger.error("拉取免费券（优惠券、团购券、折扣券、礼品券）在固定时间区间内的相关数据失败、异常",e);
		}
    	
    }
}
