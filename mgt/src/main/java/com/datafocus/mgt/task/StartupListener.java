package com.datafocus.mgt.task;

import com.datafocus.service.mapper.BillOrderMapper;
import com.datafocus.service.service.AlarmConfigService;
import com.datafocus.service.task.*;
import com.datafocus.weixin.mp.message.MpMessages;
import com.datafocus.weixin.mp.user.Tags;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class StartupListener implements ApplicationListener<ApplicationPreparedEvent> {

    private static Logger logger = LoggerFactory.getLogger(StartupListener.class);

    private ExecutorService executorService = Executors.newCachedThreadPool();

    @Autowired
    private UnTagDelayedService UnTagsDelayedService;

    @Autowired
    private RefundOrderDelayService refundDelayService;

    @Autowired
    private OnDelayedListenerImpl onDelayedListenerImpl;

    @Autowired
    private BillOrderMapper billOrderMapper;
    @Autowired
    private Tags tags;
    @Autowired
    private MpMessages mpMessages;
    @Autowired
    private AlarmConfigService alarmConfigService;

    @Override
    public void onApplicationEvent(ApplicationPreparedEvent event) {

        logger.info(">>>>>>>>>系统启动完成<<<<<<<<<<<");

        if (event.getApplicationContext().getParent() == null) {
            return;
        }

        UnTagsDelayedService.start(new UnTagDelayedService.OnDelayedListener() {

            @Override
            public void onDelayedArrived(UnTagDelayed unTagDelayed) {
                int tagId = unTagDelayed.getTagId();
                long msgId = unTagDelayed.getMsgId();
                //检查群发消息是否已发送成功
                boolean status = mpMessages.success(msgId);
                if (status) {
                    tags.delete(tagId);
                } else {
                    unTagDelayed.setExcuteTime(20);
                    UnTagsDelayedService.add(unTagDelayed);
                }

            }

        });
        //启动退款查询订单处理
        refundDelayService.start(onDelayedListenerImpl);
        //查找需要入队的订单
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                logger.info("查找需要入队的退款中的订单");
                List<String> orders = billOrderMapper.findOrderByRefund();
                if (orders != null && !orders.isEmpty()) {
                    logger.info("需要入队的退款订单数:{}条", orders.size());
                    for (String orderNo : orders) {
                        int startTime = RefundOrderDelayed.startTimeMap.get(1);//第一次查询退款状态
                        RefundOrderDelayed refundOrder = new RefundOrderDelayed(2,orderNo, startTime);
                        refundDelayService.add(refundOrder);
                    }
                } else {
                    logger.info("没有需要入队查询退款状态的订单");
                }
            }
        });
        alarmConfigService.init();
    }
}
