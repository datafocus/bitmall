package com.datafocus.mgt.task;

import com.datafocus.service.service.PromoterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component("analysisTask")
public class AnalysisTask {
    private static final Logger logger = LoggerFactory.getLogger(AnalysisTask.class);

    @Autowired
    private PromoterService promoterService;

    @Scheduled(fixedRate = 2 * 60 * 60 * 1000)
    public void analysis() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        CountDownLatch countDownLatch = new CountDownLatch(2);
        executorService.execute(() -> {
            try {
                promoterService.allAnalysis();
            } finally {
                countDownLatch.countDown();
            }
        });
        executorService.execute(() -> {
            try {
                promoterService.yearlyAnalysis();
            } finally {
                countDownLatch.countDown();
            }
        });
        countDownLatch.await();
        executorService.shutdown();
    }

}
