package com.datafocus.mgt.task;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.datafocus.common.config.MgtProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datafocus.weixin.common.util.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderRechargeClient {

    private static final Logger logger = LoggerFactory.getLogger(OrderRechargeClient.class);

    @Autowired
    private MgtProperties mgtProperties;

    public String recharge(String mobile, String flow, String orderNo) {

        StringBuffer sb = new StringBuffer();

        try {
            sb.append("name=").append(URLEncoder.encode(mgtProperties.getUsername(), "utf-8"));
            sb.append("&password=").append(URLEncoder.encode(mgtProperties.getPassword(), "utf-8"));
            sb.append("&callbackURL=").append(mgtProperties.getCallbackUrl());
            sb.append("&version=").append("1.6");
            sb.append("&type=").append("STAND_PACKAGE");
            sb.append("&body=<body><message><mobile>").append(mobile);
            sb.append("</mobile><flow>").append(flow).append("</flow>");
            sb.append("<order>").append("<orderid>").append(orderNo).append("</orderid>").append("<mobile>").append(mobile).append("</mobile>").append("</order>");
            sb.append("</message></body>");
        } catch (UnsupportedEncodingException e) {
            logger.error("流量充值请求构建报文编码异常:", e);
            return "";
        }
        return submit(mgtProperties.getRechargeUrl(), sb.toString());
    }

    public String recharge1_7(String mobile, String flow, String orderNo, BigDecimal orderPrice) {
        StringBuffer sb = new StringBuffer();
        try {
            String timestamp = getDateToString();
            String userName = mgtProperties.getUsername();
            String password = mgtProperties.getPassword();
            String sign = sign(userName, password, timestamp);
            sb.append("name=").append(userName);
            sb.append("&timestamp=").append(timestamp);
            sb.append("&version=").append("1.7");
            sb.append("&sign=").append(sign);
            sb.append("&callbackURL=").append(mgtProperties.getCallbackUrl());
            sb.append("&body=<body><message><flow>").append(flow).append("</flow>");
            sb.append("<type>").append("STAND_PACKAGE").append("</type>")
                    .append("<salePrice>").append(orderPrice).append("</salePrice>");
            sb.append("<order>").append("<orderid>").append(orderNo).append("</orderid>").append("<mobile>").append(mobile).append("</mobile>").append("</order>");
            sb.append("</message></body>");
        } catch (Exception e) {
            logger.error("流量充值请求构建报文编码异常:", e);
            return "";
        }
        return submit(mgtProperties.getRechargeUrl(), sb.toString());
    }

    public String rechargeThanks(String mobile, String flow, String orderNo, BigDecimal orderPrice) {
        StringBuffer sb = new StringBuffer();
        try {
            String timestamp = getDateToString();
            String userName = mgtProperties.getUsername();
            String password = mgtProperties.getPassword();
            String sign = sign(userName, password, timestamp);
            sb.append("name=").append(userName);
            sb.append("&timestamp=").append(timestamp);
            sb.append("&version=").append("1.7");
            sb.append("&sign=").append(sign);
            sb.append("&callbackURL=").append(mgtProperties.getCallbackUrl());
            sb.append("&body=<body><message><flow>").append(flow).append("</flow>")
                    .append("<salePrice>").append(orderPrice).append("</salePrice>");
            sb.append("<type>").append("STAND_PACKAGE").append("</type>");
            sb.append("<extendStr>activity1121</extendStr>");
            sb.append("<order>").append("<orderid>").append(orderNo).append("</orderid>").append("<mobile>").append(mobile).append("</mobile>").append("</order>");
            sb.append("</message></body>");
        } catch (Exception e) {
            logger.error("流量充值请求构建报文编码异常:", e);
            return "";
        }
        return submit(mgtProperties.getRechargeUrl(), sb.toString());
    }

    /**
     * 如果使用了卡券，则走这个逻辑，提交给流量星的报文不同
     *
     * @param mobile
     * @param flow
     * @param orderNo
     * @param orderPrice
     * @param desc
     * @return
     */
    public String recharge1_7(String mobile, String flow, String orderNo, BigDecimal orderPrice, String desc) {
        StringBuffer sb = new StringBuffer();
        try {
            String timestamp = getDateToString();
            String userName = mgtProperties.getUsername();
            String password = mgtProperties.getPassword();
            String sign = sign(userName, password, timestamp);
            sb.append("name=").append(userName);
            sb.append("&timestamp=").append(timestamp);
            sb.append("&version=").append("1.7");
            sb.append("&sign=").append(sign);
            sb.append("&callbackURL=").append(mgtProperties.getCallbackUrl());
            sb.append("&body=<body><message><flow>").append(flow).append("</flow>");
            sb.append("<type>").append("STAND_PACKAGE").append("</type>");
            sb.append("<order>")
                    .append("<orderid>").append(orderNo).append("</orderid>").append("<mobile>").append(mobile).append("</mobile>")
                    .append("<discountPrice>").append(orderPrice).append("</discountPrice>")
                    .append("<salePrice>").append(orderPrice).append("</salePrice>")
                    .append("<discountInfo>").append(desc).append("</discountInfo>")
                    .append("</order>");
            sb.append("</message></body>");
        } catch (Exception e) {
            logger.error("流量充值请求构建报文编码异常:", e);
            return "";
        }
        return submit(mgtProperties.getRechargeUrl(), sb.toString());
    }

    private String sign(String userName, String password, String timestamp) {
        StringBuffer sb = new StringBuffer();
        sb.append(password).append(timestamp).append(userName).append(timestamp).append(password);
        return MD5.MD5Encode(sb.toString()).toUpperCase();
    }

    private String getDateToString() {
        Date dt = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        return df.format(dt);
    }

    public String submit(String action, String body) {
        String SubmitResult = "";
        HttpClient httpClient = new HttpClient();
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(30000);
        httpClient.getHttpConnectionManager().getParams().setSoTimeout(60000);
        PostMethod method = new PostMethod(action);
        httpClient.getParams().setContentCharset("UTF-8");
        method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        method.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
        try {
            RequestEntity requestEntity = new StringRequestEntity(body, "application/x-www-form-urlencoded", "UTF-8");
            method.setRequestEntity(requestEntity);
            httpClient.executeMethod(method);
            SubmitResult = method.getResponseBodyAsString();
        } catch (Exception e) {
            logger.error("发送POST请求系统异常", e);
            return SubmitResult;
        }
        return SubmitResult;
    }

    public String queryStatus(String msgid) {

        StringBuffer sb = new StringBuffer();
        sb.append("name=").append(mgtProperties.getUsername());
        try {
            sb.append("&password=").append(URLEncoder.encode(mgtProperties.getPassword(), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sb.append("&msgid=").append(msgid);

        return submit(mgtProperties.getQueryRechargeStatusUrl(), sb.toString());

    }
}
