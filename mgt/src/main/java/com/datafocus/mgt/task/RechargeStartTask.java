package com.datafocus.mgt.task;

import com.datafocus.common.config.MgtProperties;
import com.datafocus.mgt.handler.AlarmHandler;
import com.datafocus.service.service.FlowRechargeService;
import com.datafocus.service.service.OrderService;
import com.datafocus.service.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Component("task")
public class RechargeStartTask {
	public static ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()	* 10);
	
    @Autowired
	private MgtProperties mgtProperties;

	@Autowired
	private FlowRechargeService flowRechargeService;

	@Autowired
	private OrderRechargeClient orderRechargeClient;

	@Autowired
	private AlarmHandler alarmHandler;

	@Autowired
	private OrderService orderService;
	@Resource
	private RedisUtil<String,Integer> redisUtil;
    /**
     * 定时任务充值
     */
    @Scheduled(cron = "0/10 * * * * ?")
	public void rechargeRun(){
		if(mgtProperties.isImagesPurgeJobEnable()){
			OperateThread operateThread = new OperateThread(flowRechargeService,orderRechargeClient,alarmHandler,orderService,mgtProperties.getRunTime(),redisUtil);
			operateThread.start();
		}
	}
	
}
