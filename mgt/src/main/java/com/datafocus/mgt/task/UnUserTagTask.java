package com.datafocus.mgt.task;

import java.util.List;

import com.datafocus.common.pojo.CustomTagUser;
import com.datafocus.service.service.CustomTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 解除用户标签任务
 * @author yyhinfo
 *
 */
@Component("unUserTagTask")
public class UnUserTagTask {

	@Autowired
	private CustomTagService customTagService;
	/**
	 * 解除新增用户标签
	 */
	@Scheduled(cron = "0 0 2 * * *")
	public void unNewUserTag(){
		final String  tagName = "新增用户";
		List<CustomTagUser> customTagUsers = customTagService.customTagUsersByTagName(tagName);
		for (CustomTagUser customTagUser : customTagUsers) {
			customTagService.deleteCustomTagUserByOpenIdAndTagId(customTagUser.getOpenId(), customTagUser.getTagId());
		}
	}
}
