package com.datafocus.mgt.task;

import com.datafocus.common.dto.FlowOrderDto;
import com.datafocus.mgt.handler.AlarmHandler;
import com.datafocus.service.service.FlowRechargeService;
import com.datafocus.service.service.OrderService;
import com.datafocus.service.utils.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class OperateThread extends Thread {

	private static Logger logger = LoggerFactory.getLogger(OperateThread.class);

	private  long runTime ;

	private FlowRechargeService flowRechargeService;

	private OrderRechargeClient rechargeClient;

	private AlarmHandler alarmHandler;

	private OrderService orderService;

	private RedisUtil<String,Integer> redisUtil;

	public OperateThread(FlowRechargeService flowRechargeService, OrderRechargeClient rechargeClient, AlarmHandler alarmHandler, OrderService orderService, long runTime, RedisUtil<String,Integer> redisUtil) {
		this.flowRechargeService = flowRechargeService;
		this.rechargeClient=rechargeClient;
		this.alarmHandler=alarmHandler;
		this.orderService=orderService;
		this.redisUtil=redisUtil;
	}
	@Override
	public void run() {
		try {
			//充值
			OperateThread.operate(flowRechargeService,rechargeClient,alarmHandler,orderService,redisUtil);
			Thread.sleep(runTime);
		} catch (Exception e) {
			logger.error("比特商城流量充值异常",e);
		}
	}

	private synchronized static void operate(FlowRechargeService flowRechargeService, OrderRechargeClient orderRechargeClient, AlarmHandler alarmHandler, OrderService orderService,RedisUtil<String,Integer> redisUtil) {
		logger.info("开始扫描订单表待充值记录... ");
		// 扫描订单表中代充值的手机号码
		List<FlowOrderDto> flowOrders = flowRechargeService.findRechargeOrderByOrderStatusAndSubmitStatus();

		logger.info("开始扫描充值记录 ，总共---------------------->" + flowOrders.size() + "条记录");

		for (FlowOrderDto flowOrderDto : flowOrders) {
			RechargeStartTask.executorService.execute(new OrderRechargeThread(flowOrderDto,flowRechargeService,orderRechargeClient,alarmHandler,orderService,redisUtil));
		}
	}
}
