package com.datafocus.mgt.task;

import com.datafocus.common.dto.FlowOrderDto;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.mgt.handler.AlarmHandler;
import com.datafocus.service.mapper.FlowOrderMapper;
import com.datafocus.service.service.CardService;
import com.datafocus.service.service.FlowRechargeService;
import com.datafocus.service.service.OrderService;
import com.datafocus.service.service.impl.OrderServiceImpl;
import com.datafocus.service.task.RefundOrderDelayService;
import com.datafocus.service.task.RefundOrderDelayed;
import com.datafocus.service.task.SmsSendThread;
import com.datafocus.service.utils.BaseResult;
import com.datafocus.service.utils.DFUtils;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.weixin.pay.base.PaySetting;
import com.datafocus.weixin.pay.mp.Orders;
import com.datafocus.weixin.pay.mp.bean.RefundRequest;
import com.datafocus.weixin.pay.mp.bean.RefundResponse;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OrderRechargeThread extends Thread {

    private static Logger logger = LoggerFactory.getLogger(OrderRechargeThread.class);
    /**
     * 模板消息推送线程池
     */
    public static ExecutorService smsPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 4);
    private static final Map<String, String> errorMsgMap = new HashMap<String, String>();
    private FlowRechargeService flowRechargeService;

    private FlowOrderDto flowOrderDto;

    private OrderRechargeClient orderRechargeClient;

    private AlarmHandler alarmHandler;

    private OrderService orderService;

    private RedisUtil<String,Integer> redisUtil;

    static {
        errorMsgMap.put("0", "成功");
        errorMsgMap.put("1", "充值失败");
        errorMsgMap.put("-1", "用户名或密码错误");
        errorMsgMap.put("-2", "参数错误");
        errorMsgMap.put("-3", "充值体格式错误");
        errorMsgMap.put("-4", "IP不对");
        errorMsgMap.put("-5", "系统异常");
        errorMsgMap.put("-6", "已超过本月最大充值额度");
        errorMsgMap.put("-7", "额度不足");
        errorMsgMap.put("-8", "用户被锁定");
        errorMsgMap.put("-9", "流量包错误");
        errorMsgMap.put("-10", "手机号码错误");
        errorMsgMap.put("-11", "参数签名错误");
        errorMsgMap.put("-12", "系统繁忙，请稍后再试");
    }

    public OrderRechargeThread(FlowOrderDto flowOrderDto, FlowRechargeService flowRechargeService, OrderRechargeClient orderRechargeClient, AlarmHandler alarmHandler,OrderService orderService,RedisUtil<String,Integer> redisUtil) {
        this.flowOrderDto = flowOrderDto;
        this.flowRechargeService = flowRechargeService;
        this.orderRechargeClient = orderRechargeClient;
        this.alarmHandler = alarmHandler;
        this.orderService=orderService;
        this.redisUtil=redisUtil;
    }

    @Override
    public void run() {
        try {
            send(this.flowOrderDto);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("比特商城流量充值提交系统异常--->", e);
        }
    }

    private void send(FlowOrderDto flowOrder) throws Exception {

        String submitStatus = "0"; //充值状态

        String gallerysubmitCode = ""; //充值错误信息

        String msgid = ""; //流量星平台订单id

        String orderId = "";
        String returnStr = "";

        //TODO  提交充值订单前，检查订单是否已提交过

        try {
            long beforeTime = System.currentTimeMillis();
            logger.info("比特星人流量充值提交订单开始请求，订单号:{},流量包:{},单价:{},充值手机号码:{}", flowOrder.getOrderNo(), flowOrder.getFlowPackage(), flowOrder.getOrderPrice(), flowOrder.getOrderPhone());
            if (StringUtils.isNotBlank(flowOrder.getCardId())) {//如果订单使用了卡券，则提交给流量星折后价格和描述
                //todo 双十一
                if("thanks".equals(flowOrder.getCardId())){
                    returnStr=orderRechargeClient.rechargeThanks(flowOrder.getOrderPhone(), flowOrder.getFlowPackage(), flowOrder.getOrderNo(),flowOrder.getOrderPrice());
                }else{
                    String desc = flowRechargeService.findCardDescByCardId(flowOrder.getCardId());
                    returnStr = orderRechargeClient.recharge1_7(flowOrder.getOrderPhone(), flowOrder.getFlowPackage(), flowOrder.getOrderNo(), flowOrder.getOrderPrice(), desc);
                }
            } else {
                returnStr = orderRechargeClient.recharge1_7(flowOrder.getOrderPhone(), flowOrder.getFlowPackage(), flowOrder.getOrderNo(),flowOrder.getOrderPrice());
            }
            long afterTime = System.currentTimeMillis();
            logger.info("比特星人流量充值提交订单请求结束 耗时:{} 毫秒,流量星返回报文:{}", afterTime - beforeTime, returnStr);

        } catch (Exception e) {
            logger.error("提交充值订单系统异常,比特星人订单号:{},充值手机号码:{},流量包型:{},堆栈信息:{}", flowOrder.getOrderNo(), flowOrder.getOrderPhone(), flowOrder.getFlowPackage(), e);
            submitStatus = "0"; //修改提交状态，重新提交
            flowRechargeService.modifyOrderSubmitStatusAndRechargeStatus(flowOrder.getOrderNo(), Integer.valueOf(submitStatus), null, null);
        }

        if (StringUtils.isBlank(returnStr)) {
            submitStatus = "0"; // 0未提交（如果充值请求没有提交成功，把提交状态重置，重新提交）
            gallerysubmitCode = "重新提交";
        } else {
            Document doc = DocumentHelper.parseText(returnStr);

            Element body = doc.getRootElement();

            submitStatus = body.elementText("code");

            if ("0".equals(submitStatus)) {

                Element result = body.element("result");

                msgid = result.elementText("msgid");

                orderId = result.elementText("orderid");
                gallerysubmitCode = translate(submitStatus);
                submitStatus = "1"; //1提交成功
                alarmHandler.submitAlarm(0);
                //发送微信模板消息
                smsPool.execute(new SmsSendThread(flowOrder.getOrderNo(), "4"));//4->业务受理模板消息
            } else {
                gallerysubmitCode = translate(submitStatus);
                if (StringUtils.isBlank(gallerysubmitCode)) {
                    gallerysubmitCode = "失败";
                }
                if ("1".equals(submitStatus) || "-8".equals(submitStatus) || "-10".equals(submitStatus)) {
                    logger.info("提交到流量星被拦截,原因："+gallerysubmitCode+",开始自动退款");
                    orderService.refund(flowOrder.getOrderNo());
                } else {
                    alarmHandler.submitAlarm(-1);
                }
                submitStatus = "2";//2代表失败
            }
        }
        logger.info("提交订单到流量星平台结果信息：订单手机号码:{},流量包：{},金额:{},流量星订单id:{},比特星人订单id:{},订单状态:{}", flowOrder.getOrderPhone(), flowOrder.getFlowPackage(), flowOrder.getOrderPrice(), msgid, orderId, returnStr);
        flowRechargeService.modifyOrderSubmitStatusAndRechargeStatus(flowOrder.getOrderNo(), Integer.valueOf(submitStatus), msgid, gallerysubmitCode);
    }


    public static String translate(String submitStatus) {
        // 0:下单成功，-1：接口参数为空，-2：用户不存在或已禁用，-3：密码不正确，-4：用户的余额不足，-5：产品不存在或已禁用，-6：内部错误，-7：商户IP限制，-8：交易流水号重复，-9：APIKEY不正确，-10：无可用通道，-11：阀值额度已满,-12:无该号码的充值权限
        return submitStatus == null ? "" : errorMsgMap.containsKey(submitStatus) ? errorMsgMap.get(submitStatus) : "";
    }

    public FlowOrderDto getFlowOrderDto() {
        return flowOrderDto;
    }

    public OrderRechargeThread setFlowOrderDto(FlowOrderDto flowOrderDto) {
        this.flowOrderDto = flowOrderDto;
        return this;
    }
}
