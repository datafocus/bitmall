package com.datafocus.mgt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.ActiveMessage;
import com.datafocus.common.pojo.Regular;
import com.datafocus.service.service.AutoReplyService;
import com.datafocus.service.service.CardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.datafocus.weixin.common.media.MediaType;
import com.datafocus.weixin.mp.media.Materials;
import com.datafocus.weixin.mp.media.bean.Counts;
import com.datafocus.weixin.mp.media.bean.MaterialPagination;
import com.datafocus.weixin.mp.media.bean.MpNewsPagination;

@Api(description = "微信自动回复")
@RestController
@RequestMapping("/autoReply")
public class AutoReplyController {

    private static final Log logger = LogFactory.getLog(AutoReplyController.class);

    @Autowired
    private CardService cardService;
    @Autowired
    private AutoReplyService autoReplyService;
    @Autowired
    private Materials materials;

    /**
     * 自动回复页面
     *
     * @return
     */
    @ApiOperation("自动回复页面数据")
    @GetMapping("/pageData")
    public FrontResponse pageData() {
        //获取素材总数
        Map<String,Object> data=new HashMap<>();
        Counts counts = materials.count();
        //素材
        MpNewsPagination mpnews = new MpNewsPagination();
        if (counts.getNews() > 0)
            mpnews = materials.listMpNews(0, counts.getNews());
        data.put("mpnews", mpnews);
        //图片
        MaterialPagination imgs = new MaterialPagination();
        if (counts.getImage() > 0)
            imgs = materials.list(MediaType.image, 0, counts.getImage());
        data.put("imgs", imgs);
        //语音
        MaterialPagination voices = new MaterialPagination();
        if (counts.getVoice() > 0)
            voices = materials.list(MediaType.voice, 0, counts.getVoice());
        data.put("voices", voices);
        //卡券
        List<Map> cards = cardService.selectCardList();
        data.put("cards", cards);
        //关键字自动回复
        List<Regular> regulars = autoReplyService.regularList();
        data.put("regulars", regulars);
        //被动回复
        ActiveMessage activeMessage = autoReplyService.passive();
        data.put("activeMessage", activeMessage);
        return  FrontResponse.success().setData(data);
    }

    /**
     * 关注后被动回复消息
     */
    @ApiOperation("关注后被动回复消息")
    @PostMapping(value = "/passive")
    public FrontResponse passive(@RequestBody ActiveMessage activeMessage) {
        autoReplyService.savePassive(activeMessage);
        return FrontResponse.success();
    }

    /**
     * 关键字
     *
     * @param regular
     * @return
     */
    @ApiOperation("关键字")
    @PostMapping(value = "/regular")
    public FrontResponse regular(@RequestBody Regular regular) {
        autoReplyService.saveRegular(regular);
        return FrontResponse.success();
    }

    @ApiOperation("删除关键字自动回复")
    @PostMapping(value = "/delete/{id}")
    public FrontResponse delete(@PathVariable("id") Integer id) {
        autoReplyService.delete(id);
        return FrontResponse.success();
    }

}
