package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.dto.ProductCollection;
import com.datafocus.common.dto.ProductVO;
import com.datafocus.service.service.ProductService;
import com.datafocus.service.service.remote.DistrictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Api(description = "后台产品管理")
@RestController
@RequestMapping("/product")
public class ProductController {

	private static final Logger logger = LoggerFactory.getLogger(FlowMgtController.class);
	@Autowired
	private ProductService productService;
	@Autowired
	private DistrictService districtService;

	@ApiOperation("添加产品")
	@PostMapping(value="/save")
	public FrontResponse save(@RequestBody ProductCollection product)
	{
		productService.save(product);
		return FrontResponse.success();
	}
	
	/**
	 * 产品列表页面
	 * @param carrierOperator
	 * @return
	 */
	@ApiOperation("产品列表信息")
	@GetMapping(value="/list/{carrierOperator}/{province}")
	public FrontResponse list(@PathVariable Integer carrierOperator,@PathVariable String province)
	{
		List<ProductVO>  products = productService.findAllProduct(carrierOperator,province);
		return FrontResponse.success().setData(products);
	}

	@ApiOperation("**列表信息")
	@GetMapping(value="/listPub")
	public FrontResponse listPub()
	{
		return FrontResponse.success().setData(districtService.findAllPubDistrict());
	}


	/**
	 * 编辑产品
	 * @return
	 */
	@ApiOperation("产品信息编辑")
	@PostMapping(value="/modify")
	public FrontResponse modify(@RequestBody  List<ProductVO> ProductVO)
	{
		productService.modify(ProductVO);
		return FrontResponse.success();
	}
	
	/**
	 * 查询产品数量
	 * @param carrierOperator
	 * @param province
	 * @return
	 */
	@ApiOperation("查询产品数量")
	@GetMapping(value="/count/{carrierOperator}/{province}")
	public FrontResponse count(@PathVariable Integer carrierOperator,@PathVariable String province)
	{
		Map<String,Object> data=new HashMap<>();
		Integer count = productService.count(carrierOperator, province);
		data.put("count",count);
	 	return FrontResponse.success().setData(data);
	}
	/**
	 * 查询流量产品
	 * @param productType
	 * @param vendorCode
	 * @param provinceCode
	 * @param cityCode
	 * @return
	 */
	@ApiOperation("查询流量产品")
	@GetMapping(value="/findProduct")
	public FrontResponse findProduct(@RequestParam String productType, @RequestParam String vendorCode,
									 @RequestParam String provinceCode,@RequestParam String cityCode) throws IOException{
		return FrontResponse.success().setData(productService.findProductForCache(productType, vendorCode, provinceCode, cityCode));
	}
	
}
