package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.CardInfoCube;
import com.datafocus.service.service.CardDataCubeService;
import com.datafocus.weixin.mp.card.bean.CardDataCube;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Api(description = "卡劵数据统计")
@RestController
@RequestMapping("/cardDataCube")
public class CardDataCubeController {

    @Autowired
    private CardDataCubeService cardDataCubeService;

    @ApiOperation("卡劵详情统计")
    @GetMapping(value = "/cardInfoCubes")
    public FrontResponse cardInfoCubes(@RequestParam("page") Integer page, @RequestParam("size") Integer size,
                                       @RequestParam("cardId") String cardId, @RequestParam("beginTime") String beginTime, @RequestParam("endTime") String endTime) {
        PageInfo<CardInfoCube> pageInfo = cardDataCubeService.list(page, size, cardId, beginTime, endTime);
        Map<String, Object> data = new HashMap<>();
        data.put("hasMore", pageInfo.isHasNextPage());
        data.put("total", pageInfo.getTotal());
        data.put("content", pageInfo.getList());
        return FrontResponse.success().setData(data);
    }

    @ApiOperation("卡劵概况统计")
    @GetMapping(value = "/cardDataCubes")
    public FrontResponse cardDataCubes(@RequestParam("page") Integer page, @RequestParam("size") Integer size,
                                       @RequestParam("beginTime") String beginTime, @RequestParam("endTime") String endTime) {
        PageInfo<CardDataCube> pageInfo = cardDataCubeService.list(page, size, beginTime, endTime);
        Map<String, Object> data = new HashMap<>();
        data.put("hasMore", pageInfo.isHasNextPage());
        data.put("total", pageInfo.getTotal());
        data.put("content", pageInfo.getList());
        return FrontResponse.success().setData(data);
    }

}
