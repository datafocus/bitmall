package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.Resources;
import com.datafocus.common.pojo.Role;
import com.datafocus.common.pojo.RoleRes;
import com.datafocus.service.service.ResourcesService;
import com.datafocus.service.service.RoleMgtService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Api(description = "角色控制器")
@RequestMapping("/roleMgt")
public class RoleMgtController {

    @Autowired
    private RoleMgtService roleMgtService;

    @Autowired
    private ResourcesService resourcesService;

    /**
     * 角色列表
     *
     * @return
     */
    @GetMapping("/findRole")
    @ApiOperation("角色列表")
    public FrontResponse findRole() {
        List<Role> roles = roleMgtService.findRole();
        return FrontResponse.success().setData(roles);
    }

    /**
     * 保存角色
     *
     * @param role
     * @return
     */
    @PostMapping("/saveRole")
    @ApiOperation("保存角色")
    public FrontResponse saveRole(@RequestBody @ApiParam("必传字段 :name 可选:roleKey description") Role role) {
        roleMgtService.saveRole(role);
        return FrontResponse.success();
    }

    /**
     * resource 列表
     *
     * @return
     */
    @GetMapping("/findResourcess")
    @ApiOperation("resource列表")
    public FrontResponse findResourcess() {
        List<Resources> resources = roleMgtService.findResourcess();
        return FrontResponse.success().setData(resources);
    }

    /**
     * 保存角色资源关联关系
     *
     * @param roleRes
     * @return
     */
    @PostMapping("/saveRoleRes")
    @ApiOperation("保存角色资源关联关系")
    public FrontResponse saveRoleRes(@RequestBody List<RoleRes> roleRes) {
        roleMgtService.saveRoleRes(roleRes);
        return FrontResponse.success();
    }

    /**
     * 查找角色资源
     *
     * @param roleId
     * @return
     */
    @GetMapping("/findResourcesRole")
    @ApiOperation("查找角色资源")
    public FrontResponse findResourcesRole(Integer roleId) {
        List<Resources> resources = resourcesService.findAllResRoles(roleId);
        return FrontResponse.success().setData(resources);
    }

    /**
     * 删除角色
     *
     * @param roleId
     * @return
     */
    @PostMapping("/deleteRole")
    @ApiOperation("删除角色")
    public FrontResponse deleteRole(@RequestParam("roleId") Integer roleId) {
        roleMgtService.deleteRole(roleId);
        return FrontResponse.success();
    }
}
