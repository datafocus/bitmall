package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.weixin.common.media.MediaType;
import com.datafocus.weixin.mp.media.Materials;
import com.datafocus.weixin.mp.media.bean.MaterialPagination;
import com.datafocus.weixin.mp.media.bean.MpNewsPagination;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "素材管理器")
@RestController
@RequestMapping("/media")
public class MediaController {

	@Autowired
	private Materials materials;

	@GetMapping(value="/listMpNews")
	public FrontResponse listMpNews(@RequestParam("page") Integer page, @RequestParam("size") Integer size){
		return FrontResponse.success().setData(materials.listMpNews((page-1)*size,size));
	}
	
	@GetMapping(value="/list")
	public FrontResponse list(@RequestParam("mediaType") MediaType mediaType,@RequestParam("page") Integer page, @RequestParam("size") Integer size){
		return FrontResponse.success().setData(materials.list(mediaType, page, size));
	}

}
