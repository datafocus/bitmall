package com.datafocus.mgt.controller;

import com.datafocus.service.handler.TextMessageHandler;
import com.datafocus.service.mapper.LocationMapper;
import com.datafocus.service.mapper.MenuEventMapper;
import com.datafocus.service.mapper.MessageMapper;
import com.datafocus.service.service.CardService;
import com.datafocus.service.service.PromoterService;
import com.datafocus.service.service.UserInfoService;
import com.datafocus.weixin.common.decrypt.AesException;
import com.datafocus.weixin.common.decrypt.MessageDecryption;
import com.datafocus.weixin.common.event.ClickEvent;
import com.datafocus.weixin.common.event.EventType;
import com.datafocus.weixin.common.event.LocationReportEvent;
import com.datafocus.weixin.common.event.ViewEvent;
import com.datafocus.weixin.common.message.XmlMessageHeader;
import com.datafocus.weixin.common.request.*;
import com.datafocus.weixin.mp.event.MessageSentEvent;
import com.datafocus.weixin.mp.event.card.*;
import com.datafocus.weixin.mp.event.ticket.SceneScanEvent;
import com.datafocus.weixin.mp.event.ticket.SceneSubEvent;
import com.datafocus.weixin.mp.message.MpXmlMessages;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/event")
public class EventController {

    private Logger logger = LoggerFactory.getLogger(EventController.class);

    @Autowired
    private MessageDecryption messageDecryption;

    @Autowired
    private CardService cardService; //卡券接口
    @Autowired
    private UserInfoService userInfoService;//用户信息接口
    @Autowired
    private TextMessageHandler textMessageHandler;//自动回复，文本消息
    @Autowired
    private MenuEventMapper menuEventMapper;
    @Autowired
    private LocationMapper locationMapper;
    @Autowired
    private MessageMapper messageMapper;

    @RequestMapping(value = "/handler")
    public void handler(String signature, String timestamp, String nonce, String echostr, HttpServletRequest request, HttpServletResponse response) throws IOException {

        try {
            String xml = IOUtils.toString(request.getInputStream());
            if (StringUtils.isNotBlank(xml)) {
                String xmlContent = messageDecryption.decrypt(signature, timestamp, nonce, xml);
                logger.info("事件推送文本:{}", xmlContent);
                final XmlMessageHeader xmlRequest = MpXmlMessages.fromXml(xmlContent);
                if (null != xmlRequest) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                handlerEvent(xmlRequest);
                            } catch (Exception e) {
                                logger.error("处理微信消息时间系统异常{},消息体：{}", e, xmlRequest);
                            }
                        }
                    }).start();
                    response.getWriter().print("");
                    return;
                }
            } else {
                // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
                if (StringUtils.isNotBlank(echostr) && messageDecryption.decryptEcho(signature, timestamp, nonce)) {
                    response.getWriter().print(echostr);
                    return;
                }
            }
        } catch (AesException e) {
            logger.error("消息验证失败:{}", e);
        } catch (Exception e) {
            logger.error("事件处理系统异常", e);
        }
        response.getWriter().print("fail");
    }


    public String handlerEvent(XmlMessageHeader xmlRequest) throws Exception {
        //卡券审核
        if (xmlRequest instanceof CheckCardEvent) {
            CheckCardEvent checkCardEvent = (CheckCardEvent) xmlRequest;

            //判断卡券审核事件类型  审核通过、审核失败
            int status = 0;
            if (checkCardEvent.getEventType().equals(EventType.card_pass_check))
                status = 1; //成功
            else
                status = 2; //失败
            cardService.updateCardStatus(checkCardEvent.getCardId(), status);
        }
        //用户领取卡券
        if (xmlRequest instanceof UserGetCardEvent) {
            UserGetCardEvent userGetCardEvent = (UserGetCardEvent) xmlRequest;
            cardService.saveCardCode(userGetCardEvent);
        }
        //用户从卡券进入公众号
        if (xmlRequest instanceof UserEnterSessionCardEvent) {
            UserEnterSessionCardEvent userEnterSessionCardEvent = (UserEnterSessionCardEvent) xmlRequest;
            cardService.saveSessionEventCard(userEnterSessionCardEvent);
        }
        //用户删除卡券
        if (xmlRequest instanceof UserDeleteCardEvent) {
            UserDeleteCardEvent userDeleteCardEvent = (UserDeleteCardEvent) xmlRequest;
            cardService.deleteCardByCardIdAndCode(userDeleteCardEvent);
        }
        //用户核销卡券
        if (xmlRequest instanceof UserConsumeCardEvent) {
            UserConsumeCardEvent userConsumeCardEvent = (UserConsumeCardEvent) xmlRequest;
            cardService.updateCardIsUseStatus(userConsumeCardEvent);
        }
        //用户关注、取消关注
        if (xmlRequest instanceof SceneSubEvent) {
            SceneSubEvent sceneSubEvent = (SceneSubEvent) xmlRequest;
            userInfoService.saveUserInfo(sceneSubEvent);
        }
        //用户发送文本消息
        if (xmlRequest instanceof TextRequest) {
            TextRequest textRequest = (TextRequest) xmlRequest;
            textMessageHandler.handler(textRequest);
            return transferCustomerService(xmlRequest);
        }
        if (xmlRequest instanceof ImageRequest) {
            //用户发送消息，转接到多客服
            return transferCustomerService(xmlRequest);
        }
        if (xmlRequest instanceof VideoRequest) {
            //用户发送消息，转接到多客服
            return transferCustomerService(xmlRequest);
        }
        if (xmlRequest instanceof ShortVideoRequest) {
            //用户发送消息，转接到多客服
            return transferCustomerService(xmlRequest);
        }
        if (xmlRequest instanceof VoiceRequest) {
            //用户发送消息，转接到多客服
            return transferCustomerService(xmlRequest);
        }
        if (xmlRequest instanceof ClickEvent) {
            //点击菜单拉取消息时的事件推送
            ClickEvent clickEvent = (ClickEvent) xmlRequest;
            menuEventMapper.saveMenuEvenFormClick(clickEvent);
        }
        if (xmlRequest instanceof ViewEvent) {
            //点击菜单跳转链接时的事件推送
            ViewEvent viewEvent = (ViewEvent) xmlRequest;
            menuEventMapper.saveMenuEvenFormView(viewEvent);
        }
        if (xmlRequest instanceof LocationReportEvent) {
            //地理位置上报时间事件
            LocationReportEvent locationReportEvent = (LocationReportEvent) xmlRequest;
            locationMapper.saveLocation(locationReportEvent);
        }
        if (xmlRequest instanceof MessageSentEvent) {

            MessageSentEvent message = (MessageSentEvent) xmlRequest;
            messageMapper.saveMessage(message);
        }
        if (xmlRequest instanceof SceneScanEvent) {
            SceneScanEvent sceneScanEvent = (SceneScanEvent) xmlRequest;
            handleEnvent(sceneScanEvent);
        }
        return "";
    }

    /**
     * 处理扫码事件
     *
     * @param sceneScanEvent
     */
    private void handleEnvent(SceneScanEvent sceneScanEvent) {
        /*List<String> promoterSenceId = promoterService.getPromoterSenceId();
        String eventKey = sceneScanEvent.getEventKey();
        if (!promoterSenceId.contains(eventKey)) return;*/
        //如果是分享用户则发卡券
    }

    private String transferCustomerService(XmlMessageHeader xmlRequest) {
        logger.info("将用户发送的消息转发到微信多客服系统");
        return new StringBuffer().append("<xml><ToUserName><![CDATA[").append(xmlRequest.getFromUser()).append("]]></ToUserName>")
                .append("<FromUserName><![CDATA[").append(xmlRequest.getToUser()).append("]]></FromUserName>")
                .append("<CreateTime>").append(xmlRequest.getCreateTime().getTime() / 1000).append("</CreateTime>")
                .append("<MsgType><![CDATA[transfer_customer_service]]></MsgType></xml>")
                .toString();
    }
}
