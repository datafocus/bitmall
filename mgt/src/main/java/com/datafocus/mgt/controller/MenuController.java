package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.weixin.common.menu.Menu;
import com.datafocus.weixin.common.menu.MenuItem;
import com.datafocus.weixin.mp.menu.Menus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;

/*
 * 微信自定义菜单控制器
 * 
 */
@Api(description = "微信菜单控制器")
@RestController
@RequestMapping("/menu")
public class MenuController {

    private static Logger logger = LoggerFactory.getLogger(Menus.class);

    @Autowired
	private Menus menus ;

	/**
	 * 创建微信自定义菜单
	 * @param menu
	 * @return
	 */
	@ApiOperation("创建微信自定义菜单")
    @PostMapping(value = "/createMenu")
	public FrontResponse createMenu(@RequestBody Menu menu){
		buildMenu(menu);
		menus.create(menu);
		return FrontResponse.success();
	}
    
    /**
     * 构建菜单列表对象。
     * @param menu
     */
    public void buildMenu(Menu menu)
    {
    	for (Iterator<MenuItem>  modalIte =  menu.getMenus().iterator(); modalIte.hasNext();)
    	{
    		MenuItem modal = modalIte.next();
    		for (Iterator<MenuItem> subIte =modal.getSubItems().iterator();subIte.hasNext();) 
    		{
    			MenuItem sub = subIte.next();
    			if(StringUtils.isEmpty(sub.getName()))
    				subIte.remove();
			}
		}
    }
    //菜单列表
	@ApiOperation("菜单列表")
    @GetMapping(value = "/list")
    public FrontResponse list()
    {
    	return FrontResponse.success().setData(menus.list());
    }
}
