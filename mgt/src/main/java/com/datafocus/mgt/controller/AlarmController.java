package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.AlarmConfig;
import com.datafocus.common.pojo.AlarmUser;
import com.datafocus.service.service.AlarmConfigService;
import com.datafocus.service.service.AlarmUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "告警后台配置")
@RestController
@RequestMapping("/alarm")
public class AlarmController {

    @Autowired
    private AlarmUserService alarmUserService;

    @Autowired
    private AlarmConfigService alarmConfigService;

    @ApiOperation("获取告警管理人员列表")
    @GetMapping("/listAlarmUser")
    public FrontResponse<AlarmUser> listAlarmUser(){
        return FrontResponse.success().setData(alarmUserService.listAlarmUser());
    }

    @ApiOperation("获取告警管理人员列表")
    @GetMapping("/listAlarmConfig")
    public FrontResponse<AlarmConfig> listAlarmConfig(){
        return FrontResponse.success().setData(alarmConfigService.listConfig());
    }


    @ApiOperation("修改管理人员状态")
    @PostMapping("/updateUserStatus")
    public FrontResponse updateUserStatus(@RequestParam("id") Integer id,@RequestParam("status") Integer status){
        alarmUserService.updateStatus(id,status);
        return FrontResponse.success();
    }

    @ApiOperation("修改告警配置")
    @PostMapping("/updateConfig")
    public FrontResponse updateConfig(AlarmConfig alarmConfig){
        alarmConfigService.updateConfig(alarmConfig);
        return FrontResponse.success();
    }

    @ApiOperation("添加告警管理人员")
    @PostMapping("/addAlarmUser")
    public FrontResponse addAlarmUser(AlarmUser AlarmUser){
        AlarmUser.setStatus(1);
        alarmUserService.addAlarmUser(AlarmUser);
        return FrontResponse.success();
    }
}
