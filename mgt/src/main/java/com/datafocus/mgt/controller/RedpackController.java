package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.utils.HttpUtils;
import com.datafocus.service.utils.DFUtils;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.pay.base.PaySetting;
import com.datafocus.weixin.pay.redpack.RedPacks;
import com.datafocus.weixin.pay.redpack.bean.RedPackRequest;
import com.datafocus.weixin.pay.redpack.bean.RedPackResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@Api(description = "微信红包处理器")
@RestController
@RequestMapping("/redpack")
public class RedpackController {

    private static Logger logger = LoggerFactory.getLogger(RedpackController.class);

    @Autowired
    private AppSetting appSetting;

    @Autowired
    private RedPacks redPacks;
    /**
     * 普通红包
     *
     * @param redPackRequest
     * @return
     */
    @ApiOperation("普通红包")
    @PostMapping(value = "/sendredpack")
    public FrontResponse sendredpack(@RequestBody RedPackRequest redPackRequest) {
        redPackRequest.setClientIp(HttpUtils.getHostIp(HttpUtils.getInetAddress()));
        redPackRequest.setAppId(appSetting.getAppId());
        redPackRequest.setAmtType("ALL_RAND");
        redPackRequest.setBillNumber(DFUtils.createBillNo("1289258601"));
        redPackRequest.setAmount(redPackRequest.getAmount());
        RedPackResponse redPackRespons = redPacks.sendSingle(redPackRequest);
        if ("SUCCESS".equals(redPackRespons.getResultCode())) {
            return FrontResponse.success();
        } else {
            return FrontResponse.response().setCode(-1).setMessage("红包发送失败");
        }

    }

    /**
     * 裂变红包
     *
     * @param redPackRequest
     * @return
     */
    @ApiOperation("裂变红包")
    @PostMapping(value = "/sendGroupRedpack")
    public FrontResponse sendGroupRedpack(@RequestBody RedPackRequest redPackRequest) {
        redPackRequest.setClientIp(HttpUtils.getHostIp(HttpUtils.getInetAddress()));
        redPackRequest.setAppId(appSetting.getAppId());
        redPackRequest.setAmtType("ALL_RAND");
        redPackRequest.setBillNumber(DFUtils.createBillNo("1289258601"));
        redPackRequest.setAmount(redPackRequest.getAmount());
        RedPackResponse redPackRespons = redPacks.sendGroup(redPackRequest);
        if ("SUCCESS".equals(redPackRespons.getResultCode())) {
            return FrontResponse.success();
        } else {
            return FrontResponse.response().setCode(-1).setMessage("红包发送失败");
        }
    }
}
