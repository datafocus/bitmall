package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.dto.FlowOrderDto;
import com.datafocus.common.dto.NewerOrderExport;
import com.datafocus.common.dto.UserInfoDto;
import com.datafocus.common.dto.UserInfoQuery;
import com.datafocus.common.utils.ExcelUtil;
import com.datafocus.service.service.UserInfoService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Api(description = "用户信息管理")
@RestController
@RequestMapping("/user")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;


    /**
     * 用户列表详情
     *
     * @return 返回用户列表页视图
     */
    @ApiOperation("用户列表详情")
    @GetMapping(value = "/userInfoDetail")
    public FrontResponse userInfoDetail(@RequestParam("openId") String openId) {
        UserInfoDto userInfo = userInfoService.get(openId);
        return FrontResponse.success().setData(userInfo);
    }

    /**
     * 用户列表数据
     *
     * @param page
     * @param size
     * @param sex
     * @param subscribe
     * @param startTime
     * @param endTime
     * @param nickName
     * @param tags
     * @return
     */
    @ApiOperation("用户列表数据")
    @GetMapping(value = "/list")
    public FrontResponse list(@ApiParam UserInfoQuery userInfoQuery) {
        PageInfo<UserInfoDto> pageInfo = userInfoService.list(userInfoQuery);
        Map<String, Object> data = new HashMap<>();
        data.put("hasMore", pageInfo.isHasNextPage());
        data.put("total", pageInfo.getTotal());
        data.put("content", pageInfo.getList());
        return FrontResponse.success().setData(data);
    }

    /**
     * 查询用户订单记录
     *
     * @return
     */
    @ApiOperation("查询用户订单记录")
    @GetMapping(value = "/order")
    public FrontResponse order(@RequestParam("page") Integer page, @RequestParam("size") Integer size, @RequestParam("openId") String openId) {
        PageInfo<FlowOrderDto> pageInfo = userInfoService.orderByOpenId(page, size, openId);
        Map<String, Object> data = new HashMap<>();
        data.put("hasMore", pageInfo.isHasNextPage());
        data.put("total", pageInfo.getTotal());
        data.put("content", pageInfo.getList());
        return FrontResponse.success().setData(data);
    }

    @ApiOperation("导出用户订单记录")
    @GetMapping(value = "/export")
    public void export(@ApiParam UserInfoQuery userInfoQuery, HttpServletResponse response) throws Exception {
        List<NewerOrderExport> data = userInfoService.listExport(userInfoQuery);
        ExcelUtil<NewerOrderExport> util = new ExcelUtil<>(NewerOrderExport.class);
        response.setContentType("application/vnd.ms-excel;charset=GBK");
        response.setHeader("Content-Disposition", "attachment; filename=order.xls");
        util.getListToExcel(data, "用户订单情况", response.getOutputStream());
    }

}
