package com.datafocus.mgt.controller;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.BillOrder;
import com.datafocus.service.service.BillOrderService;
import com.datafocus.service.task.RefundOrderDelayed;
import com.datafocus.weixin.pay.mp.bean.RefundRequest;
import com.datafocus.weixin.pay.mp.bean.RefundResponse;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

@Api(description = "话费订单")
@RestController
@RequestMapping("/billOrder")
public class BillOrderContoller {

    @Autowired
    private BillOrderService billOrderService;

    /**
     * 话费订单列表数据
     *
     * @param params
     * @return
     */
    @ApiOperation("话费订单列表数据")
    @GetMapping("/list")
    public FrontResponse list(@RequestParam(required = false) Map<String, String> params, FlowPage flowPage) {
        if (null == flowPage) flowPage = FlowPage.defaultPage();
        PageInfo<BillOrder> listInfo = billOrderService.list(params, flowPage);
        Map<String, Object> data = new HashMap<>();
        data.put("count", listInfo.getTotal());
        data.put("hasMore", listInfo.isHasNextPage());
        data.put("data", listInfo.getList());
        return FrontResponse.success().setData(data);
    }

    /**
     * 导出话费订单
     *
     * @param params
     * @param response
     */
    @ApiOperation("导出话费订单")
    @GetMapping("/exportOrderToExcel")
    public void exportOrderToExcel(@RequestParam HashMap<String, String> params, HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel;charset=GBK");
        response.setHeader("Content-Disposition", "attachment; filename=bill-order.xls");
        try {
            billOrderService.exprot(params, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改订单
     *
     * @param billOrders
     * @param dealState
     * @return
     */
    @PostMapping("/modify")
    @ApiOperation("修改订单")
    public FrontResponse modify(String[] billOrders, String dealState) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("dealState", dealState);
        params.put("billOrders", Arrays.asList(billOrders));
        billOrderService.modify(params);
        return FrontResponse.success();

    }

}
