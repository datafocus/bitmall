package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.Tag;
import com.datafocus.service.service.TagsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "微信标签")
@RestController
@RequestMapping("/tags")
public class TagsController {

    @Autowired
    private TagsService tagsService;

    /**
     * 保存标签
     *
     * @param tag
     * @return
     */
    @ApiOperation("保存标签")
    @PostMapping("/save")
    public FrontResponse save(@RequestBody Tag tag) {
        tagsService.saveTag(tag);
        return FrontResponse.success();
    }

    /**
     * 删除标签
     *
     * @param tagId
     * @return
     */
    @ApiOperation("删除标签")
    @PostMapping("/delete")
    public FrontResponse delete(@RequestParam Integer tagId) {
        tagsService.delete(tagId);
        return FrontResponse.success();
    }

    /**
     * 标签列表
     *
     * @return
     */
    @ApiOperation("标签列表")
    @GetMapping("/list")
    public FrontResponse list() {
        List<Tag> tags = tagsService.list();
        return FrontResponse.success().setData(tags);
    }

    /**
     * 保存用户标签
     *
     * @param openId
     * @param tagId
     */
    @ApiOperation("保存用户表签")
    @PostMapping("/userTags")
    public FrontResponse userTags(@RequestParam String openId, @RequestParam List<Integer> tagId) {
        tagsService.userTags(openId, tagId);
        return FrontResponse.success();
    }


    /**
     * 获取用户下所有的标签
     *
     * @param openId
     * @return
     */
    @ApiOperation("获取用户下所有的标签")
    @GetMapping("/getUserTags")
    public FrontResponse getUserTags(@RequestParam String openId) {
        List<Integer> tagIds = tagsService.userTagByOpenId(openId);
        return FrontResponse.success().setData(tagIds);
    }

    /**
     * 手工同步本地表签至微信
     *
     * @return
     */
    @ApiOperation("手工同步本地表签至微信")
    @GetMapping("/synchronizeTags")
    public FrontResponse synchronizeTags() {
        new Thread(() -> tagsService.task()).start();
        return FrontResponse.success().setMessage("正在后台同步。。。");
    }
}
