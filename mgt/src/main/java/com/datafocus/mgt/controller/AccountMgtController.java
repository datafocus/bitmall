package com.datafocus.mgt.controller;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.Account;
import com.datafocus.common.pojo.AccountRole;
import com.datafocus.mgt.service.impl.AccountService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(description = "账户处理")
@RestController
@RequestMapping("/accountMgt")
public class AccountMgtController {

    @Autowired
    private AccountService accountService;

    /**
     * 获取账户列表
     *
     * @param account
     * @param flowPage
     * @return
     */
    @ApiOperation("获取账户列表")
    @GetMapping("/getAccount")
    public FrontResponse getAccount(Account account, FlowPage flowPage) {
        if (null == flowPage) flowPage = FlowPage.defaultPage();
        Page accountGetLimit = accountService.findAccountGetLimit(account, flowPage);
        Map<String, Object> data = new HashMap<>();
        PageInfo pageInfo = accountGetLimit.toPageInfo();
        data.put("content", accountGetLimit);
        data.put("hasMore", pageInfo.isHasNextPage());
        data.put("count", pageInfo.getTotal());
        return FrontResponse.success().setData(data);
    }

    /**
     * 保存账户
     *
     * @param account
     * @return
     */
    @ApiOperation("保存账户")
    @PostMapping("/saveAccount")
    public FrontResponse saveAccount(@RequestBody Account account) {
        accountService.saveAccount(account);
        return FrontResponse.success();
    }

    /**
     * 删除账户
     *
     * @param account
     * @return
     */
    @ApiOperation("删除账户")
    @PostMapping("/deleteAccount")
    public FrontResponse deleteAccount(@RequestBody Account account) {
        accountService.deleteAccount(account);
        return FrontResponse.success();
    }

    /**
     * 保存账户角色
     *
     * @param accountRoles
     * @return
     */
    @ApiOperation("保存账户角色")
    @PostMapping("/saveAccountRole")
    public FrontResponse saveAccountRole(@RequestBody List<AccountRole> accountRoles) {
        accountService.saveAccoutRole(accountRoles);
        return FrontResponse.success();
    }

    /**
     * 查找账户角色
     *
     * @param accountName
     * @return
     */
    @ApiOperation("查找账户角色")
    @GetMapping("/findAccountRole")
    public FrontResponse findAccountRole(@RequestParam("accountName") String accountName) {
        Account account = accountService.findAccountRole(accountName);
        return FrontResponse.success().setData(account);
    }

    /**
     * 修改密码
     *
     * @param account
     * @return
     */
    @ApiOperation("修改密码")
    @PostMapping("/updateAccountPassword")
    public FrontResponse updateAccountPassword(@RequestBody Account account) {
        accountService.updateAccountPassword(account);
        return FrontResponse.success();
    }

    /**
     * 判断账户是否存在
     *
     * @param accountName
     * @return
     */
    @GetMapping("/accountIsExist")
    @ApiOperation("判断账户是否存在")
    public FrontResponse accountIsExist(@RequestParam("accountName") String accountName) {
        boolean b = accountService.accouontIsExistByAccountName(accountName);
        Map<String, Boolean> data = new HashMap<>();
        data.put("isExist", b);
        return FrontResponse.success().setData(data);
    }

    /**
     * 查找账户信息
     *
     * @param account
     * @return
     */
    @GetMapping("/findAccount")
    @ApiOperation("查找账户信息")
    public FrontResponse findAccount(Account account) {
        List<Account> accounts = accountService.findAccount(account);
        return FrontResponse.success().setData(accounts);
    }
}
