package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.CardStore;
import com.datafocus.service.service.CardStoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


@Api(description = "卡劵货架处理")
@RestController
@RequestMapping("/cardStore")
public class CardStoreController {
	
	private static Logger logger = LoggerFactory.getLogger(CardStoreController.class);
	
	@Autowired
	private CardStoreService cardStoreService;


	/**
	 * 创建货架
	 * @param cardStore
	 * @return
	 */
	@ApiOperation("创建货架,表单post传送")
	@PostMapping(value="/createCardStore")
	public FrontResponse createCardStore(CardStore cardStore,MultipartFile bannerFile) throws IOException
	{
		cardStoreService.saveCardStore(cardStore,bannerFile);
		return FrontResponse.success();
	}
	/**
	 * 卡券货架列表
	 */
	@ApiOperation("卡券货架列表")
	@GetMapping(value="/cardStoreList")
	public FrontResponse cardStoreList()
	{
		List<CardStore> list= cardStoreService.cardStoreList();
		return FrontResponse.success().setData(list);
	}
}
