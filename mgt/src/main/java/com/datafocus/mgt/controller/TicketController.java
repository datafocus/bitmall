package com.datafocus.mgt.controller;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.Ticket;
import com.datafocus.service.service.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageInfo;
import springfox.documentation.annotations.ApiIgnore;

@Api(description = "二维码管理")
@RestController
@RequestMapping("/ticket")
public class TicketController {
	
	private static Logger logger = LoggerFactory.getLogger(TicketController.class);

	@Autowired
	private TicketService ticketService;
	

	
	/**
	 * 创建带参数二维码
	 * @param ticket
	 * @return
	 */
	@ApiOperation("创建带参数二维码")
	@PostMapping(value="/save")
	public FrontResponse save(@ApiParam(value = "传递参数{actionName,expireSeconds,activityName,sceneId}") @RequestBody Ticket ticket)
	{
		ticketService.save(ticket);
		return FrontResponse.success();
	}
	
	/**
	 * 获取带参数二维码列表
	 * @param page
	 * @param size
	 * @param activityName
	 * @return
	 */
	@ApiOperation("获取带参数二维码列表")
	@GetMapping(value="/list")
	public FrontResponse  list(@RequestParam Integer page,@RequestParam Integer size,@RequestParam(required = false) String activityName)
	{
		PageInfo<Ticket> pageInfo=ticketService.list(page, size, activityName);
		Map<String,Object> data=new HashMap<>();
		data.put("hasMore",pageInfo.isHasNextPage());
		data.put("total",pageInfo.getTotal());
		data.put("content",pageInfo.getList());
		return FrontResponse.success().setData(data);
	}
	
	/**
	 * 下载带参数二维码
	 * @param ticket
	 * @param fileName
	 * @param response
	 * @throws IOException
	 */
	@ApiOperation("带参数二维码下载")
	@GetMapping(value="/getQrcode")
	public void getQrcode(@RequestParam String ticket,@RequestParam String fileName,@ApiIgnore HttpServletResponse response) throws IOException
	{
		byte[] byteArray = ticketService.getQrcode(ticket);
		response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName,"UTF-8"));
		response.getOutputStream().write(byteArray);
	}
	
}
