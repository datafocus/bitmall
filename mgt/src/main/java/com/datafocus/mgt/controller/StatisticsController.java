package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.dto.Statistics;
import com.datafocus.service.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Api(description = "数据统计")
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

	
	@Autowired
	private StatisticsService statisticsService;

	@ApiOperation("付费用户数据统计")
	@GetMapping(value="/payStatisticsData")
	public FrontResponse payStatisticsData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime)
	{
		HashMap<String, Object> param = new HashMap<>();
		param.put("startTime", startTime);
		param.put("endTime", endTime);
		List<Statistics> list = statisticsService.findPayStatistics(param);
		HashMap<String, Object> data = new HashMap<>();
		if(list!=null&&list.size()>0){
			List<Integer> y = new ArrayList<>();
			List<String>  x = new ArrayList<>();
			for (Statistics statistics : list)
			{
				y.add(statistics.getNum());
				x.add(statistics.getDateTime());
			}
			data.put("x",x);
			data.put("y",y);
		}
		return FrontResponse.success().setData(data);
	}


	@ApiOperation("流水金额统计")
	@GetMapping(value="/transactionData")
	public FrontResponse transactionData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime)
	{
		HashMap<String, Object> param = new HashMap<>();
		param.put("startTime", startTime);
		param.put("endTime", endTime);
		List<Statistics> list = statisticsService.findTransaction(param);
		HashMap<String, Object> data = new HashMap<>();
		if(list!=null&&list.size()>0){
			List<BigDecimal> y = new ArrayList<>();
			List<String>  x = new ArrayList<>();
			for (Statistics statistics : list)
			{
				y.add(statistics.getMoney());
				x.add(statistics.getDateTime());
			}
			data.put("y",y);
			data.put("x",x);
		}
		return FrontResponse.success().setData(data);
	}

	@ApiOperation("运营商占比统计")
	@GetMapping(value="/carrierOperatorStatisticsData")
	public FrontResponse carrierOperatorStatisticsData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime)
	{
		HashMap<String, Object> param = new HashMap<>();
		param.put("startTime", startTime);
		param.put("endTime", endTime);
		return FrontResponse.success().setData(statisticsService.findCarrierOperatorStatistics(param));
	}

	@ApiOperation("下单用户数据统计")
	@GetMapping(value="/createOrderStatisticsData")
	public  FrontResponse createOrderStatisticsData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime)
	{
		HashMap<String, Object> param = new HashMap<>();
		param.put("startTime", startTime);
		param.put("endTime", endTime);
		List<Statistics> list = statisticsService.findCreateOrderStatisticsData(param);
		HashMap<String, Object> data = new HashMap<>();
		if(list!=null&&list.size()>0){
			List<Integer> y = new ArrayList<>();
			List<String>  x = new ArrayList<>();
			for (Statistics statistics : list)
			{
				y.add(statistics.getNum());
				x.add(statistics.getDateTime());
			}
			data.put("x",x);
			data.put("y",y);
		}
		return FrontResponse.success().setData(data);
	}

	@ApiOperation("访问数据统计")
	@GetMapping(value="/accessStatisticsData")
	public FrontResponse accessStatisticsData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime)
	{
		HashMap<String, Object> param = new HashMap<>();
		param.put("startTime", startTime);
		param.put("endTime", endTime);
		List<Statistics> list = statisticsService.findAccessStatisticsData(param);
		HashMap<String, Object> data = new HashMap<>();
		if(list!=null&&list.size()>0){
			List<Integer> y = new ArrayList<>();
			List<String>  x = new ArrayList<>();
			for (Statistics statistics : list)
			{
				y.add(statistics.getNum());
				x.add(statistics.getDateTime());
			}
			data.put("x",x);
			data.put("y",y);
		}
		return FrontResponse.success().setData(data);
	}

	@ApiOperation("体验用户统计")
	@GetMapping(value="/phoneResolveStatisticsData")
	public  FrontResponse phoneResolveStatisticsData(@RequestParam("startTime") String startTime, @RequestParam("endTime") String endTime)
	{
		HashMap<String, Object> param = new HashMap<>();
		param.put("startTime", startTime);
		param.put("endTime", endTime);
		List<Statistics> list = statisticsService.findPhoneResolveStatistics(param);
		HashMap<String, Object> data = new HashMap<>();
		if(list!=null&&list.size()>0){
			List<Integer> y = new ArrayList<>();
			List<String>  x = new ArrayList<>();
			for (Statistics statistics : list)
			{
				y.add(statistics.getNum());
				x.add(statistics.getDateTime());
			}
			data.put("x",x);
			data.put("y",y);
		}
		return FrontResponse.success().setData(data);
	}
}
