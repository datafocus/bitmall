package com.datafocus.mgt.controller;

import com.datafocus.common.pojo.Resources;
import com.datafocus.common.pojo.Role;
import com.datafocus.common.pojo.RoleRes;
import com.datafocus.mgt.shiro.chain.ShiroFilerChainManager;
import com.datafocus.service.mapper.ResourcesMapper;
import com.datafocus.service.mapper.RoleMapper;
import com.datafocus.service.service.RoleMgtService;
import com.github.pagehelper.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @author layne
 * @date 17-8-2
 */
@Service
public class RoleMgtServiceImpl implements RoleMgtService {
    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private ResourcesMapper resourcesMapper;
    @Autowired
    private ShiroFilerChainManager shiroFilerChainManager;

    @Override
    public Page<Role> findRole() {
        return roleMapper.findRole();
    }

    @Override
    public void saveRole(Role role) {
        Objects.requireNonNull(role);
        roleMapper.insertRole(role);
    }

    @Override
    public List<Resources> findResourcess() {
        return resourcesMapper.findAllResources();
    }

    @Override
    @Transactional
    public void saveRoleRes(List<RoleRes> roleRes) {

        //1:删除角色资源关联关系，by role_id
        if(roleRes!=null && roleRes.size()>0)
        {
            roleMapper.deleteRoleRes(roleRes.get(0).getRoleId());
            //2:保存角色资源关联关系
            roleMapper.insertRoleRes(roleRes);
        }
        shiroFilerChainManager.updatePermission();
    }

    @Override
    @Transactional
    public void deleteRole(Integer roleId) {
        roleMapper.deleteRole(roleId);
        roleMapper.deleteRoleRes(roleId);
        shiroFilerChainManager.updatePermission();
    }
}
