package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.common.pojo.Resources;
import com.datafocus.common.utils.ExcelUtil;
import com.datafocus.service.service.FlowMgtService;
import com.datafocus.service.service.OrderService;
import com.datafocus.service.service.ResourcesService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(description = "通道流量管理")
@RestController
@RequestMapping("/flowmgt")
public class FlowMgtController {
	
	private static final Logger logger = LoggerFactory.getLogger(FlowMgtController.class);
	
	@Autowired
	private FlowMgtService flowMgtService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private ResourcesService resourcesService;
	/**
	 * 获取当前用户的菜单
	 * @return
	 */
	@ApiOperation("获取当前用户的菜单")
	@GetMapping("/resources")
	public FrontResponse resources()
	{
		//当前登录用户session
		Session session = SecurityUtils.getSubject().getSession();
		String accountId =  (String) session.getAttribute("userSessionId");
		List<Resources> res= resourcesService.findResourcesByAccountId(accountId);
		return FrontResponse.success().setData(res);
	}

	/**
	 * 查询通道列表
	 * @param carrierOperator
	 * @return
	 */
	@ApiOperation("查询通道列表")
	@GetMapping(value="/findFlowChannel/{carrierOperator}")
	public FrontResponse findFlowChannel(@PathVariable("carrierOperator") Integer carrierOperator)
	{
		return FrontResponse.success().setData(flowMgtService.findChannelByCarrierOperator(carrierOperator));
	}
	
	/**
	 * 修改运营商通道状态
	 * @param ids
	 * @return
	 */
	@ApiOperation("修改运营商通道状态")
	@PostMapping("/modifyFlowChannelStatus")
	public FrontResponse modifyFlowChannelStatus(@RequestParam("ids") String ids, @RequestParam("status") Integer status,  @RequestParam(value = "remark",required = false) String remark)
	{
			String []idString=ids.split(",");
			Integer []idInteger=new Integer[idString.length];
			for(int i=0;i<idString.length;i++){
				idInteger[i]=Integer.valueOf(idString[i]);
			}
			flowMgtService.modifyFlowChannelStatus(idInteger,status,remark);
			return FrontResponse.success();
	}
	
	/**
	 * 查询订单列表
	 * @param orderNo
	 * @param orderPhone
	 * @param orderStatus
	 * @param submitStatus
	 * @param rechargeStatus
	 * @param orderStartTime
	 * @param orderEndTime
	 * @param customeService
	 * @param page
	 * @param size
	 * @return
	 */
	@ApiOperation("查询订单列表")
	@GetMapping(value="/findOrder")
	public FrontResponse findOrder(@RequestParam(required = false,value="orderNo") String orderNo,@RequestParam(required = false,value="orderPhone") String orderPhone,
								   @RequestParam(required = false,value="orderStatus") Integer orderStatus,@RequestParam(required = false,value="submitStatus") Integer submitStatus,
								   @RequestParam(required = false,value="rechargeStatus") Integer rechargeStatus,@RequestParam(required = false,value="orderStartTime") String orderStartTime,
								   @RequestParam(required = false,value="orderEndTime") String orderEndTime,@RequestParam(required = false,value="customeService") Integer customeService,
								   @RequestParam("page") Integer page,@RequestParam("size") Integer size)
	{
		Map<String,Object> params=new HashMap<>();
		params.put("orderNo",orderNo);
		params.put("orderPhone",orderPhone);
		params.put("orderStatus",orderStatus);
		params.put("submitStatus",submitStatus);
		params.put("rechargeStatus",rechargeStatus);
		params.put("orderStartTime",orderStartTime);
		params.put("orderEndTime",orderEndTime);
		params.put("customeService",customeService);
		params.put("page",page);
		params.put("size",size);
		List<FlowOrder> flowOrders=orderService.findOrder(params);
		PageInfo<FlowOrder> pageInfo=new PageInfo<FlowOrder>(flowOrders);
		Map<String,Object> data=new HashMap<>();
		data.put("hasMore",pageInfo.isHasNextPage());
		data.put("content",pageInfo.getList());
		data.put("total",pageInfo.getTotal());
		return FrontResponse.success().setData(data);
	}

	/**
	 * 导出订单列表
	 * @param orderNo
	 * @param orderPhone
	 * @param orderStatus
	 * @param submitStatus
	 * @param rechargeStatus
	 * @param orderStartTime
	 * @param orderEndTime
	 * @param customeService
	 * @return
	 */
	@ApiOperation("导出订单列表")
	@GetMapping("/exportOrderToExcel")
	public void exportOrderToExcel(@RequestParam(required = false) String orderNo,@RequestParam(required = false) String orderPhone,
								   @RequestParam(required = false) Integer orderStatus,@RequestParam(required = false) Integer submitStatus,
								   @RequestParam(required = false) Integer rechargeStatus,@RequestParam(required = false) String orderStartTime,
								   @RequestParam(required = false) String orderEndTime,@RequestParam(required = false) Integer customeService,@ApiIgnore  HttpServletResponse response) throws Exception
	{
		Map<String,Object> params=new HashMap<>();
		params.put("orderNo",orderNo);
		params.put("orderPhone",orderPhone);
		params.put("orderStatus",orderStatus);
		params.put("submitStatus",submitStatus);
		params.put("rechargeStatus",rechargeStatus);
		params.put("orderStartTime",orderStartTime);
		params.put("orderEndTime",orderEndTime);
		params.put("customeService",customeService);
		List<FlowOrder> flowOrders = orderService.findOrder(params);
		ExcelUtil<FlowOrder> excelUtil = new ExcelUtil<FlowOrder>(FlowOrder.class);
		response.setContentType("application/vnd.ms-excel;charset=GBK");
		response.setHeader("Content-Disposition", "attachment; filename=order.xls");
		excelUtil.getListToExcel(flowOrders, "", response.getOutputStream());
	}
	
	/**
	 * 修改订单客服标识
	 * @param orderId
	 * @param customeService
	 * @return
	 */
	@ApiOperation("修改订单客服标识")
	@PostMapping(value="/modifyOrderCustome")
	public FrontResponse modifyOrderCustome(@RequestParam String orderId,@RequestParam Integer customeService )
	{
		orderService.modifyOrderCustome(orderId, customeService);
		return FrontResponse.success();
	}
}
