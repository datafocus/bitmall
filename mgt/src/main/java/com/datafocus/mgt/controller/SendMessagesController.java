package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.service.service.MpMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 微信高级群发控制器
 *
 * @author yyhinfo
 */
@Api(description = "微信高级群发控制器")
@RestController
@RequestMapping("/sendMessages")
public class SendMessagesController {

    private static final Logger logger = LoggerFactory.getLogger(SendMessagesController.class);
    @Autowired
    private MpMessageService mpMessageService;

    /**
     * 发送群发消息
     *
     * @return
     */
    @ApiOperation("发送群发消息")
    @PostMapping(value = "/send")
    public FrontResponse send(@RequestParam("msg") String msg, @RequestParam("target") String target,
                              @RequestParam("msgType") String msgType, @RequestParam("targetGroupId") String targetGroupId) {
        mpMessageService.send(msg, target, msgType, targetGroupId);
        return FrontResponse.success();
    }

    @ApiOperation("群发消息预览")
    @GetMapping(value = "/preview")
    public FrontResponse preview(@RequestParam("wxName") String wxName, @RequestParam("openId") String openId,
                                 @RequestParam("msgType") String msgType, @RequestParam("msg") String msg) {
        mpMessageService.preview(wxName, openId, msgType, msg);
        return FrontResponse.success();
    }

}
