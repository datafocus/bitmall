package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.CustomTag;
import com.datafocus.common.pojo.CustomTagDimension;
import com.datafocus.service.service.CustomTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 自定义系统标签控制器
 *
 * @author yyhinfo
 */
@RestController
@Api(description = "自定义系统标签控制器")
@RequestMapping("/customTag")
public class CustomTagController {

    @Autowired
    private CustomTagService customTagService;

    @GetMapping("/list")
    @ApiOperation("标签列表")
    public FrontResponse list() {
        List<CustomTag> customTags = customTagService.findCustomTags();
        return FrontResponse.success().setData(customTags);
    }

    @ApiOperation("标签维度列表")
    @GetMapping("/customTagDimensions")
    public FrontResponse customTagDimensions() {
        List<CustomTagDimension> customTagDimensions = customTagService.findCustomTagDimension();
        return FrontResponse.success().setData(customTagDimensions);
    }

}
