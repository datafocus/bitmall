package com.datafocus.mgt.controller;

import java.util.Arrays;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.service.mapper.CpsMapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Api(description = "cps处理器")
@RestController
@RequestMapping("/cps")
public class CpsController {

	
	@Autowired
	private CpsMapper cpsMapper;
	
	/**
	 * cps 统计
	 * @param sceneId
	 * @param cardId
	 * @return
	 */
	@GetMapping(value="/get/{sceneId}/{cardId}")
	public FrontResponse get(@PathVariable("sceneId")String sceneId , @PathVariable("cardId") String[] cardId)
	{
		return FrontResponse.success().setData(cpsMapper.cube(sceneId, Arrays.asList(cardId)));
//		return "{\"fansNum\":1000,\"orderCount\":650,\"orderSuccessCount\":645,\"orderFailCount\":5,\"orderSum\":18002}";
	}
}
