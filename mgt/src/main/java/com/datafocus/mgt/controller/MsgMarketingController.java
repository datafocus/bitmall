package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.pojo.MsgMarketingStatistics;
import com.datafocus.service.service.MsgMarketingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by layne on 2017/12/5.
 */
@RequestMapping("/msgMarketingActivity")
@RestController
public class MsgMarketingController {
    @Autowired
    MsgMarketingService msgMarketingService;

    @PostMapping("/addMsgMarketing")
    public FrontResponse addMsgMarketing(@RequestBody MsgMarketingStatistics msgMarketingStatistics) {
        msgMarketingService.addMsgMarketing(msgMarketingStatistics);
        return FrontResponse.success();
    }

    @PostMapping("/updateMsgMarketing")
    public FrontResponse updateMsgMarketing(@RequestBody MsgMarketingStatistics msgMarketingStatistics) {
        msgMarketingService.update(msgMarketingStatistics);
        return FrontResponse.success();
    }

    @PostMapping("/deleteMsgMarketing")
    public FrontResponse delete(Long id) {
        msgMarketingService.delete(id);
        return FrontResponse.success();
    }

    @GetMapping("/findAll")
    public FrontResponse findAll(MsgMarketingStatistics msgMarketingStatistics, Integer cycle, Integer page, Integer size) {
        if (page == null) page = 1;
        if (size == null) size = 20;
        return FrontResponse.success().setData(msgMarketingService.findAll(msgMarketingStatistics, cycle, page, size));
    }

    @GetMapping("/findAllPlatformOperator")
    public FrontResponse findAllPlatformOperator() {
        return FrontResponse.success().setData(msgMarketingService.findAllPlatformOperator());
    }

    @GetMapping("/findAllActivityType")
    public FrontResponse findAllActivityType() {
        return FrontResponse.success().setData(msgMarketingService.findAllActivityType());
    }

    @GetMapping("/addActivityType")
    public FrontResponse addActivityType(String type) {
        return FrontResponse.success().setData(msgMarketingService.addActivityType(type));
    }

    @GetMapping("/findById")
    public FrontResponse findById(Long id) {
        return FrontResponse.success().setData(msgMarketingService.findOne(id));
    }

    @Autowired
    ObjectMapper objectMapper;

    @GetMapping("/findAllProvinceCity")
    public FrontResponse findAllProvinceCity() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        restTemplate.setMessageConverters(converters);
        String s = restTemplate.getForObject("https://wukongcz.com/product/findAllProvinceCity", String.class);
        return objectMapper.readValue(s, FrontResponse.class);
    }
}
