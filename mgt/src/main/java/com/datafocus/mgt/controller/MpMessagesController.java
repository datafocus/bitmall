package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.exception.ServiceException;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.message.MpMessages;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@Api(description = "群发消息")
@RestController
@RequestMapping("/mpMessages")
public class MpMessagesController {

    @Autowired
    private MpMessages mpMessages;

    /**
     * 高级群发
     *
     * @return
     */
    @ApiOperation("高级群发")
    @PostMapping("/sendMessage")
    public FrontResponse sendMessage(
            @RequestParam String target,
            @RequestParam(value = "targetGroup", required = false) Integer targetGroup,
            @RequestParam String messageType,
            @RequestParam String message) {

        // 这里群发消息，targetGroup就是微信tarid
        switch (messageType) {
            case "0":
                //文字消息
                if ("-1".equalsIgnoreCase(target)) {
                    //发送所有人
                    mpMessages.text(message);
                } else {
                    //按分组标签发送
                    mpMessages.text(targetGroup, message);
                }
                break;
            case "1":
                //图文素材
                if ("-1".equalsIgnoreCase(target)) {
                    //发送给所有人
                    mpMessages.mpNews(message);
                } else {
                    //按分组标签发送
                    mpMessages.mpNews(targetGroup, message);
                }
                break;
            case "2":
                //图片
                if ("-1".equalsIgnoreCase(target)) {
                    //发送给所有人
                    mpMessages.image(message);
                } else {
                    //按分组标签发送
                    mpMessages.image(targetGroup, message);
                }
                break;
            case "3":
                //卡券
                if ("-1".equalsIgnoreCase(target)) {
                    //发送给所有人
                    mpMessages.card(message);
                } else {
                    //按分组标签发送
                    mpMessages.card(targetGroup, message);
                }
                break;
            default:
                break;
        }
        return FrontResponse.success();
    }

    /**
     * 高级群发预览
     *
     * @param wxName
     * @param openId
     * @param msgType
     * @param message
     * @return
     */
    @ApiOperation("高级群发预览")
    @PostMapping("/preview")
    public FrontResponse preview(@RequestParam String wxName,
                                 @RequestParam String openId,
                                 @RequestParam String msgType,
                                 @RequestParam String message) {
        switch (msgType) {
            case "0":
                //文字消息
                mpMessages.textPreview(wxName, openId, message);
                break;
            case "1":
                //图文消息
                mpMessages.mpNewsPreview(wxName, openId, message);
                break;
            case "2":
                //图片
                mpMessages.imagePreview(wxName, openId, message);
                break;
            case "3":
                //卡券
                mpMessages.cardPreview(wxName, openId, message);
                break;
            default:
                throw new ServiceException(FrontResponseCode.NULL_PARAM.getCode(), "无效的消息类型");
        }
        return FrontResponse.success();
    }

}
