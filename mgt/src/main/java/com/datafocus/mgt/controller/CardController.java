package com.datafocus.mgt.controller;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.dto.CardDto;
import com.datafocus.common.dto.CardDtoMgt;
import com.datafocus.common.exception.ServiceException;
import com.datafocus.common.pojo.CardAcceptCategorys;
import com.datafocus.common.pojo.DistrictData;
import com.datafocus.common.pojo.PubDistrict;
import com.datafocus.service.service.CardService;
import com.datafocus.service.service.remote.DistrictService;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.card.Cards;
import com.datafocus.weixin.mp.card.bean.Card;
import com.datafocus.weixin.mp.card.bean.CardDataCube;
import com.datafocus.weixin.mp.media.Materials;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 卡券处理器
 *
 * @author yyhinfo
 */
@Api(description = "卡券处理器")
@RestController
@RequestMapping("/card")
public class CardController {
    @Autowired
    private Cards cards;
    @Autowired
    private Materials materials;

    @Autowired
    private CardService cardService;


    ;


    /**
     * 创建卡券
     *
     * @param cardType
     * @param card
     * @param cardAcceptCategorys
     * @param imageUrl
     * @param logoUrl
     * @return
     * @throws IOException
     */
    @ApiOperation("创建卡券")
    @PostMapping("/addCard/{cardType}")
    public FrontResponse addCard(@PathVariable("cardType") String cardType,
                                 Card card, CardAcceptCategorys cardAcceptCategorys,
                                 @RequestParam("imageUrl") MultipartFile imageUrl,
                                 @RequestParam("logoUrl") MultipartFile logoUrl) throws IOException {
        //上传卡券logo
        String url = materials.addMpNewsImage(logoUrl.getInputStream(), logoUrl.getOriginalFilename());

        String imgUlr = materials.addMpNewsImage(imageUrl.getInputStream(), imageUrl.getOriginalFilename());

        String cardId = null;
        if ("DISCOUNT".equals(cardType))//折扣券
        {
            card.getDiscount().getBaseInfo().setLogoUrl(url);
            card.getDiscount().getAdvancedInfo().getTextImageList().get(0).setImageUrl(imgUlr);
            cardId = cards.discount(card.getDiscount());
            card.getDiscount().getBaseInfo().setId(cardId);
        } else if ("CASH".equals(cardType))//代金券
        {
            card.getCash().getBaseInfo().setLogoUrl(url);
            card.getCash().getAdvancedInfo().getTextImageList().get(0).setImageUrl(imgUlr);
            cardId = cards.cash(card.getCash());
            card.getCash().getBaseInfo().setId(cardId);
        } else if ("GENERAL_COUPON".equals(cardType))//优惠券
        {
            card.getCoupon().getAdvancedInfo().getTextImageList().get(0).setImageUrl(imgUlr);
            card.getCoupon().getBaseInfo().setLogoUrl(url);
            cardId = cards.coupon(card.getCoupon());
            card.getCoupon().getBaseInfo().setId(cardId);
        } else
            throw new ServiceException(FrontResponseCode.NULL_PARAM.getCode(), "卡券参数错误");
        // 保存卡券到本地数据库
        cardService.saveCard(card, cardAcceptCategorys);
        return FrontResponse.success();
    }

    /**
     * 卡券id列表
     * 原接口 {@link /list}
     *
     * @return
     */
    @ApiOperation("卡券id列表")
    @GetMapping("/listCardId")
    public FrontResponse listCardId() {
        return FrontResponse.success().setData(cardService.listCardId());
    }

    /**
     * 卡券列表分页后端
     * 原接口 {@link /list}
     *
     * @return
     */
    @ApiOperation("卡券列表分页后端,原来的 /card/list接口")
    @GetMapping("/listCard")
    public FrontResponse list(FlowPage page) {
        if (null == page) page = FlowPage.defaultPage();
        PageInfo<CardDtoMgt> cardDtoPageInfo = cardService.listCard(page);
        Map<String, Object> data = new HashMap<>();
        data.put("hasMore", cardDtoPageInfo.isHasNextPage());
        data.put("count", cardDtoPageInfo.getTotal());
        data.put("content", cardDtoPageInfo.getList());
        return FrontResponse.success().setData(data);
    }

    /**
     * 删除卡券
     *
     * @param cardId 卡券id
     * @return
     */
    @ApiOperation("删除卡券")
    @PostMapping("/deleteCard")
    public FrontResponse delete(@RequestParam("cardId") String cardId) {
        cardService.deleteCard(cardId);
        return FrontResponse.success();
    }

    /**
     * 修改卡券库存
     *
     * @param cardId
     * @param increaseStock
     * @param reduceStock
     * @return
     */
    @ApiOperation("修改卡券库存")
    @PostMapping("/modifyStock")
    public FrontResponse modifyStock(@RequestParam("cardId") String cardId,
                                     @RequestParam(defaultValue = "0",value = "increaseStock") Integer increaseStock,
                                     @RequestParam(defaultValue = "0",value = "reduceStock") Integer reduceStock) {
        cardService.modifyStock(cardId, increaseStock, reduceStock);
        return FrontResponse.success();
    }

    /**
     * 根据openId查询已领取的卡券
     *
     * @param openId
     * @return
     */
    @ApiOperation("根据openId查询已领取的卡券")
    @PostMapping("/cardByOpenId")
    public FrontResponse cardByOpenId(@RequestParam("openId") String openId, FlowPage page) {
        if (null == page) page = FlowPage.defaultPage();
        PageInfo<CardDtoMgt> cardDtoPageInfo = cardService.list(openId, page);
        Map<String, Object> data = new HashMap<>();
        data.put("hasMore", cardDtoPageInfo.isHasNextPage());
        data.put("count", cardDtoPageInfo.getTotal());
        data.put("content", cardDtoPageInfo.getList());
        return FrontResponse.success().setData(data);
    }

    /**
     * 本商户的总体数据情况，包括时间区间内的各指标总量。
     *
     * @param beginDate 查询数据的起始时间。
     * @param endDate   查询数据的截至时间。
     * @return
     */
    @ApiOperation("本商户的总体数据情况，包括时间区间内的各指标总量。")
    @GetMapping("/getCardBizuIninfo")
    public FrontResponse getCardBizuIninfo(@RequestParam @ApiParam("起始日期") String beginDate, @RequestParam @ApiParam("结束日期") String endDate) {
        List<CardDataCube> cardDataCubes = cards.getCardBizuinInfo(beginDate, endDate, 1);
        return FrontResponse.success().setData(cardDataCubes);
    }

    /**
     * 调用该接口拉取免费券（优惠券、团购券、折扣券、礼品券）在固定时间区间内的相关数据。
     *
     * @param cardId    卡券ID
     * @param beginDate 查询数据的起始时间。
     * @param endDate   查询数据的截至时间。
     * @return
     */
    @ApiOperation("调用该接口拉取免费券（优惠券、团购券、折扣券、礼品券）在固定时间区间内的相关数据。")
    @GetMapping("/getCardCardInfo")
    public FrontResponse getCardCardInfo(String cardId, String beginDate, String endDate) {
        List<CardDataCube.CardInfoDataCube> cardInfoDataCubes = cards.getCardCardInfo(cardId, beginDate, endDate, 1, true);
        return FrontResponse.success().setData(cardInfoDataCubes);
    }

}
