package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.dto.TemplateSendReq;
import com.datafocus.mgt.service.impl.ActiveTemplateService;
import com.datafocus.service.service.TemplateMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/template")
public class TemplateController {

    @Autowired
    private ActiveTemplateService activeTemplateService;

    @PostMapping("/send")
    public FrontResponse send(@RequestBody TemplateSendReq sendReq){
        activeTemplateService.sendMarket(sendReq);
        return FrontResponse.success();
    }
}
