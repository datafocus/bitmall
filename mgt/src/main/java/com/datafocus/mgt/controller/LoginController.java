package com.datafocus.mgt.controller;

import com.datafocus.common.domain.response.FrontResponse;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.pojo.Account;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(description = "登录入口")
@RestController
public class LoginController {

    /**
     * 登录
     *
     * @return
     */
    @ApiOperation("登录")
    @PostMapping(value = "/login")
    public FrontResponse login(@RequestBody Account account, HttpServletResponse response) {
        try {
            if (StringUtils.isEmpty(account.getAccountName()) || StringUtils.isEmpty(account.getPassword())) {
                return FrontResponse.response().setCode(FrontResponseCode.NULL_PARAM.getCode()).setMessage("帐号或者密码为空！");
            }
            Subject user = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(account.getAccountName(), account.getPassword());
            try {
                user.login(token);
            } catch (LockedAccountException lae) {
                token.clear();
                return FrontResponse.response().setCode(FrontResponseCode.ACCOUNT_LOCKED.getCode()).setMessage("用户已经被锁定不能登录，请与管理员联系！");
            } catch (ExcessiveAttemptsException e) {
                token.clear();
                return FrontResponse.response().setCode(FrontResponseCode.ACCOUNT_LOCKED.getCode()).setMessage("账号：" + account + " 登录失败次数过多,锁定10分钟!");
            } catch (AuthenticationException e) {
                token.clear();
                return FrontResponse.response().setCode(FrontResponseCode.WRONG_PASSWORD.getCode()).setMessage("用户或密码不正确");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return FrontResponse.response().setCode(FrontResponseCode.SYSTEM_ERROR.getCode()).setMessage("系统错误，请联系管理员");
        }
        return FrontResponse.success();
    }

    @GetMapping("/logout")
    public FrontResponse logout(HttpServletResponse response) {
        SecurityUtils.getSubject().logout();
        return FrontResponse.success();
    }

    /**
     * 权限不够返回
     *
     * @return
     */
    @GetMapping("/403")
    public FrontResponse permissions() {
        return FrontResponse.response().setMessage("权限不够！").setCode(FrontResponseCode.NO_PERMISIONS.getCode());
    }

    /**
     * 未登录返回
     *
     * @return
     */
    @GetMapping("/405")
    public FrontResponse loginFirst(HttpServletResponse response) {
        return FrontResponse.response().setMessage("您未登录，请先登录！").setCode(FrontResponseCode.NEED_LOGIN.getCode());
    }

}
