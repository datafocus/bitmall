package com.datafocus.mgt;

import com.datafocus.service.config.MybatisConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author layne
 * @date 17-7-21
 */
@SpringBootApplication
@ComponentScan({"com.datafocus.service","com.datafocus.mgt"})
@EnableEurekaClient
@EnableFeignClients({"com.datafocus.service.service.remote"})
@EnableScheduling
@AutoConfigureAfter(MybatisConfig.class)
public class MgtApplication {
    public static void main(String[] args) {
        SpringApplication.run(MgtApplication.class, args);
    }
}
