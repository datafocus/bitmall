package com.datafocus.service.config;

import com.datafocus.common.config.MapProperties;
import com.datafocus.common.config.MgtProperties;
import com.datafocus.common.config.ProtalProperties;
import com.datafocus.service.service.impl.DBAccessTokenHolder;
import com.datafocus.service.task.SmsSendThread;
import com.datafocus.service.utils.SmsClient;
import com.datafocus.weixin.common.decrypt.AesException;
import com.datafocus.weixin.common.decrypt.MessageDecryption;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.card.Cards;
import com.datafocus.weixin.mp.care.CareMessages;
import com.datafocus.weixin.mp.media.Materials;
import com.datafocus.weixin.mp.menu.Menus;
import com.datafocus.weixin.mp.message.MpMessages;
import com.datafocus.weixin.mp.ticket.Tickets;
import com.datafocus.weixin.mp.user.Tags;
import com.datafocus.weixin.pay.base.PaySetting;
import com.datafocus.weixin.pay.mp.Orders;
import com.datafocus.weixin.pay.redpack.RedPacks;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author layne
 * @date 17-7-24
 */
@Configuration
public class ServiceConfig {

    @Bean
    @Lazy
    @ConfigurationProperties("bit.protalProperties")
    public ProtalProperties protalProperties() {
        return new ProtalProperties();
    }

    @Bean
    @Lazy
    @ConfigurationProperties("bit.mgtProperties")
    public MgtProperties mgtProperties() {
        return new MgtProperties();
    }

    @Bean
    @Lazy
    @ConfigurationProperties("mapProperties")
    public MapProperties mapProperties() {
        return new MapProperties();
    }

    @Bean
    @Lazy
    public SmsClient smsClient(MgtProperties mgtProperties) {
        return new SmsClient(mgtProperties.getNotifyTemplateUrl());
    }

    @Bean
    @ConfigurationProperties("bit.wx.appSetting")
    public AppSetting appSetting() {
        return new AppSetting();
    }

    @Bean
    @ConfigurationProperties("bit.wx.paySetting")
    public PaySetting paySetting() {
        return new PaySetting();
    }

    @Bean
    @ConfigurationProperties("bit.wx.appSetting")
    public com.datafocus.weixin.open.base.AppSetting openAppSetting() {
        return new com.datafocus.weixin.open.base.AppSetting();
    }

    @Bean
    public Orders orders(PaySetting paySetting) {
        return Orders.with(paySetting);
    }

    @Bean
    public Cards cards(AppSetting appSetting) {
        return Cards.with(appSetting);
    }

    @Bean
    public Tickets tickets(AppSetting appSetting) {
        return Tickets.with(appSetting);
    }

    @Bean
    public Menus menus(AppSetting appSetting) {
        return Menus.with(appSetting);
    }

    @Bean
    public Materials materials(AppSetting appSetting) {
        return Materials.with(appSetting);
    }

    @Bean
    public MpMessages mpMessages(AppSetting appSetting) {
        return MpMessages.with(appSetting);
    }

    @Bean
    public RedPacks redPacks(PaySetting paySetting) {
        return RedPacks.with(paySetting);
    }

    @Bean
    public MessageDecryption messageDecryption(AppSetting appSetting) throws AesException {
        return new MessageDecryption(appSetting.getToken(), appSetting.getAesKey(), appSetting.getAppId());
    }

    @Bean
    public CareMessages careMessages(AppSetting appSetting) {
        return CareMessages.with(appSetting);
    }

    @Bean
    public Tags tags(AppSetting appSetting) {
        return Tags.with(appSetting);
    }
}
