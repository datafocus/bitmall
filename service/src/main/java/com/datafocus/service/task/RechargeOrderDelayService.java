package com.datafocus.service.task;

import java.util.concurrent.DelayQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



/**
 * 充值延时订单启动
 * @author yyhinfo
 *
 */

@Service
public class RechargeOrderDelayService {

	private static final Logger logger = LoggerFactory.getLogger(RechargeOrderDelayService.class);
	
	private boolean start=true;
	
	private OnDelayedListener listener;
	
	private DelayQueue<RechargeOrderDelayed> delayQueue = new DelayQueue<RechargeOrderDelayed>();
	
	public void start(OnDelayedListener listener){
		
		if(listener==null){
			throw new  NullPointerException("Cannot set OnDelayedListener to null  ");
		}
		
		if(start){
			return ;
		}
		
		this.listener = listener;

		logger.info("RechargeOrderDelayService 充值延时订单处理已启动 ");
		
		start = true;
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					while(true){
						RechargeOrderDelayed rechargeOrderDelayed = delayQueue.take();
						if(RechargeOrderDelayService.this.listener != null){
							RechargeOrderDelayService.this.listener.onRechargeDelayedArrived(rechargeOrderDelayed);
						}
					}
				} catch (Exception e) {
					logger.error("充值延时订单处理系统异常",e);
				}
			}
		}).start();
	}
	
	
	public void add(RechargeOrderDelayed rechargeOrderDelayed)
	{
		delayQueue.put(rechargeOrderDelayed);
	}
	
	public boolean remove(RechargeOrderDelayed rechargeOrderDelayed)
	{
		return delayQueue.remove(rechargeOrderDelayed);
	}
	
	public void add(String orderNo)
	{
		delayQueue.put(new RechargeOrderDelayed(orderNo, 1));
	}
	
	public void remove(String orderNo)
	{
		RechargeOrderDelayed [] array = delayQueue.toArray(new RechargeOrderDelayed[]{});
		if(array == null || array.length <= 0){
			return ;
		}
		RechargeOrderDelayed target =null;
		
		for(RechargeOrderDelayed rechargeOrder : array){
			if(rechargeOrder.getOrderNo().equals(orderNo)){
				target = rechargeOrder;
				break;
			}
		}
		
		if(null != target){
			delayQueue.remove(target);
		}
	}
}
