package com.datafocus.service.task;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class UnTagDelayed implements Delayed {

	private int tagId; //微信标签id
	
	private long msgId; //群发消息id
	
	private long excuteTime;//执行时间
 
	public UnTagDelayed(int tagId, long msgId, long delayTime) {
		super();
		this.tagId = tagId;
		this.msgId = msgId;
		this.excuteTime = TimeUnit.NANOSECONDS.convert(delayTime, TimeUnit.MILLISECONDS) + System.nanoTime();
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public long getExcuteTime() {
		return excuteTime;
	}

	public void setExcuteTime(long excuteTime) {
		this.excuteTime = excuteTime;
	}

	@Override
	public int compareTo(Delayed other) {
		if (other == this) {
			return 0;
		}
		if (other instanceof UnTagDelayed) {
			UnTagDelayed otherRequest = (UnTagDelayed) other;
			long otherStartTime = otherRequest.getExcuteTime();
			return (int) (this.excuteTime - otherStartTime);
		}
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		 return  unit.convert(this.excuteTime - System.nanoTime(), TimeUnit.NANOSECONDS);     
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Long.valueOf(msgId) ^ (Long.valueOf(msgId) >>> 32));
		result = prime * result + (int) (excuteTime ^ (excuteTime >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnTagDelayed other = (UnTagDelayed) obj;
		if (msgId != other.msgId)
			return false;
		if (excuteTime != other.excuteTime)
			return false;
		return true;
	}

}
