package com.datafocus.service.task;

import com.datafocus.common.pojo.BillOrder;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.service.mapper.BillOrderMapper;
import com.datafocus.service.mapper.FlowOrderMapper;
import com.datafocus.service.service.impl.TemplateMsgServiceImpl;
import com.datafocus.service.utils.BaseResult;
import com.datafocus.weixin.pay.mp.Orders;
import com.datafocus.weixin.pay.mp.bean.RefundQuery;
import com.datafocus.weixin.pay.mp.bean.RefundQuery.Refund;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class OnDelayedListenerImpl implements OnDelayedListener {

	private static final Logger logger = LoggerFactory.getLogger(OnDelayedListenerImpl.class);

	@Autowired
	private Orders orders;
	
	@Autowired
	private FlowOrderMapper flowOrderMapper;
	
	@Autowired
	private RefundOrderDelayService delayService;
	
	@Autowired
	private TemplateMsgServiceImpl template;

	@Autowired
	private BillOrderMapper billOrderMapper;


	
	@Override
	public void onRefundDelayedArrived(RefundOrderDelayed refund)
	{
		String orderNo = refund.getOrderNo();
		if(refund.getType()==1){
			FlowOrder order  =  flowOrderMapper.findOrderByOrderNo(orderNo);
			//先从数据库查询该订单的退款状态，判断是已退款。退款状态已返回，不用查询，否则调用微信远程接口查询
			if(order!=null &&  order.getCustomeService()==4){

				int status = 4; //退款中

				RefundQuery refundQuery = orders.refundQueryByTradeNumber(orderNo);

				List<Refund> refundList = refundQuery.getRefunds();

				HashMap<String, Object> params = new HashMap<>();
				params.put("orderNo", orderNo);
				for (Refund t : refundList)
				{
					String refundStatus = t.getRefundStatus();
					if("SUCCESS".equals(refundStatus)){//退款成功
						status = 6;
						params.put("refundStatus", status);
						flowOrderMapper.modify(params);
					}else if("FAIL".equals(refundStatus)){//退款失败
						status = 5;
						params.put("refundStatus", status);
						flowOrderMapper.modify(params);
					}else if("PROCESSING".equals(refundStatus)){//退款中
						refund.setCurrentCount(refund.getCurrentCount()+1);
						int timeout = RefundOrderDelayed.startTimeMap.get(refund.getCurrentCount());
						refund.setStartTime(System.currentTimeMillis() + timeout);
						delayService.add(refund);
					}else if("CHANGE".equals(refundStatus)){//退款账户异常
						logger.error("比特星人订单号:{},退款账户异常,—转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。",orderNo);
					}else{
						delayService.add(refund);
					}
				}
			}
		}else{
			BillOrder order  =  billOrderMapper.findBillOrderByOrderNo(orderNo);
			//先从数据库查询该订单的退款状态，判断是已退款。退款状态已返回，不用查询，否则调用微信远程接口查询
			if(order!=null &&  order.getRefundState()==1){
				int refundState = 1; //退款中
				RefundQuery refundQuery = orders.refundQueryByTradeNumber(orderNo);
				List<Refund> refundList = refundQuery.getRefunds();
				HashMap<String, Object> params = new HashMap<>();
				params.put("orders", Arrays.asList(orderNo));
				for (Refund t : refundList)
				{
					String refundStatus = t.getRefundStatus();
					if("SUCCESS".equals(refundStatus)){//退款成功
						refundState = 2;
					}else if("FAIL".equals(refundStatus)){//退款失败
						refundState = 3;
						params.put("refundState", refundState);
					}else if("PROCESSING".equals(refundStatus)){//退款中
						refund.setCurrentCount(refund.getCurrentCount()+1);
						int timeout = RefundOrderDelayed.startTimeMap.get(refund.getCurrentCount());
						refund.setStartTime(System.currentTimeMillis() + timeout);
						delayService.add(refund);
					}else if("CHANGE".equals(refundStatus)){//退款账户异常
						//TODO  还没有来的及写，反正也不重要
						logger.error("比特星人订单号:{},退款账户异常,—转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。",orderNo);
					}else{
						delayService.add(refund);
					}
					//更新数据库状态
					params.put("refundState", refundState);
					billOrderMapper.batchModify(params);
				}
			}
		}

	}

	@Override
	public void onRechargeDelayedArrived(RechargeOrderDelayed rechargeOrder)
	{
		String orderNo =  rechargeOrder.getOrderNo();
		
		//先从数据库查询该订单的充值状态，判断一次，如果订单是发送中则发送延时通知消息
		FlowOrder order  =  flowOrderMapper.findOrderByOrderNo(orderNo);
		
		if(order!=null && order.getRechargeStatus()==0){
			//0:未充值
			//充值超过延时时间发送模板消息提示用户
			BaseResult result = template.sendTempletMsg(orderNo, "3");
			logger.info("充值延时订单模板消息发送状态:{}",result.toString());
		}
	}

}
