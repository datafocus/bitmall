package com.datafocus.service.task;


import com.datafocus.service.task.RechargeOrderDelayed;
import com.datafocus.service.task.RefundOrderDelayed;

public interface OnDelayedListener {

	/**
	 * 退款延时订单处理
	 * @param refund
	 */
	void onRefundDelayedArrived(RefundOrderDelayed refund);
	
	/**
	 * 充值延时订单处理(订单充值之后超过10分钟之后、推送订单延时通知)
	 * @param rechargeOrder
	 */
	void onRechargeDelayedArrived(RechargeOrderDelayed rechargeOrder);
}
