package com.datafocus.service.task;

import com.datafocus.service.utils.SmsClient;
import com.datafocus.service.utils.SpringUtils;
import com.datafocus.weixin.common.util.JsonMapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

public class SmsSendThread extends Thread {
    /**
     * 日志句柄
     */
    private static final Logger logger = LoggerFactory.getLogger(SmsSendThread.class);
    private String orderId;
    private String msgType;

    public SmsSendThread(String orderId, String msgType) {
        this.orderId = orderId;
        this.msgType = msgType;
    }

    @Override
    public void run() {
        try {
            String result = SpringUtils.getApplicationContext().getBean(SmsClient.class).sendSms(orderId, msgType);
            BaseResult baseResult = JsonMapper.defaultMapper().fromJson(result, BaseResult.class);
            if (!"0".equals(baseResult.getCode())) {
                logger.error("发送微信模板消息错误:" + baseResult.toString());
            }

        } catch (Exception e) {
            logger.error("发送模板消息系统异常", e);
        }
    }

    public static class BaseResult implements Serializable{

        /**
         *
         */
        private static final long serialVersionUID = 729124220375898092L;

        @JsonProperty("errcode")
        private String code;
        @JsonProperty("errmsg")
        private String msg;


        public BaseResult(){};

        public BaseResult(String code ,String msg){
            this.code= code;
            this.msg = msg;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @Override
        public String toString() {
            return "BaseResult [code=" + code + ", msg=" + msg + "]";
        }
    }

}
