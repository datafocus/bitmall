package com.datafocus.service.task;

import java.util.concurrent.DelayQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


/**
 * 退款延时订单启动
 * @author yyhinfo
 *
 */

@Service
public class RefundOrderDelayService {

	private static final Logger logger = LoggerFactory.getLogger(RefundOrderDelayService.class);

	private boolean start;

	private OnDelayedListener listener;

	private DelayQueue<RefundOrderDelayed> delayQueue = new DelayQueue<RefundOrderDelayed>();
 

	public void start(OnDelayedListener listener) 
	{
		if(listener==null){
			throw new  NullPointerException("Cannot set OnDelayedListener to null  ");
		}
		if (start) {
			return;
		}
		logger.info("RefundOrderDelayService  退款查询服务已启动");
		
		start = true;
		
		this.listener = listener;
		
		new Thread(new Runnable() {
			public void run() {
				try {
					while (true) {
						RefundOrderDelayed refund = delayQueue.take();
						if (RefundOrderDelayService.this.listener != null) {
							RefundOrderDelayService.this.listener.onRefundDelayedArrived(refund);
						}
					}
				} catch (Exception e) {
					logger.error("退款延时订单处理异常",e);
				}
			}
		}).start();
	}

	public void add(RefundOrderDelayed refund)
	{
		delayQueue.put(refund);
	}

	public boolean remove(RefundOrderDelayed refund)
	{
		return delayQueue.remove(refund);
	}

	public void remove(String orderId)
	{
		RefundOrderDelayed[] array = delayQueue.toArray(new RefundOrderDelayed[] {});
		if (array == null || array.length <= 0) {
			return;
		}
		RefundOrderDelayed target = null;
		for (RefundOrderDelayed order : array) {
			if (order.getOrderNo().equals(orderId)) {
				target = order;
				break;
			}
		}
		if (target != null) {
			delayQueue.remove(target);
		}
	}

}
