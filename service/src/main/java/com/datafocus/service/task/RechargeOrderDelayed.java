package com.datafocus.service.task;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class RechargeOrderDelayed implements Delayed {

	private String orderNo;
	
	private long startTime;
	
	
	/**
	 * 
	 * @param orderNo
	 * @param timeout
	 */
	public RechargeOrderDelayed(String orderNo,long timeout){
		this.orderNo = orderNo;
		this.startTime = System.currentTimeMillis()+timeout;
	}
	
	
	@Override
	public int compareTo(Delayed o) {
		if(o == this){
			return 0;
		}
		if(o instanceof RechargeOrderDelayed){
			RechargeOrderDelayed rod = (RechargeOrderDelayed) o;
			long otherStartTime = rod.getStartTime();
			return (int) (this.startTime - otherStartTime);
		}
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		return unit.convert(startTime-System.currentTimeMillis(),TimeUnit.MILLISECONDS);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Long.valueOf(orderNo) ^ (Long.valueOf(orderNo) >>> 32));
		result = prime * result + (int) (startTime ^ (startTime >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RechargeOrderDelayed other = (RechargeOrderDelayed) obj;
		if (orderNo == null) {
			if (other.orderNo != null)
				return false;
		} else if (!orderNo.equals(other.orderNo))
			return false;
		if (startTime != other.startTime)
			return false;
		return true;
	}
	
	public String getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}


	public long getStartTime() {
		return startTime;
	}


	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}


	@Override
	public String toString() {
		return "RechargeOrderDelayed [orderNo=" + orderNo + ", startTime="
				+ startTime + "]";
	}
	
}
