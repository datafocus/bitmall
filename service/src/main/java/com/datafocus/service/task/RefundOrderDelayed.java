package com.datafocus.service.task;

import java.util.HashMap;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class RefundOrderDelayed implements Delayed {

	private String orderNo;
	private Integer type; //1,流量 2,话费
	private long startTime;
	
	private Integer currentCount=1;

	//次数-出队相隔时间（毫秒）
	public static HashMap<Integer, Integer> startTimeMap = new HashMap<Integer, Integer>();
	
	static{
		startTimeMap.put(1,  15*1000);
		startTimeMap.put(2,  2*60*1000);
		startTimeMap.put(3,  20*60*1000);
		startTimeMap.put(4,  1*24*60*60*1000);
		startTimeMap.put(5,  1*24*60*60*1000);
		startTimeMap.put(6,  2*24*60*60*1000);
		startTimeMap.put(7,  2*24*60*60*1000);
	}

	/**
	 * @param orderNo:订单id 
	 * @param timeout：查询退款超时间
	 * */
	public RefundOrderDelayed(Integer type,String orderNo, int timeout) {
		this.type=type;
		this.orderNo = orderNo;
		this.startTime = System.currentTimeMillis() + timeout;
	}

	@Override
	public int compareTo(Delayed other) {
		if (other == this) {
			return 0;
		}
		if (other instanceof RefundOrderDelayed) {
			RefundOrderDelayed otherRequest = (RefundOrderDelayed) other;
			long otherStartTime = otherRequest.getStartTime();
			return (int) (this.startTime - otherStartTime);
		}
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		return unit.convert(startTime - System.currentTimeMillis(),
				TimeUnit.MILLISECONDS);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Long.valueOf(orderNo) ^ (Long.valueOf(orderNo) >>> 32));
		result = prime * result + (int) (startTime ^ (startTime >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RefundOrderDelayed other = (RefundOrderDelayed) obj;
		if (orderNo != other.orderNo)
			return false;
		if (startTime != other.startTime)
			return false;
		return true;
	}

	public Integer getType() {
		return type;
	}

	public RefundOrderDelayed setType(Integer type) {
		this.type = type;
		return this;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public Integer getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(Integer currentCount) {
		this.currentCount = currentCount;
	}

	@Override
	public String toString() {
		return "RefundOrderDelayed [orderNo=" + orderNo + ", startTime="
				+ startTime + ", currentCount=" + currentCount + "]";
	}
 
}
