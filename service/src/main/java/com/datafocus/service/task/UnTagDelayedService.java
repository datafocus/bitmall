package com.datafocus.service.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.DelayQueue;

@Service
public class UnTagDelayedService {

	
	private Logger logger = LoggerFactory.getLogger(UnTagDelayedService.class);
	
	private boolean start;
	
	private DelayQueue<UnTagDelayed> delayQueue = new DelayQueue<UnTagDelayed>();  
	
	private OnDelayedListener listener;
	
	public static interface OnDelayedListener{
		public void onDelayedArrived(UnTagDelayed unTagsDelayed);
	}
	
	
	public void start(OnDelayedListener listener){
		
		if(start){
			return;
		}
		
		this.listener = listener;
		
		logger.info("删除微信标签，延时任务启动");
		start = true;
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					while(true){
						UnTagDelayed unTag = delayQueue.take();
						OnDelayedListener listener = UnTagDelayedService.this.listener;
						if(listener!=null){
							listener.onDelayedArrived(unTag);
						}
					}
				} catch (InterruptedException e) {
					logger.error("延时删除微信标签，系统异常", e);
				}
			}
		}).start();
	}
	
	public void add(UnTagDelayed unTag){
		delayQueue.put(unTag);
	}
	
	public boolean remove(UnTagDelayed unTag){
		return delayQueue.remove(unTag);
	}
	
	public void  remove(long msgId){
		UnTagDelayed []  array = delayQueue.toArray(new UnTagDelayed[]{});
		
		if(array==null || array.length<=0){
			return;
		}
		UnTagDelayed target = null;
		
		for (UnTagDelayed o : array) {
			if(o.getMsgId()==msgId){
				target = o;
				break;
			}
		}
		
		if(target!=null){
			delayQueue.remove(target);
		}
	}
	
}
