package com.datafocus.service.utils;

import java.io.Serializable;

public class BaseResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 729124220375898092L;
	
	private String errcode;
	
	private String errmsg;

	
	public BaseResult(){}
	
	public BaseResult(String errcode ,String errmsg){
		this.errcode=errcode;
		this.errmsg= errmsg;
	}


	public String getErrcode() {
		return errcode;
	}


	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}


	public String getErrmsg() {
		return errmsg;
	}


	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	@Override
	public String toString() {
		return "BaseResult [errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}


 


 
	
	 

	
	
}
