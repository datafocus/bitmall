package com.datafocus.service.utils;

import com.datafocus.common.pojo.Account;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

public class PasswordHelper {
    public static void encryptPassword(Account account) {

        String algorithmName = "md5";

        int hashIterations = 2;

        String salt1 = account.getAccountName();

        String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();

        SimpleHash hash = new SimpleHash(algorithmName, account.getPassword(), salt1 + salt2, hashIterations);

        String encodedPassword = hash.toHex();

        account.setCredentialsSalt(salt2);

        account.setPassword(encodedPassword);
    }
}