package com.datafocus.service.utils;

import com.datafocus.common.utils.HttpUtils;
import com.datafocus.weixin.common.util.JsonMapper;
import com.datafocus.weixin.common.util.MD5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by liuheng on 2017/8/31.
 */
public class SmsUtils {
    private static Logger logger = LoggerFactory.getLogger(SmsUtils.class);

    public static Integer send(String content, String mobile, String url, String username, String password) {
        Integer result = 1;
        try {
            String json = HttpUtils.sendPost(url, buildSmsParams(content, mobile, username, password));
            Map<String, Object> resultMap = JsonMapper.defaultMapper().fromJson(json, HashMap.class);
            System.out.println(resultMap.get("msg"));
            if (resultMap != null && "0".equals(resultMap.get("code").toString())) {
                result = 0;
                logger.info("发送短信成功mobile=" + mobile);
            } else {
                logger.info("发送短信失败mobile=" + mobile);
            }
        } catch (Exception e) {
            logger.error("调用流量星短信服务异常mobile=" + mobile, e);
        }
        return result;
    }

    public static Map<String, String> buildSmsParams(String content, String mobile, String username, String password) {
        Map<String, String> result = new HashMap<String, String>();
        String timestamp = String.valueOf(System.currentTimeMillis());
        String sign = MD5.MD5Encode(username + password + timestamp).toUpperCase();
        result.put("timestamp", timestamp);
        result.put("sign", sign);
        result.put("account", username);
        result.put("content", content);
        result.put("mobile", mobile);
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        result.put("orderId", dateFormat.format(new Date()) + "_alarm");
        System.out.println(result.get("orderId"));
        return result;
    }

    /*public static void main(String[] args) {
        String content="【平台告警】流量星充值异常，微信消息查看详情，快去看看！！！";
        Integer a=SmsUtils.send(content,"15926599530","http://120.76.98.95:8281/sms/receive","10004","E3ZCL7E6XhIKuwtBLKVriE5EFog=");
        System.out.println(a);
    }*/
    public static MsgRes sendSms(String mobile, String content, String msgId) {
        String timestamp = String.valueOf(System.currentTimeMillis());

        String account = "10005";

        String sign = MD5.MD5Encode(account + "ItjWro/bnPWHndvKYd3f7ZciNXo=" + timestamp).toUpperCase();

        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(new ByteArrayHttpMessageConverter());
        converters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        converters.add(new ResourceHttpMessageConverter());
        converters.add(new SourceHttpMessageConverter<>());
        converters.add(new AllEncompassingFormHttpMessageConverter());
        converters.add(new MappingJackson2XmlHttpMessageConverter());
        converters.add(new MappingJackson2HttpMessageConverter());
        restTemplate.setMessageConverters(converters);

        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("account", account);
        params.add("timestamp", timestamp);
        params.add("sign", sign);
        params.add("mobile", mobile);
        params.add("content", content);
        params.add("orderId", msgId);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<String> httpEntity = new HttpEntity(params, headers);
        MsgRes msgRes = restTemplate.postForObject("http://120.76.98.95:8281/sms/receive", httpEntity, MsgRes.class);
        logger.info("==============短信发送成功，mobile={},content={},msgId={}",mobile,content,msgId);
        logger.info("==============短信发送成功，code={},msg={}",msgRes.getCode(),msgRes.getMsg());
        return msgRes;
    }

    public static class MsgRes {
        private String code;
        private String msg;

        public String getCode() {
            return code;
        }

        public MsgRes setCode(String code) {
            this.code = code;
            return this;
        }

        public String getMsg() {
            return msg;
        }

        public MsgRes setMsg(String msg) {
            this.msg = msg;
            return this;
        }
    }

}
