package com.datafocus.service.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis工具类
 * Created by layne on 17-6-11.
 */
@Component
public class RedisUtil<K extends Serializable, V extends Serializable> {

    @Autowired
    private RedisTemplate redisTemplate;

    private ValueOperations<K, V> valueOpts;

    @PostConstruct
    private void init() {
      /*  redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setStringSerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setKeySerializer(new StringRedisSerializer());*/
        valueOpts = redisTemplate.opsForValue();
    }

    /**
     * 更新或者存入
     *
     * @param key     键
     * @param value   值
     * @param timeout 过期时间 单位：秒
     */
    public void addOrUpdate(K key, V value, long timeout) {
        valueOpts.set(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * @param key
     * @param value
     * @param timeout
     * @param unit    时间单位
     */
    public void addOrUpdate(K key, V value, long timeout, TimeUnit unit) {
        valueOpts.set(key, value, timeout, unit);
    }


    /**
     * 没有过期时间的缓存
     *
     * @param key
     * @param value
     */
    public void addOrUpdate(K key, V value) {
        valueOpts.set(key, value);
    }

    /**
     * 获取值
     *
     * @param key
     * @return
     */
    public V getValue(K key) {
        return valueOpts.get(key);
    }

    /**
     * @param pattern
     * @return
     */
    public Set keys(K pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * 删除单个
     *
     * @param key
     */
    public void del(K key) {
        redisTemplate.delete(key);
    }

    /**
     * 删除多个
     *
     * @param pattern key的表达式
     */
    public void multipleDel(K pattern) {
        redisTemplate.delete(keys(pattern));
    }

    /**
     * 批量查询
     *
     * @param keys
     * @return
     */
    public List<V> multiGet(Set<K> keys) {
        return valueOpts.multiGet(keys);
    }

    ;
}
