package com.datafocus.service.utils;


import com.datafocus.common.utils.HttpUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class SmsClient implements ApplicationContextAware {

    /**
     * 日志句柄
     */
    private static final Logger logger = LoggerFactory.getLogger(SmsClient.class);

    private String notify_template_url;

    public SmsClient(String notify_template_url) {
        this.notify_template_url = notify_template_url;//微信模板消息地址
    }

    public String sendSms(String orderId, String msgType) throws Exception {
        HashMap<String, String> body = new HashMap<>();
        body.put("orderId", orderId);
        body.put("msgType", msgType);
        return HttpUtils.sendPost(notify_template_url, body);
    }

    public static String sign(String text, String key, String input_charset) {
        text = text + key;
        return DigestUtils.md5Hex(getContentBytes(text, input_charset));
    }

    public static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错??指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }
}