package com.datafocus.service.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;


public class DFUtils {
	
	private static PublicKey P_KEY=null;
	
	//话费面额
	private static HashMap<Integer, BigDecimal> amountMap = new HashMap<>();
	
	private static HashMap<Integer, BigDecimal> saleMap = new HashMap<>();
	
	static {
		amountMap.put(0, new BigDecimal(50));
		amountMap.put(1, new BigDecimal(100));
		amountMap.put(2, new BigDecimal(150));
		amountMap.put(3, new BigDecimal(200));
		amountMap.put(4, new BigDecimal(300));
		amountMap.put(5, new BigDecimal(500));
		saleMap.put(0, new BigDecimal(45));
		saleMap.put(1, new BigDecimal(90));
		saleMap.put(2, new BigDecimal(135));
		saleMap.put(3, new BigDecimal(175));
		saleMap.put(4, new BigDecimal(260));
		saleMap.put(5, new BigDecimal(430));
		
	}
	
	public static BigDecimal getAmountByProductId(Integer productId){
		return amountMap.get(productId);
	}
	
	public static BigDecimal getSaleByProductId(Integer productId){
		return saleMap.get(productId);
	}
	/**
	 * 生成商户订单号
	 * @param mchId  商户号
	 * @return
	 */
	public static String createBillNo(String mchId){		
		//组成： mch_id+YYYYMMddhhmm+10位一天内不能重复的数字
		Date dt=new Date();
		SimpleDateFormat df = new SimpleDateFormat("YYYYMMddhhmm");
		String nowTime= df.format(dt);		
		int length = 10 ;		
		return mchId + nowTime  + getRandomNum(length);
	}
	/**
	 * 生成特定位数的随机数字
	 * @param length
	 * @return
	 */
	public static String getRandomNum(int length) {
		String val = "";
		Random random = new Random();		
		for (int i = 0; i < length; i++) {
			val += String.valueOf(random.nextInt(10));
		}		
		return val;
	}
	
	// 获得客户端真实IP地址的方法二：
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-FORWARDED-FOR");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return null ==ip ?"" : ip.split(",")[0];
	}
	
	/*public static CarryOperator checkMobile(String line)
	{
	 
		Pattern py = Pattern.compile(RechargeProperties.getCmcc());
		Pattern py2 = Pattern.compile(RechargeProperties.getCucc());
		Pattern py3 = Pattern.compile(RechargeProperties.getCtcc());
		Matcher my = py.matcher(line.trim());
		Matcher my2 = py2.matcher(line.trim());
		Matcher my3 = py3.matcher(line.trim());
		if (my.matches()) {
			return CarryOperator.CMCC;
		} else if (my2.matches()) {
			return CarryOperator.CUCC;
		} else if (my3.matches()) {
			return CarryOperator.CTCC;
		} else {
			return CarryOperator.UNKNOWN;
		}
	}*/
 
    /**
     * 读取文件, 部署 web 程序的时候, 签名和验签内容需要从 request 中获得
     * @param filePath
     * @return
     * @throws Exception
     */
    public static String getStringFromFile(String fileName) throws Exception {
        InputStreamReader inReader =new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName),"UTF-8");
        BufferedReader bf = new BufferedReader(inReader);
        StringBuilder sb = new StringBuilder();
        String line;
        do {
            line = bf.readLine();
            if (line != null) {
                if (sb.length() != 0) {
                    sb.append("\n");
                }
                sb.append(line);
            }
        } while (line != null);

        return sb.toString();
    }
	/**
	 * 获得公钥
	 * @return
	 * @throws Exception
	 */
	public static PublicKey getPubKey() throws Exception {
		if(P_KEY==null)
		{
			String pubKeyString = DFUtils.getStringFromFile("pingpp_public_key.pem");
			
			pubKeyString = pubKeyString.replaceAll("(-+BEGIN PUBLIC KEY-+\\r?\\n|-+END PUBLIC KEY-+\\r?\\n?)", "");
			byte[] keyBytes = Base64.decodeBase64(pubKeyString.getBytes());
			// generate public key
			X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			P_KEY = keyFactory.generatePublic(spec);
		}
		return P_KEY;
	}
	
    public static InetAddress getInetAddress(){  
  	  
        try{  
            return InetAddress.getLocalHost();  
        }catch(UnknownHostException e){  
            System.out.println("unknown host!");  
        }  
        return null;  
  
    }  
  
    public static String getHostIp(InetAddress netAddress){  
        if(null == netAddress){  
            return null;  
        }  
        String ip = netAddress.getHostAddress(); //get the ip address  
        return ip;  
    }  
  
    public static String getHostName(InetAddress netAddress){  
        if(null == netAddress){  
            return null;  
        }  
        String name = netAddress.getHostName(); //get the host address  
        return name;  
    }
}
