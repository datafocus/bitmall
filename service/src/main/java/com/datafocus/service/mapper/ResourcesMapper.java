package com.datafocus.service.mapper;

import com.datafocus.common.pojo.Resources;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResourcesMapper {

    List<Resources> findAllResources();

    List<Resources> findAllResRoles(@Param("roleId") Integer roleId);

    List<Resources> findResourcesByAccountId(@Param("accountId") String accountId);
}
