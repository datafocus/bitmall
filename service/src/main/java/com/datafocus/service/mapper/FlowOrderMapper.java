package com.datafocus.service.mapper;

import com.datafocus.common.dto.OrderCountDto;
import com.datafocus.common.pojo.FlowOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.*;

@Repository
public interface FlowOrderMapper {

    /**
     * 根据openId查询历史充值记录
     *
     * @param openId
     * @return
     */
    List<FlowOrder> findOrderByOpenId(@Param("openId") String openId);

    /**
     * 查询用户最近30条充值记录
     *
     * @param openId
     * @return
     */
    List<FlowOrder> findFeeFlowOrderByOpenId(String openId);

    /**
     * 保存订单信息
     *
     * @param order
     */
    void save(FlowOrder order);

    /**
     * 更新订单，退款、支付、充值状态
     */
    void modify(HashMap<String, Object> param);

    /**
     * 查询订单
     *
     * @param orderNo 订单号
     * @return
     */
    FlowOrder findOrderByOrderNo(@Param("orderNo") String orderNo);

    /**
     * 查询退款中的订单
     *
     * @return
     */
    List<String> findOrderByRefund();

    /**
     * 查询已支付还未充值的订单
     *
     * @return
     */
    List<String> findOrderByOrderStatusAndSubmitStatus();

    /**
     * 查询订单列表
     */
    List<FlowOrder> findOrderByOrderNoOrOrderPhoneOrOrderStatus(Map<String, Object> param);

    /**
     * 修改订单客服标识
     *
     * @param orderId
     * @param customeService
     */
    void modifyOrderCustome(@Param("orderId") String orderId, @Param("customeService") int customeService);

    /**
     * 查找一段时间内某个用户某种充值状态的订单
     *
     * @param openId opendId
     * @param start  开始时间
     * @param end    结束时间
     * @param status 状态
     * @return
     */
    List<FlowOrder> findByOrderTimeBetween(@Param("openId") String openId, @Param("start") LocalDateTime start, @Param("end") LocalDateTime end, @Param("status") Integer status);

    /**
     * @param openId
     * @param startTime
     * @param endTime   @return
     */
    List<Map> findProductByOpneId(@Param("openId") String openId, @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

    /**
     * 更新订单利润
     *
     * @param orderNo
     * @param profit
     * @param v
     */
    void updateProfit(@Param("orderNo") String orderNo, @Param("profit") Double profit);

    /**
     * @param openIds
     * @param start
     * @param end
     * @return
     */
    List<FlowOrder> findAllByOpenIds(@Param("openIds") Set<String> openIds,
                                     @Param("start") Date start,
                                     @Param("end") Date end);

    /**
     * 统计每个用户订单数
     *
     * @param openIds
     * @return
     */
    List<OrderCountDto> countOrderByOpenids(@Param("openIds") Set<String> openIds);
}
