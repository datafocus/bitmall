package com.datafocus.service.mapper;

import com.datafocus.weixin.common.event.LocationReportEvent;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationMapper {
	
	/**
	 * 保存地理位置信息
	 * @param location
	 */
	void saveLocation(LocationReportEvent location);

}
