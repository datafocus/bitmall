package com.datafocus.service.mapper;

import com.datafocus.common.pojo.Promoter;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Repository
public interface PromoterMapper {
    /**
     * 根据openId查找
     *
     * @param openId
     * @return
     */
    Promoter findOneByOpenId(@Param("openId") String openId);

    /**
     * 更新推客信息
     *
     * @param promoter
     */
    void updateOne(Promoter promoter);

    /**
     * 保存
     *
     * @param promoter
     */
    void save(Promoter promoter);

    /**
     * 统计每个用户今年消费情况
     */
    List<Map> findAnalysisInfo(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

    /**
     * 统计每个用户的订单金额
     *
     * @return
     */
    List<Map> findAnalysisInfo();

    /**
     * @param type
     * @return
     */
    List<Map> findAnalysisInfoByType(@Param("analysisType") int analysisType);

    /**
     * @param analysisType
     * @param openId
     * @return
     */
    Map findAnalysisInfoByTypeAndOpenId(@Param("analysisType") int analysisType, @Param("openId") String openId);

    /**
     * @param AnalysisInfos
     * @param value
     */
    void updateAnalysisInfoBatch(@Param("list") List<Map> AnalysisInfos, @Param("analysisType") int analysisType);

    /**
     * @param insert
     */
    void savaAnalysisInfoBatch(@Param("list") List<Map> insert, @Param("analysisType") int analysisType);

    /**
     * 参与总人数
     *
     * @return
     */
    Integer allCount();

    /**
     * 查找所有的推广用户
     *
     * @return
     */
    List<Promoter> findAll();

    /**
     * 根据票据查找
     *
     * @param ticket
     * @return
     */
    int countByTickt(@Param("tickt") String tickt);

    /**
     * @param openId
     * @param cost
     * @param stCost
     */
    void updateByOpenId(@Param("openId") String openId, @Param("cost") double cost, @Param("stCost") double stCost);

    /**
     * @param openId
     * @return
     */
    int countByOpenId(@Param("openId") String openId);

    void insert(@Param("openId") String openId, @Param("cost") double cost, @Param("stCost") double stCost);
}
