package com.datafocus.service.mapper;

import com.datafocus.common.dto.CardDto;
import com.datafocus.common.dto.CardDtoMgt;
import com.datafocus.common.dto.OrderCountDto;
import com.datafocus.common.pojo.CardAcceptCategorys;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.*;

@Repository
public interface CardMapper {

    /**
     * 获取用户已领取的卡券
     *
     * @param openId
     */
    List<CardDto> list(@Param("openId") String openId);

    /**
     * 获取卡券使用的商品
     *
     * @param cardId
     */
    List<CardDto.CardAcceptCategory> cardAcceptCategoryByCardId(@Param("cardId") String cardId);

    /**
     * 根据卡券id查询卡券
     *
     * @param cardId
     * @return
     */
    CardDto findCardByCardId(@Param("cardId") String cardId);

    /**
     * 删除用户已领取的卡券
     *
     * @param openId
     * @param cardId
     * @param cardCode
     */
    void deleteCardByCardAndCode(@Param("openId") String openId, @Param("cardId") String cardId, @Param("cardCode") String cardCode);

    /**
     * 修改用户使用状态
     *
     * @param openId
     * @param cardId
     * @param cardCode
     */
    void modifyUseStatus(@Param("isUse") int isUse, @Param("isLock") int isLock, @Param("openId") String openId, @Param("cardId") String cardId, @Param("cardCode") String cardCode);

    /**
     * 获取卡券锁定状态
     *
     * @param code
     * @return
     */
    int getCardIsLockByCardCode(@Param("code") String code, @Param("openId") String openId);

    /**
     * 查询用户使用的卡券
     *
     * @param openId
     * @param productType
     * @param vendorCode
     * @param provinceCode
     * @param cityCode
     * @param products
     * @return
     */
    List<CardDto> findCardByOpenId(@Param("openId") String openId, @Param("productType") String productType, @Param("vendorCode") String vendorCode,
                                   @Param("provinceCode") String provinceCode, @Param("cityCode") String cityCode, @Param("products") List<String> products);

    /**
     * @param cardIds
     * @return
     */
    List<CardDto> notUseCard(@Param("openId") String openId, @Param("cardIds") List<String> cardIds);

    /**
     * 查询卡券列表
     *
     * @return
     */
    List<Map> findAll();

    /**
     * 保存微信卡券
     *
     * @param param
     */
    void saveCard(HashMap<String, Object> param);

    /**
     * 保存卡券适用商品
     *
     * @param cardAcceptCategorys
     */
    void saveCardAcceptCategorys(@Param("cardAcceptCategory") CardAcceptCategorys cardAcceptCategorys);

    /**
     * 卡劵列表id
     *
     * @return
     */
    List<Map<String, Object>> listCardId();

    /**
     * 微信卡券列表
     *
     * @return
     */
    Page<CardDtoMgt> listCard();

    /**
     * 删除卡券
     *
     * @param cardId
     */
    void deleteCard(String cardId);

    /**
     * 更新卡券库存
     *
     * @param increaseStock
     * @param reduceStock
     * @throws Exception
     */
    void modifyStock(@Param("cardId") String cardId, @Param("increaseStock") int increaseStock, @Param("reduceStock") int reduceStock);

    /**
     * 查询卡券列表
     *
     * @param openId
     * @return
     */
    Page<CardDtoMgt> selectCardByOpenId(@Param("openId") String openId);

    /**
     * 更新卡券状态
     *
     * @param cardId
     */
    public void updateCardStatus(@Param("cardId") String cardId, @Param("status") int status) throws Exception;

    /**
     * 保存卡券code
     */
    void saveCardCode(HashMap<String, Object> param) throws Exception;

    /**
     * 保存从卡券进入公众号会话事件推送
     *
     * @param param
     */
    void saveSessionEventCard(HashMap<String, Object> param) throws Exception;

    /**
     * 用户删除已领取的卡券
     *
     * @param param
     */
    void deleteCardByCardIdAndCode(HashMap<String, Object> param) throws Exception;

    /**
     * 更新用户卡券核销状态
     *
     * @param param
     */
    void updateCardIsUseStatus(HashMap<String, Object> param) throws Exception;

    /**
     * 根据id查找描述信息
     *
     * @param cardId
     * @return
     */
    String selectDescById(String cardId);

    /**
     * @param desc
     * @return
     */
    List<CardDto> findByDescription(@Param("desc") String desc);

    /**
     * @param openIds
     * @param start
     * @param end
     * @return
     */
    Long countByFromUserAndTime(@Param("openIds") Set<String> openIds,
                                @Param("start") Date start,
                                @Param("end") Date end);

    List<OrderCountDto> countByOpenId(@Param("openIds") Set<String> openIds);
}
