package com.datafocus.service.mapper;

import com.datafocus.common.pojo.Account;
import com.datafocus.common.pojo.AccountRole;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AccountMapper {
    /**
     * @param account
     * @return
     */
    Page findAccountGetLimit(@Param("account") Account account);

    /**
     * 增加账户
     *
     * @param account
     */
    void saveAccount(Account account);

    /**
     * 删除账户
     *
     * @param accountId
     */
    void deleteAccount(@Param("accountId") String accountId);

    /**
     * 根据名字查找
     *
     * @param loginName
     * @return
     */
    Account findAccountRole(@Param("accountName") String accountName);

    /**
     * 根据账户名称查找
     *
     * @param accountName
     * @return
     */
    Account findAccountByAccountName(@Param("accountName") String accountName);

    /**
     * 删除角色
     *
     * @param accountId
     */
    void deleteAccountRole(@Param("accountId") String accountId);

    /**
     * @param accountRoles
     */
    void saveAccoutRole(List<AccountRole> accountRoles);

    /**
     * 更新账户信息
     *
     * @param account
     */
    void updateAccountPassword(@Param("account") Account account);

    /**
     * @param account
     * @return
     */
    List<Account> findAccount(@Param("account") Account account);
}
