package com.datafocus.service.mapper;

import com.datafocus.weixin.common.event.ClickEvent;
import com.datafocus.weixin.common.event.ViewEvent;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuEventMapper {
	/**
	 * 保存菜单事件 CLICK
	 */
	void saveMenuEvenFormClick(ClickEvent clickEvent);
	
	/**
	 * 保存菜单事件 链接
	 */
	void saveMenuEvenFormView(ViewEvent viewEvent);

}
