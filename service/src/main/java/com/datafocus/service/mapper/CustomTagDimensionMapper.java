package com.datafocus.service.mapper;

import java.util.List;

import com.datafocus.common.pojo.CustomTagDimension;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomTagDimensionMapper {
	
	/**
	 * 保存标签维度
	 * @param customTagDimension
	 */
	void saveCustomTagDimension(CustomTagDimension customTagDimension)throws Exception;
	
	/**
	 * 根据标签维度名称查询标签维度
	 * @param dimensionName
	 * @return
	 */
	CustomTagDimension customTagDimension(String dimensionName);
	
	/**
	 * 保存标签维度和标签的关联关系
	 */
	void  saveCustomTagDimesionTag(@Param("tagDimesionId") int tagDimesionId, @Param("tagId") int tagId);
	
	/**
	 * 查询标签维度下面的所有标签
	 * @return
	 */
	List<CustomTagDimension> findCustomTagDimension();
	
}
