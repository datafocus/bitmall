package com.datafocus.service.mapper;

import org.springframework.stereotype.Repository;

/**
 * @author layne
 * @date 17-7-25
 */
@Repository
public interface AccountRoleMapper {
    /**
     * 删除角色
     *
     * @param accountId
     */
    void deleteAccountRole(String accountId);
}
