package com.datafocus.service.mapper;


import com.datafocus.common.pojo.Partner;
import com.datafocus.common.pojo.PartnerOrder;
import org.springframework.stereotype.Repository;

/**
 * @author layne
 * @date 17-7-5
 */
@Repository
public interface PartnerMapper {
    /**
     * 根据key查找合作商
     * @param partnerName
     * @return
     */
    Partner findByPartnerKey(String partnerName);

    /**
     * 保存合作商订单
     * @param partnerOrder
     */
    void savePartnerOrder(PartnerOrder partnerOrder);
}
