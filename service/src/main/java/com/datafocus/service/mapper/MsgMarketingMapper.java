package com.datafocus.service.mapper;

import com.datafocus.common.pojo.MsgMarketingStatistics;
import com.github.pagehelper.PageInfo;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by layne on 2017/12/5.
 */
@Repository
public interface MsgMarketingMapper {
    /**
     * @param msgMarketingStatistics
     * @return
     */
    void save(MsgMarketingStatistics msgMarketingStatistics);

    /**
     * @param msgMarketingStatistics
     */
    void update(MsgMarketingStatistics msgMarketingStatistics);

    /**
     *
     * @param id
     */
    void delete(@Param("id") Long id);

    /**
     *
     * @param msgMarketingStatistics
     * @return
     */
    List<MsgMarketingStatistics> findAll(MsgMarketingStatistics msgMarketingStatistics);

    /**
     * 查询所有渠道商
     * @return
     */
    Set<String> findAllPlatformOperator();

    /**
     *
     * @param id
     * @return
     */
    MsgMarketingStatistics findOne(@Param("id") Long id);
}
