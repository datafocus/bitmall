package com.datafocus.service.mapper;

import com.datafocus.common.pojo.EnterRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Set;

/**
 * Created by layne on 2017/12/6.
 */
@Repository
public interface EnterRecordMapper {
    /**
     * 保存记录
     *
     * @param enterRecord
     */
    void save(EnterRecord enterRecord);

    /**
     * @param openId
     * @param date
     */
    int enterCount(@Param("openId") String openId);

    /**
     *
     * @param opendIds
     * @return
     */
    Long countByOpenIds(@Param("openIds") Set<String> openIds);
}
