package com.datafocus.service.mapper;

import com.datafocus.common.pojo.AlarmConfig;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by liuheng on 2017/9/1.
 */
@Repository
public interface AlarmConfigMapper {
    void updateConfig(AlarmConfig alarmConfig);

    List<AlarmConfig> listConfig();
}
