package com.datafocus.service.mapper;

import com.datafocus.common.pojo.Role;
import com.datafocus.common.pojo.RoleRes;
import com.github.pagehelper.Page;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author layne
 * @date 17-8-2
 */
@Repository
public interface RoleMapper {
    /**
     * 角色列表
     *
     * @return
     */
    Page<Role> findRole();

    /**
     * 保存角色
     *
     * @param role
     */
    void insertRole(Role role);

    /**
     * 删除角色资源关系
     *
     * @param roleId
     */
    void deleteRoleRes(int roleId);

    /**
     * 保存角色资源关系
     *
     * @param roleRes
     */
    void insertRoleRes(List<RoleRes> roleRes);

    /**
     * 删除角色
     *
     * @param roleId
     */
    void deleteRole(Integer roleId);
}
