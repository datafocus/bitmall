package com.datafocus.service.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datafocus.common.pojo.FlowChannel;
import com.datafocus.common.pojo.FlowOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowMgtMapper {
	
	
	/**
	 * 根据carrierOperator 查询每个省下面的通道列表
	 * @param carrierOperator
	 * @return
	 */
	List<FlowChannel> findChannelByCarrierOperator(int carrierOperator);
	
	/**
	 * 根据通道id修改通道状态
	 */
	void modifyFlowChannelStatusById(@Param("ids") Integer[] ids, @Param("status") int status, @Param("remark") String remark);
	

}
