package com.datafocus.service.mapper;

import com.datafocus.common.pojo.ActiveMessage;
import com.datafocus.common.pojo.Regular;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AutoReplyMapper {
	/**
	 * 
	 * @param activeMessage
	 */
	void savePassive(ActiveMessage activeMessage);
	/**
	 * 查询主动回复
	 * @return
	 */
	ActiveMessage one();
	
	/**
	 * 保存规则名称
	 * @param regular
	 */
	int saveRegular(Regular regular);
	
	/**
	 * 保存关键字
	 * @param keyWords
	 */
	void saveKeyword(List<Regular.KeyWord> keyWords);
	
	/**
	 * 保存关键字回复消息内容
	 * @param keyWordMessages
	 */
	void saveKeywordMessage(List<Regular.KeyWordMessage> keyWordMessages);
	
	/**
	 * 查询规则列表
	 * @return
	 */
	List<Regular> selectRegularList();
	
	/**
	 * 查询关键字列表
	 * @param id 规则id
	 * @return
	 */
	List<Regular.KeyWord> selectKeyWords(Integer id);
	
	/**
	 * 查询关键字回复消息内容列表
	 * @param id 规则id
	 * @return
	 */
	List<Regular.KeyWordMessage> selectKeyWordMessages(Integer id);
	
	/**
	 * 删除关键字自动回复
	 * @param id
	 */
	void delete(Integer id);
	
	
}
