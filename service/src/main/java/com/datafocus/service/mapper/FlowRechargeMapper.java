package com.datafocus.service.mapper;

import com.datafocus.common.dto.FlowOrderDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlowRechargeMapper {
	
	/**
	 * 查询待充值的手机号码
	 */
	List<FlowOrderDto> findRechargeOrderByOrderStatusAndSubmitStatus();
	
	/**
	 * 批量更新充值订单提交状态 改为 : 1 ;  0：代表为提交，1：代表已提交
	 */
	void batchUpdateRechargeSubmitStatus(List<Integer> ids);
	
	/**
	 * 修改流量充值提交状态和充值状态
	 * @param orderNo
	 * @param submitStatus
	 */
	void modifyOrderSubmitStatus(@Param("orderNo") String orderNo, @Param("submitStatus") int submitStatus, @Param("msgid") String msgid, @Param("gallerysubmitCode") String gallerysubmitCode);
	
	
}
