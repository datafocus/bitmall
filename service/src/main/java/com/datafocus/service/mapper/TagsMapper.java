package com.datafocus.service.mapper;

import com.datafocus.common.pojo.Tag;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author layne
 * @date 17-7-26
 */
@Repository
public interface TagsMapper {
    /**
     * 保存用户标签
     *
     * @param tag
     */
    void saveTag(Tag tag);

    /**
     * 删除标签
     *
     * @param tagId
     */
    void delete(@Param("tagId") Integer tagId);

    /**
     * 标签列表
     *
     * @return
     */
    List<Tag> list();

    List<String> getOpenIdList(@Param("tagId") Integer tagId);

    /**
     * 查存用户标签Id
     *
     * @param openId
     * @return
     */
    List<Integer> userTagsByOpenId(@Param("openId") String openId);

    /**
     * 删除用户已有的标签
     *
     * @param openId
     * @param id
     */
    void deleteUserTag(@Param("openId") String openId, @Param("tagId") Integer tagId);

    /**
     * 保存用户标签
     *
     * @param openId
     * @param tag
     */
    void saveUserTag(@Param("openId") String openId, @Param("tagId") Integer tagId);


    /**
     * 批量保存用户标签
     */
    void saveTagUsers(@Param("tagId") Integer tagId, @Param("openIds") List<String> openIds);

    /**
     * 批量保存微信标签
     *
     * @param tags
     */
    void saveTagBatch(@Param("tags") List<Tag> tags);

    /**
     * 查找所有本地标签名字
     *
     * @return
     */
    List<Map> findAllLocalTags();

    /**
     * 根据Id查找用户标签
     *
     * @param tagId
     * @return
     */
    List<String> findOpenIdByTagId(@Param("tagId") Integer tagId);

    /**
     * 判断是否有这个标签名
     *
     * @param tagName
     * @return
     */
    Integer findByTagName(@Param("tagName") String tagName);

    /**
     * @param wxTagId
     * @param i
     */
    void updateNum(@Param("tagId") Integer tagId, @Param("num") Integer num);

    /**
     *
     * @return
     */
    List<Integer> findWxTagId();
}
