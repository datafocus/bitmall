package com.datafocus.service.mapper;

import java.util.List;
import java.util.Map;

import com.datafocus.common.pojo.BillOrder;
import com.github.pagehelper.Page;
import org.springframework.stereotype.Repository;

@Repository
public interface BillOrderMapper {


    /**
     * 插入一条话费订单记录
     *
     * @param order
     */
    void saveBillOrder(BillOrder order);

    /**
     * 根据订单号查询话费订单
     *
     * @param orderNo
     * @return
     */
    BillOrder findBillOrderByOrderNo(String orderNo);

    /**
     * 修改订单支付状态
     *
     * @param orderNo
     */
    void modify(String orderNo);

    /**
     * 用户充值记录
     *
     * @return
     */
    List<BillOrder> feeBillOrder(String openId);

    /**
     * 查询话费列表
     *
     * @return
     */
    Page<BillOrder> list(Map<String, String> params);

    /**
     * 修改订单
     *
     * @param modify
     */
    void batchModify(Map<String, Object> modify);

    /**
     * 查询退款中的话费订单
     * @return
     */
    List<String> findOrderByRefund();
}
