package com.datafocus.service.mapper;

import com.datafocus.common.dto.Statistics;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public interface StatisticsMapper {

	List<Statistics> findPayStatistics(Map<String, Object> map);
	
	List<Statistics> findTransaction(Map<String, Object> map);
	
	List<Statistics> findCarrierOperatorStatistics(Map<String, Object> map);
	
	List<Statistics> findCreateOrderStatisticsData(Map<String, Object> map);
	
	List<Statistics> findAccessStatisticsData(Map<String, Object> map);
	
	List<Statistics> findPhoneResolveStatistics(Map<String, Object> map);
}
