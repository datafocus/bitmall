package com.datafocus.service.mapper;

import com.datafocus.weixin.mp.event.MessageSentEvent;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageMapper {
	
	/**
	 * 保存群发消息事件
	 * @param message
	 */
	void saveMessage(MessageSentEvent message);
}
				