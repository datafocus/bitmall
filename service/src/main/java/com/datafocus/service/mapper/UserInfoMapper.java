package com.datafocus.service.mapper;

import com.datafocus.common.dto.FlowOrderDto;
import com.datafocus.common.dto.UserInfoDto;
import com.datafocus.common.dto.UserInfoQuery;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.common.pojo.UserInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Repository
public interface UserInfoMapper {

    /**
     * 根据openid获取用户信息
     *
     * @param openId
     * @return
     */
    UserInfo findUserInfoByOpenId(@Param("openId") String openId);

    /**
     * 保存微信用户信息
     *
     * @param userInfo
     */
    void insertUserInfo(UserInfo userInfo);

    /**
     * 保存用户信息
     *
     * @param userInfoDto
     */
    void saveUserInfo(UserInfoDto userInfoDto);

    /**
     * 保存用户标签
     *
     * @param userTags
     */
    void saveUserTags(List<UserInfoDto.UserTags> userTags);

    /**
     * 更改用户关注状态（取消关注、关注）
     *
     * @param openId
     * @param subscribe
     */
    void updateUserSubscribe(@Param("openId") String openId, @Param("subscribe") int subscribe);

    /**
     * 根据openId查询用户是否存在
     *
     * @param openId
     * @return
     */
    Integer countUserByOpenId(@Param("openId") String openId);

    /**
     * 获取用户列表
     *
     * @param userInfoQuery 查询参数
     * @return
     */
    List<UserInfoDto> list(UserInfoQuery userInfoQuery);

    /**
     * 获取用户订单数
     *
     * @param openId
     * @return
     */
    List<FlowOrderDto> queryOrderByOpenId(@Param("openId") String openId);

    /**
     * 根据openId 查询用户
     *
     * @param openId
     * @return
     */
    UserInfoDto get(@Param("openId") String openId);

    /**
     * @param start
     * @param end
     * @return
     */
    List<UserInfo> findSubscriberByTime(@Param("start") Date start, @Param("end") Date end);

    List<String> getAllOpenId();
}
