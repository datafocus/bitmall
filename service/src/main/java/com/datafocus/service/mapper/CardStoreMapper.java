package com.datafocus.service.mapper;

import com.datafocus.common.pojo.CardStore;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardStoreMapper {
	
	/**
	 * 保存卡券货架
	 */
	void saveCardStore(CardStore cardStore);
	
	/**
	 * 保存货架上对应的卡券
	 */
	void saveCardStoreExt(List<CardStore.CardStoreExt> cardStoreExts);
	
	/**
	 * 删除卡券货架
	 * @param pageId
	 */
	void deleteStore(@Param("pageId") String pageId);
	
	/**
	 * 卡券货架列表
	 * @return
	 */
	List<CardStore> selectCardStoreList();

}
