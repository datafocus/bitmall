package com.datafocus.service.mapper;

import java.util.List;

import com.datafocus.common.pojo.CardInfoCube;
import org.apache.ibatis.annotations.Param;

import com.datafocus.weixin.mp.card.bean.CardDataCube;
import com.datafocus.weixin.mp.card.bean.CardDataCube.CardInfoDataCube;
import org.springframework.stereotype.Repository;

@Repository
public interface CardDataCubeMapper {
	
	/**
	 * 保存商户下所有卡券的概况统计
	 * @param cardDataCubes
	 * @throws Exception
	 */
	void saveCardDataCubes(List<CardDataCube> cardDataCubes);
	
	/**
	 * 免费券（优惠券、团购券、折扣券、礼品券）在固定时间区间内的相关数据。
	 * @param cardInfoDataCubes
	 */
	void saveCardInfoDataCubes(List<CardInfoDataCube> cardInfoDataCubes);
	
	/**
	 * 查询卡券详细数据统计
	 * @param cardId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<CardInfoCube> cardInfoCubeList(@Param("cardId") String cardId, @Param("beginTime") String beginTime, @Param("endTime") String endTime);
	
	/**
	 * 查询卡券概况数据统计
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<CardDataCube> cardDataCubeList(@Param("beginTime") String beginTime, @Param("endTime") String endTime);

}
