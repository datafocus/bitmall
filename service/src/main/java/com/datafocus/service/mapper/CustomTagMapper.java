package com.datafocus.service.mapper;

import com.datafocus.common.dto.UserInfoDto;
import com.datafocus.common.pojo.CustomTag;
import com.datafocus.common.pojo.CustomTagUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author layne
 * @date 17-7-26
 */
@Repository
public interface CustomTagMapper {
    /**
     * 保存自定义标签
     */
    void saveCustomTag(CustomTag customTag);

    /**
     * 查询自定义标签
     *
     * @return
     */
    CustomTag customTag(@Param("tagName") String tagName, @Param("tagId") int tagId);

    /**
     * 保存用户与自定义标签的关联关系
     *
     * @param openId
     * @param tagId
     */
    void saveCustomTagUser(@Param("openId") String openId, @Param("tagId") int tagId);

    /**
     * 根据用户id，标签id查询是否有关联关系
     */
    int countCustomTagUser(@Param("openId") String openId, @Param("tagId") int tagId);

    /**
     * @param tagName
     * @return
     */
    public List<CustomTagUser> customTagUsersByTagName(String tagName);

    /**
     * 解除用户与标签直接的关系
     *
     * @param openId
     * @param tagId
     */
    void deleteCustomTagUserByOpenIdAndTagId(@Param("openId") String openId, @Param("tagId") int tagId);

    /**
     * 获取标签下所有的用户openId
     *
     * @param tagId
     * @return
     */
    List<String> customTagUsersByTagId(int tagId);

    /**
     * 获取所有的系统标签
     *
     * @return
     */
    public List<CustomTag> findCustomTags();

    /**
     * 更新标签下用户数
     *
     * @param tagId
     * @param flag  1:增，0：减少
     */
    void updateCustomUserNum(@Param("tagId") int tagId, @Param("flag") int flag);

    /**
     * @param dismensionName
     * @param openId
     * @return
     */
    List<CustomTagUser> customTagUsersByDismensionNameAndOpenId(@Param("dismensionName") String dismensionName, @Param("openId") String openId);

    /**
     * 根据id查标签名
     *
     * @param tagId
     * @return
     */
    String findTagNameById(@Param("tagId") Integer tagId);

    /**
     *
     * @param openId
     * @return
     */
    List<UserInfoDto.UserTags> findTagsByOpenId(@Param("openId") String openId);
}
