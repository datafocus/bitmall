package com.datafocus.service.mapper;

import java.util.HashMap;
import java.util.List;

import com.datafocus.common.pojo.Ticket;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketMapper {

    /**
     * 保存二维码信息
     *
     * @param ticket
     */
    public void save(Ticket ticket);

    /**
     * 根据场景值ID查询二维码信息
     *
     * @param sceneId
     * @return
     */
    public Ticket ticket(int sceneId);

    /**
     * 查询二维码列表
     *
     * @param params
     * @return
     */
    public List<Ticket> list(HashMap<String, Object> params);

    /**
     * 更新二维码下面的粉丝数
     *
     * @param sceneId
     * @param ticket
     */
    public void updateTicketFansNum(@Param("sceneId") int sceneId, @Param("ticket") String ticket);

    /**
     * 是否存在sencensId
     *
     * @param sencensId
     * @return
     */
    int countBySencesId(@Param("sceneId") String sceneId);
}
