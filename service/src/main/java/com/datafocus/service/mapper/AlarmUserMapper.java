package com.datafocus.service.mapper;

import com.datafocus.common.pojo.AlarmUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by liuheng on 2017/9/1.
 */

@Repository
public interface AlarmUserMapper {

    void addAlarmUser(AlarmUser alarmUser);

    List<AlarmUser> listAlarmUser();

    List<AlarmUser> listOpenUser();

    void updateStatus(@Param("id") int id,@Param("status") int status);
}
