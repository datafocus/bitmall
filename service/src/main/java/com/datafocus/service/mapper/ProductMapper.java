package com.datafocus.service.mapper;


import java.util.List;
import java.util.Map;

import com.datafocus.common.dto.ProductCollection;
import com.datafocus.common.dto.ProductDto;
import com.datafocus.common.dto.ProductVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductMapper {
	
	/**
	 * 流量包产品
	 * @param carrierOperator
	 * @param province
	 * @return
	 */
	List<ProductDto.Product> list(@Param("carrierOperator") Integer carrierOperator, @Param("province") String province);
	/**
	 * 运营商地区通道状态
	 * @param carrierOperator
	 * @param province
	 * @return
	 */
	Map<String, Object> channe(@Param("carrierOperator") Integer carrierOperator, @Param("province") String province);
	
	/**
	 * 根据产品id查询产品
	 * @param id
	 * @return
	 */
	ProductDto.Product findProductById(@Param("id") int id);

	/**
	 * 查询产品列表
	 * @param carrierOperator 运营商id 参数为-1时查询所有的运营商的包型
	 * @return
	 */
	List<ProductVO> productList(@Param("carrierOperator") Integer carrierOperator, @Param("province") String province);
	/**
	 * 修改产品备注
	 */
	void modify(List<ProductVO> productVO);
	/**
	 * 新增流量产品
	 * @param product
	 * @throws Exception
	 */
	void save(@Param("product") ProductCollection product);

	/**
	 * 查询产品数量
	 * @param carrierOperator
	 * @param province
	 * @return
	 */
	int count(@Param("carrierOperator") Integer carrierOperator,@Param("province") String province);

}
