package com.datafocus.service.mapper;

import java.util.List;

import com.datafocus.common.dto.Cps;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CpsMapper {

    Cps cube(@Param("sceneId") String sceneId, @Param("list") List<String> cardId);

}
