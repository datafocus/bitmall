package com.datafocus.service.service.impl;

import com.datafocus.common.pojo.Tag;
import com.datafocus.service.mapper.CustomTagMapper;
import com.datafocus.service.mapper.TagsMapper;
import com.datafocus.service.service.TagsService;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.user.Tags;
import com.datafocus.weixin.mp.user.bean.UserPagination;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * @author layne
 * @date 17-7-26
 */
@Service
public class TagsServiceImpl implements TagsService {

    private static final Log logger = LogFactory.getLog(TagsServiceImpl.class);

    /**
     * 微信
     */
    private Tags tags;

    @Autowired
    private TagsMapper tagsMapper;

    @Autowired
    private CustomTagMapper customTagMapper;

    @Autowired
    private AppSetting appSetting;

    @PostConstruct
    public void init() {
        tags = Tags.with(appSetting);
    }

    @Override
    public void saveTag(Tag tag) {
        //同步标签到微信
        com.datafocus.weixin.mp.user.bean.Tag t = tags.create(tag.getName());
        tag.setTagId(t.getId());
        tagsMapper.saveTag(tag);
    }

    @Override
    public void delete(Integer tagId) {
        tags.delete(tagId);
        tagsMapper.delete(tagId);
    }

    @Override
    public List<Tag> list() {
        return tagsMapper.list();
    }

    @Override
    public List<String> getOpenIdList(Integer tagId) {
        return tagsMapper.getOpenIdList(tagId);
    }

    @Override
    public void userTags(String openId, List<Integer> tagId) {
        //获取用户已有的标签
        List<Integer> userTags = tagsMapper.userTagsByOpenId(openId);
        for (Integer id : userTags) {
            //删除用户已有的标签
            tagsMapper.deleteUserTag(openId, id);

        }
        for (Integer tag : tagId) {
            //保存用户标签
            tagsMapper.saveUserTag(openId, tag);
            //取消用户标签同步到微信
            tags.unTagUsers(tag, Arrays.asList(openId));
            //同步用户标签到微信
            tags.tagUsers(tag, Arrays.asList(openId));
        }
    }

    @Override
    public List<Integer> userTagByOpenId(String openId) {
        return tagsMapper.userTagsByOpenId(openId);
    }

    @Override
    public void task() {
        ExecutorService service = Executors.newFixedThreadPool(10 * Runtime.getRuntime().availableProcessors());
        List<CompletableFuture> tasks = new ArrayList<>();
        long start = System.currentTimeMillis();
        logger.info("开始同步系统标签至微信");
        //删除本地所有标签 tag表
        //同步微信所有标签
        List<com.datafocus.weixin.mp.user.bean.Tag> wxTags = this.tags.list();
        List<Tag> tags = wxTags.stream().map(wxTag -> {
            Tag tag = new Tag();
            tag.setName(wxTag.getName());
            tag.setTagId(wxTag.getId());
            tag.setNum(wxTag.getCount());
            return tag;
        }).collect(toList());

        //同步微信用户标签到本地的user_tags表
        for (com.datafocus.weixin.mp.user.bean.Tag wxTag : wxTags) {
            //获取标签下粉丝列表
            UserPagination userPagination = this.tags.listUsers(wxTag.getId());
            try {
                List<String> openIds = userPagination.getUsers();
                if (null != openIds && openIds.size() > 0) try {
                    tagsMapper.saveTagUsers(wxTag.getId(), openIds);
                } catch (Exception e) {

                }
            } catch (Exception e) {
            }
        }

        //所有本地标签
        List<Map> localTags = tagsMapper.findAllLocalTags();
        //分成微信有的和没有的两组
        //微信中已经有的标签名字
        List<String> wxTagNames = wxTags.stream().map(com.datafocus.weixin.mp.user.bean.Tag::getName).collect(toList());
        //按是否存在分成两组
        Map<String, List<Map>> groupLocalTags = localTags.stream().collect(groupingBy(localTag -> wxTagNames.contains(localTag.get("tag_name")) ? "exist" : "notexist"));
        //本地有微信侧没有的标签，需要先在微信侧创建标签
        List<Map> notexist = groupLocalTags.get("notexist");
        if (null != notexist && notexist.size() > 0) {
            notexist.stream().forEach(notexistTag -> {
                try {
                    com.datafocus.weixin.mp.user.bean.Tag wxTag = this.tags.create(String.valueOf(notexistTag.get("tag_name")));
                    Tag localTag = new Tag();
                    localTag.setTagId(wxTag.getId());
                    localTag.setName(wxTag.getName());
                    Integer tagId = Integer.valueOf(notexistTag.get("tag_id").toString());
                    List<String> ctu = tagsMapper.findOpenIdByTagId(tagId);
                    if (null != ctu) {
                        localTag.setNum(ctu.size());
                        if (ctu.size() > 0) {
                            //一次最多同步50个
                            WxList<String> wxList = new WxList(ctu, 50);
                            while (wxList.hasMore()) {
                                CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
                                    try {
                                        //同步本地标签到微信
                                        this.tags.tagUsers(tagId, wxList.next());
                                    } catch (Exception e) {
                                        logger.error("向标签:" + notexistTag.get("tag_name") + "中添加用户失败", e);
                                    }
                                    return true;
                                }, service);
                                tasks.add(future);
                            }
                        }
                    } else localTag.setNum(0);
                    tags.add(localTag);
                } catch (Exception e) {
                    logger.error("创建微信标签失败:" + notexistTag.get("tag_name"), e);
                }

            });
        }
        List<Integer> wxTagIds = tagsMapper.findWxTagId();
        List<Tag> save = tags.parallelStream().filter(wxTag -> !wxTagIds.contains(wxTag.getTagId())).collect(toList());
        //保存微信标签到本地
        try {
            tagsMapper.saveTagBatch(save);
        } catch (Exception e) {
            logger.error("同步微信到本地失败", e);
        }

        //找出微信侧已有的标签，同步本地用户到相应的标签下
        List<Map> exist = groupLocalTags.get("exist");
        if (null != exist && exist.size() > 0) {
            exist.stream().forEach(existTag -> {
                String tagName = existTag.get("tag_name").toString();
                Integer tagId = Integer.parseInt(existTag.get("tag_id").toString());
                List<String> openIdByTagId = tagsMapper.findOpenIdByTagId(tagId);
                for (com.datafocus.weixin.mp.user.bean.Tag wxTag : wxTags) {
                    if (tagName.equals(wxTag.getName())) {
                        List<String> needToAdd = null;
                        try {
                            UserPagination userPagination = this.tags.listUsers(wxTag.getId());
                            List<String> wxExsitOpenIds = userPagination.getUsers();
                            needToAdd = openIdByTagId.stream().filter(openId -> !wxExsitOpenIds.contains(openId)).collect(toList());
                        } catch (Exception e) {

                        }
                        //需要添加到微信的标签用户
                        if (needToAdd == null || needToAdd.size() == 0) {
                            needToAdd = openIdByTagId;
                        }
                        if (null != needToAdd && needToAdd.size() > 0) {
                            try {
                                tagsMapper.saveTagUsers(tagId, needToAdd);
                            } catch (Exception e) {
                            }
                            WxList wxList = new WxList(needToAdd, 50);
                            while (wxList.hasMore()) {
                                CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
                                    try {
                                        //同步本地标签到微信
                                        this.tags.tagUsers(wxTag.getId(), wxList.next());
                                    } catch (Exception e) {
                                        logger.error("向标签:" + wxTag.getName() + "中添加用户失败", e);
                                    }
                                    return true;
                                }, service);
                                tasks.add(future);
                            }
                        }
                        break;
                    }
                }
            });
        }

        tasks.forEach(task -> {
            try {
                task.get();
            } catch (InterruptedException | ExecutionException e) {
                logger.error("任务出错", e);
            }
        });
        service.shutdown();
        logger.info("同步结束,耗时：" + (System.currentTimeMillis() - start));
    }

    private class WxList<T> {

        private List<T> header;

        private List<T> tail;

        private final Integer limit;

        private final Integer times;

        public WxList(List<T> data, Integer limit) {
            Objects.requireNonNull(data);
            if (limit <= 0) throw new IllegalArgumentException();
            this.limit = limit;
            this.times = data.size() / limit + 1;
            if (data.size() > limit) {
                this.header = data.subList(0, limit);
                this.tail = data.subList(limit, data.size());
            } else {
                this.header = data;
                this.tail = new ArrayList<>();
            }
        }

        public boolean hasMore() {
            return header.size() > 0;
        }

        public List<T> next() {
            List<T> result = header;
            if (tail.size() > 0) {
                if (tail.size() > limit) {
                    header = tail.subList(0, limit);
                    tail = tail.subList(limit, tail.size());
                } else {
                    header = tail;
                    tail = new ArrayList<>();
                }
            } else header = new ArrayList<>();
            return result;
        }

        public Integer getTimes() {
            return times;
        }
    }
}
