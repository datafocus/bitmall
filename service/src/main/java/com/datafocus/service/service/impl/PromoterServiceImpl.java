package com.datafocus.service.service.impl;

import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.common.pojo.Promoter;
import com.datafocus.common.pojo.Ticket;
import com.datafocus.service.mapper.FlowOrderMapper;
import com.datafocus.service.mapper.PromoterMapper;
import com.datafocus.service.mapper.TicketMapper;
import com.datafocus.service.mapper.UserInfoMapper;
import com.datafocus.service.service.PromoterService;
import com.datafocus.service.service.TicketService;
import org.apache.commons.lang.math.Fraction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
public class PromoterServiceImpl implements PromoterService {
    private static final Logger logger = LoggerFactory.getLogger(PromoterServiceImpl.class);
    @Autowired
    private PromoterMapper promoterMapper;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private TicketMapper ticketMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private FlowOrderMapper flowOrderMapper;

    @Override
    @Transactional
    public String save(Promoter promoter) {
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> generateSencesId());//异步生成场景ID
        //1 校验是否曾经是推客，沿用之前的信息及二维码
        Promoter oneByOpenId = promoterMapper.findOneByOpenId(promoter.getOpenId());
        Date dateTime = Date.from(Instant.now());
        if (null != oneByOpenId) {//如果存在
            //如果二维码过期
            //promoter.setUpdateTime(dateTime);
            //promoterMapper.updateOne(promoter);
            return oneByOpenId.getTickt();
        }
        Integer sencensId = future.join();
        logger.info("------------------------" + sencensId + "-----------------------------");
        Ticket ticket = new Ticket();//生成票据
        ticket.setActionName("QR_SCENE");//临时
        ticket.setSceneId(sencensId);
        ticket.setActivityName("分享活动");
        ticket.setExpireSeconds(2592000);//30天
        promoter.setCreateTime(dateTime)
                .setUpdateTime(dateTime)
                .setScenesId(sencensId);
        //2 生成二维码
        Ticket save = ticketService.save(ticket);
        promoter.setTickt(save.getTicket());
        //3 入库
        promoterMapper.save(promoter);
        return promoter.getTickt();
    }

    /**
     * 生成场景Id
     *
     * @return
     */
    private synchronized int generateSencesId() {
        final Random random = new Random();
        int limit = random.nextInt(10);
        Optional<String> reduce = Stream.generate(() -> random.nextInt(10))
                .limit(limit == 0 ? 5 : limit)
                .map(String::valueOf)
                .reduce((s, s2) -> s + s2);
        Integer sencensId = Integer.parseInt(reduce.get());
        int i = ticketMapper.countBySencesId(String.valueOf(sencensId));
        return i == 0 || sencensId != 0 ? sencensId : generateSencesId();
    }

    public Map<String, Object> myFlowAnalysis(String openId) {
        Map<String, Object> result = new HashMap<>();
        Map byTypeAndOpenId = promoterMapper.findAnalysisInfoByTypeAndOpenId(AnalysisInfoType.YEAR.value, openId);
        //统计今年的
        LocalDateTime endTime = LocalDateTime.now();
        LocalDateTime startTime = LocalDateTime.of(endTime.getYear(), 1, 1, 0, 0);
        List<Map> productByOpneId = flowOrderMapper.findProductByOpneId(openId, startTime, endTime);
        List<String> names = productByOpneId.stream().map(map -> map.get("product_name").toString()).collect(toList());
        Long totalFlow = getTotalFlow(names);
        double totalCost = Double.parseDouble(byTypeAndOpenId.get("cost").toString());
        double saved = Double.parseDouble(byTypeAndOpenId.get("st_cost").toString()) - totalCost;
        result.put("totalFlows", totalFlow);
        result.put("save", saved);
        List<Map> analysisInfoByType = promoterMapper.findAnalysisInfoByType(AnalysisInfoType.YEAR.value);
        long countMoreThen = analysisInfoByType.stream().filter(map -> (Double.parseDouble(map.get("st_cost").toString()) - Double.parseDouble(map.get("cost").toString())) - saved < 0).count();
        Integer allCount = promoterMapper.allCount();
        Fraction fraction = Fraction.getFraction((int) countMoreThen, allCount);
        String str = String.valueOf(fraction.doubleValue());
        Matcher matcher = Pattern.compile("(\\d+\\.)(\\d{1,4})").matcher(str);
        String rank = null;
        if (matcher.find()) rank = matcher.group(1) + matcher.group(2);
        result.put("rank", rank);
        return result;
    }

    private Long getTotalFlow(List<String> products) {
        Pattern compile = Pattern.compile("(\\d+)(G|M)");
        Optional<Long> reduce = products.stream().map(p -> {
            Matcher matcher = compile.matcher(p.toUpperCase());
            if (matcher.find()) {
                String num = matcher.group(1);
                String gOrM = matcher.group(2);
                switch (gOrM) {
                    case "G":
                        return Long.parseLong(num) * 1024;
                    case "M":
                        return Long.parseLong(num);
                    default:
                        return 0l;
                }
            } else return 0l;
        }).reduce((x, y) -> x + y);
        return reduce.get();
    }

    @Transactional

    public void yearlyAnalysis() {
        logger.info("开始统计用户今年消费数据");
        long start = System.currentTimeMillis();
        //统计今年的
        LocalDateTime endTime = LocalDateTime.now();
        LocalDateTime startTime = LocalDateTime.of(endTime.getYear(), 1, 1, 0, 0);
        List<Map> analysisInfo = promoterMapper.findAnalysisInfo(startTime, endTime);

        List<Map> analysisInfoByType = promoterMapper.findAnalysisInfoByType(AnalysisInfoType.YEAR.value);
        List<String> needUpdate = analysisInfoByType.stream().map(info -> String.valueOf(info.get("open_id"))).collect(toList());
        Map<String, List<Map>> grouped = analysisInfo.stream().collect(groupingBy((Map map) -> needUpdate.contains(map.get("openId")) ? "update" : "insert"));
        if (null != grouped.get("update") && grouped.get("update").size() > 0)
            promoterMapper.updateAnalysisInfoBatch(grouped.get("update"), AnalysisInfoType.YEAR.value);
        if (null != grouped.get("insert") && grouped.get("insert").size() > 0)
            promoterMapper.savaAnalysisInfoBatch(grouped.get("insert"), AnalysisInfoType.YEAR.value);
        logger.info("自动统计今年用户消费数据结束，耗时：" + (System.currentTimeMillis() - start) + "毫秒");
    }

    @Transactional
    public void allAnalysis() {
        logger.info("开始统计用户全部消费数据");
        long start = System.currentTimeMillis();
        //统计全部的
        List<Map> analysisInfoAll = promoterMapper.findAnalysisInfo();
        List<Map> analysisInfoByTypeAll = promoterMapper.findAnalysisInfoByType(AnalysisInfoType.ALL.value);
        List<String> needUpdateAll = analysisInfoByTypeAll.stream().map(info -> String.valueOf(info.get("open_id"))).collect(toList());
        Map<String, List<Map>> groupedAll = analysisInfoAll.stream().collect(groupingBy((Map map) -> needUpdateAll.contains(String.valueOf(map.get("openId"))) ? "update" : "insert"));
        if (null != groupedAll.get("update") && groupedAll.get("update").size() > 0)
            promoterMapper.updateAnalysisInfoBatch(groupedAll.get("update"), AnalysisInfoType.ALL.value);
        if (null != groupedAll.get("insert") && groupedAll.get("insert").size() > 0)
            promoterMapper.savaAnalysisInfoBatch(groupedAll.get("insert"), AnalysisInfoType.ALL.value);
        logger.info("自动统计用户全部消费数据结束，耗时：" + (System.currentTimeMillis() - start) + "毫秒");
    }

    public List<String> getPromoterSenceId() {
        List<Promoter> promoters = promoterMapper.findAll();
        return promoters.stream().map(Promoter::getScenesId).map(String::valueOf).collect(toList());
    }

    @Override
    public boolean isNewer(String openId) {
        int count = userInfoMapper.countUserByOpenId(openId);
        return count == 0;
    }

    @Override
    public void addOrUpdate(FlowOrder order) {
        Objects.requireNonNull(order);
        int count = promoterMapper.countByOpenId(order.getOpenId());
        if (count > 0) {
            logger.info("更新统计信息");
            promoterMapper.updateByOpenId(order.getOpenId(), order.getOrderPrice().doubleValue(), order.getStandardPrice().doubleValue());
        } else {
            promoterMapper.insert(order.getOpenId(), order.getOrderPrice().doubleValue(), order.getStandardPrice().doubleValue());
            logger.info("保存统计信息");
        }
    }

    public enum AnalysisInfoType {
        YEAR(1), ALL(0);
        private int value;

        AnalysisInfoType(int value) {
            this.value = value;
        }
    }
}
