package com.datafocus.service.service;

import com.datafocus.common.pojo.AlarmConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by liuheng on 2017/9/1.
 */
public interface AlarmConfigService {

    void updateConfig(AlarmConfig alarmConfig);

    List<AlarmConfig> listConfig();

    AlarmConfig getConfig(int id);

    List<AlarmConfig> init();
}
