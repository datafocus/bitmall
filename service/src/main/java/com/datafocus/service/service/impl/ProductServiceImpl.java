package com.datafocus.service.service.impl;

import com.datafocus.common.config.MapProperties;
import com.datafocus.common.config.MgtProperties;
import com.datafocus.common.config.ProtalProperties;
import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.dto.*;
import com.datafocus.common.exception.ApplicationException;
import com.datafocus.common.exception.ServiceException;
import com.datafocus.common.utils.HttpUtils;
import com.datafocus.service.mapper.CardMapper;
import com.datafocus.service.mapper.ProductMapper;
import com.datafocus.service.service.CardService;
import com.datafocus.service.service.ProductService;
import com.datafocus.service.service.remote.DistrictService;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.weixin.common.util.JsonMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

@Service
public class ProductServiceImpl implements ProductService {


    private static Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private DistrictService districtService;
    @Autowired
    private CardService cardService;
    @Autowired
    private CardMapper cardMapper;
    @Autowired
    private StringRedisTemplate redisUtil;
    @Autowired
    private ProtalProperties protalProperties;
    @Autowired
    private MgtProperties mgtProperties;
    @Autowired
    private MapProperties mapProperties;

    @Override
    public Map<String, Object> list(String mobile) {
        // 根据手机号码解析出所在的运营商，地区
        DistrictDto districtDto = districtService.district(mobile.substring(0, 7));
        if (null == districtDto)
            throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), "暂不支持该手机所在地区充值");
        //查询通道是否已开启
        Map<String, Object> channeMap = productMapper.channe(districtDto.getCarryOperator(), districtDto.getProvince());
        // 1：通道关闭，0：通道开启
        if ("1".equals(channeMap.get("status").toString()))
            throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), channeMap.get("remark").toString());
        //目标地区流量包产品
        List<ProductDto.Product> products = productMapper.list(districtDto.getCarryOperator(), districtDto.getProvince());
        if (null == products || products.size() == 0) {
            //查询全国流量包产品
            products = productMapper.list(districtDto.getCarryOperator(), "00");
        }
        //湖南衡阳特殊产品
        if ("43".equals(districtDto.getProvince()) && !"衡阳".equals(districtDto.getCity()) && districtDto.getProvince().equals(1)) {
            for (Iterator<ProductDto.Product> iter = products.listIterator(); iter.hasNext(); ) {
                ProductDto.Product product = iter.next();
                if ("1G半年包".equals(product.getProductName())) {
                    iter.remove();
                }
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("carryOperator", districtDto.getCarryOperator());
        result.put("province", districtDto.getProvince());
        result.put("products", products);
        return result;
    }

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public DistrictDto queryMobileStation(String mobile) {
        return districtService.district(mobile.substring(0, 7));
    }

    @Override
    public void getProductForFlowStar() {
        String operatorId = protalProperties.getOperatorId();
        String url = protalProperties.getFlowProductUrl();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("operatorId", operatorId);
        try {
            String response = HttpUtils.sendGet(url, params);
            List<ProductInfo.ProductInfoWapper> productWappers = objectMapper.readValue(response, new TypeReference<List<ProductInfo.ProductInfoWapper>>() {
            });
            List<ProductInfo> productInfos = new ArrayList<>();
            productWappers.forEach(productWapper -> {
                List<ProductInfo> infos = productWapper.getList().stream().map(product -> {
                    product.setCity(productWapper.getCityCode());
                    product.setCarrierOperator(productWapper.getCarryOperator());
                    product.setProvince(productWapper.getProvinceCode());
                    product.setOperatorId(protalProperties.getOperatorId());
                    product.setType("STAND_PACKAGE");
                    product.setProductCode(product.getOperatorId() +
                            "STAND_PACKAGE" +
                            productWapper.getCarryOperator() +
                            product.getProvince() +
                            product.getCity() +
                            product.getProductName());
                    return product;
                }).collect(toList());
                productInfos.addAll(infos);
            });
            storeProductForCache(productInfos);
        } catch (Exception e) {
            logger.error("调用远程接口，查询流量产品，异常", e);
        }
    }

    private void storeProductForCache(List<ProductInfo> list) {
        HashMap<String, List<ProductInfo>> temp = new HashMap<String, List<ProductInfo>>();
        for (ProductInfo product : list) {
            String key = product.getProductCode();
            String json = JsonMapper.defaultMapper().toJson(product);
            stringRedisTemplate.opsForValue().set(protalProperties.getOperatorId() + key, json);
            String groupKey = product.groupKey(protalProperties.getOperatorId());//流量类型+运营商+省份+地市
            if (temp.containsKey(groupKey)) {
                List<ProductInfo> products = temp.get(groupKey);
                products.add(product);
            } else {
                List<ProductInfo> products = new ArrayList<>();
                products.add(product);
                temp.put(groupKey, products);
            }
        }

        //分组完毕，存入缓存
        for (Map.Entry<String, List<ProductInfo>> entry : temp.entrySet()) {
            String key = entry.getKey();
            List<ProductInfo> products = entry.getValue();
            String json = JsonMapper.defaultMapper().toJson(products);
            stringRedisTemplate.opsForValue().set(key, json);
        }
    }

    @Override
    public ProductResultDto findTraffic(String mobile, String openId) {
        ProductResultDto result = new ProductResultDto();
        try {
            //解析手机号运营商，省份，地区
            MoblileDistrictDto mobileDistrict = districtService.mobileDistrict(mobile.substring(0, 7));
            if (null == mobileDistrict)
                throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), "暂不支持当前地区充值");
            result.setVendor(mobileDistrict.getVendor());
            result.setProvince(mobileDistrict.getProvince());
            result.setCity(mobileDistrict.getCity());

            //------------------------------------------------------------------通用流量产品--------------------------------------------------------------

            //查询通道是否已开启
            Map<String, Object> channeMap = productMapper.channe(Integer.parseInt(mobileDistrict.getVendorCode()), mobileDistrict.getProvinceCode());
            // 1：通道关闭，0：通道开启
            if ("1".equals(channeMap.get("status").toString()))
                throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), "通道未开启");


            List<Product> standProducts = findProductForCache("STAND_PACKAGE", mobileDistrict, result, openId, protalProperties.getOperatorId());
            if (standProducts != null) {
                result.setStandProducts(standProducts);
            }
            //-------------------------------------------------- ----------------特殊流量产品--------------------------------------------------------------
            List<Product> specilProducts = findProductForCache("SPECIL_PACKAGE", mobileDistrict, result, openId, protalProperties.getOperatorId());
            if (specilProducts != null) {
                result.setSpecilProducts(specilProducts);
            }
        } catch (IOException exception) {
            throw new ApplicationException(FrontResponseCode.SYSTEM_ERROR.getCode(), "系统异常", exception);
        }
        return result;

    }


    /**
     * 从redis缓存中查询流量产品, 同时查询产品适用的卡券和不适用的卡券
     *
     * @param productType    产品类型（通用流量\特殊流量）
     * @param mobileDistrict 手机号码归属地对象
     * @return
     * @throws IOException
     */
    private List<Product> findProductForCache(String productType, MoblileDistrictDto mobileDistrict, ProductResultDto result, String openId, String operatorId) throws IOException {

        String key = mobileDistrict.getVPCKey(operatorId, productType);

        String json = redisUtil.opsForValue().get(key);//地市流量

        String vendorCode = mobileDistrict.getVendorCode();

        String provinceCode = mobileDistrict.getProvinceCode();

        String cityCode = mobileDistrict.getCityCode();

        int productLevle = 0;//默认地市流量

        if (StringUtils.isBlank(json)) {
            key = mobileDistrict.getVP0000Key(operatorId, productType);//分省流量
            json = redisUtil.opsForValue().get(key);
            productLevle = 1;//分省流量
        }
        if (StringUtils.isBlank(json)) {
            key = mobileDistrict.getV000000Key(operatorId, productType);//全国流量
            json = redisUtil.opsForValue().get(key);
            productLevle = 2;//全国流量
        }
        if (StringUtils.isNotBlank(json)) {
            List<Product> products = JsonMapper.defaultMapper().fromJsons(json, Product.class);
            for (Iterator<Product> it = products.iterator(); it.hasNext(); ) {
                Product product = it.next();
                if (product.getSalePrice().compareTo(BigDecimal.ZERO) == 0 || product.getState() == 0) {
                    it.remove();
                }
            }
            if (products == null || products.isEmpty() || "SPECIL_PACKAGE".equals(productType)) {
                return products;
            }
            Product firstProduct = products.get(0); //获取第一个流量包元素
            List<String> productIds = new ArrayList<>();
            for (Product product : products) {
                productIds.add(product.getProductName());
            }
            //适用卡券
            List<CardDto> useCard = null;
            //不适用卡券
            List<CardDto> notUseCard = null;
            //查询第一个包型下适用的卡券
            List<CardDto> productUseCard = null;

            if (productLevle == 0) {
                useCard = cardService.list(openId, vendorCode, provinceCode, cityCode, productType, productIds);
                productUseCard = cardService.list(openId, vendorCode, provinceCode, cityCode, productType, Arrays.asList(firstProduct.getProductName()));
            } else if (productLevle == 1) {
                useCard = cardService.list(openId, vendorCode, provinceCode, "", productType, productIds);
                productUseCard = cardService.list(openId, vendorCode, provinceCode, "", productType, Arrays.asList(firstProduct.getProductName()));
            } else if (productLevle == 2) {
                useCard = cardService.list(openId, vendorCode, "", "", productType, productIds);
                productUseCard = cardService.list(openId, vendorCode, "", "", productType, Arrays.asList(firstProduct.getProductName()));
            }
            List<String> cardIds = new ArrayList<String>();
            for (CardDto card : useCard) {
                cardIds.add(card.getCardId());
                CardServiceImpl.isCardUse(card);
            }

            notUseCard = cardService.notUseCard(openId, cardIds);

            BigDecimal stPrice = firstProduct.getStPrice();

            BigDecimal salePrice = firstProduct.getSalePrice();

            for (CardDto card : productUseCard) {

                CardServiceImpl.isCardUse(card);
                //计算第一个包型下最优的卡券(计算公式，折扣券：原价*折扣，代金券：销售价-减免金额)
                if ("DISCOUNT".equals(card.getCardType())) {

                    BigDecimal discount = new BigDecimal(100).subtract(new BigDecimal(card.getDiscount())).divide(new BigDecimal(10));

                    BigDecimal unitPrice = stPrice.multiply(discount).divide(new BigDecimal(10));

                    if (unitPrice.compareTo(salePrice) == 1 || unitPrice.compareTo(salePrice) == 0) {
                        //适用卡券更贵或者跟销售价一样
                        card.setCostlyFlag(true);
                    }
                    card.setCheapPrice(unitPrice);
                } else {
                    //代金券
                    BigDecimal unitPrice = salePrice.subtract(new BigDecimal(card.getReduceCost()).divide(new BigDecimal(100)));
                    card.setCheapPrice(unitPrice);
                }
            }
            if (notUseCard != null) {
                for (CardDto card : notUseCard) {
                    CardServiceImpl.isCardUse(card);
                }
            }

            //可适用卡券
            if (useCard != null)
                for (Iterator<CardDto> it = useCard.iterator(); it.hasNext(); ) {
                    CardDto card = it.next();
                    Date beginTimestamp = card.getBeginTimestamp();
                    Date endTimestamp = card.getEndTimestamp();
                    Date now = new Date();
                    if (card.getStatus() != 0 || now.before(beginTimestamp) || now.after(endTimestamp)) {
                        it.remove();
                    } else {
                        List<CardDto.CardAcceptCategory> cardAcceptCategorys = cardMapper.cardAcceptCategoryByCardId(card.getCardId());
                        card.setCardAcceptCategorys(cardAcceptCategorys);
                    }
                }

            //具体包型可适用卡券
            if (productUseCard != null)
                for (Iterator<CardDto> it = productUseCard.iterator(); it.hasNext(); ) {
                    CardDto card = it.next();
                    if (card.getStatus() != 0) {
                        it.remove();
                    } else {
                        List<CardDto.CardAcceptCategory> cardAcceptCategorys = cardMapper.cardAcceptCategoryByCardId(card.getCardId());
                        card.setCardAcceptCategorys(cardAcceptCategorys);
                    }
                }

            //不可以适用
            if (notUseCard != null)
                for (Iterator<CardDto> it = notUseCard.iterator(); it.hasNext(); ) {
                    CardDto card = it.next();
                    if (card.getStatus() != 0) {
                        it.remove();
                    }
                }
            result.setProductLevle(productLevle);
            result.setNotUseCard(notUseCard);
            result.setUseCard(useCard);
            result.setProductUseCard(productUseCard);
          /*  if (productType.equals("STAND_PACKAGE") && productLevle == 1 || productLevle == 0) {
                List<String> strings = activityProductsList();
                products.forEach(product -> {
                    if (strings.contains(protalProperties.getOperatorId()+product.getProductId()))
                        product.setIsActivity(true);
                });
            }*/
            return products;
        }
        return null;
    }

    @Override
    public List<ProductVO> findAllProduct(Integer carrierOperator, String province) {
        return productMapper.productList(carrierOperator, province);
    }

    @Override
    public void modify(List<ProductVO> productVO) {
        productMapper.modify(productVO);
    }

    @Override
    public void save(ProductCollection productCollection) {
        productMapper.save(productCollection);
    }

    @Override
    public int count(int carrierOperator, String province) {
        return productMapper.count(carrierOperator, province);
    }

    @Override
    public List<Product> findProductForCache(String productType, String vendor, String province, String city) throws IOException {

        String key = mgtProperties.getUsername() + productType + vendor + province + city;
        String json = stringRedisTemplate.opsForValue().get(key);
        logger.info("key:" + key + "----value" + json);
        List<Product> lists = JsonMapper.defaultMapper().fromJsons(json, Product.class);

        for (Iterator<Product> it = lists.iterator(); it.hasNext(); ) {
            Product product = it.next();
            if (product.getSalePrice().compareTo(BigDecimal.ZERO) == 0) {
                it.remove();
            }
        }
        return lists;
    }

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public ProductResultDto findActivityProducts(String mobile) {
        MoblileDistrictDto mobileDistrict = districtService.mobileDistrict(mobile.substring(0, 7));
        if (null == mobileDistrict)
            throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), "暂不支持当前地区充值");
        ProductResultDto productResultDto = new ProductResultDto();
        productResultDto.setVendor(mobileDistrict.getVendor());
        productResultDto.setProvince(mobileDistrict.getProvince());
        productResultDto.setCity(mobileDistrict.getCity());
        final List<Product> products = new ArrayList<>();
        mapProperties.getActivityProductPrice().forEach((k, v) -> {
            //1005STAND_PACKAGE 1 2100004G
            String vend = null;
            switch (mobileDistrict.getVendorCode()) {
                case "1":
                    vend = "CMCC";
                    break;
                case "2":
                    vend = "CUCC";
                    break;
                case "3":
                    vend = "CTCC";
                    break;
            }
            String key = protalProperties.getOperatorId() + "STAND_PACKAGE" + vend + mobileDistrict.getProvinceCode() + "0000" + k;
            String value = stringRedisTemplate.opsForValue().get(key);
            if (StringUtils.isBlank(value)) {
                key = protalProperties.getOperatorId() + "STAND_PACKAGE" + vend + "000000" + k;
                value = stringRedisTemplate.opsForValue().get(key);
            }
            logger.info("key:" + key + "------" + "value:" + value);
            Product product = JsonMapper.defaultMapper().fromJson(value, Product.class);
            products.add(product);
        });
        productResultDto.setStandProducts(products.stream().sorted(Comparator.comparing(Product::getSalePrice)).collect(toList()));
        return productResultDto;
    }

    @Override
    public List<String> activityProductsList() {
        return null;
       /* Set<String> keys = stringRedisTemplate.keys("*" + protalProperties.getOperatorId() + "STAND_PACKAGECMCC34*");
        return keys.stream().filter(s -> Pattern.compile("STAND_PACKAGECMCC34(\\d{4})1G|100M|500M").matcher(s).find()).collect(toList());*/
    }
}
