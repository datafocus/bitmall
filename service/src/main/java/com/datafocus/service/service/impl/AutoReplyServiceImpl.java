package com.datafocus.service.service.impl;

import java.util.List;

import com.datafocus.common.pojo.ActiveMessage;
import com.datafocus.common.pojo.Regular;
import com.datafocus.service.mapper.AutoReplyMapper;
import com.datafocus.service.service.AutoReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AutoReplyServiceImpl implements AutoReplyService {

	@Autowired
	private AutoReplyMapper autoReplyMapper;
	
	@Override
	public void saveRegular(Regular regular)
	{
		autoReplyMapper.delete(regular.getId());
		autoReplyMapper.saveRegular(regular);
		generateRegular(regular);
		autoReplyMapper.saveKeyword(regular.getKeyWords());
		autoReplyMapper.saveKeywordMessage(regular.getKeyWordMessages());
	}

	public void generateRegular(Regular regular)
	{
		for (Regular.KeyWord keyWord : regular.getKeyWords())
		{
			keyWord.setRegularId(regular.getId());
		}
		for (Regular.KeyWordMessage keyWordMsg : regular.getKeyWordMessages())
		{
			keyWordMsg.setRegularId(regular.getId());
		}
	}
	
	@Override
	public void savePassive(ActiveMessage activeMessage)
	{
		autoReplyMapper.savePassive(activeMessage);
	}

	@Override
	public List<Regular> regularList()
	{
		List<Regular> regulars =autoReplyMapper.selectRegularList();
		for (Regular regular : regulars)
		{
			List<Regular.KeyWord> KeyWords =autoReplyMapper.selectKeyWords(regular.getId());
			regular.setKeyWords(KeyWords);
			List<Regular.KeyWordMessage> keyWordMessages = autoReplyMapper.selectKeyWordMessages(regular.getId());
			regular.setKeyWordMessages(keyWordMessages);
		}
		return regulars;
	}

	@Override
	public ActiveMessage passive() 
	{
		return autoReplyMapper.one();
	}

	@Override
	public void delete(Integer id)
	{
		autoReplyMapper.delete(id);
		
	}

}
