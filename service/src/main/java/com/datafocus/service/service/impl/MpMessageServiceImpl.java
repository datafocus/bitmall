package com.datafocus.service.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.datafocus.common.pojo.CustomTag;
import com.datafocus.service.mapper.CustomTagMapper;
import com.datafocus.service.service.MpMessageService;
import com.datafocus.service.task.UnTagDelayed;
import com.datafocus.service.task.UnTagDelayedService;
import com.datafocus.weixin.mp.base.AppSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datafocus.weixin.mp.message.MpMessages;
import com.datafocus.weixin.mp.user.Tags;

@Service
public class MpMessageServiceImpl implements MpMessageService {

	private static Logger logger = LoggerFactory.getLogger(MpMessageServiceImpl.class);
	@Autowired
	private CustomTagMapper customTagMapper;
	@Autowired
	private UnTagDelayedService unTagDelayedService;
	@Autowired
	private AppSetting mpSetting;

	private MpMessages  mpMessages;
	private Tags tags;
	
	@PostConstruct
	public void init(){
		tags = Tags.with(mpSetting);
		mpMessages = MpMessages.with(mpSetting);
	}

	@Override
	public void send(String msg, String target, String msgType, String targetGroupId){
		
		int tagId=0;
		int customTagId = Integer.parseInt(targetGroupId);
		// 按群组发送消息
		// 1,先同系统标签到到微信 .
		// 2,同步标签和用户的关系到微信
		if(!"-1".equals(target)){
			//按照用户群组发送,按照系统用户群组id查找标签名称
			
			CustomTag customTag = customTagMapper.customTag("", customTagId);
			
			String tagName = customTag.getTagName();
			
			//同步标签名称到微信
			com.datafocus.weixin.mp.user.bean.Tag tag = tags.create(tagName);
			
			//获取系统标签下所有的用户openid
			
			List<String> openIds = customTagMapper.customTagUsersByTagId(customTagId);
			
			tagId = tag.getId();
			//同步微信标签和用户openid的关联关系
			syncUsersTag(openIds,tagId);
		}
		
		long msgId =0L;
		// 判断群发类型（群组，全部用户）,消息类型（卡券,文本,图片,素材......）
		switch (msgType) {
		// 开始调用微信接口发送群发消息
		case "0"://文字消息
			if("-1".equalsIgnoreCase(target)){
				//发送所有人
				msgId = mpMessages.text(msg);
			}else{
				//按分组标签发送
				msgId = mpMessages.text(tagId, msg);
			}
			break;
		case "1": //图文素材
			
			if("-1".equalsIgnoreCase(target)){
				//发送给所有人
				msgId = mpMessages.mpNews(msg);
			}else{
				//按分组标签发送
				msgId = mpMessages.mpNews(tagId, msg);
			}
			break;
		case "2"://图片
			if("-1".equalsIgnoreCase(target)){
				//发送给所有人
				msgId = mpMessages.image(msg);
			}else{
				//按分组标签发送
				msgId = mpMessages.image(tagId, msg);
			}
			break;
		case "3": //卡券
			if("-1".equalsIgnoreCase(target)){
				//发送给所有人
				msgId = mpMessages.card(msg);
			}else{
				//按分组标签发送
				msgId = mpMessages.card(tagId, msg);
			}
			break;
		default:
			break;
		}
		
		
		
		//延时处理，删除创建的微信标签
		if(!"-1".equals(target)){
			long delayTime = 20;
			UnTagDelayed delayed = new UnTagDelayed(tagId, msgId, delayTime);
			unTagDelayedService.add(delayed);
		}
		
		
	}
	
	/**
	 * 同步微信标签与用户的关系
	 * @param openIds
	 * @param tagId
	 */
	private  void syncUsersTag(List<String> openIds ,final int tagId){
		List<List<String>> lists = groupListByQuantity(openIds,50);
		logger.info("群发消息，用户组长度.{}"+lists.size());
		for ( final List<String> list : lists) 
		{
			new Thread(new Runnable() {
				@Override
				public void run() {
					tags.tagUsers(tagId, list);
				}
			}).start();
		}
	}
	
	/**
	 * 按数据切割list
	 * @param list
	 * @param quantity
	 * @return
	 */
    private  static List groupListByQuantity(List list, int quantity) {
        if (list == null || list.size() == 0) {
            return list;
        }
        
        if (quantity <= 0) {
            new IllegalArgumentException("Wrong quantity.");
        }
        
        List wrapList = new ArrayList();
        int count = 0;
        while (count < list.size()) {
            wrapList.add(new ArrayList(list.subList(count, (count + quantity) > list.size() ? list.size() : count + quantity)));
            count += quantity;
        }
        
        return wrapList;
    }

	@Override
	public void preview(String wxName, String openId, String msgType, String message){
		if (StringUtils.isBlank(msgType)) {
			throw new IllegalArgumentException("微信号不能为空");
		}
		try {
			switch (msgType) {
			case "0":
				// 文字消息
				mpMessages.textPreview(wxName, openId, message);
				break;
			case "1":
				// 图文消息
				mpMessages.mpNewsPreview(wxName, openId, message);
				break;
			case "2":
				// 图片
				mpMessages.imagePreview(wxName, openId, message);
				break;
			case "3":
				// 卡券
				mpMessages.cardPreview(wxName, openId, message);
				break;
			default:
				throw new IllegalArgumentException("无效的消息类型");
			}
		} catch (Exception e) {
			logger.error("微信高级群发预览系统异常", e);
			throw e;
		}

	}
    
}
