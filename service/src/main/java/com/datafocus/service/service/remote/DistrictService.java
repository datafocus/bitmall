package com.datafocus.service.service.remote;

import com.datafocus.common.dto.DistrictDto;
import com.datafocus.common.dto.MoblileDistrictDto;
import com.datafocus.common.interfaces.IDistrictController;
import com.datafocus.common.pojo.DistrictData;
import com.datafocus.common.pojo.PubDistrict;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author layne
 * @date 17-8-3
 */
@FeignClient("DISTRICT-SERVICE")
public interface DistrictService extends IDistrictController {
    @Override
    @GetMapping("/bitmall-district/district/findByMobile")
    DistrictDto district(@RequestParam("mobile") String mobile);

    @Override
    @GetMapping("/bitmall-district/district/findAllPubDistrict")
    List<PubDistrict> findAllPubDistrict();

    @Override
    @GetMapping("/bitmall-district/district/findPubDistrictByDname")
    PubDistrict findPubDistrictByDname(@RequestParam("name") String name);

    @Override
    @GetMapping("/bitmall-district/district/findDistrictData")
    List<DistrictData> findDistrictData();

    @Override
    @GetMapping("/bitmall-district/district/mobileDistrict")
    MoblileDistrictDto mobileDistrict(@RequestParam("mobile") String phone);
}
