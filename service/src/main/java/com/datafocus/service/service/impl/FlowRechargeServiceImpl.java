package com.datafocus.service.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.datafocus.common.dto.FlowOrderDto;
import com.datafocus.service.mapper.CardMapper;
import com.datafocus.service.mapper.FlowRechargeMapper;
import com.datafocus.service.service.FlowRechargeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class FlowRechargeServiceImpl implements FlowRechargeService {

    private static final Logger logger = LoggerFactory.getLogger(FlowRechargeServiceImpl.class);

    @Autowired
    private FlowRechargeMapper flowRechargeMapper;

    @Autowired
    private CardMapper cardMapper;

    @Override
    @Transactional
    public List<FlowOrderDto> findRechargeOrderByOrderStatusAndSubmitStatus() {
        List<FlowOrderDto> rechargeOrder = flowRechargeMapper.findRechargeOrderByOrderStatusAndSubmitStatus();

        List<Integer> ids = new ArrayList<Integer>();
        try {
            for (FlowOrderDto flowOrder : rechargeOrder)
                ids.add(flowOrder.getId());
            if (ids.size() > 0)
                flowRechargeMapper.batchUpdateRechargeSubmitStatus(ids);

        } catch (Exception e) {
            logger.error("批量更新充值订单状态sql 异常---------->", e);
            rechargeOrder = null;
            throw e;
        }
        return rechargeOrder;
    }

    @Override
    public void modifyOrderSubmitStatusAndRechargeStatus(String orderNo, int submitStatus, String msgid, String gallerysubmitCode) {
        flowRechargeMapper.modifyOrderSubmitStatus(orderNo, submitStatus, msgid, gallerysubmitCode);
    }

    public String findCardDescByCardId(String cardId) {
        return cardMapper.selectDescById(cardId);
    }

}
