package com.datafocus.service.service;

import java.util.List;

import com.datafocus.common.dto.FlowOrderDto;
import com.datafocus.common.dto.NewerOrderExport;
import com.datafocus.common.dto.UserInfoDto;
import com.datafocus.common.dto.UserInfoQuery;
import com.datafocus.weixin.mp.event.ticket.SceneSubEvent;
import com.github.pagehelper.PageInfo;

public interface UserInfoService {
	
	/**
	 * 保存用户信息（用户关注）
	 * @param userSub
	 * @throws Exception
	 */
	void saveUserInfo(SceneSubEvent userSub);
	
	/**
	 * 获取用户列表
	 * @param userInfoQuery
	 * @return
	 */
	PageInfo<UserInfoDto> list(UserInfoQuery userInfoQuery);

    /**
     *
     * @param userInfoQuery
     * @return
     */
	List<NewerOrderExport> listExport(UserInfoQuery userInfoQuery);

	/**
	 * 查询用户充值记录
	 * @return
	 */
	PageInfo<FlowOrderDto> orderByOpenId(Integer page,Integer size,String openId);
	
	/**
	 * 根据openId 查询用户记录
	 * @param openId
	 * @return
	 */
	UserInfoDto get(String openId);

	List<String> getAllOpenId();

}
