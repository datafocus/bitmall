package com.datafocus.service.service;


import com.datafocus.common.pojo.Tag;

import java.util.List;

/**
 * @author layne
 * @date 17-7-26
 */
public interface TagsService {

    /**
     * 保存标签
     *
     * @param tag
     */
    void saveTag(Tag tag);

    /**
     * 删除标签
     *
     * @param tagId
     */
    void delete(Integer tagId);

    /**
     * 标签列表
     *
     * @return
     */
    List<Tag> list();

    List<String> getOpenIdList(Integer tagId);
    /**
     * 保存用户标签
     *
     * @param openId
     * @param tagId
     */
    void userTags(String openId, List<Integer> tagId);

    /**
     * 用户的所有标签 Id
     *
     * @param openId
     * @return
     */
    List<Integer> userTagByOpenId(String openId);

    /**
     * 手工同步本地用户标签至微信标签
     */
    void task();
}
