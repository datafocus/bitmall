package com.datafocus.service.service;

import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.common.pojo.Promoter;

import java.util.List;
import java.util.Map;

public interface PromoterService {
    /**
     * 保存
     */
    String save(Promoter promoter);

    /**
     * 我的数据同居
     *
     * @param openId
     * @return
     */
    Map<String, Object> myFlowAnalysis(String openId);

    /**
     *
     */
    void yearlyAnalysis();

    /**
     *
     */
    void allAnalysis();

    /**
     * @return
     */
    List<String> getPromoterSenceId();

    boolean isNewer(String openId);

    void addOrUpdate(FlowOrder order);
}
