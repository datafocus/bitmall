package com.datafocus.service.service;


import com.datafocus.service.utils.BaseResult;

public interface TemplateMsgService {
	
	/**
	 * 发送模板消息
	 * @param orderId
	 * @param msgType 消息类型 ->1,充值成功、2,充值失败
	 */
	BaseResult sendTempletMsg(String orderId, String msgType);

	public void sendAlarmMessage(int type, String title, String openId, String info);
}
