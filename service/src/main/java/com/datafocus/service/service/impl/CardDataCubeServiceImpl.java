package com.datafocus.service.service.impl;

import java.util.List;

import com.datafocus.common.pojo.CardInfoCube;
import com.datafocus.service.mapper.CardDataCubeMapper;
import com.datafocus.service.service.CardDataCubeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.datafocus.weixin.mp.card.bean.CardDataCube;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class CardDataCubeServiceImpl implements CardDataCubeService {

	@Autowired
	private CardDataCubeMapper cardDataCubeMapper;
	
	@Override
	public PageInfo<CardInfoCube> list(Integer page, Integer size , String cardId, String beginTime, String endTime) {
		PageHelper.startPage(page, size);
		List<CardInfoCube> cardInfoCubes = cardDataCubeMapper.cardInfoCubeList(cardId, beginTime, endTime);
		PageInfo<CardInfoCube> pageInfo = new PageInfo<CardInfoCube>(cardInfoCubes);
		return pageInfo;
	}

	@Override
	public PageInfo<CardDataCube> list(Integer page, Integer size,String beginTime, String endTime) {
		PageHelper.startPage(page, size);
		List<CardDataCube> cardDataCubes = cardDataCubeMapper.cardDataCubeList(beginTime, endTime);
		PageInfo<CardDataCube> pageInfo = new PageInfo<CardDataCube>(cardDataCubes);
		return pageInfo;
	}

}
