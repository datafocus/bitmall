package com.datafocus.service.service.impl;

import com.datafocus.common.pojo.EnterRecord;
import com.datafocus.service.mapper.EnterRecordMapper;
import com.datafocus.service.service.EnterRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by layne on 2017/12/6.
 */
@Service
public class EnterRecordServiceImpl implements EnterRecordService {

    private Logger logger = LoggerFactory.getLogger(EnterRecordServiceImpl.class);

    @Autowired
    EnterRecordMapper enterRecordMapper;

    @Override
    public void enterRecord(String openId) {
        Date now = new Date();
        int i = enterRecordMapper.enterCount(openId);
        if (i == 0){
            enterRecordMapper.save(new EnterRecord().setOpenId(openId).setEnterTime(now));
            logger.info("********保存用户进入H5页面信息********");
        }
    }

}
