package com.datafocus.service.service;

import com.datafocus.common.pojo.CardStore;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


/**
 * 卡券货架接口
 * @author yyhinfo
 *
 */
public interface CardStoreService {

	/**
	 * 保存卡券货架
	 * @param cardStore
	 * @return
	 * @throws Exception
	 */
	void saveCardStore(CardStore cardStore, MultipartFile bannerFile) throws IOException;
	
	/**
	 * 删除卡券货架
	 * @param pageId
	 * @return
	 * @throws Exception
	 */
	void deleteCardStore(String pageId);
	
	/**
	 * 卡券货架列表
	 * @return
	 */
	List<CardStore> cardStoreList();
 
}
