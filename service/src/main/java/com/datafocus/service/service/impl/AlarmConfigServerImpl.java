package com.datafocus.service.service.impl;

import com.datafocus.common.pojo.AlarmConfig;
import com.datafocus.service.mapper.AlarmConfigMapper;
import com.datafocus.service.service.AlarmConfigService;
import com.datafocus.service.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by liuheng on 2017/9/1.
 */
@Service
public class AlarmConfigServerImpl implements AlarmConfigService {

    @Autowired
    private AlarmConfigMapper alarmConfigMapper;
    @Autowired
    private RedisUtil<String,AlarmConfig> redisUtil;
    @Value("${alarm.configKey}")
    private String configKey;
    @Value("${bit.wx.appSetting.appId}")
    private String appId;



    @Override
    public void updateConfig(AlarmConfig alarmConfig) {
        alarmConfigMapper.updateConfig(alarmConfig);
        AlarmConfig config=redisUtil.getValue(appId+configKey+alarmConfig.getId());
        if(alarmConfig.getPercent()!=null){
            config.setPercent(alarmConfig.getPercent());
        }
        if(alarmConfig.getConTimes()!=null){
            config.setConTimes(alarmConfig.getConTimes());
        }
        if(alarmConfig.getBaseTimes()!=null){
            config.setBaseTimes(alarmConfig.getBaseTimes());
        }
        if(alarmConfig.getStatus()!=null){
            config.setStatus(alarmConfig.getStatus());
        }
        if(alarmConfig.getDescription()!=null){
            config.setDescription(alarmConfig.getDescription());
        }
        redisUtil.addOrUpdate(appId+configKey+config.getId(),config);
    }

    @Override
    public List<AlarmConfig> listConfig() {
        List<AlarmConfig> result = new ArrayList<>();
        Set<AlarmConfig> set=redisUtil.keys(appId+configKey);
        if(set!=null&&!set.isEmpty()){
            result.addAll(set);
        }else{
            result=init();
        }
        return result;
    }

    @Override
    public AlarmConfig getConfig(int id) {
        AlarmConfig alarmConfig=redisUtil.getValue(appId+configKey+id);
        if(alarmConfig==null){
            init();
            alarmConfig=redisUtil.getValue(appId+configKey+id);
        }
        return alarmConfig;
    }

    @Override
    public List<AlarmConfig> init(){
        List<AlarmConfig> list=alarmConfigMapper.listConfig();
        for (AlarmConfig config : list) {
            redisUtil.addOrUpdate(appId+configKey+config.getId(),config);
        }
        return list;
    }
}
