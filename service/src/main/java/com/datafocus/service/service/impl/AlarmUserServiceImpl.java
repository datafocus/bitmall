package com.datafocus.service.service.impl;

import com.datafocus.common.pojo.AlarmUser;
import com.datafocus.service.mapper.AlarmUserMapper;
import com.datafocus.service.service.AlarmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by liuheng on 2017/9/1.
 */
@Service
public class AlarmUserServiceImpl implements AlarmUserService {

    @Autowired
    private AlarmUserMapper alarmUserMapper;

    @Override
    public void addAlarmUser(AlarmUser alarmUser) {
        alarmUserMapper.addAlarmUser(alarmUser);
    }

    @Override
    public List<AlarmUser> listAlarmUser() {
        return alarmUserMapper.listAlarmUser();
    }

    @Override
    public List<AlarmUser> listOpenUser() {
        return alarmUserMapper.listOpenUser();
    }

    @Override
    public void updateStatus(int id, int status) {
        alarmUserMapper.updateStatus(id, status);
    }

}
