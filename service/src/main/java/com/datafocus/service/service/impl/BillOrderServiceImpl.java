package com.datafocus.service.service.impl;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.pojo.BillOrder;
import com.datafocus.common.utils.ExcelUtil;
import com.datafocus.service.mapper.BillOrderMapper;
import com.datafocus.service.service.BillOrderService;
import com.datafocus.service.task.RefundOrderDelayed;
import com.datafocus.service.task.SmsSendThread;
import com.datafocus.service.utils.DFUtils;
import com.datafocus.weixin.pay.base.PaySetting;
import com.datafocus.weixin.pay.mp.Orders;
import com.datafocus.weixin.pay.mp.bean.RefundRequest;
import com.datafocus.weixin.pay.mp.bean.RefundResponse;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by liuheng on 2017/7/27.
 */
@Service
public class BillOrderServiceImpl implements BillOrderService {

    private static Logger logger = LoggerFactory.getLogger(BillOrderServiceImpl.class);

    @Autowired
    private BillOrderMapper billOrderMapper;

    @Autowired
    private Orders orders;
    @Autowired
    private PaySetting paySetting;

    @Override
    public void saveBillOrder(BillOrder billOrder) {
        billOrderMapper.saveBillOrder(billOrder);
    }

    @Override
    public PageInfo<BillOrder> list(Map<String, String> params, FlowPage flowPage) {
        PageHelper.startPage(flowPage.getPage(), flowPage.getSize(), "create_time desc");
        Page<BillOrder> billOrders = billOrderMapper.list(params);
        return billOrders.toPageInfo();
    }

    @Override
    public void modify(Map<String, Object> params) {

        ExecutorService smsPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 4);

        String dealState = (String) params.get("dealState");

        List<String> billOrders = (List<String>) params.get("billOrders");
        params.put("modifyTime", new Date());
        if ("2".equals(dealState)) {        //充值成功

            //更新数据库状态
            params.put("orders", billOrders);
            billOrderMapper.batchModify(params);
            /**模板消息推送线程池*/

            //发送模板消息，充值成功
            for (String orderNo : billOrders) {
                //发送模板消息，充值失败
                smsPool.execute(new SmsSendThread(orderNo, "5"));//5->话费充值成功模板消息
            }

        } else if ("3".equals(dealState)) {//充值失败
            int refundState = 0;
            for (String orderNo : billOrders) {
                BillOrder billorder = billOrderMapper.findBillOrderByOrderNo(orderNo);
                RefundRequest refundRequest = bulidRefundRequest(billorder);
                try {
                    RefundResponse refundResponse = orders.refund(refundRequest);
                    if (refundResponse.success()) {
                        refundState = 1;//退款中
                        int startTime = RefundOrderDelayed.startTimeMap.get(1);//第一次查询
                        RefundOrderDelayed refund = new RefundOrderDelayed(2,orderNo, startTime);
                        // refundOrderDelayService.add(refund);
                    } else {
                        refundState = 3;//退款失败
                        logger.error("微信自动退款失败,比特星人订单号:{},错误信息{},业务错误信息:{}", orderNo, refundResponse.getReturnMessage(), refundResponse.getErrorCodeDesc());
                    }
                } catch (Exception e) {
                    refundState = 5;//退款失败
                    logger.error("微信自动退款异常,异常堆栈信息:{}", e);
                    throw e;
                }
                //更新数据库状态
                params.put("refundState", refundState);
                params.put("orders", Arrays.asList(orderNo));
                billOrderMapper.batchModify(params);
                //发送模板消息，充值失败
                smsPool.execute(new SmsSendThread(orderNo, "6"));//4->话费充值失败模板消息
            }
        }
    }

    private RefundRequest bulidRefundRequest(BillOrder order) {
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOperatorId(paySetting.getMchId());
        refundRequest.setTotalFee(order.getSalePrice().multiply(new BigDecimal("100")).intValue());
        refundRequest.setRefundFee(order.getSalePrice().multiply(new BigDecimal("100")).intValue());
        refundRequest.setRefundFeeType("CNY");
        refundRequest.setRefundNumber(DFUtils.createBillNo("2000"));
        refundRequest.setTradeNumber(order.getOrderNo());
        return refundRequest;
    }

    @Override
    public void exprot(HashMap<String, String> params, OutputStream outputStream) {
        List<BillOrder> orders = billOrderMapper.list(params);

        ExcelUtil<BillOrder> excelUtil = new ExcelUtil<>(BillOrder.class);

        //修改订单状态为充值中
        List<String> list = new ArrayList<>();
        for (BillOrder billOrder : orders) {
            list.add(billOrder.getOrderNo());
        }
        if (list.size() > 0) {
            HashMap<String, Object> modify = new HashMap<>();
            modify.put("dealState", 1);
            modify.put("orders", list);
            modify.put("flag", 1);
            billOrderMapper.batchModify(modify);
        }

        try {
            excelUtil.getListToExcel(orders, "", outputStream);
        } catch (Exception e) {
            logger.error("导出话费订单异常", e);
        }

    }
}
