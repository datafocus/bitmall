package com.datafocus.service.service.impl;


import com.datafocus.common.config.ProtalProperties;
import com.datafocus.common.pojo.BillOrder;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.common.utils.DateUtil;
import com.datafocus.service.mapper.BillOrderMapper;
import com.datafocus.service.mapper.FlowOrderMapper;
import com.datafocus.service.service.TemplateMsgService;
import com.datafocus.service.utils.BaseResult;
import com.datafocus.weixin.common.exception.WxRuntimeException;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.template.Data;
import com.datafocus.weixin.mp.template.Templates;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class TemplateMsgServiceImpl implements TemplateMsgService {

	private static final Logger logger = LoggerFactory.getLogger(TemplateMsgServiceImpl.class);
	@Autowired
	private FlowOrderMapper flowOrderMapper;
	@Autowired
	private BillOrderMapper billOrderMapper;
	private Templates templates;
	@Autowired
	private ProtalProperties protalProperties;
	@Autowired
	private AppSetting mpSetting;

	@PostConstruct
	public void init(){
		templates= Templates.with(mpSetting);
	}

	@Override
	public BaseResult sendTempletMsg(String orderId, String msgType)
	{
		
		try {
			if(StringUtils.isNotBlank(orderId))
			{
				//推送模板消息给微信用户
				FlowOrder order = flowOrderMapper.findOrderByOrderNo(orderId);
				BillOrder billOrder = billOrderMapper.findBillOrderByOrderNo(orderId);
				Map<String, Data> messages;
				switch (msgType) {
				//发送流量充值成功模板消息
				case "1":
					messages = bulidTrafficForSuccess(order);
					templates.send(order.getOpenId(), protalProperties.getTemplateSuccess(), "", messages);
					break;
				case "2":
					messages = bulidTrafficForFail(order);
					templates.send(order.getOpenId(), protalProperties.getTemplateFail(), "", messages);
					break;
				case "3":
					messages = bulidTrafficForDelay(order);
					templates.send(order.getOpenId(), protalProperties.getTemplateDelay(), "", messages);
					break;
				case "4":
					messages = bulidTrafficForAccept(order);
					templates.send(order.getOpenId(), protalProperties.getTemplateAccept(), "", messages);
					break;
					//话费充值模板消息
				case "5"://成功
					billOrderMapper.findBillOrderByOrderNo(orderId);
					messages = bulidBillForSuccess(billOrder);
					templates.send(billOrder.getOpenId(), "ZKyfXiiKBnb3D4-9zh4dkrdYusiLaqL_GIASkU6cUCg", "", messages);
					break;
				case "6"://失败
					messages = bulidBillForFail(billOrder);
					templates.send(billOrder.getOpenId(), "IYNgVAfUxK-84uAXPqulI9T-ATcxJqYP7RqKNMAjW78", "", messages);
					break;
				default:
					return new BaseResult("-1","无效的消息类型");
				}
			}
		} catch (WxRuntimeException e) {
			logger.error("发送模板消息接口异常",e);
			return new BaseResult("-1",e.getMessage());
		}catch(Exception e){
			logger.error("发送模板消息异常",e);
			return new BaseResult("-1","发送模板消息失败");
		}
		return new BaseResult("0","发送成功"); 
	}

	@Override
	public void sendAlarmMessage(int type,String title,String openId,String info){
		Map<String, Data> messages = new HashMap<>();
		messages.put("first", new Data(title, "#173177"));
		messages.put("remark", new Data("情况紧急！", "#173177"));
		messages.put("keyword1", new Data("告警模块", "#173177"));
		if(type==1){
			messages.put("keyword2", new Data("已经连续失败"+info+"单", "#173177"));
		}else{
			messages.put("keyword2", new Data("充值失败率高达"+info+"%", "#173177"));
		}
		messages.put("keyword3", new Data(DateUtil.DateToString(new Date(), DateUtil.DateStyle.YYYY_MM_DD_HH_MM_SS), "#173177"));
		templates.send(openId, protalProperties.getTemplateAlarm(),"",messages);
		logger.info("发送模板消息openId="+openId);
	}



	/**
	 * 流量充值成功通知
	 * @param order
	 * @return
	 */
	private Map<String, Data> bulidTrafficForSuccess(FlowOrder order)
	{
		Map<String, Data> messages = new HashMap<>();
		messages.put("first", new Data("您好，您已成功购买流量，所购买流量的充值到账请以运营商短信通知为准。", "#173177"));
		messages.put("keyword1", new Data(order.getOrderNo(), "#173177"));
		messages.put("keyword2", new Data(getFlowPackage(order), "#173177"));
		messages.put("keyword3", new Data(new DecimalFormat("0.00").format(order.getOrderPrice()), "#173177"));
		messages.put("keyword4", new Data(order.getOrderPhone(), "#173177"));
		messages.put("keyword5", new Data(DateUtil.DateToString(order.getOrderTime(), DateUtil.DateStyle.YYYY_MM_DD_HH_MM_SS), "#173177"));
//		messages.put("remark", new Data("如您充值国内流量未成功使用优惠券（买一赠一不与优惠券同享），可联系公众号客服退差价哦！", "#ff0000"));
		return messages;
	}
	/**
	 * 流量充值失败通知
	 * @param order
	 * @return
	 */
	private Map<String, Data> bulidTrafficForFail(FlowOrder order)
	{
		Map<String, Data> messages = new HashMap<>();
		messages.put("first", new Data("您好，非常抱歉，您购买的流量因运营商系统原因充值失败，现将为您退款至原支付账户，退款将在1-3个工作日到帐。", "#173177"));
		messages.put("keyword1", new Data(order.getOrderNo(), "#173177"));
		messages.put("keyword2", new Data(getFlowPackage(order), "#173177"));
		messages.put("keyword3", new Data(new DecimalFormat("0.00").format(order.getOrderPrice()), "#173177"));
		messages.put("keyword4", new Data( order.getOrderPhone(), "#173177"));
		messages.put("remark", new Data("欢迎再次购买！如有疑问，请通过公众号联系我们！", "#173177"));
		return messages;
	}
	/**
	 * 发送流量充值延时模板消息
	 * @param order
	 * @return
	 */
	private Map<String, Data> bulidTrafficForDelay(FlowOrder order)
	{
		Map<String, Data> messages = new HashMap<>();
		messages.put("first", new Data("您好，非常抱歉，由于当地运营商充值系统繁忙，您的"+getFlowPackage(order)+"流量包目前处于订单排队状态。", "#173177"));
		messages.put("number", new Data(order.getOrderPhone(), "#173177"));
		messages.put("amount", new Data(new DecimalFormat("0.00").format(order.getOrderPrice()), "#173177"));
		messages.put("result", new Data("努力充值中", "#173177"));
		messages.put("remark", new Data("再等一下，马上就好，请您耐心等待。谢谢谅解！", "#173177"));
		return messages;
	}
	/**
	 * 流量受理通知
	 * @param order
	 * @return
	 */
	private Map<String, Data> bulidTrafficForAccept(FlowOrder order)
	{
		Map<String, Data> messages = new HashMap<>();
		messages.put("first", new Data("您的订单"+getFlowPackage(order)+"流量包已被受理，正在充值中。", "#173177"));
		messages.put("number", new Data(order.getOrderPhone(), "#173177"));
		messages.put("value", new Data(new DecimalFormat("0.00").format(order.getOrderPrice()), "#173177"));
		messages.put("remark", new Data( "温馨提示：月初月末由于充值用户过多会导致运营商充值系统繁忙，因此可能会发生充值延迟到账。如需帮助请联系“在线客服”。", "#173177"));
		return messages;
	}
	
	private Map<String,Data> bulidBillForSuccess(BillOrder order){
		Map<String, Data> messages = new HashMap<>();
		messages.put("first", new Data("恭喜您，"+order.getAmount()+"元话费已经充值成功！", "#173177"));
		messages.put("keyword1", new Data(order.getOrderPhone(), "#173177"));
		messages.put("keyword2", new Data(new DecimalFormat("0.00").format(order.getSalePrice()), "#173177"));
		messages.put("remark", new Data("感谢使用比特星人话费充值", "#173177"));
		return messages;
	}
	private Map<String,Data> bulidBillForFail(BillOrder order){
		Map<String, Data> messages = new HashMap<>();
		messages.put("first", new Data("非常抱歉，运营商的充值系统忙晕了，我们话费小分队虽然几经奋力尝试，但小主的"+order.getAmount()+"元话费还是没能充值成功，T_T，我们已为您办理退款。", "#173177"));
		messages.put("number", new Data(order.getOrderPhone(), "#173177"));
		messages.put("value", new Data(new DecimalFormat("0.00").format(order.getSalePrice()), "#173177"));
		messages.put("remark", new Data("退还的银两一般需要1-3个工作日返回到您的原支付账户中。", "#173177"));
		return messages;
	}

	public String getFlowPackage(FlowOrder flowOrder){
		String flowPackage=flowOrder.getFlowPackage();
		if("thanks".equals(flowOrder.getCardId())){
			switch (flowPackage){
				case "100M":
					flowPackage = "300M";
					break;
				case "500M":
					flowPackage = "1G";
					break;
				case "1G":
					flowPackage = "2G";
					break;
				default:
			}
		}
		return flowPackage;
	}

}
