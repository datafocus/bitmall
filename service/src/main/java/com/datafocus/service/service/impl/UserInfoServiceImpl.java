package com.datafocus.service.service.impl;

import com.datafocus.common.config.MgtProperties;
import com.datafocus.common.dto.*;
import com.datafocus.common.pojo.*;
import com.datafocus.service.mapper.*;
import com.datafocus.service.service.CustomTagService;
import com.datafocus.service.service.UserInfoService;
import com.datafocus.service.service.remote.DistrictService;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.care.CareMessages;
import com.datafocus.weixin.mp.event.ticket.SceneSubEvent;
import com.datafocus.weixin.mp.user.Users;
import com.datafocus.weixin.mp.user.bean.User;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;


@Service
public class UserInfoServiceImpl implements UserInfoService {

    private static Logger logger = LoggerFactory.getLogger(UserInfoServiceImpl.class);

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private AutoReplyMapper autoReplyMapper;
    @Autowired
    private TicketMapper ticketMapper;
    @Autowired
    private CustomTagService customTagService;
    @Autowired
    private DistrictService districtService;
    @Autowired
    private AppSetting mpSetting;
    @Autowired
    private PromoterMapper promoterMapper;
    @Autowired
    private CustomTagMapper customTagMapper;
    @Autowired
    private CardMapper cardMapper;
    @Autowired
    private FlowOrderMapper flowOrderMapper;

    private Users users;
    private CareMessages careMessages;

    @Autowired
    private MgtProperties mgtProperties;

    @PostConstruct
    public void init() {
        users = Users.with(mpSetting);
        careMessages = CareMessages.with(mpSetting);
    }

    @Override
    public void saveUserInfo(SceneSubEvent userSub) {
        if (userSub.isSub()) {
            boolean welcome = true;
            //关注
            ActiveMessage message = autoReplyMapper.one();
            int count = userInfoMapper.countUserByOpenId(userSub.getFromUser());
            //判断该用户是历史已关注过
            if (count == 0) {
                User user = users.get(userSub.getFromUser());
                UserInfoDto userInfo = generateUserInfo(user);
                if (StringUtils.isNotBlank(userSub.getEventKey()) && StringUtils.isNotBlank(userSub.getTicket())) {
                    String id = userSub.getEventKey().replace("qrscene_", "").trim();
                    int sceneId = Integer.parseInt(id);
                    ticketMapper.updateTicketFansNum(sceneId, userSub.getTicket());
                    Ticket ticket = ticketMapper.ticket(sceneId);
                    userInfo.setSource(ticket.getActivityName());
                    userInfo.setSceneId(id);
                    /**-------------------用户渠道标签----------------------**/
                    //渠道标签
                    CustomTag ct = customTagService.findCustomTag("渠道标签", ticket.getActivityName());
                    //建立用户与标签之间的关系
                    customTagService.saveCustomTagUser(userInfo.getOpenId(), ct.getTagId());
                }
                /**-------------------用户地区标签----------------------**/

                //查找当前用户的地理位置属性是否未国内省份
                PubDistrict pubDistrict = districtService.findPubDistrictByDname(userInfo.getProvince());
                if (pubDistrict == null) {
                    //地理位置，未知标签
                    CustomTag ct = customTagService.findCustomTag("地区", "未知");
                    //建立用户与标签之间的关系
                    customTagService.saveCustomTagUser(userInfo.getOpenId(), ct.getTagId());
                } else {
                    //地区标签
                    CustomTag ct = customTagService.findCustomTag("地区", userInfo.getProvince());
                    //建立用户与标签之间的关系
                    customTagService.saveCustomTagUser(userInfo.getOpenId(), ct.getTagId());
                }
                /**-------------------新增用户标签----------------------**/
                //新增用户
                CustomTag ct = customTagService.findCustomTag("新增用户", "新增用户");
                //建立用户与标签之间的关系
                customTagService.saveCustomTagUser(userInfo.getOpenId(), ct.getTagId());

                /**--------------------未消费---------------------------**/
                CustomTag noPayCt = customTagService.findCustomTag("付费用户", "未消费");
                //建立用户与标签之间的关系
                customTagService.saveCustomTagUser(userInfo.getOpenId(), noPayCt.getTagId());
                userInfoMapper.saveUserInfo(userInfo);
                String ticket = userSub.getTicket();
                if (StringUtils.isNotBlank(ticket)) {
                    int countByTickt = promoterMapper.countByTickt(ticket);
                    if (countByTickt > 0) {
                        List<CardDto> card = cardMapper.findByDescription(mgtProperties.getShareCardId());
                        Optional<CardDto> reduce = card.stream().reduce((x, y) -> x.getGetTime().after(y.getGetTime()) ? x : y);
                        if (reduce.orElse(null) != null) {
                            careMessages.card(userSub.getFromUser(), reduce.get().getCardId());
                            welcome = false;//不在发送欢迎的语句
                        }
                    }
                }
            } else {
                //更改关注状态1
                userInfoMapper.updateUserSubscribe(userSub.getFromUser(), userSub.isSub() ? 1 : 0);
            }
            //发送关注欢迎语
            if (null != message && message.getType() == 0 && welcome) {
                if (message.getMsgType() == 0) {
                    String context = message.getMessage();
        /*			String url = ActivUtils.getActivUrl();
                    String msg = String.format(context, url);*/
                    //文本
                    careMessages.text(userSub.getFromUser(), context);
                } else if (message.getMsgType() == 1) {
                    //图片
                    careMessages.image(userSub.getFromUser(), message.getMessage());
                } else {
                    //卡券
                    careMessages.card(userSub.getFromUser(), message.getMessage());
                }
            }
        } else {
            //取消关注
            userInfoMapper.updateUserSubscribe(userSub.getFromUser(), userSub.isSub() ? 1 : 0);
            /**-------------------流失用户标签----------------------**/
            //活跃度标签--->流失用户
            CustomTag ct = customTagService.findCustomTag("活跃度", "流失用户");
            //建立用户与标签之间的关系
            customTagService.saveCustomTagUser(userSub.getFromUser(), ct.getTagId());
        }
    }

    public UserInfoDto generateUserInfo(User user) {
        UserInfoDto userInfo = new UserInfoDto();
        userInfo.setOpenId(user.getOpenId());
        userInfo.setSex(user.getSex().getCode());
        userInfo.setGroupid(user.getGroup());
        userInfo.setLanguage(user.getLanguage());
        userInfo.setNickName(user.getNickName());
        userInfo.setHeadImgurl(user.getHeadImgUrl());
        userInfo.setProvince(user.getProvince());
        userInfo.setCity(user.getCity());
        userInfo.setCountry(user.getCountry());
        userInfo.setRemark(user.getRemark());
        userInfo.setSubscribe(user.isSubscribed() ? 1 : 0);
        userInfo.setSubscribeTime(user.getSubscribedTime());
        userInfo.setUnionid(user.getUnionId());
        for (Integer tag : user.getTags()) {
            UserInfoDto.UserTags userTags = new UserInfoDto.UserTags();
            userTags.setOpenId(user.getOpenId());
            userTags.setTagId(tag);
            userInfo.getUserTags().add(userTags);
        }
        return userInfo;
    }

    @Override
    public PageInfo<UserInfoDto> list(UserInfoQuery userInfoQuery) {
        PageHelper.startPage(userInfoQuery.getPage(), userInfoQuery.getSize(), "subscribe_time desc");
        List<UserInfoDto> list = userInfoMapper.list(userInfoQuery);
        list.forEach(userInfoDto -> userInfoDto.setUserTags(customTagMapper.findTagsByOpenId(userInfoDto.getOpenId()))
        );
        PageInfo<UserInfoDto> pageInfo = new PageInfo<>(list);
        Set<String> openids = pageInfo.getList().stream().map(UserInfoDto::getOpenId).collect(Collectors.toSet());
        List<OrderCountDto> orderCountDtos = flowOrderMapper.countOrderByOpenids(openids);
        List<OrderCountDto> cardCountDtos = cardMapper.countByOpenId(openids);
        final Map<String,Integer> orderMap = new HashMap<>();
        final Map<String,Integer> cardMap = new HashMap<>();
        orderCountDtos.forEach(orderCountDto -> orderMap.put(orderCountDto.getOpenId(),orderCountDto.getCount()));
        cardCountDtos.forEach(orderCountDto -> cardMap.put(orderCountDto.getOpenId(),orderCountDto.getCount()));
        pageInfo.getList().forEach(userInfoDto ->
                userInfoDto.setOrderNum(orderMap.getOrDefault(userInfoDto.getOpenId(),0))
                .setCardNum(cardMap.getOrDefault(userInfoDto.getOpenId(),0))
        );
        return pageInfo;
    }

    @Override
    public List<NewerOrderExport> listExport(UserInfoQuery userInfoQuery) {
        List<UserInfoDto> userInfoDtos = userInfoMapper.list(userInfoQuery);
        final Map<String, UserInfoDto> groupByOpenID = new HashMap<>();
        userInfoDtos.forEach(userInfoDto -> groupByOpenID.put(userInfoDto.getOpenId(), userInfoDto));
        List<String> openIds = userInfoDtos.stream().map(UserInfoDto::getOpenId).distinct().collect(toList());
        List<NewerOrderExport> results = new ArrayList<>();
        for (String openId : openIds) {
            UserInfoDto userInfoDto = groupByOpenID.get(openId);
            if (null == userInfoDto) break;
            List<FlowOrder> orderByOpenId = flowOrderMapper.findOrderByOpenId(openId);
            for (FlowOrder order : orderByOpenId) {
                NewerOrderExport newerOrderExport = new NewerOrderExport().setOrderNo(order.getOrderNo())
                        .setOrderNum(userInfoDto.getOrderNum())
                        .setOrderPhone(order.getOrderPhone())
                        .setOrderPrice(order.getOrderPrice())
                        .setProfit(order.getProfit())
                        .setOrderTime(order.getOrderTime())
                        .setCardNum(userInfoDto.getCardNum())
                        .setFlowPackage(order.getFlowPackage())
                        .setNickName(userInfoDto.getNickName())
                        .setRechargeStatus(order.getRechargeStatus())
                        .setSex(userInfoDto.getSex() == 1 ? "男" : "女")
                        .setSubscribe(userInfoDto.getSubscribe())
                        .setSubscribeTime(userInfoDto.getSubscribeTime())
                        .setRegoin(userInfoDto.getProvince() + userInfoDto.getCity());
                results.add(newerOrderExport);
            }
        }
        return results;
    }


    @Override
    public PageInfo<FlowOrderDto> orderByOpenId(Integer page, Integer size, String openId) {
        PageHelper.startPage(page, size, "order_time desc");
        List<FlowOrderDto> list = userInfoMapper.queryOrderByOpenId(openId);
        PageInfo<FlowOrderDto> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public UserInfoDto get(String openId) {
        return userInfoMapper.get(openId);
    }

    @Override
    public List<String> getAllOpenId() {
        return userInfoMapper.getAllOpenId();
    }
}
