package com.datafocus.service.service.impl;

import com.datafocus.common.config.ProtalProperties;
import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.dto.CardDto;
import com.datafocus.common.dto.CardDtoMgt;
import com.datafocus.common.dto.Product;
import com.datafocus.common.pojo.CardAcceptCategorys;
import com.datafocus.common.utils.DateUtil;
import com.datafocus.service.mapper.CardMapper;
import com.datafocus.service.service.CardService;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.weixin.common.util.JsonMapper;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.card.Cards;
import com.datafocus.weixin.mp.card.bean.AdvancedInfo;
import com.datafocus.weixin.mp.card.bean.BaseInfo;
import com.datafocus.weixin.mp.card.bean.Card;
import com.datafocus.weixin.mp.event.card.UserConsumeCardEvent;
import com.datafocus.weixin.mp.event.card.UserDeleteCardEvent;
import com.datafocus.weixin.mp.event.card.UserEnterSessionCardEvent;
import com.datafocus.weixin.mp.event.card.UserGetCardEvent;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author layne
 * @date 17-7-27
 */
@Service
public class CardServiceImpl implements CardService {
    private static Logger logger = LoggerFactory.getLogger(CardServiceImpl.class);

    @Autowired
    private CardMapper cardMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ProtalProperties protalProperties;
    @Autowired
    private Cards cards;
    @Autowired
    private AppSetting appSetting;


    @Override
    public List<Map> selectCardList() {
        return cardMapper.findAll();
    }

    @Override
    public PageInfo<CardDto> list(String openId, Integer page, Integer size) {
        if(page!=null&&size!=null){
            PageHelper.startPage(page, size);
        }
        List<CardDto> cardDtos = cardMapper.list(openId);
        for (CardDto cardDto : cardDtos) {

            isCardUse(cardDto);
            List<CardDto.CardAcceptCategory> cardAcceptCategorys = cardMapper.cardAcceptCategoryByCardId(cardDto.getCardId());
            cardDto.setCardAcceptCategorys(cardAcceptCategorys);
        }
        if (null != cardDtos && cardDtos.size() > 0)
            Collections.sort(cardDtos);
        PageInfo<CardDto> pageInfo = new PageInfo<>(cardDtos);
        return pageInfo;
    }

    /**
     * 卡券是否可用
     *
     * @param cardDto
     */
    public static void isCardUse(CardDto cardDto) {
        cardDto.setStatus(0);

        // 判断卡券是否已使用
        if (cardDto.getIsUse() == 1) {
            cardDto.setStatus(2);
        }
        //是否已过期
        if (cardDto.getType().equals("DATE_TYPE_FIX_TIME_RANGE")) {
            cardDto.setEndTime(DateUtil.DateToString(cardDto.getEndTimestamp(), DateUtil.DateStyle.YYYY_MM_DD));
            int i = DateUtil.compareDateWithNow(DateUtil.dateToUnixTimestamp(), DateUtil.weeHours(cardDto.getEndTimestamp(), 1).getTime());
            if (i != 1 && cardDto.getIsUse() != 1) {
                cardDto.setStatus(1);
                return;
            }
        } else if (cardDto.getType().equals("DATE_TYPE_FIX_TERM")) {
            Date getTime = cardDto.getGetTime(); //卡券领取时间
            int fixeTerm = cardDto.getFixeTerm(); //取后多少天内有效
            int fixedBeginTerm = cardDto.getFixedBeginTerm();//多少天开始生效


            if (fixedBeginTerm == 0) {
                //(领取时间-1)+取后多少天内有效
                Date date = DateUtils.addDays(DateUtils.addDays(getTime, -1), fixeTerm);
                date = DateUtil.weeHours(date, 1);
                cardDto.setEndTime(DateUtil.DateToString(date, DateUtil.DateStyle.YYYY_MM_DD));
                int i = DateUtil.compareDateWithNow(date.getTime());
                if (i != 1 && cardDto.getIsUse() != 1) {
                    cardDto.setStatus(1);
                    return;
                }
            } else {
                //生效日期
                Date fixeTime = DateUtils.addDays(getTime, fixedBeginTerm);
                //失效日期
                Date time = DateUtils.addDays(DateUtils.addDays(fixeTime, -1), fixeTerm);
                time = DateUtil.weeHours(time, 1);
                cardDto.setEndTime(DateUtil.DateToString(time, DateUtil.DateStyle.YYYY_MM_DD));
                int i = DateUtil.compareDateWithNow(fixeTime.getTime());
                if (i == 1 && cardDto.getIsUse() != 1)//判断是否到使用时间
                {
                    cardDto.setStatus(3);
                    return;
                }
                int k = DateUtil.compareDateWithNow(time.getTime());
                if (k == 1 && cardDto.getIsUse() != 1) {//判断是否过期
                    cardDto.setStatus(1);
                    return;
                }
            }
        }
    }

    @Override
    public List<CardDto> list(String openId, String vendor, String province, String city, String productType, List<String> products) {

        return cardMapper.findCardByOpenId(openId, productType, vendor, province, city, products);
    }

    @Override
    public List<CardDto> notUseCard(String openId, List<String> cardIds) {


        return cardMapper.notUseCard(openId, cardIds);
    }

    @Override
    public List<CardDto> findCardByOpenId(String openId, String productId, int productLevle, String productName) {

        String json = stringRedisTemplate.opsForValue().get(protalProperties.getOperatorId() + productId);

        Product product = JsonMapper.defaultMapper().fromJson(json, Product.class);

        String vendorCode = product.getVendor();

        String provinceCode = product.getProvince();

        String cityCode = product.getCity();

        String productType = product.getType();

        List<CardDto> productUseCard = null;
        if (productLevle == 0)//地市流量
        {
            productUseCard = this.list(openId, vendorCode, provinceCode, cityCode, productType, Arrays.asList(productName));
        } else if (productLevle == 1)//分省流量
        {
            productUseCard = this.list(openId, vendorCode, provinceCode, "", productType, Arrays.asList(productName));
        } else if (productLevle == 2)//全国流量
        {
            productUseCard = this.list(openId, vendorCode, "", "", productType, Arrays.asList(productName));
        }

        BigDecimal stPrice = product.getStPrice();

        BigDecimal salePrice = product.getSalePrice();

        for (CardDto card : productUseCard) {

            CardServiceImpl.isCardUse(card);
            //计算第一个包型下最优的卡券(计算公式，折扣券：原价*折扣，代金券：销售价-减免金额)
            if ("DISCOUNT".equals(card.getType())) {

                BigDecimal discount = new BigDecimal(100).subtract(new BigDecimal(card.getDiscount())).divide(new BigDecimal(10));

                BigDecimal unitPrice = stPrice.multiply(discount).divide(new BigDecimal(10));

                if (unitPrice.compareTo(salePrice) == 1 || unitPrice.compareTo(salePrice) == 0) {
                    //适用卡券更贵或者跟销售价一样
                    card.setCostlyFlag(true);
                }
                card.setCheapPrice(unitPrice);
            } else {
                //代金券
                BigDecimal unitPrice = salePrice.subtract(new BigDecimal(card.getReduceCost()).divide(new BigDecimal(100)));
                card.setCheapPrice(unitPrice);
            }
        }
        //具体包型可适用卡券
        for (Iterator<CardDto> it = productUseCard.iterator(); it.hasNext(); ) {
            CardDto card = it.next();
            if (card.getStatus() != 0) {
                it.remove();
            } else {
                List<CardDto.CardAcceptCategory> cardAcceptCategorys = cardMapper.cardAcceptCategoryByCardId(card.getCardId());
                card.setCardAcceptCategorys(cardAcceptCategorys);
            }
        }

        return productUseCard;
    }

    @Override
    @Transactional
    public void saveCard(Card card, CardAcceptCategorys cardAcceptCategorys) {
        HashMap<String, Object> param = generateCard(card);
        generateCardAcceptCategory(card, cardAcceptCategorys);
        try {
            cardMapper.saveCard(param);
            cardMapper.saveCardAcceptCategorys(cardAcceptCategorys);
        } catch (Exception e) {
            logger.error("创建卡券保存数据失败:{}", e);
            throw new RuntimeException("卡券保存数据库失败", e);
        }
    }

    public static void generateCardAcceptCategory(Card card, CardAcceptCategorys cardAcceptCategorys) {
        BaseInfo baseinfo = generateBaseInfo(card);
        for (CardAcceptCategorys.CardAcceptCategory cardAcceptCategory : cardAcceptCategorys.getCardAcceptCategorys()) {
            cardAcceptCategory.setCardId(baseinfo.getId());
        }

    }

    public static BaseInfo generateBaseInfo(Card card) {
        switch (card.getCardType()) {
            case "DISCOUNT":
                return card.getDiscount().getBaseInfo();
            case "CASH":
                return card.getCash().getBaseInfo();
            case "GENERAL_COUPON":
                return card.getCoupon().getBaseInfo();
            default:
                throw new IllegalArgumentException("卡券类型错误");
        }
    }

    public static HashMap<String, Object> generateCard(Card card) {
        HashMap<String, Object> data = new HashMap<>();

        BaseInfo baseinfo = generateBaseInfo(card);
        AdvancedInfo advancedInfo = generateAdvancedInfo(card);

        data.put("cardType", card.getCardType());
        data.put("cardId", baseinfo.getId());
        data.put("title", baseinfo.getTitle());
        data.put("subTitle", baseinfo.getSubTitle());
        data.put("logoUrl", baseinfo.getLogoUrl());
        data.put("brandName", baseinfo.getBrandName());
        data.put("color", baseinfo.getColor());
        data.put("notice", baseinfo.getNotice());
        data.put("description", baseinfo.getDescription());
        data.put("quantity", baseinfo.getSku().getQuantity());
        data.put("type", baseinfo.getDateInfo().getType());
        if(baseinfo.getDateInfo().getBeginTime()==null){
            Date now = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(now);
            calendar.add(Calendar.DATE,baseinfo.getDateInfo().getFixedTermBegin());
            data.put("beginTimestamp",calendar.getTime());
            calendar.add(Calendar.DATE,baseinfo.getDateInfo().getFixedTerm());
            data.put("endTimestamp", calendar.getTime());
        }else{
            data.put("beginTimestamp", baseinfo.getDateInfo().getBeginTime());
            data.put("endTimestamp", baseinfo.getDateInfo().getEndTime());
        }
        data.put("fixedTerm", baseinfo.getDateInfo().getFixedTerm());
        data.put("fixedBeginTerm", baseinfo.getDateInfo().getFixedTermBegin());
        data.put("getLimet", baseinfo.getGetLimit());
        data.put("canShare", baseinfo.isCanShare() ? 1 : 0);
        data.put("canGrveFriend", baseinfo.isCanGiveFriend() ? 1 : 0);
        data.put("centerUrl", baseinfo.getCenterUrl());
        data.put("imgUrl", advancedInfo.getTextImageList().get(0).getImageUrl());
        data.put("text", advancedInfo.getTextImageList().get(0).getText());
//		data.put("rejectCategory", advancedInfo.getUseCondition().getRejectCategory());
//		data.put("acceptCategory", advancedInfo.getUseCondition().getAcceptCategory());
        switch (card.getCardType()) {
            case "DISCOUNT":
                data.put("discount", card.getDiscount().getDiscount());
                break;
            case "CASH":
                data.put("leastCost", card.getCash().getLeastCost());
                data.put("reduceCost", card.getCash().getReduceCost());
                break;
            case "GENERAL_COUPON":
                data.put("defaultDetail", card.getCoupon().getDetail());
                break;
            default:
                break;
        }
        return data;
    }

    public static AdvancedInfo generateAdvancedInfo(Card card) {
        switch (card.getCardType()) {
            case "DISCOUNT":
                return card.getDiscount().getAdvancedInfo();
            case "CASH":
                return card.getCash().getAdvancedInfo();
            case "GENERAL_COUPON":
                return card.getCoupon().getAdvancedInfo();
            default:
                throw new IllegalArgumentException("卡券类型错误");
        }
    }

    @Override
    public List<Map<String,Object>> listCardId() {
        return cardMapper.listCardId();
    }

    @Override
    public PageInfo<CardDtoMgt> listCard(FlowPage page) {
        PageHelper.startPage(page.getPage(), page.getSize(), "create_time desc");
        Page<CardDtoMgt> cardDtos = cardMapper.listCard();
        return cardDtos.toPageInfo();
    }

    @Override
    @Transactional
    public void deleteCard(String cardId) {
        //删除本地数据库
        cardMapper.deleteCard(cardId);
        //同步微信删除卡券
        cards.delete(cardId);
    }

    @Override
    public void modifyStock(String cardId, int increaseStock, int reduceStock) {
        cardMapper.modifyStock(cardId, increaseStock, reduceStock);
        cards.modifyStock(cardId, increaseStock, reduceStock);
    }

    @Override
    public PageInfo<CardDtoMgt> list(String openId, FlowPage page) {
        PageHelper.startPage(page.getPage(), page.getSize());
        Page<CardDtoMgt> cardDtos = cardMapper.selectCardByOpenId(openId);
        return cardDtos.toPageInfo();
    }

    @Override
    public void updateCardStatus(String cardId,int status) throws Exception
    {
        cardMapper.updateCardStatus(cardId, status);
    }

    @Override
    public void saveCardCode(UserGetCardEvent userGetCardEvent) throws Exception
    {
        HashMap<String, Object> param = new HashMap<>();
        param.put("cardId", userGetCardEvent.getCardId());
        param.put("isGiveFriend", userGetCardEvent.getIsGiveByFriend());
        param.put("userCardCode", userGetCardEvent.getUserCardCode());
        param.put("oldUserCardCode", userGetCardEvent.getOldUserCardCode());
        param.put("outerId", userGetCardEvent.getOuterId());
        param.put("outerStr", userGetCardEvent.getOuterStr());
        param.put("toUserName", userGetCardEvent.getToUser());
        param.put("fromUserName", userGetCardEvent.getFromUser());
        param.put("friendUserName", userGetCardEvent.getFriendUserName());

        cardMapper.saveCardCode(param);
    }

    @Override
    public void saveSessionEventCard(UserEnterSessionCardEvent userEnterSessionCardEvent) throws Exception
    {
        HashMap<String, Object> param = new HashMap<>();
        param.put("cardId", userEnterSessionCardEvent.getCard());
        param.put("userCardCode", userEnterSessionCardEvent.getUserCardCode());
        param.put("toUserName", userEnterSessionCardEvent.getToUser());
        param.put("fromUserName", userEnterSessionCardEvent.getFromUser());
        cardMapper.saveSessionEventCard(param);
    }

    @Override
    public void deleteCardByCardIdAndCode(UserDeleteCardEvent userDeleteCardEvent) throws Exception
    {
        HashMap<String, Object> param = new HashMap<>();
        param.put("cardId", userDeleteCardEvent.getCardId());
        param.put("code", userDeleteCardEvent.getUserCardCode());
        param.put("fromUserName", userDeleteCardEvent.getFromUser());
        cardMapper.deleteCardByCardIdAndCode(param);

    }

    @Override
    public void updateCardIsUseStatus(UserConsumeCardEvent userConsumeCardEvent) throws Exception
    {
        HashMap<String, Object> param = new HashMap<>();
        param.put("cardId", userConsumeCardEvent.getCardId());
        param.put("code", userConsumeCardEvent.getUserCardCode());
        param.put("fromUserName", userConsumeCardEvent.getFromUser());
        cardMapper.updateCardIsUseStatus(param);
    }
}
