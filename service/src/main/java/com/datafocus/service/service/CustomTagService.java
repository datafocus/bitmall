package com.datafocus.service.service;

import com.datafocus.common.pojo.CustomTag;
import com.datafocus.common.pojo.CustomTagDimension;
import com.datafocus.common.pojo.CustomTagUser;

import java.util.List;



public interface CustomTagService {

	/**
	 * 保存自定义标签信息
	 * @param customTag
	 */
	 public void saveCustomTag(CustomTag customTag);
	 
	 /**
	  * 根据自定义标签名称查询自定义标签信息
	  * @param tagName
	  * @return
	  */
	 public CustomTag findCustomTag(String dimensionName, String tagName);
	 
	 /**
	  * 保存用户与自定义标签的关联关系
	  * @param openId
	  * @param tagId
	  */
	 public void saveCustomTagUser(String openId, int tagId);
	 
	 /**
	  * 根据用户id，标签id查询用户与标签的关联关系
	  * @param openId
	  * @param tagId
	  * @return
	  */
	 public int countCustomTagUser(String openId, int tagId);
	 
	 /**
	  * 
	  * @param tagName
	  * @return
	  */
	 public List<CustomTagUser> customTagUsersByTagName(String tagName);
	 
	 /**
	  * 解除用户与标签的关联关系
	  * @param openId
	  * @param tagId
	  */
	 public void deleteCustomTagUserByOpenIdAndTagId(String openId, int tagId);
	 
	 /**
	  * 获取所有的系统标签
	  * @return
	  */
	 public List<CustomTag> findCustomTags();
	 
	 /**
	  * 根据标签范围名称，和openid查询自定义标签id
	  * @return
	  */
	 List<CustomTagUser> customTagUsersByDismensionNameAndOpenId(String dismensionName, String openId);

    /**
     *
     * @return
     */
    List<CustomTagDimension> findCustomTagDimension();

}
