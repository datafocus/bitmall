package com.datafocus.service.service.impl;

import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.exception.ApplicationException;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.service.utils.SpringUtils;
import com.datafocus.weixin.common.AccessToken;
import com.datafocus.weixin.common.AccessTokenHolder;
import com.datafocus.weixin.mp.base.AppSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBAccessTokenHolder extends AccessTokenHolder {

    private static final Logger logger = LoggerFactory.getLogger(DBAccessTokenHolder.class);

    private static final String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    private AccessToken accessToken;

    public DBAccessTokenHolder() {
        super(TOKEN_URL, "default", "default");
    }

    @Override
    public com.datafocus.weixin.common.AccessToken getAccessToken() {
        //从redis获取缓存的token
        RedisUtil<String, String> redisUtil = SpringUtils.getApplicationContext().getBean(RedisUtil.class);
        String tokenData = redisUtil.getValue(getRedisKey());
        logger.info("获取的access_token:"+tokenData);
        if (StringUtils.isNotBlank(tokenData)) accessToken = AccessToken.fromJson(tokenData);
        else refreshToken();
        return accessToken;
    }

    @Override
    public void refreshToken() {
        synchronized (this) {
            logger.info("开始刷新微信accesstoken数据");
            String content = fetchAccessToken();
            AccessToken accessToken = AccessToken.fromJson(content);
            this.accessToken = accessToken;
            try {
                //保存token信息到DB
                RedisUtil<String, String> redisUtil = SpringUtils.getApplicationContext().getBean(RedisUtil.class);
                redisUtil.addOrUpdate(getRedisKey(), content, accessToken.getExpiresIn());
            } catch (Exception e) {
                logger.error("刷新token，保存accesstoken到数据库异常", e);
                throw new ApplicationException(FrontResponseCode.SYSTEM_ERROR.getCode(), "保存accesstoken到redis异常");
            }
            logger.info("刷新微信accesstoken信息完毕，token：{},有效期：{} 秒", accessToken.getAccessToken(), accessToken.getExpiresIn());
        }
    }

    @Override
    public void expireToken() {
        accessToken.setExpiresIn(-30);//强制设置为无效
    }


    /**
     * 生成存入redis token的键
     *
     * @return
     */
    private String getRedisKey() {
        return SpringUtils.getApplicationContext().getBean(AppSetting.class).getAppId() + "_ACCESSTOKEN_KEY";
    }

}
