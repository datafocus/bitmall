package com.datafocus.service.service;

import com.datafocus.common.pojo.Ticket;
import com.github.pagehelper.PageInfo;

public interface TicketService {
	
	
	/**
	 * 保存二维码
     * @param ticket
     */
	Ticket save(Ticket ticket);
	
	/**
	 * 获取二维码列表
	 * @param page
	 * @param pageSize
	 * @param activityName
	 * @return
	 */
	PageInfo<Ticket> list(int page, int pageSize, String activityName);
	
	/**
	 * 通过ticket换取二维码
	 * @param ticket
	 * @return
	 */
	byte[] getQrcode(String ticket);

}
