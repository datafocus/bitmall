package com.datafocus.service.service;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.dto.CardDto;
import com.datafocus.common.dto.CardDtoMgt;
import com.datafocus.common.pojo.CardAcceptCategorys;
import com.datafocus.weixin.mp.card.bean.Card;
import com.datafocus.weixin.mp.event.card.UserConsumeCardEvent;
import com.datafocus.weixin.mp.event.card.UserDeleteCardEvent;
import com.datafocus.weixin.mp.event.card.UserEnterSessionCardEvent;
import com.datafocus.weixin.mp.event.card.UserGetCardEvent;
import com.github.pagehelper.PageInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author layne
 * @date 17-7-27
 */
public interface CardService {
    /**
     * @return
     */
    List<Map> selectCardList();

    /**
     * 获取用户已领取的卡券
     *
     * @param openId
     * @param page
     * @param size
     * @return
     */
    PageInfo<CardDto> list(String openId, Integer page, Integer size);


    /**
     * 根据运营商，地区，产品类型，产品ID 查询适用当前条件的卡券
     *
     * @param openId
     * @param vendor
     * @param province
     * @param products
     * @return
     */
    List<CardDto> list(String openId, String vendor, String province, String city, String productType, List<String> products);

    /**
     * 查询不适用的卡券
     *
     * @param cardIds
     * @return
     */
    List<CardDto> notUseCard(String openId, List<String> cardIds);

    /**
     * 查询包型下适用的卡券
     *
     * @param openId
     * @param productId
     * @param productLevle
     * @param productName
     * @return
     */
    List<CardDto> findCardByOpenId(String openId, String productId, int productLevle, String productName);

    /**
     * 保存微信卡券
     *
     * @param card
     * @param cardAcceptCategorys
     */
    void saveCard(Card card, CardAcceptCategorys cardAcceptCategorys);

    List<Map<String,Object>> listCardId();
    /**
     * @param page
     * @return
     */
    PageInfo<CardDtoMgt> listCard(FlowPage page);

    /**
     * 管理员删除用户卡券
     *
     * @param cardId
     */
    void deleteCard(String cardId);

    /**
     * 修改卡券库存
     *
     * @param increaseStock
     * @param reduceStock
     * @throws Exception
     */
    void modifyStock(String cardId, int increaseStock, int reduceStock);

    /**
     * 根据OpenId查询已经获取的卡券
     *
     * @param openId
     * @param page
     * @return
     */
    PageInfo<CardDtoMgt> list(String openId, FlowPage page);

    /**
     * 审核事件
     * @param cardId
     */
    public void updateCardStatus(String cardId,int status)throws Exception;

    /**
     * 保存卡券code
     * @param userGetCardEvent
     */
    void saveCardCode(UserGetCardEvent userGetCardEvent)throws Exception;

    /**
     * 保存从卡券进入公众号会话事件推送
     */
    void saveSessionEventCard(UserEnterSessionCardEvent userEnterSessionCardEvent)throws Exception;

    /**
     * 用户删除微信卡券
     */
    void deleteCardByCardIdAndCode(UserDeleteCardEvent userDeleteCardEvent)throws Exception;

    /**
     * 更新用户卡券核销状态
     * @param userConsumeCardEvent
     */
    void updateCardIsUseStatus(UserConsumeCardEvent userConsumeCardEvent)throws Exception;
}
