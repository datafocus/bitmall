package com.datafocus.service.service;

import com.datafocus.common.dto.FlowOrderDto;
import com.datafocus.common.pojo.FlowOrder;

import java.util.List;


public interface FlowRechargeService {

    List<FlowOrderDto> findRechargeOrderByOrderStatusAndSubmitStatus();

    void modifyOrderSubmitStatusAndRechargeStatus(String orderNo, int submitStatus, String msgid, String gallerysubmitCode) throws Exception;

    /**
     * 根据cardid查找描述信息
     * @param cardId
     * @return
     */
    String findCardDescByCardId(String cardId);

}
