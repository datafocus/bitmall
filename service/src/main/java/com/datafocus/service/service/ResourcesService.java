package com.datafocus.service.service;

import com.datafocus.common.pojo.Resources;

import java.util.List;


public interface ResourcesService {
	
	List<Resources> findAllResRoles(Integer roleId);
	
	List<Resources> findResourcesByAccountId(String accountId);
}
