package com.datafocus.service.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.pojo.FlowChannel;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.service.mapper.FlowMgtMapper;
import com.datafocus.service.service.FlowMgtService;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;


@Service
public class FlowMgtServiceImpl implements FlowMgtService {

	@Autowired
	private FlowMgtMapper flowMgtMapper;
	
	@Override
	public List<FlowChannel> findChannelByCarrierOperator(int carrierOperator)
	{
		if(0==carrierOperator)
		{
			carrierOperator = 1; //1 代表中国移动  默认先查询中国移动通道列表
		}
		
		return flowMgtMapper.findChannelByCarrierOperator(carrierOperator);
	}

	@Override
	public void modifyFlowChannelStatus(Integer [] ids,int stauts,String remark)
	{
		flowMgtMapper.modifyFlowChannelStatusById(ids,stauts,remark);
	}


	
}