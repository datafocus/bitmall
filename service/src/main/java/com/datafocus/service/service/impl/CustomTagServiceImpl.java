package com.datafocus.service.service.impl;

import com.datafocus.common.pojo.CustomTag;
import com.datafocus.common.pojo.CustomTagDimension;
import com.datafocus.common.pojo.CustomTagUser;
import com.datafocus.service.mapper.CustomTagDimensionMapper;
import com.datafocus.service.mapper.CustomTagMapper;
import com.datafocus.service.mapper.TagsMapper;
import com.datafocus.service.service.CustomTagService;
import com.datafocus.weixin.mp.user.Tags;
import com.datafocus.weixin.mp.user.bean.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class CustomTagServiceImpl implements CustomTagService {

    private static final Logger logger = LoggerFactory.getLogger(CustomTagServiceImpl.class);

    @Autowired
    private CustomTagMapper customTagMapper;
    @Autowired
    private CustomTagDimensionMapper customTagDimensionMapper;
    @Autowired
    private TagsMapper tagsMapper;
    @Autowired
    private Tags tags;

    @Override
    public void saveCustomTag(CustomTag customTag) {
        customTagMapper.saveCustomTag(customTag);
    }

    @Override
    @Transactional
    public CustomTag findCustomTag(String dimensionName, String tagName) {
        //判断自定义标签维度是否存在
        CustomTagDimension ctd = customTagDimensionMapper.customTagDimension(dimensionName);
        if (ctd == null) {
            ctd = new CustomTagDimension();
            ctd.setDimensionName(dimensionName);
            try {
                customTagDimensionMapper.saveCustomTagDimension(ctd);
            } catch (Exception e) {
                logger.error("保存自定义标签系统异常", e);
                throw new RuntimeException("保存自定义标签维度sql异常", e);
            }
            ctd = customTagDimensionMapper.customTagDimension(dimensionName);
        }
        //创建自定义标签
        CustomTag ct = customTagMapper.customTag(tagName, 0);

        if (ct == null) {
            ct = new CustomTag();
            ct.setTagName(tagName);
            customTagMapper.saveCustomTag(ct);
            ct = customTagMapper.customTag(tagName, 0);
            //保存自定义标签与标签维度的关联关系
            customTagDimensionMapper.saveCustomTagDimesionTag(ctd.getId(), ct.getTagId());
        }
        return ct;
    }

    @Override
    @Transactional
    public void saveCustomTagUser(String openId, int tagId) {
        //判断用户是否已存在当前这个标签
        int count = customTagMapper.countCustomTagUser(openId, tagId);
        if (count == 0) {
            customTagMapper.saveCustomTagUser(openId, tagId);
            customTagMapper.updateCustomUserNum(tagId, 1);
        }
        try {
            String tagName = customTagMapper.findTagNameById(tagId);
            Integer wxTagId = tagsMapper.findByTagName(tagName);
            if (wxTagId != null) {
                try {
                    tagsMapper.saveUserTag(openId, wxTagId);
                    tagsMapper.updateNum(wxTagId, 1);
                } catch (Exception e) {
                    logger.warn("保存到本地标签失败", e);
                }
                try {
                    tags.tagUsers(wxTagId, Arrays.asList(openId));
                } catch (Exception e) {
                    logger.warn("保存到微信标签失败", e);
                }
            } else {//微信侧创建标签
                try {
                    Tag tag = null;
                    try {
                        tag = tags.create(tagName);
                    } catch (Exception e) {
                        List<Tag> wxTags = tags.list();
                        Optional<Tag> tagOptional = wxTags.stream().filter(tag1 -> tagName.equals(tag1.getName())).findFirst();
                        tag = tagOptional.get();//如果为空 抛异常了
                    }
                    com.datafocus.common.pojo.Tag localTag = new com.datafocus.common.pojo.Tag();
                    localTag.setNum(tag.getCount());
                    localTag.setName(tagName);
                    localTag.setTagId(tag.getId());
                    tagsMapper.saveTagBatch(Arrays.asList(localTag));
                } catch (Exception e) {
                    logger.warn("创建微信标签失败");
                }
            }
        } catch (Exception e) {
            logger.info("同步微信出错");
        }
    }

    @Override
    public int countCustomTagUser(String openId, int tagId) {
        return customTagMapper.countCustomTagUser(openId, tagId);
    }

    @Override
    public List<CustomTagUser> customTagUsersByTagName(String tagName) {

        return customTagMapper.customTagUsersByTagName(tagName);
    }

    @Override
    @Transactional
    public void deleteCustomTagUserByOpenIdAndTagId(String openId, int tagId) {
        customTagMapper.deleteCustomTagUserByOpenIdAndTagId(openId, tagId);
        customTagMapper.updateCustomUserNum(tagId, 0);
    }

    @Override
    public List<CustomTag> findCustomTags() {
        return customTagMapper.findCustomTags();
    }

    @Override
    public List<CustomTagUser> customTagUsersByDismensionNameAndOpenId(String dismensionName, String openId) {
        return customTagMapper.customTagUsersByDismensionNameAndOpenId(dismensionName, openId);
    }

    @Override
    public List<CustomTagDimension> findCustomTagDimension() {
        return customTagDimensionMapper.findCustomTagDimension();
    }
}
