package com.datafocus.service.service.impl;

import java.util.HashMap;
import java.util.List;

import com.datafocus.common.domain.response.FrontResponseCode;
import com.datafocus.common.exception.ServiceException;
import com.datafocus.common.pojo.Ticket;
import com.datafocus.service.mapper.TicketMapper;
import com.datafocus.service.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datafocus.weixin.mp.ticket.Tickets;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketMapper ticketMapper;

    @Autowired
    private Tickets tickets;

    @Override
    public Ticket save(Ticket ticket) {
        //校验场景ID是否存在
        Ticket tk = ticketMapper.ticket(ticket.getSceneId());
        if (tk != null)
            throw new ServiceException(FrontResponseCode.SERVICE_ERROR.getCode(), "场景值ID不能重复");
        com.datafocus.weixin.mp.ticket.bean.Ticket wxTicket = null;
        if ("QR_SCENE".equals(ticket.getActionName())) {
            wxTicket = tickets.temporary(ticket.getExpireSeconds(), ticket.getSceneId());
        } else {
            wxTicket = tickets.permanent(ticket.getSceneId());
        }
        ticket.setTicket(wxTicket.getTicket());
        ticket.setUrl(wxTicket.getUrl());
        ticket.setExpireSeconds(wxTicket.getExpiresIn());
        ticketMapper.save(ticket);
        return ticket;
    }

    @Override
    public PageInfo<Ticket> list(int page, int pageSize, String activityName) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("activityName", activityName);
        PageHelper.startPage(page, pageSize, "create_time desc");
        List<Ticket> list = ticketMapper.list(params);
        PageInfo<Ticket> info = new PageInfo<>(list);
        return info;
    }

    @Override
    public byte[] getQrcode(String ticket) {
        return tickets.getQrcode(ticket);
    }

}
