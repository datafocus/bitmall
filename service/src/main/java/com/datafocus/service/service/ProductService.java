package com.datafocus.service.service;

import com.datafocus.common.dto.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ProductService {


    /**
     * 查询流量产品
     *
     * @param mobile
     * @return
     */
    Map<String, Object> list(String mobile);

    /**
     * 查询号码归属地
     *
     * @return
     */
    DistrictDto queryMobileStation(String mobile);

    /**
     * 调用远程接口获取流量产品信息
     */
    void getProductForFlowStar();

    /**
     * @param mobile
     * @param openId
     */
    ProductResultDto findTraffic(String mobile, String openId);

    List<ProductVO> findAllProduct(Integer carrierOperator, String province);

    void modify(List<ProductVO> productVO);

    void save(ProductCollection product);

    int count(int carrierOperator, String province);

    List<Product> findProductForCache(String productType, String vendorCode, String provinceCode, String cityCode) throws IOException;

    ProductResultDto findActivityProducts(String mobile);

    List<String> activityProductsList();
}
