package com.datafocus.service.service;

/**
 * Created by layne on 2017/12/6.
 */
public interface EnterRecordService {
    /**
     *
     * @param openId
     */
    void enterRecord(String openId);
}
