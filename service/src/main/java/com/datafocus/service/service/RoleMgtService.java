package com.datafocus.service.service;

import com.datafocus.common.pojo.Resources;
import com.datafocus.common.pojo.Role;
import com.datafocus.common.pojo.RoleRes;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * @author layne
 * @date 17-8-2
 */
public interface RoleMgtService {
    /**
     * 角色列表
     *
     * @return
     */
    Page<Role> findRole();

    /**
     * @param role
     */
    void saveRole(Role role);

    /**
     * 资源列表
     *
     * @return
     */
    List<Resources> findResourcess();

    /**
     * 更新角色资源
     *
     * @param roleRes
     */
    void saveRoleRes(List<RoleRes> roleRes);

    /**
     * 删除角色
     *
     * @param roleId
     */
    void deleteRole(Integer roleId);
}
