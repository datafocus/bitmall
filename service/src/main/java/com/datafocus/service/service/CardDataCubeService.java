package com.datafocus.service.service;

import com.datafocus.common.pojo.CardInfoCube;
import com.datafocus.weixin.mp.card.bean.CardDataCube;
import com.github.pagehelper.PageInfo;

public interface CardDataCubeService {
	
	/**
	 * 卡券详情数据统计
	 * @param cardId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	PageInfo<CardInfoCube> list(Integer page, Integer size, String cardId, String beginTime, String endTime);
	
	/**
	 * 卡券概况数据统计
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	PageInfo<CardDataCube> list(Integer page, Integer size, String beginTime, String endTime);

}
