package com.datafocus.service.service.impl;

import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.common.pojo.MsgMarketingRes;
import com.datafocus.common.pojo.MsgMarketingStatistics;
import com.datafocus.common.pojo.UserInfo;
import com.datafocus.service.mapper.*;
import com.datafocus.service.service.MsgMarketingService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by layne on 2017/12/5.
 */
@Service
public class MsgMarketingServiceImpl implements MsgMarketingService {

    @Autowired
    MsgMarketingMapper msgMarketingMapper;

    @Autowired
    UserInfoMapper userInfoMapper;

    @Autowired
    FlowOrderMapper flowOrderMapper;

    @Autowired
    CardMapper cardMapper;

    @Autowired
    EnterRecordMapper enterRecordMapper;

    @Override
    public void addMsgMarketing(MsgMarketingStatistics msgMarketingStatistics) {
        if (StringUtils.isNotBlank(msgMarketingStatistics.getActivityType())) {
            addActivityType(msgMarketingStatistics.getActivityType());
        }
        msgMarketingMapper.save(msgMarketingStatistics);
    }

    @Override
    public void update(MsgMarketingStatistics msgMarketingStatistics) {
        msgMarketingMapper.update(msgMarketingStatistics);
    }

    @Override
    public void delete(Long id) {
        msgMarketingMapper.delete(id);
    }

    @Override
    public Map<String, Object> findAll(MsgMarketingStatistics msgMarketingStatistics, Integer cycle, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<MsgMarketingStatistics> statisticsList = msgMarketingMapper.findAll(msgMarketingStatistics);
        PageInfo<MsgMarketingStatistics> pageInfo = new PageInfo<>(statisticsList);
        List<MsgMarketingRes> resList = pageInfo.getList().parallelStream().map(statistics -> {
            //周期时间
            Date endTime;
            if (null != cycle) {
                long l = statistics.getStartTime().toInstant().plusSeconds(24 * 60 * 60 * cycle).toEpochMilli();
                endTime = new Date(l);
            } else endTime = new Date();

            MsgMarketingRes res = new MsgMarketingRes(statistics);
            //新关注用户
            List<UserInfo> subscriberByTime = userInfoMapper.findSubscriberByTime(statistics.getStartTime(), statistics.getEndTime());
            if (subscriberByTime == null || subscriberByTime.size() == 0)
                return res;
            Set<String> openIds = subscriberByTime.stream().map(UserInfo::getOpenid).collect(Collectors.toSet());
            //所有新关注用户订单
            List<FlowOrder> orders = flowOrderMapper.findAllByOpenIds(openIds, statistics.getStartTime(), endTime);
            //统计新关注用户数
            res.setSubscriberCount((long) subscriberByTime.size());
            //关注率 关注人数/短信成功数
            double subRate = Double.valueOf(subscriberByTime.size()) / Double.valueOf(statistics.getMsgSuccessCount());
            res.setSubRate(subRate);
            //取关用户数
            res.setUnSubscriberCount(subscriberByTime.stream().filter(userInfo -> userInfo.getSubscribe().equals("0")).count());
            //领取卡券用户数
            res.setCardCount(cardMapper.countByFromUserAndTime(openIds, statistics.getStartTime(), endTime));
            //金进入H5用户数
            Long enterCount = enterRecordMapper.countByOpenIds(openIds);
            res.setEnterConut(enterCount);
            //进入H5率 进入H5用户数/关注用户数
            double enterRate = Double.valueOf(enterCount) / Double.valueOf(res.getSubscriberCount());
            res.setEnterRate(enterRate);
            //解析号码用户数
            res.setTypeNumConut(0L);
            //消费用户数
            long payCount = orders
                    .parallelStream()
                    .filter(flowOrder -> flowOrder.getOrderStatus() == 1)
                    .map(FlowOrder::getOpenId)
                    .distinct()
                    .count();
            res.setPayCount(payCount);
            //生成订单数
            res.setOrderCount((long) orders.size());
            //成功支付订单数
            res.setPaidOrderCount(orders
                    .parallelStream()
                    .filter(flowOrder -> flowOrder.getOrderStatus() == 1).count());
            //充值成功订单数
            List<FlowOrder> successOrders = orders.stream()
                    .filter(flowOrder -> flowOrder.getOrderStatus() == 1 &&
                            flowOrder.getRechargeStatus() == 1 &&
                            flowOrder.getCustomeService() == 0)
                    .collect(Collectors.toList());

            res.setSuccessCount((long) successOrders.size());

            if (successOrders != null && successOrders.size() > 0) {
                //营收
                BigDecimal amount = successOrders.stream()
                        .map(FlowOrder::getOrderPrice)
                        .reduce((p, p1) -> p.add(p1))
                        .get();
                res.setAmount(amount);
                //利润
                Double profit = successOrders.stream()
                        .map(FlowOrder::getProfit)
                        .reduce((p, p1) -> p + p1)
                        .get();
                res.setProfit(new BigDecimal(profit));
                //支付成功率 支付成功的人数/下单用户数
                double payRate = Double.valueOf(payCount) / Double.valueOf(orders
                        .parallelStream()
                        .map(FlowOrder::getOpenId)
                        .distinct()
                        .count());
                res.setPayRate(payRate);
                //充值成功率 充值成功人数 / 支付成功人数
                double successRate = Double.valueOf(successOrders.parallelStream().map(FlowOrder::getOpenId).distinct().count()) / Double.valueOf(payCount);
                res.setSuccessRate(successRate);
                res.setTotalRate(subRate * enterRate * payRate * successRate);
            } else {//没有成功订单
                BigDecimal zero = new BigDecimal(0);
                res.setAmount(zero);
                res.setProfit(zero);
                res.setTotalRate(Double.valueOf(0));
            }
            return res;
        }).collect(Collectors.toList());
        Map<String, Object> data = new HashMap<>();
        data.put("total", pageInfo.getTotal());
        data.put("hasMore", pageInfo.isHasNextPage());
        data.put("content", resList);
        return data;
    }

    @Override
    public Set<String> findAllPlatformOperator() {
        return msgMarketingMapper.findAllPlatformOperator();
    }

    @Override
    public Set<String> findAllActivityType() {
        Set<String> strings = new HashSet<>();
        Path path = Paths.get("activityType.txt");
        if (!Files.exists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try (Scanner scanner = new Scanner(path)) {
                while (scanner.hasNext())
                    strings.add(scanner.nextLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return strings;
    }

    @Override
    public Set<String> addActivityType(String type) {
        Set<String> strings = new HashSet<>();
        Path path = Paths.get("activityType.txt");
        if (!Files.exists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        boolean add = false;
        try (Scanner scanner = new Scanner(path)) {
            while (scanner.hasNext())
                strings.add(scanner.nextLine());
            if (!strings.contains(type)) {
                strings.add(type);
                add = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (add) {
            try (FileWriter fileWriter = new FileWriter(path.toFile().getAbsolutePath())) {
                strings.forEach(s -> {
                    try {
                        fileWriter.write(s + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return strings;
    }

    @Override
    public MsgMarketingStatistics findOne(Long id) {
        return msgMarketingMapper.findOne(id);
    }
}
