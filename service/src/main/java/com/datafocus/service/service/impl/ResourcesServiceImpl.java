package com.datafocus.service.service.impl;

import java.util.List;

import com.datafocus.common.pojo.Resources;
import com.datafocus.service.mapper.ResourcesMapper;
import com.datafocus.service.service.ResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class ResourcesServiceImpl implements ResourcesService {
	
	@Autowired
	private ResourcesMapper resourcesMapper;

	@Override
	public List<Resources> findAllResRoles(Integer roleId) {
		return resourcesMapper.findAllResRoles(roleId);
	}

	@Override
	public List<Resources> findResourcesByAccountId(String accountId)
	{
		return resourcesMapper.findResourcesByAccountId(accountId);
	}

}
