package com.datafocus.service.service;

import com.datafocus.common.pojo.ActiveMessage;
import com.datafocus.common.pojo.Regular;

import java.util.List;


public interface AutoReplyService {
    /**
     * 保存关键字自动回复规则
     *
     * @param regular
     */
    void saveRegular(Regular regular);

    /**
     * 保存被动回复消息内容
     *
     * @param activeMessage
     */
    void savePassive(ActiveMessage activeMessage);

    /**
     * 查询被动回复消息
     *
     * @return
     */
    ActiveMessage passive();


    /**
     * 关键字自动回复列表
     *
     * @return
     */
    List<Regular> regularList();

    /**
     * 删除关键字自动回复
     *
     * @param id
     */
    void delete(Integer id);

}
