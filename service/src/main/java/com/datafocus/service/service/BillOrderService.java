package com.datafocus.service.service;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.pojo.BillOrder;
import com.github.pagehelper.PageInfo;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liuheng on 2017/7/27.
 */

public interface BillOrderService {

    /**
     * 查询话费订单列表 ,分页查询
     *
     * @param params
     * @param billOrder
     * @return
     */
    PageInfo<BillOrder> list(Map<String, String> billOrder, FlowPage page);

    void saveBillOrder(BillOrder billOrder);

    void modify(Map<String, Object> params);

    /**
     * 导出话费订单
     * @param params
     * @param outputStream
     */
    void exprot(HashMap<String, String> params, OutputStream outputStream);
}
