package com.datafocus.service.service.impl;

import com.datafocus.common.config.MapProperties;
import com.datafocus.common.config.ProtalProperties;
import com.datafocus.common.dto.*;
import com.datafocus.common.pojo.*;
import com.datafocus.service.mapper.*;
import com.datafocus.service.service.CustomTagService;
import com.datafocus.service.service.OrderService;
import com.datafocus.service.service.PromoterService;
import com.datafocus.service.service.TemplateMsgService;
import com.datafocus.service.service.remote.DistrictService;
import com.datafocus.service.task.RechargeOrderDelayService;
import com.datafocus.service.task.RechargeOrderDelayed;
import com.datafocus.service.task.RefundOrderDelayService;
import com.datafocus.service.task.RefundOrderDelayed;
import com.datafocus.service.utils.BaseResult;
import com.datafocus.service.utils.DFUtils;
import com.datafocus.service.utils.RedisUtil;
import com.datafocus.weixin.common.exception.WxError;
import com.datafocus.weixin.common.exception.WxRuntimeException;
import com.datafocus.weixin.common.util.JsonMapper;
import com.datafocus.weixin.mp.base.AppSetting;
import com.datafocus.weixin.mp.card.Cards;
import com.datafocus.weixin.pay.base.PaySetting;
import com.datafocus.weixin.pay.mp.JsSigns;
import com.datafocus.weixin.pay.mp.Orders;
import com.datafocus.weixin.pay.mp.bean.*;
import com.datafocus.weixin.pay.redpack.RedPacks;
import com.datafocus.weixin.pay.redpack.bean.RedPackRequest;
import com.datafocus.weixin.pay.redpack.bean.RedPackResponse;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);
    private static final String AMT_TYPE = "ALL_RAND";
    private static final String MCH_NAME = "流量充值中心";

    @Autowired
    private FlowOrderMapper flowOrderMapper;
    @Autowired
    private CardMapper cardMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private TemplateMsgService templateMsgService;
    @Autowired
    private RechargeOrderDelayService rechargeOrderDelayService;
    @Autowired
    private RefundOrderDelayService refundOrderDelayService;
    @Autowired
    private CustomTagService customTagService;
    @Autowired
    private CustomTagMapper customTagMapper;
    @Autowired
    private BillOrderMapper billOrderMapper;
    @Autowired
    private DistrictService districtService;
    @Autowired
    private PartnerMapper partnerMapper;

    @Autowired
    private ProtalProperties protalProperties;
    @Autowired
    private Cards cards;
    @Autowired
    private Orders orders;
    @Autowired
    private PaySetting paySetting;
    @Autowired
    private PromoterService promoterService;


    private JsSigns jsSigns;

    private RedPacks redPacks;

    @PostConstruct
    public void init() {
        jsSigns = JsSigns.with(paySetting);
        redPacks = RedPacks.with(paySetting);
    }


    @Override
    public PageInfo<FlowOrder> list(String openId, Integer page, Integer size) {
        if (page != null && size != null) {
            PageHelper.startPage(page, size, "order_time desc");
        }
        List<FlowOrder> list = flowOrderMapper.findOrderByOpenId(openId);
        PageInfo<FlowOrder> pageInfo = new PageInfo<FlowOrder>(list);
        return pageInfo;
    }

    /**
     * 创建订单
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, noRollbackFor = WxRuntimeException.class)
    public JSSignature saveOrder(Map<String, Object> params) {
        FlowOrder order = this.buildOrder(params);
        logger.info("调用微信统一下单接口生成订单");
        UnifiedOrderResponse unifiedOrderResponse = unifiedOrder(order, params.get("ip").toString());
        logger.info("调用微信统一下单接口结束");
        JSSignature jSSignature = jsSigns.createJsSignature(unifiedOrderResponse.getPrepayId());
        flowOrderMapper.save(order);
        return jSSignature;
    }

    /**
     * 微信统一下单
     *
     * @param order
     * @return
     */
    public UnifiedOrderResponse unifiedOrder(FlowOrder order, String ip) {
        UnifiedOrderRequest unifiedOrderRequest = bulidUnifiedOrderRequest(order, ip);
        UnifiedOrderResponse unifiedOrderResponse = orders.unifiedOrder(unifiedOrderRequest);
        return unifiedOrderResponse;
    }


    /**
     * 构建微信统一下单对象
     *
     * @return
     */
    public UnifiedOrderRequest bulidUnifiedOrderRequest(FlowOrder order, String ip) {

        UnifiedOrderRequest unifiedOrderRequest = new UnifiedOrderRequest();
        unifiedOrderRequest.setBody(MCH_NAME);
        unifiedOrderRequest.setDetail(order.getProductRemark());
        unifiedOrderRequest.setTradeNumber(order.getOrderNo());
        unifiedOrderRequest.setTotalFee(order.getOrderPrice().multiply(new BigDecimal(100)).intValue());
        unifiedOrderRequest.setBillCreatedIp(ip);
        unifiedOrderRequest.setNotifyUrl(protalProperties.getNotifyUrl());
        unifiedOrderRequest.setTradeType("JSAPI");
        unifiedOrderRequest.setOpenId(order.getOpenId());
        return unifiedOrderRequest;
    }


    /**
     * 构建订单对象
     */
    public FlowOrder buildOrder(Map<String, Object> params) {
        //code 是微信获取用户的opendId的授权码
//		String openId =WXUtil.getOpenId(chargeRequest.getCode(), request);
        //String openId = "oNE_mwZZYFX7FyavJe3rkCbPHMTU";
        int productId = (Integer) params.get("productId");
        String cardId = ObjectUtils.toString(params.get("cardId"));
        String userCardCode = ObjectUtils.toString(params.get("userCardCode"));
        String phone = ObjectUtils.toString(params.get("phone"));
        String payType = ObjectUtils.toString(params.get("payType"));
        String openId = ObjectUtils.toString(params.get("openId"));

        ProductDto.Product product = productMapper.findProductById(productId);
        BigDecimal unitPrice = product.getDiscount();
        //当前是否有卡券
        if (StringUtils.isNotBlank(cardId)) {
            try {
                //校验code
                cards.getCode(cardId, userCardCode, true);
            } catch (WxRuntimeException e) {
                WxError error = e.getWxError();
                if (error != null) {
                    if (error.getErrorCode() == 40127 || error.getErrorCode() == 40099) {
                        //处理已被核销， 删除或转赠中的卡券
                        cardMapper.deleteCardByCardAndCode(openId, cardId, userCardCode);
                    }
                }
                throw e;
            }
            // 获取卡券信息
            CardDto card = cardMapper.findCardByCardId(cardId);

            if ("DISCOUNT".equals(card.getCardType())) {

                BigDecimal discount = new BigDecimal(100).subtract(new BigDecimal(card.getDiscount())).divide(new BigDecimal(10));

                unitPrice = product.getPrice().multiply(discount).divide(new BigDecimal(10));

            } else {//代金券
                unitPrice = product.getPrice().subtract(new BigDecimal(card.getReduceCost()).divide(new BigDecimal(100)));
            }
        }
        unitPrice = new BigDecimal(new java.text.DecimalFormat("0.00").format(unitPrice));

        DistrictDto districtDto = districtService.district(phone.substring(0, 7));
        //创建订单
        FlowOrder flowOrder = new FlowOrder();
        flowOrder.setOrderNo(DFUtils.createBillNo("1000"));
        flowOrder.setOrderPhone(phone);
        flowOrder.setOrderStatus(0);// 0：未支付
        flowOrder.setPayType(payType);
        flowOrder.setOrderPrice(unitPrice);
        flowOrder.setProductId(productId);
        flowOrder.setFlowPackage(product.getProductName());
        flowOrder.setProductRemark(product.getDescription());
        flowOrder.setCarrierOperator(CarryOperator.valueOf(districtDto.getCarryOperator()));
        flowOrder.setProvince(districtDto.getProvince());
        flowOrder.setOpenId(openId);
        flowOrder.setCardId(cardId);
        flowOrder.setCardCode(userCardCode);
        return flowOrder;
    }

    @Override
    public void webhooks(PaymentNotification paymentNotification) {
        boolean result = orders.checkSignature(paymentNotification);
        if (result) {
            if (paymentNotification.success()) {
                String orderNo = paymentNotification.getTradeNumber();
                FlowOrder order = flowOrderMapper.findOrderByOrderNo(orderNo);
                //todo 双十一
                if (null != order && StringUtils.isNotBlank(order.getCardId()) && StringUtils.isNotBlank(order.getCardCode()) && !"thanks".equals(order.getCardId())) {
                    //支付成功锁定卡券
                    cardMapper.modifyUseStatus(1, 1, order.getOpenId(), order.getCardId(), order.getCardCode());
                }
                if (order != null) {
                    //流量
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("orderStatus", 1);//1:支付成功
                    params.put("orderNo", orderNo);
                    //更新支付状态
                    flowOrderMapper.modify(params);
                    //加入订单充值延时消息队列,(充值回调之后清除消息队列)
                    RechargeOrderDelayed rechargeOrderDelayed = new RechargeOrderDelayed(orderNo, 10 * 60 * 1000);
                    rechargeOrderDelayService.add(rechargeOrderDelayed);
                    try {
                        String openId = order.getOpenId();
                        /*****已消费标签****/
                        List<CustomTagUser> cupay = customTagService.customTagUsersByDismensionNameAndOpenId("付费用户", openId);

                        if (CollectionUtils.isNotEmpty(cupay)) {
                            for (CustomTagUser tagUser : cupay) {
                                customTagMapper.deleteCustomTagUserByOpenIdAndTagId(openId, tagUser.getTagId());
                                customTagMapper.updateCustomUserNum(tagUser.getTagId(), 0);
                            }
                        }
                        CustomTag ct = customTagService.findCustomTag("付费用户", "已消费");
                        //建立用户与标签之间的关系
                        customTagService.saveCustomTagUser(openId, ct.getTagId());

                        //手机号运营商，地区归属地标签
                        String mobile = order.getOrderPhone();

                        DistrictDto districtDto = districtService.district(mobile.substring(0, 7));


                        List<CustomTagUser> cu = customTagService.customTagUsersByDismensionNameAndOpenId("运营商", openId);

                        if (CollectionUtils.isNotEmpty(cu)) {
                            for (CustomTagUser tagUser : cu) {
                                //删除原有的标签关系
                                customTagMapper.deleteCustomTagUserByOpenIdAndTagId(openId, tagUser.getTagId());
                                customTagMapper.updateCustomUserNum(tagUser.getTagId(), 0);
                            }
                        }

                        String carryOperatorName = districtDto.getCarryOperatorName();

                        CustomTag carryTag = customTagService.findCustomTag("运营商", carryOperatorName);

                        //建立用户与标签之间的关系
                        customTagService.saveCustomTagUser(openId, carryTag.getTagId());

                        List<CustomTagUser> cup = customTagService.customTagUsersByDismensionNameAndOpenId("地区", openId);

                        if (CollectionUtils.isNotEmpty(cup)) {
                            for (CustomTagUser tagUser : cup) {
                                //删除原有的标签关系
                                customTagMapper.deleteCustomTagUserByOpenIdAndTagId(openId, tagUser.getTagId());
                                customTagMapper.updateCustomUserNum(tagUser.getTagId(), 0);
                            }
                        }
                        String provinceName = districtDto.getDname();

                        CustomTag provinceTag = customTagService.findCustomTag("地区", provinceName);
                        //建立用户与标签之间的关系
                        customTagService.saveCustomTagUser(openId, provinceTag.getTagId());

                    } catch (Exception e) {
                        logger.error("支付成功回调，给用户打标签，sql异常", e);
                    }
                } else {
                    //话费
                    BillOrder billOrder = billOrderMapper.findBillOrderByOrderNo(orderNo);
                    if (billOrder != null) {
                        billOrderMapper.modify(orderNo);
                    }
                }
            } else {
                logger.error("微信支付回调状态失败,{}");
            }
        } else {
            throw new RuntimeException("微信支付异步通知验证签名失败");
        }
    }


    @Override
    public void operatorCallBack(String orderNo) {
        FlowOrder order = flowOrderMapper.findOrderByOrderNo(orderNo);
        if (null != order && StringUtils.isNotBlank(order.getCardId()) && StringUtils.isNotBlank(order.getCardCode())) {
            //支付成功锁定卡券
            cardMapper.modifyUseStatus(1, 1, order.getOpenId(), order.getCardId(), order.getCardCode());
        }

        if (order != null) {
            //流量
            HashMap<String, Object> params = new HashMap<>();
            params.put("orderStatus", 1);//1:支付成功
            params.put("orderNo", orderNo);
            //更新支付状态
            flowOrderMapper.modify(params);
            //加入订单充值延时消息队列,(充值回调之后清除消息队列)
            RechargeOrderDelayed rechargeOrderDelayed = new RechargeOrderDelayed(orderNo, 10 * 60 * 1000);
            rechargeOrderDelayService.add(rechargeOrderDelayed);
            try {
                String openId = order.getOpenId();
                /*****已消费标签****/
                List<CustomTagUser> cupay = customTagService.customTagUsersByDismensionNameAndOpenId("付费用户", openId);

                if (CollectionUtils.isNotEmpty(cupay)) {
                    for (CustomTagUser tagUser : cupay) {
                        customTagMapper.deleteCustomTagUserByOpenIdAndTagId(openId, tagUser.getTagId());
                        customTagMapper.updateCustomUserNum(tagUser.getTagId(), 0);
                    }
                }
                CustomTag ct = customTagService.findCustomTag("付费用户", "已消费");
                //建立用户与标签之间的关系
                customTagService.saveCustomTagUser(openId, ct.getTagId());

                //手机号运营商，地区归属地标签
                String mobile = order.getOrderPhone();

                DistrictDto districtDto = districtService.district(mobile.substring(0, 7));


                List<CustomTagUser> cu = customTagService.customTagUsersByDismensionNameAndOpenId("运营商", openId);

                if (CollectionUtils.isNotEmpty(cu)) {
                    for (CustomTagUser tagUser : cu) {
                        //删除原有的标签关系
                        customTagMapper.deleteCustomTagUserByOpenIdAndTagId(openId, tagUser.getTagId());
                        customTagMapper.updateCustomUserNum(tagUser.getTagId(), 0);
                    }
                }

                String carryOperatorName = districtDto.getCarryOperatorName();

                CustomTag carryTag = customTagService.findCustomTag("运营商", carryOperatorName);

                //建立用户与标签之间的关系
                customTagService.saveCustomTagUser(openId, carryTag.getTagId());

                List<CustomTagUser> cup = customTagService.customTagUsersByDismensionNameAndOpenId("地区", openId);

                if (CollectionUtils.isNotEmpty(cup)) {
                    for (CustomTagUser tagUser : cup) {
                        //删除原有的标签关系
                        customTagMapper.deleteCustomTagUserByOpenIdAndTagId(openId, tagUser.getTagId());
                        customTagMapper.updateCustomUserNum(tagUser.getTagId(), 0);
                    }
                }
                String provinceName = districtDto.getDname();

                CustomTag provinceTag = customTagService.findCustomTag("地区", provinceName);
                //建立用户与标签之间的关系
                customTagService.saveCustomTagUser(openId, provinceTag.getTagId());

            } catch (Exception e) {
                logger.error("支付成功回调，给用户打标签，sql异常", e);
            }
        } else {
            //话费
            BillOrder billOrder = billOrderMapper.findBillOrderByOrderNo(orderNo);
            if (billOrder != null) {
                billOrderMapper.modify(orderNo);
            }
        }
    }

    @Override
    public Integer rechargeCallBack(String msgIn) throws Exception {
        String xml = java.net.URLDecoder.decode(msgIn, "utf-8");
        logger.info("流量星平台回调状态, 报文信息:{}", xml);
        return resolveCallBackXml(xml);
    }

    /**
     * 解析手机充值状态回调XML
     *
     * @param xml
     * @throws DocumentException
     */
    public Integer resolveCallBackXml(String xml) throws DocumentException {
        Integer retStatus = 0;
        Document doc = DocumentHelper.parseText(xml);
        Element body = doc.getRootElement();
        Element result = body.element("result");
        String msgid = result.elementText("msgid").trim();
        String orderNo = result.elementText("orderid").trim();
        String rechargeStatus = result.elementText("rechargeStatus").trim();
        String profitPrice = result.elementText("profitPrice");
        logger.info("profitPrice***************" + profitPrice + "************");
        String rechargeDesc = result.elementText("rechargeDesc").trim();
        FlowOrder order = flowOrderMapper.findOrderByOrderNo(orderNo);
        if (order.getRechargeStatus() == 0) {
            int refundStatus = 0;
            rechargeOrderDelayService.remove(orderNo);//移除订单充值延时队列;
            if ("0".equals(rechargeStatus)) {
                rechargeStatus = "1"; //成功
                //更新统计分析表
                promoterService.addOrUpdate(order);
            } else {
                logger.info("订单号：{}充值失败，全额退款，充值失败原因：{}", orderNo, rechargeDesc);
                retStatus = -1;
                rechargeStatus = "2"; //失败
                //订单充值回调失败发送退款请求
                RefundRequest refundRequest = new RefundRequest();
                refundRequest.setOperatorId(paySetting.getMchId());
                refundRequest.setTotalFee(order.getOrderPrice().multiply(new BigDecimal("100")).intValue());
                refundRequest.setRefundFee(order.getOrderPrice().multiply(new BigDecimal("100")).intValue());
                refundRequest.setRefundFeeType("CNY");
                refundRequest.setRefundNumber(DFUtils.createBillNo("1000"));
                refundRequest.setTradeNumber(order.getOrderNo());
                try {
                    RefundResponse refundResponse = orders.refund(refundRequest);
                    if (refundResponse.success()) {
                        refundStatus = 4;//退款中
                        int startTime = RefundOrderDelayed.startTimeMap.get(1);//第一次查询
                        RefundOrderDelayed refund = new RefundOrderDelayed(1, order.getOrderNo(), startTime);
                        refundOrderDelayService.add(refund);
                    } else {
                        refundStatus = 5;//退款失败
                        logger.error("微信自动退款失败,比特星人订单号:{},错误信息{},业务错误信息:{}", order.getOrderNo(), refundResponse.getReturnMessage(), refundResponse.getErrorCodeDesc());
                    }
                } catch (Exception e) {
                    refundStatus = 5;//退款失败
                    logger.error("微信自动退款异常,异常堆栈信息:{}", e);
                }
            }

            //todo 双十一
            if (null != order && StringUtils.isNotBlank(order.getCardId()) && StringUtils.isNotBlank(order.getCardCode()) && !"thanks".equals(order.getCardId())) {
                if ("1".equals(rechargeStatus)) {//充值成功核销卡券
                    //核销卡券
                    try {
                        cardMapper.modifyUseStatus(1, 1, order.getOpenId(), order.getCardId(), order.getCardCode());
                        cards.consumeCode(order.getCardId(), order.getCardCode());
                    } catch (Exception e) {
                        logger.error("核销卡券失败,系统异常,用户openid:{},卡券id:{},卡券code {},充值号码:{},堆栈信息:{}", order.getOpenId(), order.getCardId(), order.getCardCode(), order.getOrderPhone(), e);
                    }
                } else {//充值失败，解锁卡券
                    cardMapper.modifyUseStatus(0, 0, order.getOpenId(), order.getCardId(), order.getCardCode());
                }
            }

            HashMap<String, Object> params = new HashMap<>();
            params.put("orderNo", orderNo);
            params.put("msgid", msgid);
            params.put("rechargeStatus", rechargeStatus);
            params.put("rechargeDesc", rechargeDesc);
            params.put("refundStatus", refundStatus);
            params.put("profitPrice", profitPrice == null ? new BigDecimal(0) : profitPrice);
            flowOrderMapper.modify(params);
            BaseResult baseResult = templateMsgService.sendTempletMsg(orderNo, rechargeStatus);
            logger.info("发送回调模板消息：{}");
            //如果订单充值成功，且该订单满足发红包要求，没有发过红包
            if ("1".equals(rechargeStatus) && order.getRedpackStatus() == 0 && 0 == order.getUseRedpack().compareTo(BigDecimal.ZERO)) {
                sendRedPack(order.getOpenId(), order.getRedpack().multiply(new BigDecimal(100)).intValue(), order.getOrderNo());
            }
        }
        return retStatus;
    }

    @Override
    public void refund(String orderNo) {
        FlowOrder flowOrder = flowOrderMapper.findOrderByOrderNo(orderNo);
        int refundStatus = 5;
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOperatorId(paySetting.getMchId());
        refundRequest.setTotalFee(flowOrder.getOrderPrice().multiply(new BigDecimal("100")).intValue());
        refundRequest.setRefundFee(flowOrder.getOrderPrice().multiply(new BigDecimal("100")).intValue());
        refundRequest.setRefundFeeType("CNY");
        refundRequest.setRefundNumber(DFUtils.createBillNo("1000"));
        refundRequest.setTradeNumber(flowOrder.getOrderNo());
        try {
            RefundResponse refundResponse = orders.refund(refundRequest);
            if (refundResponse.success()) {
                refundStatus = 4;//退款中
                int startTime = RefundOrderDelayed.startTimeMap.get(1);//第一次查询
                RefundOrderDelayed refund = new RefundOrderDelayed(1, flowOrder.getOrderNo(), startTime);
                refundOrderDelayService.add(refund);
                logger.info("自动退款成功，流量订单号：" + orderNo);
            } else {
                logger.error("微信自动退款失败,比特星人订单号:{},错误信息{},业务错误信息:{}", flowOrder.getOrderNo(), refundResponse.getReturnMessage(), refundResponse.getErrorCodeDesc());
            }
        } catch (Exception e) {
            logger.error("微信自动退款异常,异常堆栈信息:{}", e);
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("orderNo", flowOrder.getOrderNo());
        params.put("refundStatus", refundStatus);
        flowOrderMapper.modify(params);
        templateMsgService.sendTempletMsg(flowOrder.getOrderNo(), "2");
        cardMapper.modifyUseStatus(0, 0, flowOrder.getOpenId(), flowOrder.getCardId(), flowOrder.getCardCode());
    }

    /**
     * @param openId 接受红包的用户用户在wxappid下的openid
     * @param amount 付款金额，单位分
     */
    public void sendRedPack(String openId, int amount, String orderNo) {
        try {
            logger.info("流量购买成功自动发送红包，订单号：{}，openid:{},发放金额:{}", orderNo, openId, amount / 100);
            RedPackRequest redPackRequest = new RedPackRequest();
            redPackRequest.setAmount(amount);
            redPackRequest.setOpenId(openId);
            redPackRequest.setAmtType(AMT_TYPE);
            redPackRequest.setClientIp(DFUtils.getHostIp(DFUtils.getInetAddress()));
            redPackRequest.setAppId(paySetting.getAppId());
            redPackRequest.setNumber(1);
            redPackRequest.setRemark("订购商品返现,系统自动发送");
            redPackRequest.setSendName("比特星人");
            redPackRequest.setWishing("感谢您关注比特星人，祝您生活快乐！");
            redPackRequest.setBillNumber(DFUtils.createBillNo(paySetting.getMchId()));
            redPackRequest.setActivityName("充流量，抢红包");
            RedPackResponse redPackResponse = redPacks.sendSingle(redPackRequest);
            if ("SUCCESS".equals(redPackResponse.getReturnCode()) && "SUCCESS".equals(redPackResponse.getResultCode())) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("orderNo", orderNo);
                param.put("redpack", redPackResponse.getAmount() / 100);
                flowOrderMapper.modify(param);
            }
            logger.info("订购流量商品返现,返回状态码:{},返回信息:{},商户订单号:{},用户openid:{},红包金额:{}元", redPackResponse.getResultCode(), redPackResponse.getReturnMessage(), redPackResponse.getBillNumber(), redPackResponse.getOpenId(), redPackResponse.getAmount() / 100);
        } catch (Exception e) {
            logger.error("订购流量商品返现，发红包失败，{}", e);
        }
    }

    @Override
    public List<BillOrder> freeBillOrders(String openId) {
        return billOrderMapper.feeBillOrder(openId);
    }

    @Override
    public JSSignature createFlowOrder(Map<String, Object> params) {
        String partnerKey = ObjectUtils.toString(params.get("partnerKey"));
        FlowOrder flowOrder = bulidFlowOrder(params);
        flowOrderMapper.save(flowOrder);
        logger.info("调用微信统一下单接口生成订单");
        UnifiedOrderResponse unifiedOrderResponse = unifiedOrder(flowOrder, params.get("ip").toString());
        logger.info("调用微信统一下单接口结束");
        JSSignature jSSignature = jsSigns.createJsSignature(unifiedOrderResponse.getPrepayId());
        if (StringUtils.isNotBlank(partnerKey)) {
            Partner partner = partnerMapper.findByPartnerKey(partnerKey);
            if (null != partner) {
                logger.info("合作商: " + partner.getPartnerName() + " 订单！");
                PartnerOrder partnerOrder = new PartnerOrder();
                partnerOrder.setOrderNo(flowOrder.getOrderNo())
                        .setPartnerKey(partner.getPartnerKey());
                partnerMapper.savePartnerOrder(partnerOrder);
            } else {
                logger.info("错误的合作商标识： " + partnerKey);
            }
        }
        return jSSignature;
    }

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private FlowOrder bulidFlowOrder(Map<String, Object> params) {

        String productId = ObjectUtils.toString(params.get("productId"));
        String openId = ObjectUtils.toString(params.get("openId"));
        String cardId = ObjectUtils.toString(params.get("cardId"));
        String cardCode = ObjectUtils.toString(params.get("cardCode"));
        String mobile = ObjectUtils.toString(params.get("mobile"));
        //获取流量产品对象
        String json = stringRedisTemplate.opsForValue().get(protalProperties.getOperatorId() + productId);
        logger.info("key:" + protalProperties.getOperatorId() + productId + "------" + "value:" + json);
        logger.info("创建订单查询产品信息,产品ID:{},redis返回数据:{}", productId, json);
        Product product = JsonMapper.defaultMapper().fromJson(json, Product.class);
        BigDecimal stPrice = product.getStPrice();//产品标准价
        BigDecimal salePrice = product.getSalePrice();//产品销售价
        BigDecimal unitPrice = product.getSalePrice();//支付价格
        MoblileDistrictDto mobileDistrict = districtService.mobileDistrict(mobile.substring(0, 7));
        //todo 双十一
        if (StringUtils.isNotBlank(cardId) && StringUtils.isNotBlank(cardCode) && !"thanks".equals(cardId)) {// 如果用户使用卡券
            try {
                // 校验code是否已核销
                cards.getCode(cardId, cardCode, true);
                //校验code是否已锁定
                int isLock = cardMapper.getCardIsLockByCardCode(cardCode, openId);
                if (isLock == 1) {//锁定状态
                    throw new WxRuntimeException(-1, "卡券已使用");
                }
            } catch (WxRuntimeException e) {
                WxError error = e.getWxError();
                if (error != null) {
                    if (error.getErrorCode() == 40127 || error.getErrorCode() == 40099) {
                        // 处理已被核销， 删除或转赠中的卡券
                        cardMapper.deleteCardByCardAndCode(openId, cardId, cardCode);
                    }
                }
                throw e;
            } catch (Exception e) {
                throw new RuntimeException("卡券系统异常", e);
            }

            //如果是特殊包型，不能使用优惠券
            if (productId.contains("SPECIL_PACKAGE")) throw new RuntimeException("当前卡券不适用该商品");
            //检查卡券是否适用该商品
            List<CardDto.CardAcceptCategory> cardAcceptCategories = cardMapper.cardAcceptCategoryByCardId(cardId);
            //检查卡券适用范围是否包含当前商品
            Pattern pattern = Pattern.compile("(STAND_PACKAGE)(\\d{1})(\\d{2})(\\d{4})(\\d+)(G|M)");
            List<String> suitables = cardAcceptCategories.stream()
                    .map(CardDto.CardAcceptCategory::getProductId).filter(s -> {
                        Matcher matcher = pattern.matcher(s);
                        if (matcher.find()) {
                            String vendor = convert(matcher.group(2)),
                                    province = matcher.group(3),
                                    city = matcher.group(4),
                                    productName = matcher.group(5) + matcher.group(6);
                            //包型不相等
                            if (!productName.equals(product.getProductName())) return false;
                            logger.info("包型验证通过");
                            //运营商不同
                            if (!vendor.equals(product.getVendor())) return false;
                            logger.info("运营商验证通过");
                            //如果是全国直接不判断地市
                            if (province.equals("00")) return true;
                            logger.info("非全国券，即将验证省份");
                            // 省份不是全国 且 省份不相等
                            if (!province.equals(product.getProvince())) return false;
                            logger.info("省份验证通过");
                            //省验证通过的情况下 地市为全市表示适用
                            if (city.equals("0000")) return true;
                            logger.info("非全省券，即将验证地市");
                            // 地市不相等 且 地市不是 全省
                            if (!city.equals(product.getCity())) return false;
                            logger.info("地市验证通过");
                        }
                        return true;
                    }).collect(Collectors.toList());
            if (suitables.isEmpty()) throw new RuntimeException("当前卡券不适用该商品");
            // 获取卡券信息
            CardDto card = cardMapper.findCardByCardId(cardId);
            if ("DISCOUNT".equals(card.getCardType())) { //折扣券==>(标准价*折扣)
                BigDecimal discount = new BigDecimal(100).subtract(new BigDecimal(card.getDiscount())).divide(new BigDecimal(10));
                unitPrice = stPrice.multiply(discount).divide(new BigDecimal(10));
            } else {// 代金券==>(销售价-代金券额度)
                unitPrice = salePrice.subtract(new BigDecimal(card.getReduceCost()).divide(new BigDecimal(100)));
            }
        }
        FlowOrder flowOrder = new FlowOrder();
        flowOrder.setOrderNo(DFUtils.createBillNo("1000"));
        flowOrder.setOrderPhone(mobile);
        flowOrder.setOrderStatus(0);// 0：未支付
        flowOrder.setPayType("wx_pub");
        flowOrder.setOrderPrice(unitPrice);
        flowOrder.setFlowPackage(product.getProductName());
        flowOrder.setProductRemark(product.getDesc());
        flowOrder.setCarrierOperator(CarryOperator.valueOf(Integer.parseInt(mobileDistrict.getVendorCode())));
        flowOrder.setProvince(mobileDistrict.getProvinceCode());
        flowOrder.setCity(mobileDistrict.getCityCode());
        flowOrder.setOpenId(openId);
        flowOrder.setCardId(cardId);
        flowOrder.setCardCode(cardCode);
        flowOrder.setStandardPrice(stPrice);
        return flowOrder;
    }

    private String convert(String source) {
        switch (source) {
            case "1":
                return "CMCC";
            case "2":
                return "CUCC";
            case "3":
                return "CTCC";
        }
        return "UNKOWN";
    }


    @Override
    public List<FlowOrder> list(String openId) {
        return flowOrderMapper.findFeeFlowOrderByOpenId(openId);
    }


    @Override
    public List<FlowOrder> findOrder(Map<String, Object> params) {
        if (params != null && params.get("page") != null && params.get("size") != null) {
            PageHelper.startPage(NumberUtils.toInt(ObjectUtils.toString(params.get("page"))), NumberUtils.toInt(ObjectUtils.toString(params.get("size"))), "order_time desc ");
        }
        return flowOrderMapper.findOrderByOrderNoOrOrderPhoneOrOrderStatus(params);
    }

    @Override
    public void modifyOrderCustome(String orderId, Integer customeService) {
        flowOrderMapper.modifyOrderCustome(orderId, customeService);
    }

    @Override
    public FlowOrder findByOrderNo(String orderNo) {
        return flowOrderMapper.findOrderByOrderNo(orderNo);
    }
}
