package com.datafocus.service.service;

import com.datafocus.common.pojo.AlarmUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by liuheng on 2017/9/1.
 */
public interface AlarmUserService {

    void addAlarmUser(AlarmUser alarmUser);

    List<AlarmUser> listAlarmUser();

    List<AlarmUser> listOpenUser();

    void updateStatus(int id,int status);

}
