package com.datafocus.service.service;

import com.datafocus.common.dto.Statistics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public interface StatisticsService {

	public List<Statistics> findPayStatistics(Map<String, Object> map);
	
	public List<Statistics> findTransaction(Map<String, Object> map);
	
	public List<Statistics> findCarrierOperatorStatistics(Map<String, Object> map);
	
	public List<Statistics> findCreateOrderStatisticsData(Map<String, Object> map);
	
	public List<Statistics> findAccessStatisticsData(Map<String, Object> map);
	
	public List<Statistics> findPhoneResolveStatistics(Map<String, Object> map);
	
}
