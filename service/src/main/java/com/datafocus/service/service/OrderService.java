package com.datafocus.service.service;

import com.datafocus.common.pojo.BillOrder;
import com.datafocus.common.pojo.FlowOrder;
import com.datafocus.weixin.pay.mp.bean.JSSignature;
import com.datafocus.weixin.pay.mp.bean.PaymentNotification;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface OrderService {
	
	/**
	 * 流量充值历史记录
	 * @param openId
	 * @return
	 */
	PageInfo<FlowOrder> list(String openId, Integer page,Integer size);
	
	
	/**
	 * 流量历史充值记录
	 * @param openId
	 * @return
	 */
	List<FlowOrder> list(String openId);
	
	/**
	 * 话费充值历史记录
	 * @param openId
	 * @return
	 */
	List<BillOrder> freeBillOrders(String openId);
	
	/**
	 * 创建订单
	 * @return
	 */
	JSSignature saveOrder(Map<String,Object> params);
	
	
	/**
	 * 创建流量订单
	 * @return
	 */
	JSSignature createFlowOrder(Map<String,Object> params);
	/**
	 * 订单支付回调
	 */
	void webhooks(PaymentNotification paymentNotification);
	
	/**
	 * 手机充值状态回调
	 * @param msgIn 流量星回调报文
	 */
	Integer rechargeCallBack(String msgIn) throws Exception;

	List<FlowOrder> findOrder(Map<String, Object> param);

	void modifyOrderCustome(String orderId, Integer customeService);

	void refund(String orderNo);

	void operatorCallBack(String orderNo);

	FlowOrder findByOrderNo(String orderNo);
}
