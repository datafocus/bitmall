package com.datafocus.service.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datafocus.common.domain.page.FlowPage;
import com.datafocus.common.pojo.FlowChannel;
import com.datafocus.common.pojo.FlowOrder;
import com.github.pagehelper.PageInfo;

public interface FlowMgtService {

	List<FlowChannel> findChannelByCarrierOperator(int carrierOperator);
	
	void modifyFlowChannelStatus(Integer[] ids, int status, String remark);

}
