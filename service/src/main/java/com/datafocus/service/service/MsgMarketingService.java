package com.datafocus.service.service;

import com.datafocus.common.pojo.MsgMarketingStatistics;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by layne on 2017/12/5.
 */
public interface MsgMarketingService {
    /**
     * 添加活动
     *
     * @param msgMarketingStatistics
     * @return
     */
    void addMsgMarketing(MsgMarketingStatistics msgMarketingStatistics);

    /**
     * @param msgMarketingStatistics
     */
    void update(MsgMarketingStatistics msgMarketingStatistics);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * @param start
     * @param msgMarketingStatistics
     * @param cycle
     * @param size
     * @return
     */
    Map<String, Object> findAll(MsgMarketingStatistics msgMarketingStatistics, Integer cycle, Integer page, Integer size);

    /**
     * 查询渠道商
     * @return
     */
    Set<String> findAllPlatformOperator();

    /**
     * 获取所有活动类型
     * @return
     */
    Set<String> findAllActivityType();

    /**
     * 添加活动类型
     * @param type
     * @return
     */
    Set<String> addActivityType(String type);

    /**
     *
     * @param id
     * @return
     */
    MsgMarketingStatistics findOne(Long id);
}
