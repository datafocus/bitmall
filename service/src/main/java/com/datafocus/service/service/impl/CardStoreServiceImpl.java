package com.datafocus.service.service.impl;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import com.datafocus.common.pojo.CardStore;
import com.datafocus.service.mapper.CardStoreMapper;
import com.datafocus.service.service.CardStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datafocus.weixin.mp.card.Cards;
import com.datafocus.weixin.mp.card.bean.LandingCard;
import com.datafocus.weixin.mp.card.bean.LandingCard.Landing;
import com.datafocus.weixin.mp.media.Materials;
import org.springframework.web.multipart.MultipartFile;


@Service
public class CardStoreServiceImpl implements CardStoreService {

	@Autowired
	private CardStoreMapper cardStoreMapper;
	@Autowired
	private Cards cards ;
	@Autowired
	private Materials materials;
	

	@Transactional
	@Override
	public void saveCardStore(CardStore cardStore, MultipartFile bannerFile) throws IOException
	{
		LandingCard landingCard = buildLandingCard(cardStore,bannerFile);
		
		//卡券货架同步到微信服务器
		Landing landing = cards.createCardLanding(landingCard);
		
		cardStore.setPageId(landing.getPageId());
		
		cardStore.setUrl(landing.getUrl());
		//卡券货架同步DB
		cardStoreMapper.saveCardStore(cardStore);
		for (CardStore.CardStoreExt ext : cardStore.getCardStoreExt())
		{
			ext.setPageId(cardStore.getPageId());
		}
		cardStoreMapper.saveCardStoreExt(cardStore.getCardStoreExt());
	}

	public LandingCard  buildLandingCard(CardStore cardStore, MultipartFile bannerFile) throws IOException
	{
		String banner = materials.addMpNewsImage(bannerFile.getInputStream(), bannerFile.getOriginalFilename());
		cardStore.setBanner(banner);
		LandingCard landingCard = new LandingCard();
		landingCard.setBanner(cardStore.getBanner());
		landingCard.setCanShare(cardStore.getCanShare()==0 ?true : false);
		landingCard.setScene(cardStore.getScene());
		landingCard.setTitle(cardStore.getTitle());
		
		for (CardStore.CardStoreExt card : cardStore.getCardStoreExt())
		{
			LandingCard.CardList list = new LandingCard.CardList();
			card.setThumbUrl(card.getThumbUrl());
			list.setCardId(card.getCardId());
			landingCard.getCardList().add(list);
		}
		return landingCard;
	}
	
	@Override
	public void deleteCardStore(String pageId)
	{
		cardStoreMapper.deleteStore(pageId);
	}

	@Override
	public List<CardStore> cardStoreList() 
	{
		return cardStoreMapper.selectCardStoreList();
	}

}
