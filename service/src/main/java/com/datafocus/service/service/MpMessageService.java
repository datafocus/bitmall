package com.datafocus.service.service;

import org.springframework.stereotype.Service;

/**
 * 高级群发接口类
 * @author yyhinfo
 *
 */
@Service
public interface MpMessageService {
	
	/**高级群发
	 * @param msg 消息体
	 * @param target 发送群体（-1：全部发送，0：按照用户群组发送）
	 * @param msgType 消息类型
	 * @param targetGroupId 目标用户群组id
	 */
	void send(String msg, String target, String msgType, String targetGroupId);
	
	/**
	 * 高级群发预览
	 * @param wxName
	 * @param openId
	 * @param msgType
	 * @param message
	 */
	void preview(String wxName, String openId, String msgType, String message);

}
