package com.datafocus.service.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datafocus.common.dto.Statistics;
import com.datafocus.service.mapper.StatisticsMapper;
import com.datafocus.service.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class StatisticsServiceImpl implements StatisticsService {

	@Autowired
	private StatisticsMapper statisticsMapper;
	@Override
	public List<Statistics> findPayStatistics(Map<String, Object> map)
	{
		return statisticsMapper.findPayStatistics(map);
	}
	@Override
	public List<Statistics> findTransaction(Map<String, Object> map) {
		return statisticsMapper.findTransaction(map);
	}
	@Override
	public List<Statistics> findCarrierOperatorStatistics(Map<String, Object> map)
	{
		return statisticsMapper.findCarrierOperatorStatistics(map);
	}
	@Override
	public List<Statistics> findCreateOrderStatisticsData(Map<String, Object> map)
	{
		return statisticsMapper.findCreateOrderStatisticsData(map);
	}
	@Override
	public List<Statistics> findAccessStatisticsData(Map<String, Object> map)
	{
		return statisticsMapper.findAccessStatisticsData(map);
	}
	@Override
	public List<Statistics> findPhoneResolveStatistics(Map<String, Object> map)
	{
		return statisticsMapper.findPhoneResolveStatistics(map);
	}

}
