package com.datafocus.service.handler;

import java.util.List;

import javax.annotation.PostConstruct;

import com.datafocus.common.pojo.Regular;
import com.datafocus.service.service.AutoReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datafocus.weixin.common.request.TextRequest;
import com.datafocus.weixin.mp.care.CareMessages;

/**
 * 处理文本消息
 * @author yyhinfo
 *
 */
@Service
public class TextMessageHandler {
	
	@Autowired
	private AutoReplyService autoReplyService;
	@Autowired
	private CareMessages careMessages;

	
	public void handler(TextRequest textRequest)
	{
		
		List<Regular> list = autoReplyService.regularList();
		
		//获取消息类容
		String text = textRequest.getContent();
		
			for (Regular  regular : list)
			{
				
				for (Regular.KeyWord keyWord : regular.getKeyWords())
				{
					//关键字的匹配规则
					if(keyWord.getMatch()==0)
					{//全匹配
						if(text.trim().equals(keyWord.getKey().trim()))
						{
							if(regular.getFull()==0)//全部关键字对应的回复内容
							{
								for (Regular.KeyWordMessage KeyWordMessage : regular.getKeyWordMessages())
								{
									send(KeyWordMessage,textRequest.getFromUser());
								}
							}else
							{
								send(regular.getKeyWordMessages().get(0),textRequest.getFromUser());
							}
						}
					}else
					{ //模糊匹配
						if(text.trim().contains(keyWord.getKey().trim()))
						{
							if(regular.getFull()==0)//全部关键字对应的回复内容
							{
								for (Regular.KeyWordMessage KeyWordMessage : regular.getKeyWordMessages())
								{
									
									send(KeyWordMessage,textRequest.getFromUser());
								}
							}else
							{
								send(regular.getKeyWordMessages().get(0),textRequest.getFromUser());
							}
						}
					}
				}
			}
	}
	
	private  void send(Regular.KeyWordMessage keyWordMessage, String openId){
		if(keyWordMessage.getMsgType()==0)//文本
			careMessages.text(openId, keyWordMessage.getMessage());
		else if(keyWordMessage.getMsgType()==2)//图片
			careMessages.image(openId, keyWordMessage.getMessage());
		else if(keyWordMessage.getMsgType()==3)//卡券
			careMessages.card(openId, keyWordMessage.getMessage());
		else if(keyWordMessage.getMsgType()==4)//图文
			careMessages.mpNews(openId, keyWordMessage.getMessage());
		else if(keyWordMessage.getMsgType()==5)//语音消息
			careMessages.voice(openId, keyWordMessage.getMessage());
	}
}
