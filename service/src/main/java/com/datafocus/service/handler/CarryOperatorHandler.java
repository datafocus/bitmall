package com.datafocus.service.handler;


import com.datafocus.common.dto.CarryOperator;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CarryOperatorHandler implements TypeHandler<CarryOperator> {

	@Override
	public void setParameter(PreparedStatement preparedStatement, int i, CarryOperator parameter, JdbcType jdbcType)
			throws SQLException {
		 preparedStatement.setInt(i, parameter.getValue());
		
	}

	@Override
	public CarryOperator getResult(ResultSet resultSet, String s) throws SQLException {
		 return CarryOperator.valueOf(resultSet.getInt(s));
	}

	@Override
	public CarryOperator getResult(ResultSet resultSet, int s) throws SQLException {
		 return CarryOperator.valueOf(resultSet.getInt(s));
	}

	@Override
	public CarryOperator getResult(CallableStatement callableStatement, int i) throws SQLException {
		 return CarryOperator.valueOf(callableStatement.getInt(i));
	}

}
