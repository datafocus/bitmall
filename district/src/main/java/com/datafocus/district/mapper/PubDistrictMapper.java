package com.datafocus.district.mapper;

import com.datafocus.common.pojo.DistrictData;
import com.datafocus.common.pojo.PubDistrict;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author layne
 * @date 17-7-27
 */
@Repository
public interface PubDistrictMapper {
    /**
     * @return
     */
    List<PubDistrict> findAllPubDistrict();

    PubDistrict findPubDistrictByDname(String dname);

    List<DistrictData> findDistrictData();
}
