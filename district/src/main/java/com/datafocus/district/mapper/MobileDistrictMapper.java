package com.datafocus.district.mapper;

import com.datafocus.common.dto.MoblileDistrictDto;
import org.springframework.stereotype.Repository;

@Repository
public interface MobileDistrictMapper {
	
	/**
	 * 查询手机号码归属地
	 * @param mobile 手机号码前七位
	 * @return
	 */
	MoblileDistrictDto findMobileDistrictByPhone(String mobile);

}
