package com.datafocus.district.mapper;

import com.datafocus.common.dto.DistrictDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneDistrictMapper {
	
	/**
	 * 获取手机号码的归属地
	 * @param mobile 手机号码前七位
	 * @return
	 */
	DistrictDto district(@Param("mobile") String mobile);

}
