package com.datafocus.district.service;

import com.datafocus.common.dto.DistrictDto;

/**
 * Created by liuheng on 2017/7/27.
 */
public interface PhoneDistrictService {

    DistrictDto district(String mobile);
}
