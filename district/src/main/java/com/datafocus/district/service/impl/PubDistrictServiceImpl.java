package com.datafocus.district.service.impl;

import com.datafocus.common.pojo.DistrictData;
import com.datafocus.common.pojo.PubDistrict;
import com.datafocus.district.mapper.PubDistrictMapper;
import com.datafocus.district.service.PubDistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author layne
 * @date 17-7-27
 */
@Service
public class PubDistrictServiceImpl implements PubDistrictService {

    @Autowired
    private PubDistrictMapper pubDistrictMapper;


    @Override
    public List<PubDistrict> findAllPubDistrict() {
        return pubDistrictMapper.findAllPubDistrict();
    }

    @Override
    public PubDistrict findPubDistrictByDname(String dname) {
        return pubDistrictMapper.findPubDistrictByDname(dname);
    }

    @Override
    public List<DistrictData> findDistrictData() {
        List<DistrictData> list = pubDistrictMapper.findDistrictData();

        for (DistrictData districtData : list) {
            DistrictData.Cities cities = new DistrictData.Cities();
            cities.setId("0000");
            cities.setName("全省");
            List<DistrictData.Cities> c = districtData.getCities();
            c.add(cities);
            Collections.reverse(c);
        }
        DistrictData data = new DistrictData();
        data.setId("00");
        data.setName("全国");
        DistrictData.Cities cities = new DistrictData.Cities();
        cities.setId("0000");
        cities.setName("全省");
        data.setCities(Arrays.asList(cities));
        list.add(data);
        Collections.reverse(list);
        return list;
    }

}
