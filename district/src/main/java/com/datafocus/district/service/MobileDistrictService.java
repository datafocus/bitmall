package com.datafocus.district.service;


import com.datafocus.common.dto.MoblileDistrictDto;

public interface MobileDistrictService {
	
	/**
	 *  根据手机号码前七位查询号码归属地
	 * @param phone
	 * @return
	 */
	MoblileDistrictDto mobileDistrict(String phone);

}
