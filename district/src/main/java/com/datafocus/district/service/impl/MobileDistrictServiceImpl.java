package com.datafocus.district.service.impl;


import com.datafocus.common.dto.MoblileDistrictDto;
import com.datafocus.district.service.MobileDistrictService;
import com.datafocus.district.mapper.MobileDistrictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MobileDistrictServiceImpl implements MobileDistrictService {

	@Autowired
	private MobileDistrictMapper mobileDistrictMapper;
	
	@Override
	public MoblileDistrictDto mobileDistrict(String phone) {
		return mobileDistrictMapper.findMobileDistrictByPhone(phone.substring(0,7));
	}

}
