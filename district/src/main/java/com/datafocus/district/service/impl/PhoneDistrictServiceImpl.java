package com.datafocus.district.service.impl;

import com.datafocus.common.dto.DistrictDto;
import com.datafocus.district.mapper.PhoneDistrictMapper;
import com.datafocus.district.service.PhoneDistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by liuheng on 2017/7/27.
 */
@Service
public class PhoneDistrictServiceImpl implements PhoneDistrictService {

    @Autowired
    private PhoneDistrictMapper phoneDistrictMapper;

    @Override
    public DistrictDto district(String mobile) {
        return phoneDistrictMapper.district(mobile);
    }
}

