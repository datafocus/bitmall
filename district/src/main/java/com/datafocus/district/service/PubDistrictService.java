package com.datafocus.district.service;

import com.datafocus.common.pojo.DistrictData;
import com.datafocus.common.pojo.PubDistrict;

import java.util.List;

/**
 * @author layne
 * @date 17-7-27
 */
public interface PubDistrictService {
    /**
     *
     * @return
     */

    List<PubDistrict> findAllPubDistrict();

    PubDistrict findPubDistrictByDname(String dname);

    List<DistrictData> findDistrictData();
}
