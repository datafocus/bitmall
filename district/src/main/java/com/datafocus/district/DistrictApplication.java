package com.datafocus.district;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author layne
 * @date 17-8-3
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = {"com.datafocus.district.mapper"})
public class DistrictApplication {
    public static void main(String[] args) {
        SpringApplication.run(DistrictApplication.class, args);
    }
}
