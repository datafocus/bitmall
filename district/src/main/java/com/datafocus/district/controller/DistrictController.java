package com.datafocus.district.controller;



import com.datafocus.common.dto.DistrictDto;
import com.datafocus.common.dto.MoblileDistrictDto;
import com.datafocus.common.interfaces.IDistrictController;
import com.datafocus.common.pojo.DistrictData;
import com.datafocus.common.pojo.PubDistrict;
import com.datafocus.district.service.MobileDistrictService;
import com.datafocus.district.service.PhoneDistrictService;
import com.datafocus.district.service.PubDistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author layne
 * @date 17-8-3
 */

/**
 * 归属地查询接口，仅仅提供查询服务
 */
@RestController
@RequestMapping("/district")
public class DistrictController implements IDistrictController {
    @Autowired
    private PhoneDistrictService phoneDistrictService;

    @Autowired
    private PubDistrictService pubDistrictService;

    @Autowired
    private MobileDistrictService mobileDistrictService;

    @Override
    @GetMapping("/findByMobile")
    public DistrictDto district(String mobile) {
        return phoneDistrictService.district(mobile);
    }

    @Override
    @GetMapping("/findAllPubDistrict")
    public List<PubDistrict> findAllPubDistrict() {
        return pubDistrictService.findAllPubDistrict();
    }

    @Override
    @GetMapping("/findPubDistrictByDname")
    public PubDistrict findPubDistrictByDname(String name) {
        return pubDistrictService.findPubDistrictByDname(name);
    }

    @Override
    @GetMapping("/findDistrictData")
    public List<DistrictData> findDistrictData() {
        return pubDistrictService.findDistrictData();
    }

    @Override
    @GetMapping("/mobileDistrict")
    public MoblileDistrictDto mobileDistrict(String mobile) {
        return mobileDistrictService.mobileDistrict(mobile);
    }
}

